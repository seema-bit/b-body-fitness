import Toast from 'react-native-root-toast';
import axios from "axios";
import Constant from '../constants/Constant';
import ApiService from '../webservice/ApiService';
import { AsyncStorage } from 'react-native';
import * as Sentry from 'sentry-expo';
import Constants from 'expo-constants';
import React from 'react';
import { View, StyleSheet, Image, ActivityIndicator } from 'react-native';
import Colors from '../constants/Colors';
import Animated from 'react-native-reanimated';
import { Easing } from 'react-native';


//Sentry.setRelease(Constants.manifest.revisionId);

_emailValidate = (text) => {
  console.log(text);
  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (reg.test(text) === false) {
    console.log("Email is Not Correct");
    return false;
  } else {
    console.log("Email is Correct");
    return true;
  }
}

heightValidation = (heightft) => {
  if (heightft >= 2 && heightft <= 9) {
    return true;
  }
  else {
    return false;
  }
}
// Add a Toast on screen.
toastShow = (msg) => {
  Toast.show(msg, {
    duration: Toast.durations.SHORT,
    position: Toast.positions.BOTTOM,
    shadow: false,
    animation: true,
    hideOnPress: true,
    delay: 0,
    onShow: () => {
      // calls on toast\`s appear animation start
    },
    onShown: () => {
      // calls on toast\`s appear animation end.
    },
    onHide: () => {
      // calls on toast\`s hide animation start.
    },
    onHidden: () => {
      // calls on toast\`s hide animation end.
    }
  });

}

handleClick = () => {
  const min = 1;
  const max = 100;
  const rand = min + Math.random() * (max - min);
  return rand
}

is_url = (str) => {
  regexp = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
  if (regexp.test(str)) {
    return true;
  }
  else {
    return false;
  }
}

_signOutAsync = async (navigation) => {

  navigation.navigate('Login');
  await AsyncStorage.clear();
  ApiService.api.get(ApiService.logout)
    .then((response) => {

    })
    .catch(function (error) {
      //Utils.toastShow(Constant.message)

      console.log("error " + error);
    })


};

_FoodApiTokenGenerate = () => {
  var headers = {
    "Content-Type": "application/json",
  }
  var body = new FormData();
  body.append('grant_type', 'client_credentials');
  body.append('scope', 'basic')


  var headers = {
    'content-type': 'application/json'
  }

  axios.post(ApiService.TOKEN_URL, body, {
    auth: {
      username: Constant.client_id,
      password: Constant.client_secret,
    }
  }, { headers: headers })
    .then((response) => {
      console.log("data " + JSON.stringify(response.data))
      if (response !== null && response.data !== null) {
        if (response.data.access_token !== undefined && response.data.access_token !== null) {
          let dataStore = {
            secret_token: response.data.token_type + " " + response.data.access_token,
            expires_in: response.data.expires_in,
            token_time: Date.now()
          }
          console.log('dataStore ', JSON.stringify(dataStore))
          AsyncStorage.setItem(Constant.FoodAPI, JSON.stringify(dataStore));
        }


      }
    }).catch((error) => {
      console.error(error);
    });
}

_SentryReport = (error) => {
  Sentry.init({
    dsn: 'https://c5cdf0cc0e1a4dcf9c894dc0b2e99f37@sentry.io/2230830',
    enableInExpoDevelopment: true,
    debug: true,
    enableNative: false
  });
  Sentry.captureException(new Error(error));
}

ActivityIndicatorLoadingView = () => {
  //making a view to show to while loading the webpage
  return (
    <View style={[styles.container, styles.horizontal]}>
      <ActivityIndicator size="small" color={Colors.primaryColor} />
    </View>

  )
}
_checkAllNonZero = (str) => {

  let str_array = str.trim().split('')
  var check = str_array.every((val, i, arr) => (val === arr[0]))
  console.log('check', check)
  let result = false
  if (check) {
    for (let i = 0; i < str_array.length; i++) {
      if (str_array[i] <= 0) {
        console.log('check number')
        result = true;
        break;
      }
    }
  }

  return result
}

const arrowStyle = `<style>
#arrowDown {
    position:fixed;
    bottom: 0;
    left: 50%;
    font-size: 30px;
    line-height: 1.1;
    color: #0000FF;
}

#scrollLoc{
  overflow: hidden;
   position: fixed;
    bottom: 0px;
    right: 0;
}
</style>`


const scrollScript = `
var fullHeight = document.documentElement.scrollHeight;
var height = window.pageYOffset;
var lastScroll = 0;

window.onscroll=function(){

  var height = window.pageYOffset;
  height = height+400
  var x = document.getElementById("arrowDown");
  if (fullHeight < height) {
        x.style.display = "none";
    } else {
        x.style.display = "block";
        if(height>500){
          x.style.opacity = "0.4";
        }else{
          x.style.opacity = "1";
        }

        let currentScroll = document.documentElement.scrollTop || document.body.scrollTop; // Get Current Scroll Value

      if (currentScroll > 0 && lastScroll <= currentScroll){
        lastScroll = currentScroll;
        x.style.transform = "rotate(0deg)";
      }else{
        lastScroll = currentScroll;
        x.style.transform = "rotate(180deg)";
      }

  }

};true;
`

const styles = StyleSheet.create({
  container: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',

  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  }
});

transitionConfig = () => {
  return {
    transitionSpec: {
      duration: 750,
      easing: Easing.out(Easing.poly(4)),
      //timing: Animation.timing,
      useNativeDriver: true,
    },
    screenInterpolator: sceneProps => {
      const { position, layout, scene, index, scenes } = sceneProps
      const toIndex = index
      const thisSceneIndex = scene.index
      const height = layout.initHeight
      const width = layout.initWidth

      const translateX = position.interpolate({
        inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
        outputRange: [width, 0, 0]
      })

      // Since we want the card to take the same amount of time
      // to animate downwards no matter if it's 3rd on the stack
      // or 53rd, we interpolate over the entire range from 0 - thisSceneIndex
      const translateY = position.interpolate({
        inputRange: [0, thisSceneIndex],
        outputRange: [height, 0]
      })

      const slideFromRight = { transform: [{ translateX }] }
      const slideFromBottom = { transform: [{ translateY }] }

      const lastSceneIndex = scenes[scenes.length - 1].index

      // Test whether we're skipping back more than one screen
      if (lastSceneIndex - toIndex > 1) {
        // Do not transoform the screen being navigated to
        if (scene.index === toIndex) return
        // Hide all screens in between
        if (scene.index !== lastSceneIndex) return { opacity: 0 }
        // Slide top screen down
        return slideFromBottom
      }

      return slideFromRight
    },
  }
}

export default {
  _emailValidate,
  toastShow,
  handleClick,
  _FoodApiTokenGenerate,
  is_url,
  _signOutAsync,
  heightValidation,
  _SentryReport,
  ActivityIndicatorLoadingView,
  scrollScript,
  arrowStyle,
  transitionConfig,
  _checkAllNonZero
}