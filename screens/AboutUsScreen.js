import React, { Component } from 'react'
import { ScrollView, StyleSheet, Image, Text, TouchableOpacity, ActivityIndicator, ImageBackground } from 'react-native';
import { View } from 'native-base';
import HeaderLeft from '../components/HeaderLeft';
import Constant from '../constants/Constant';
import Colors from '../constants/Colors';
import { textHeader, font_style } from '../components/styles';
import HeaderBackground from '../components/HeaderBackground';
import ApiService from '../webservice/ApiService';
import Loader from '../components/Loader';
import { WebView } from 'react-native-webview';
import { SafeAreaView } from 'react-navigation';
import Utils from '../constants/Utils';

let WebViewRef;
export default class AboutUsScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.about_us}`.toUpperCase()}</Text>,
    headerLeft: <HeaderLeft navigationProps={navigation} />,

  });

  state = {
    selectedTab: 0,
    detail: null
  }

  componentDidMount = () => {
    // ApiService.api.get(ApiService.gym_info)
    //   .then((response) => {
    //     if (response !== null && response.data != null) {
    //       if (response.data.status === Constant.SUCCESS) {
    //         this.setState({
    //           detail: response.data.data != null ? response.data.data : {},
    //         })
    //       } else if (response.data.status === Constant.WARNING) {
    //         if (response.data.session !== undefined && response.data.session !== null) {
    //           if (response.data.session === false) {
    //             Utils.toastShow(response.data.message)
    //             Utils._signOutAsync(this.props.navigation)
    //           }
    //         }
    //       }
    //     }

    //   })
    //   .catch(function (error) {
    //     console.log("error " + JSON.stringify(error.response));
    //   })

  }

  componentWillReceiveProps() {
    WebViewRef && WebViewRef.reload();
  }
  render() {
    return (
      <View style={styles.container}>
        <WebView
          //ref={WEBVIEW_REF => (WebViewRef = WEBVIEW_REF)}
          source={{ uri: ApiService.BASE_URL + ApiService.about_us }}
          style={[styles.container]}
          renderLoading={Utils.ActivityIndicatorLoadingView}
          startInLoadingState={true}


        />
        {/* <View style={{
          flexDirection: 'row', height: 46,
          borderBottomColor: Colors.light_gray,
          borderBottomWidth: 1,
        }}>

          <TouchableOpacity onPress={() => { this.setState({ selectedTab: 0 }) }}
            style={{ width: '49%', alignItems: 'center', justifyContent: 'center', }}>

            <View style={{ alignSelf: 'center', flexDirection: 'row', }}>
              <Text style={[this.state.selectedTab === 0 ? { color: Colors.primaryColor } : { color: Colors.dark_gray }, { fontSize: 16, }]}>{'App info'.toUpperCase()}</Text>
            </View>

          </TouchableOpacity>
          <View style={{
            borderLeftColor: Colors.light_gray,
            borderLeftWidth: 1,
            marginTop: 6,
            marginBottom: 6
          }}>
          </View>
          <TouchableOpacity onPress={() => { this.setState({ selectedTab: 1 }) }}
            style={{ width: '49%', alignItems: 'center', justifyContent: 'center', }}>

            <View style={{ alignSelf: 'center', flexDirection: 'row', }}>
              <Text style={[this.state.selectedTab === 1 ? { color: Colors.primaryColor } : { color: Colors.dark_gray }, { fontSize: 16, }]}>{'Gym info'.toUpperCase()}</Text>
            </View>

          </TouchableOpacity>
        </View>
        <View style={{ flex: 1 }}>
          {this.state.selectedTab === 0 ?
            (<WebView
              source={{ uri: ApiService.BASE_URL + ApiService.about_us }}
              style={[styles.container]}

            />)
            :
            this.state.detail === undefined || this.state.detail === null ?
              (<Loader />) :
              (

                <ScrollView style={{ flex: 1, marginStart: 20, marginEnd: 20, }} showsVerticalScrollIndicator={false}>

                  <View style={{ alignItems: 'center', marginBottom: 20, marginTop: 20 }}>
                    {this.state.detail.image ?
                      (<ImageBackground source={require('../assets/images/thumbnail.png')} style={styles.image}>
                        <Image source={{ uri: this.state.detail.image }} style={styles.image} />
                      </ImageBackground>)
                      : (<Image source={require('../assets/images/thumbnail.png')} style={styles.image} />)
                    }
                    <Text style={[font_style.font_medium, { fontSize: 14, textTransform: 'capitalize', alignItems: 'center', marginTop: 10 }]}>{this.state.detail.name}</Text>

                  </View>

                  {this.state.detail.admin_name !== null &&
                    (<View style={styles.row}>
                      <Text style={styles.txt_key}>Owner Name</Text>
                      <Text style={styles.txt_value}>: {this.state.detail.admin_name}</Text>
                    </View>)}

                  {this.state.detail.gym_name !== null &&
                    (<View style={styles.row}>
                      <Text style={styles.txt_key}>Gym Name</Text>
                      <Text style={styles.txt_value}>: {this.state.detail.gym_name}</Text>
                    </View>)}

                  {this.state.detail.mobile !== null &&
                    (<View style={styles.row}>
                      <Text style={styles.txt_key}>Mobile No. </Text>
                      <Text style={styles.txt_value}>: {this.state.detail.mobile}</Text>
                    </View>)}


                  {this.state.detail.email !== null && (<View style={styles.row}>
                    <Text style={styles.txt_key}>Email </Text>
                    <Text style={styles.txt_value}>: {this.state.detail.email}</Text>
                  </View>)}

                  {this.state.detail.address !== null && (<View style={styles.row}>
                    <Text style={styles.txt_key}>Address </Text>
                    <Text style={styles.txt_value}>: {this.state.detail.address}</Text>
                  </View>)}

                  {this.state.detail.about_gym !== null && (<View style={styles.row}>
                    <Text style={styles.txt_key}>About Gym </Text>
                    <Text style={styles.txt_value}>: {this.state.detail.about_gym}</Text>
                  </View>)}


                </ScrollView>
              )}
        </View> */}

        <SafeAreaView />

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white_color,
    paddingStart: 8, paddingEnd: 8
  },
  webview_style: {
    marginTop: 30,
    textAlign: 'center',
    fontSize: 20, marginStart: 20,
    marginEnd: 20
  },
  image: {
    width: '100%', height: 150,
    alignItems: 'center'
  },
  row: {

    flexDirection: 'row',
    marginTop: 10
  },
  txt_value: {
    flex: .8, fontSize: 14,
    color: Colors.dark_gray
  },
  txt_key: {
    flex: .4,
    fontSize: 14,
    color: Colors.dark_gray
  }
})