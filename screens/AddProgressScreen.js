import React, { Component } from 'react'
import { ScrollView, StyleSheet, Image, Text, TouchableOpacity, AsyncStorage, Keyboard } from 'react-native';
import { View } from 'native-base';
import HeaderLeft from '../components/HeaderLeft';
import { textHeader, font_style, buttonStyle, modalSelector } from '../components/styles';
import HeaderBackground from '../components/HeaderBackground';
import Colors from '../constants/Colors';
import Constant from '../constants/Constant';
import { TextInput } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
import moment from 'moment'
import ModalSelector from 'react-native-modal-selector'
import Utils from '../constants/Utils';
import ApiService from '../webservice/ApiService';
import HeaderBackLeft from '../components/HeaderBackLeft';
import Spinner from 'react-native-loading-spinner-overlay';
import CustomProgressBar from '../components/CustomProgressBar';
import * as ImagePicker from 'expo-image-picker'
import * as Permissions from 'expo-permissions'
import { Appearance } from 'react-native-appearance';

export default class AddProgressScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.weight}`.toUpperCase()}</Text>,
    headerLeft: <TouchableOpacity
      onPress={() => {
        navigation.navigate("Progress", {
          refresh: 'yes'
        })
      }}
      style={textHeader.leftIcon}>
      <Image source={require('./../assets/images/icons/arrow-right_3x.png')}
        resizeMode="contain" style={{ width: 20, height: 20, alignSelf: 'flex-start' }} />
    </TouchableOpacity>,
    headerRight: <TouchableOpacity activeOpacity={1}
      onPress={() => { _this._AddProgress() }}
      style={textHeader.rightIcon}>
      <Image source={require('./../assets/images/icons/tick_3x.png')}
        resizeMode="contain" style={{ width: 20, height: 20, alignSelf: 'flex-end' }} />
    </TouchableOpacity>,
  });

  state = {
    startDate: "15-05-2019",
    image: null,
    userInfo: '',
    weight: '',
    body_fat: '',
    spinner: false
  }

  componentDidMount = () => {
    _this = this;
    this.getCurrentDate()
  }
  getCurrentDate = () => {
    const date_obj = moment().format('MM-DD-YYYY')
    this.setState({ startDate: date_obj })

  }

  _RedirectToPage = () => {
    this.props.navigation.navigate("Progress", {
      refresh: 'yes'
    })
  }

  _AddProgress = () => {

    Keyboard.dismiss()

    if (this.state.weight !== null && this.state.weight !== '') {

      if (this.state.image !== null && this.state.image !== '') {
        this.setState({ spinner: true })
        this.setState({ random: Utils.handleClick() })
        var params = new FormData();
        params.append('type', this.props.navigation.getParam('type', null))
        params.append('weight', this.state.weight);
        params.append('body_fat', this.state.body_fat);
        params.append('current_date', this.state.startDate);

        if (this.state.image !== null || this.state.imageUpdate === true) {
          params.append('image',
            { uri: this.state.image, name: `${this.state.random}` + '.jpg', type: 'image/jpg' });
        } else {
          params.append('image')
        }

        console.log('params', params)
        AsyncStorage.getItem(Constant.USERINFO)
          .then(req => JSON.parse(req))
          .then(json => {
            this.setState({ userInfo: json })
            var headers = {
              'X-Access-Token': json.access_key,
            }
            ApiService.api.post(ApiService.addProgress, params, { headers: headers })
              .then((response) => {
                this.setState({ spinner: false })
                if (response !== null && response.data != null) {
                  if (response.data.status === Constant.SUCCESS) {
                    Utils.toastShow(response.data.message)
                    this.props.navigation.navigate("Progress", {
                      refresh: 'yes'
                    })

                  } else if (response.data.status === Constant.WARNING) {
                    if (response.data.session !== undefined && response.data.session !== null) {
                      if (response.data.session === false) {
                        Utils.toastShow(response.data.message)
                        Utils._signOutAsync(this.props.navigation)
                      }
                    }
                  }
                }


              })
              .catch(function (error) {
                //this.setState({ spinner: false })
                console.log("error " + JSON.stringify(error.response));
                Utils._SentryReport(error)
              })

          })
          .catch(error => console.log('error!' + JSON.stringify(error.response)));


      } else {
        Utils.toastShow("Please upload image")
      }


    } else {
      Utils.toastShow("Please enter a valid weight")
    }


  }

  _pickImageFromGallery = async () => {

    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

    if (status === 'granted') {
      let result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        aspect: [4, 3],
      });


      if (!result.cancelled) {
        this.setState({ image: result.uri, visible: false, imageUpdate: true });
      }
    }

  };

  _pickImageFromCamera = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL);

    if (status === 'granted') {
      let result = await ImagePicker.launchCameraAsync(
        {
          allowsEditing: true,
          aspect: [4, 3]
        })

      if (!result.cancelled) {
        this.setState({ image: result.uri, visible: false, imageUpdate: true });
      }

      this.handleClick()
    }
  }

  handleClick() {
    const min = 1;
    const max = 100;
    const rand = min + Math.random() * (max - min);
    this.setState({ random: this.state.random + rand });
  }

  showPopup = () => {
    this.setState({ visible: true });
  }

  showImageSelectDialog = (key) => {
    if (key !== null && key !== '') {
      if (key === 0) {
        this._pickImageFromCamera()
      } else if (key === 1) {
        this._pickImageFromGallery()
      }
    }
  }

  render() {
    let index = 0;
    const data = [
      { key: index++, label: 'Take a Photo...' },
      { key: index++, label: 'Choose from Library...' },
    ];

    const colorScheme = Appearance.getColorScheme();
    let bgColor;
    if (colorScheme === 'dark') {
      bgColor = '#000';
    } else {
      bgColor = '#fff';
    }

    return (
      <View>
        <CustomProgressBar spinner={this.state.spinner} />

        <View style={{
          flexDirection: 'row',
          paddingBottom: 8,
          marginTop: 8,
          borderBottomColor: Colors.light_gray,
          borderBottomWidth: 1,
          height: 46, alignItems: 'center'
        }}>
          <Text style={[{ flex: 1, paddingStart: 16 }, styles.txt_style, font_style.font_medium]}>Date</Text>
          <DatePicker
            style={{ width: 100 }}
            date={this.state.startDate}
            mode="date"
            showIcon={false}
            placeholder="select date"
            format="MM-DD-YYYY"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: {
                position: 'absolute',
                left: 0,
                marginLeft: 0,
                borderColor: '#fff'
              },
              dateInput: {
                borderColor: '#fff',
                color: '#797979'
              },
              datePickerCon: {
                backgroundColor: bgColor
              }
              // ... You can check the source to find the other keys.
            }}
            onDateChange={(date) => { this.setState({ startDate: date }) }}
          />
        </View>

        <View style={{
          flexDirection: 'row',
          paddingBottom: 8,
          marginTop: 8,
          borderBottomColor: Colors.light_gray,
          borderBottomWidth: 1,
          height: 46, alignItems: 'center'
        }}>
          <Text style={[{ flex: 1, paddingStart: 16 }, styles.txt_style, font_style.font_medium]}>Weight (lbs)</Text>
          <TextInput style={[{ alignItems: 'flex-end', paddingEnd: 16, textAlign: 'right' }, styles.txt_style, font_style.font_medium]}
            placeholder="Weight"
            keyboardType={'numeric'}
            onChangeText={(weight) => this.setState({ weight })}
            value={this.state.weight}
          />
        </View>

        <View style={{
          flexDirection: 'row',
          paddingBottom: 8,
          marginTop: 8,
          borderBottomColor: Colors.light_gray,
          borderBottomWidth: 1,
          height: 46, alignItems: 'center'
        }}>
          <Text style={[{ flex: 1, paddingStart: 16 }, styles.txt_style, font_style.font_medium]}>Body Fat (lbs)</Text>
          <TextInput style={[{ alignItems: 'flex-end', paddingEnd: 16, flex: 1.5, textAlign: 'right' }, styles.txt_style, font_style.font_medium]}
            placeholder="Body Fat(Optional)"
            keyboardType={'numeric'}
            onChangeText={(body_fat) => this.setState({ body_fat })}
            value={this.state.body_fat}
          />
        </View>

        <View style={{
          flexDirection: 'row',
          paddingBottom: 8,
          marginTop: 8,
          borderBottomColor: Colors.light_gray,
          borderBottomWidth: 1,
          height: 46, alignItems: 'center'
        }}>
          <Text style={[{ flex: 1, paddingStart: 16 }, styles.txt_style, font_style.font_medium]}>Progress Photo</Text>
          <View style={[{ alignItems: 'flex-end', paddingEnd: 16, width: 46, height: 46 }]} >
            <ModalSelector
              data={data}
              onChange={option => { this.showImageSelectDialog(option.key) }}
              overlayStyle={modalSelector.overlayStyle}
              optionContainerStyle={modalSelector.optionContainerStyle}

              optionStyle={modalSelector.optionStyle}
              optionTextStyle={modalSelector.optionTextStyle}

              cancelStyle={modalSelector.cancelStyle}
              cancelTextStyle={modalSelector.cancelTextStyle}
            >
              <TouchableOpacity activeOpacity={1}>
                <View style={styles.header}>

                  {this.state.image !== null ? (
                    <Image style={styles.avatar} source={{ uri: this.state.image }} />
                  ) :
                    <Image
                      style={{
                        width: 46,
                        height: 46,
                      }}
                      source={require('./../assets/images/icons/add_photo_3x.png')}
                    />}

                </View>
              </TouchableOpacity>
            </ModalSelector>
          </View>

        </View>

      </View>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
    padding: 24
  },
  avatar: {
    width: 46,
    height: 46,
  },
  header: {
    alignItems: 'center',
  },
  txt_style: {
    fontSize: 14,
    color: Colors.dark_gray,
  },
})
