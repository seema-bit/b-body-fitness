import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Text, Dimensions, Platform, TouchableOpacity, Image, AsyncStorage } from 'react-native';
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import HeaderLeft from '../components/HeaderLeft'
import HeaderRight from '../components/HeaderRight'
import Constant from '../constants/Constant'
import { textHeader, font_style } from '../components/styles'
import Colors from '../constants/Colors';
import CalendarStrip from '../components/calender/CalendarStrip';
import HeaderBackground from '../components/HeaderBackground';
import Loader from '../components/Loader';
import ApiService from '../webservice/ApiService';
import moment from 'moment';
import DataNotFound from '../components/DataNotFound';
import { Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';

import { Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from 'native-base'
import MyWorkoutScreen from './MyWorkoutScreen';
import TeamWorkoutScreen from './TeamWorkoutScreen';

const screenWidth = Math.round(Dimensions.get('window').width);


export default class WorkoutScreen extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.my_workout}`.toUpperCase()}</Text>,
    headerLeft: <HeaderLeft navigationProps={navigation} />,
    headerRight: <TouchableOpacity
      onPress={() => { navigation.navigate('Instructor') }}
      style={{
        justifyContent: 'center',
        marginRight: 20,
      }}>
      <Image source={require('./../assets/images/icons/info.png')}
        resizeMode="contain" style={{ width: 20, height: 20, tintColor: '#fff' }} />
    </TouchableOpacity>,
  });
  state = {

    updateCount: 0,
    selectedTab: 0,
  }

  setTab(tab) {
    this.setState({ selectedTab: tab });
    //this.renderScreen(tab)
  }


  addWorkout = () => {
    this.props.navigation.navigate('WorkoutList', {
      date: this.state.selectedDate
    })
  }

  render() {
    const state = this.state
    return (

      <View style={styles.container}>

        <View style={{
          flexDirection: 'row', height: 46,
          borderBottomColor: Colors.light_gray,
          borderBottomWidth: 1,
        }}>

          <TouchableOpacity onPress={() => this.setTab(0)} style={{ width: '49%', alignItems: 'center', justifyContent: 'center', }}>

            <View style={{ alignSelf: 'center', flexDirection: 'row', }}>
              <Text style={[this.state.selectedTab === 0 ? { color: Colors.primaryColor } : { color: Colors.dark_gray }, { fontSize: 19, }]}>{'Workouts'.toUpperCase()}</Text>
            </View>

          </TouchableOpacity>
          <View style={{
            borderLeftColor: Colors.light_gray,
            borderLeftWidth: 1,
            marginTop: 6,
            marginBottom: 6
          }}>
          </View>
          <TouchableOpacity onPress={() => this.setTab(1)} style={{ width: '49%', alignItems: 'center', justifyContent: 'center', }}>

            <View style={{ alignSelf: 'center', flexDirection: 'row', }}>
              <Text style={[this.state.selectedTab === 1 ? { color: Colors.primaryColor } : { color: Colors.dark_gray }, { fontSize: 19, }]}>{'Challenges'.toUpperCase()}</Text>
            </View>

          </TouchableOpacity>
        </View>


        {this.state.selectedTab === 0
          ?
          (<MyWorkoutScreen navigationProps={this.props.navigation} />)
          :
          (<TeamWorkoutScreen navigationProps={this.props.navigation} screen={Constant.meal} />)
        }


      </View >
    )
    // }

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color
  },
  header: {
    height: 50,
    backgroundColor: Colors.dark_gray,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,

  },
  headText: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 14
  },
  text: {
    textAlign: 'center',
    color: Colors.dark_gray,
    fontSize: 14

  },
  dataWrapper: {
    marginTop: -1
  },
  row: {
    flexDirection: 'row',
    height: 40,
    backgroundColor: '#fff',
  },

  TouchableOpacityStyle: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 20,
  },

  FloatingButtonStyle: {
    resizeMode: 'contain',
    width: 50,
    height: 50,
    //
  },

});