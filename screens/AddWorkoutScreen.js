import React, { Component } from 'react'
import { ScrollView, StyleSheet, Image, Text, TouchableOpacity, AsyncStorage } from 'react-native';
import { View } from 'native-base';
import HeaderLeft from '../components/HeaderLeft';
import { textHeader, font_style, buttonStyle } from '../components/styles';
import HeaderBackground from '../components/HeaderBackground';
import Colors from '../constants/Colors';
import Constant from '../constants/Constant';
import { TextInput } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
import moment from 'moment'
import ModalSelector from 'react-native-modal-selector'
import Utils from '../constants/Utils';
import ApiService from '../webservice/ApiService';
import HeaderBackLeft from '../components/HeaderBackLeft';
import Spinner from 'react-native-loading-spinner-overlay';
import CustomProgressBar from '../components/CustomProgressBar';

export default class AddWorkoutScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${navigation.state.params.title}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,
    headerRight: <TouchableOpacity
      onPress={() => { _this._AddProgress() }}
      style={textHeader.rightIcon}
      activeOpacity={1}>
      <Image source={require('./../assets/images/icons/tick_3x.png')}
        resizeMode="contain" style={{ width: 20, height: 20, alignSelf: 'flex-end' }} />
    </TouchableOpacity>,
  });

  state = {
    startDate: "15-05-2019",
    set: '',
    repetition: '',
    weight: '',
    userInfo: '',
    calories: 0,
    spinner: false
  }

  componentDidMount = () => {
    _this = this;

  }

  _AddProgress = () => {

    if (this.state.set !== null && this.state.set !== '') {
      if (this.state.repetition !== null && this.state.repetition !== '') {

        this.setState({ spinner: true })
        const date_obj = this.props.navigation.state.params.date
        let data = this.props.navigation.getParam('data', null);
        const params = {
          workout_id: data.id,
          sets: this.state.set,
          reps: this.state.repetition,
          weight_per_reps: this.state.weight,
          calories_burn: this.state.calories,
          curent_date: date_obj
        }

        AsyncStorage.getItem(Constant.USERINFO)
          .then(req => JSON.parse(req))
          .then(json => {
            this.setState({ userInfo: json })
            var headers = {
              'X-Access-Token': json.access_key,
            }
            ApiService.api.post(ApiService.addWorkout, params, { headers: headers })
              .then((response) => {
                this.setState({ spinner: false })
                if (response !== null && response.data != null) {
                  if (response.data.status === Constant.SUCCESS) {
                    //this.props.navigation.state.params._UpdateData()
                    this.props.navigation.navigate("Workout",
                      {
                        refresh: "yes",
                        //date: this.props.navigation.state.params.date
                      })

                  } else if (response.data.status === Constant.WARNING) {
                    if (response.data.session !== undefined && response.data.session !== null) {
                      if (response.data.session === false) {
                        Utils.toastShow(response.data.message)
                        Utils._signOutAsync(this.props.navigation)
                      }
                    }
                  }
                }


              })
              .catch(function (error) {
                Utils._SentryReport(error)

                this.setState({ spinner: false })
                console.log("error " + JSON.stringify(error.response));
              })

          })
          .catch(error => console.log('error!' + JSON.stringify(error.response)));



      } else {
        Utils.toastShow("Please enter repetitions")
      }
    } else {
      Utils.toastShow("Please enter no. of sets")
    }

  }


  showPopup = () => {
    this.setState({ visible: true });
  }

  render() {
    let index = 0;
    const data = [
      { key: index++, label: 'Take a Photo...' },
      { key: index++, label: 'Choose from Library...' },
    ];

    return (
      <View>
        <CustomProgressBar spinner={this.state.spinner} />
        <View style={{
          flexDirection: 'row',
          paddingBottom: 8,
          marginTop: 8,
          borderBottomColor: Colors.light_gray,
          borderBottomWidth: 1,
          height: 46, alignItems: 'center'
        }}>
          <Text style={[{ flex: 1, paddingStart: 16 }, styles.txt_style, font_style.font_medium]}># of set</Text>
          <TextInput style={[{ alignItems: 'flex-end', paddingEnd: 16 }, styles.txt_style, font_style.font_medium]}
            placeholder="Required"
            keyboardType={'numeric'}
            onChangeText={(set) => this.setState({ set })}
            value={this.state.set}
          />
        </View>
        <View style={{
          flexDirection: 'row',
          paddingBottom: 8,
          marginTop: 8,
          borderBottomColor: Colors.light_gray,
          borderBottomWidth: 1,
          height: 46, alignItems: 'center'
        }}>
          <Text style={[{ flex: 1, paddingStart: 16 }, styles.txt_style, font_style.font_medium]}>Repetitions</Text>
          <TextInput style={[{ alignItems: 'flex-end', paddingEnd: 16 }, styles.txt_style, font_style.font_medium]}
            placeholder="Required"
            keyboardType={'numeric'}
            onChangeText={(repetition) => this.setState({ repetition })}
            value={this.state.repetition}
          />
        </View>

        <View style={{
          flexDirection: 'row',
          paddingBottom: 8,
          marginTop: 8,
          // borderBottomColor: Colors.light_gray,
          // borderBottomWidth: 1,
          height: 46, alignItems: 'center'
        }}>
          <Text style={[{ flex: 1, paddingStart: 16 }, styles.txt_style, font_style.font_medium]}>Weight per repetition</Text>
          <TextInput style={[{ alignItems: 'flex-end', paddingEnd: 16 }, styles.txt_style, font_style.font_medium]}
            placeholder="Optional"
            keyboardType={'numeric'}
            onChangeText={(weight) => this.setState({ weight })}
            value={this.state.weight}
          />
        </View>

        {/* <View style={{
          flexDirection: 'row',
          paddingBottom: 8,
          marginTop: 8,
          borderBottomColor: Colors.light_gray,
          borderBottomWidth: 1,
          height: 46, alignItems: 'center'
        }}>
          <Text style={[{ flex: 1, paddingStart: 16 }, styles.txt_style, font_style.font_medium]}>Weight per repetition</Text>
          <Text style={[{ alignItems: 'flex-end', paddingEnd: 16 }, styles.txt_style, font_style.font_medium]}>N/A</Text>
        </View> */}


      </View>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
    padding: 24
  },
  avatar: {
    width: 46,
    height: 46,
  },
  header: {
    alignItems: 'center',
  },
  txt_style: {
    fontSize: 14,
    color: Colors.dark_gray,
  },
})
