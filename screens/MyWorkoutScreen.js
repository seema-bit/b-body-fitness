import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Text, Dimensions, Platform, TouchableOpacity, Image, AsyncStorage } from 'react-native';
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import HeaderLeft from '../components/HeaderLeft'
import HeaderRight from '../components/HeaderRight'
import Constant from '../constants/Constant'
import { textHeader, font_style } from '../components/styles'
import Colors from '../constants/Colors';
import CalendarStrip from '../components/calender/CalendarStrip';
import HeaderBackground from '../components/HeaderBackground';
import Loader from '../components/Loader';
import ApiService from '../webservice/ApiService';
import moment from 'moment';
import DataNotFound from '../components/DataNotFound';
import { Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';

import { Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from 'native-base'

const screenWidth = Math.round(Dimensions.get('window').width);




export default class MyWorkoutScreen extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.my_workout}`.toUpperCase()}</Text>,
    headerLeft: <HeaderLeft navigationProps={navigation} />,
    headerRight: <TouchableOpacity
      onPress={() => { navigation.navigate('Instructor') }}
      style={{
        justifyContent: 'center',
        marginRight: 20,
      }}>
      <Image source={require('./../assets/images/icons/info.png')}
        resizeMode="contain" style={{ width: 20, height: 20, tintColor: '#fff' }} />
    </TouchableOpacity>,
  });
  state = {
    tableHead: ['Exercise', 'Weight', 'Reps'],
    widthArr: [screenWidth / 3.3, screenWidth / 3.3, screenWidth / 3.3],
    allWorkout: null,
    selectedDate: null,
    userInfo: '',
    loading_status: false,
    updateCount: 0,
    selectedTab: 0,
  }


  componentDidMount = () => {
    //this.refreshEvent = evEventsent.subscribe('RefreshList', () => this.refresh());
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {

        this.setState({ userInfo: json, selectedDate: moment().format('MM-DD-YYYY') })
        this._FetchClasses(moment().format('MM-DD-YYYY'));

      })
      .catch(error => console.log('error!'));

  }

  componentWillReceiveProps = () => {
    this.setState({ allWorkout: null })
    this._FetchClasses(this.state.selectedDate)
  }


  _UpdateData() {
    this.setState({ allWorkout: null })
    this._FetchClasses(this.state.selectedDate)
  }

  _FetchClasses = (dateFormate) => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    const params = {
      date: dateFormate
    }

    //this.setState({ allClasses: [] })
    ApiService.api.post(ApiService.getWorkout, params, { headers: headers })
      .then((response) => {
        this.setState({ allWorkout: [], loading_status: false })
        if (response !== null && response.data != null) {

          if (response.data.status === Constant.SUCCESS) {

            //this.setState({ allWorkout: response.data.data != null ? response.data.data : [] })
            if (response.data.data !== undefined && response.data.data !== null && response.data.data.length !== 0) {
              response.data.data.forEach(element => {

                let newArray = [element.workout.exercise, element.weight_per_reps !== null && element.weight_per_reps !== '' ? element.weight_per_reps + " kg" : '-', element.reps]
                this.setState({ allWorkout: [...this.state.allWorkout, newArray] })
              });

            } else {
              this.setState({ allWorkout: [] })
            }
          } else if (response.data.status === Constant.WARNING) {

          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })
  }

  _DateSelected = (date) => {
    let dateFormate = moment(date).format("MM-DD-YYYY")

    this.setState({ selectedDate: dateFormate, })
    this.setState({ loading_status: true })
    this._FetchClasses(dateFormate)
  }

  addWorkout = () => {
    this.props.navigationProps.navigate('WorkoutList', {
      _UpdateData: () => this._UpdateData(),
      date: this.state.selectedDate
    })
  }

  setTab(tab) {
    this.setState({ selectedTab: tab });
    //this.renderScreen(tab)
  }


  render() {
    state = this.state

    const element = (cellData, cellIndex) => (

      <View key={cellIndex} style={[{
        width: '100%',
        height: '100%',

        marginTop: -1,
        justifyContent: 'center'
      }, cellIndex === 1 && {
        borderRightColor: Colors.light_gray,
        borderRightWidth: 0.5,
        borderLeftColor: Colors.light_gray,
        borderLeftWidth: 0.5,
      }]}>
        <Text style={[styles.text, font_style.font_medium, cellIndex === 0 && { textAlign: 'left', paddingStart: 8, paddingEnd: 8 }]} numberOfLines={1} >{cellData}</Text>
      </View>

    );
    return (

      <View style={styles.container}>

        <CalendarStrip
          calendarAnimation={{ type: 'sequence', duration: 30 }}
          daySelectionAnimation={{ type: 'border', duration: 200, borderWidth: 1, borderHighlightColor: 'black', backgroundColor: Colors.primaryColor }}
          style={{ height: 80, paddingTop: 0, paddingBottom: 0 }}
          calendarHeaderStyle={{ color: 'black' }}
          calendarColor={'#fff'}
          showMonth={false}
          dateNumberStyle={{ color: 'black' }}
          dateNameStyle={{ color: 'black' }}
          dateMonthStyle={{ color: 'black' }}
          highlightDateNumberStyle={{ color: 'black' }}
          highlightDateNameStyle={{ color: 'black' }}
          disabledDateNameStyle={{ color: 'black' }}
          //datesWhitelist={datesWhitelist}
          disabledDateNumberStyle={{ color: 'black' }}
          iconContainer={{ flex: 0.1 }}
          onDateSelected={date => { this._DateSelected(date) }}
        />


        {this.state.loading_status ?
          (<View style={{ flex: 1 }}>
            <Loader />
          </View>)
          :
          (<View style={{ flex: 1 }}>{
            this.state.allWorkout === undefined || this.state.allWorkout === null ? (
              <Loader />
            ) :
              this.state.allWorkout.length === 0 ?
                (<DataNotFound />)
                :
                (
                  <ScrollView style={styles.dataWrapper} showsVerticalScrollIndicator={false}>
                    <View style={{
                      marginBottom: 80,
                      margin: 24,
                      ...Platform.select({
                        ios: {
                          shadowColor: 'black',
                          shadowOffset: { height: 1, width: 1 },
                          shadowOpacity: 0.2,
                          shadowRadius: 10,
                        },
                        android: {
                          elevation: 20,
                        },
                      }),
                    }}>
                      <Table borderStyle={{ borderColor: 'transparent' }} style={{ borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
                        <Row data={this.state.tableHead} flexArr={[1, 0.5, 0.5]} style={styles.header} textStyle={[styles.headText, font_style.font_bold]} />
                      </Table>

                      <Table borderStyle={{ borderColor: 'transparent' }} style={{ borderRadius: 100 }}>
                        {
                          this.state.allWorkout.map((rowData, index) => (
                            <TableWrapper key={index} style={styles.row} style={[styles.row, index % 2 ? { backgroundColor: '#f5f5f6' } : { backgroundColor: '#ffff', }, index === this.state.allWorkout.length - 1 && { borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }]} >
                              {
                                rowData.map((cellData, cellIndex) => (
                                  <Cell key={cellIndex} data={element(cellData, cellIndex)} textStyle={[styles.text, font_style.font_medium]} style={cellIndex === 0 ? { flex: 1 } : { flex: 0.5 }} />
                                ))
                              }
                            </TableWrapper>
                          ))
                        }
                      </Table>


                    </View>
                  </ScrollView>
                )

          }

          </View>)}


        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => { this.addWorkout() }}
          style={styles.TouchableOpacityStyle}>
          <View

            style={{
              width: 50,
              height: 50,
              borderRadius: 50 / 2,
              alignItems: 'center',
              justifyContent: 'center',
              marginBottom: 16,
              backgroundColor: Colors.primaryColor
            }}>
            <Ionicons name="md-add" size={32} color='#fff' />
          </View>
        </TouchableOpacity>

      </View >
    )

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color
  },
  header: {
    height: 50,
    backgroundColor: Colors.dark_gray,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,

  },
  headText: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 14
  },
  text: {
    textAlign: 'center',
    color: Colors.dark_gray,
    fontSize: 14

  },
  dataWrapper: {
    marginTop: -1
  },
  row: {
    flexDirection: 'row',
    height: 40,
    backgroundColor: '#fff',
  },

  TouchableOpacityStyle: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 20,
  },

  FloatingButtonStyle: {
    resizeMode: 'contain',
    width: 50,
    height: 50,
    //
  },

});