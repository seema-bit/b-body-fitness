import React from 'react';
import { ScrollView, StyleSheet, Text, FlatList, View, Image, TouchableOpacity, AsyncStorage } from 'react-native';
import HeaderBackLeft from '../components/HeaderBackLeft';
import Constant from '../constants/Constant';
import { textHeader, font_style } from '../components/styles';
import Colors from '../constants/Colors';
import HeaderBackground from '../components/HeaderBackground';
import ApiService from '../webservice/ApiService';
import Loader from '../components/Loader';
import { Container, Content, Card, CardItem, Thumbnail, Left, Body, Right, } from 'native-base';
import Utils from '../constants/Utils';

export default class InstructorDetailsScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.instructor}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,
  });


  state = {

    detail: null,
    access_key: '',
    userInfo: {},
  }


  componentDidMount() {
  }

  componentWillMount() {

    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        var headers = {
          'X-Access-Token': json.access_key,
        }

        ApiService.api.get(ApiService.instructor_details, { headers: headers })
          .then((response) => {
            if (response !== null && response.data != null) {
              if (response.data.status === Constant.SUCCESS) {
                this.setState({
                  detail: response.data.data != null ? response.data.data : null,
                })
              } else if (response.data.status === Constant.WARNING) {
                if (response.data.session !== undefined && response.data.session !== null) {
                  if (response.data.session === false) {
                    Utils.toastShow(response.data.message)
                    Utils._signOutAsync(this.props.navigation)
                  }
                }
              }
            }

          })
          .catch(function (error) {
            console.log("error " + JSON.stringify(error.response));
            Utils._SentryReport(error)
          })

      })
      .catch(error => {
        console.log('error!')
        Utils._SentryReport(error)
      });
  }

  render() {
    if (this.state.detail === undefined || this.state.detail === null) {
      return (<Loader />)
    } else {
      return (
        <View style={styles.container} transparent>
          <View style={{ alignItems: 'center', marginBottom: 20 }}>
            {this.state.detail.image ?
              (<Image source={{ uri: this.state.detail.image }} style={styles.image} />)
              : (<Image source={require('../assets/images/admin.jpg')} style={styles.image} />)
            }
            <Text style={[font_style.font_medium, { fontSize: 14, textTransform: 'capitalize', alignItems: 'center', marginTop: 10 }]}>{this.state.detail.name}</Text>

          </View>


          {this.state.detail.phone !== null &&
            (<View style={styles.row}>
              <Text style={styles.txt_key}>Mobile No. </Text>
              <Text style={styles.txt_value}>: {this.state.detail.phone}</Text>
            </View>)}


          {this.state.detail.email !== null && (<View style={styles.row}>
            <Text style={styles.txt_key}>Email </Text>
            <Text style={styles.txt_value}>: {this.state.detail.email}</Text>
          </View>)}

          {this.state.detail.address !== null && (<View style={styles.row}>
            <Text style={styles.txt_key}>Address </Text>
            <Text style={styles.txt_value}>: {this.state.detail.address}</Text>
          </View>)}


        </View>

      )


    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
    padding: 20,
  },
  image: {
    width: 100, height: 100,
    borderColor: Colors.light_gray,
    borderWidth: 2,
    borderRadius: 100 / 2,
    alignItems: 'center'
  },
  row: {

    flexDirection: 'row',
    marginTop: 10
  },
  txt_value: {
    flex: .8, fontSize: 14,
    color: Colors.dark_gray
  },
  txt_key: {
    flex: .4,
    fontSize: 14,
    color: Colors.dark_gray
  }
});

