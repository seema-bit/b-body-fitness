import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Text, Dimensions, Platform, TouchableOpacity, Image, AsyncStorage } from 'react-native';
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import HeaderLeft from '../components/HeaderLeft'
import HeaderRight from '../components/HeaderRight'
import Constant from '../constants/Constant'
import { textHeader, font_style } from '../components/styles'
import Colors from '../constants/Colors';
import { Icon } from 'react-native-elements';
import CalendarStrip from '../components/calender/CalendarStrip';
import HeaderBackground from '../components/HeaderBackground';
import ApiService from '../webservice/ApiService';
import Loader from '../components/Loader';
import moment from 'moment';
import ModalSelector from 'react-native-modal-selector'
import Utils from '../constants/Utils';

const screenWidth = Math.round(Dimensions.get('window').width);

export default class MyMealScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      tableHead: ['Name', 'Calories'],
      tableHeadWater: ['Name', 'Calories'],
      selectedDate: null,
      foodData: null,
      userInfo: '',
      refresh: false,
      updateCount: 0
    }
  }

  componentWillReceiveProps = () => {

    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json, })
        if (this.state.selectedDate !== null) {
          this.setState({ selectedDate: moment().format('MM-DD-YYYY') })
          this.fetchMeals(moment().format('MM-DD-YYYY'))
        } else {
          this.fetchMeals(this.state.selectedDate)
        }


      })
      .catch(error => console.log('error!'));
  }
  componentDidMount = () => {

    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json, selectedDate: moment().format('MM-DD-YYYY') })
        this.fetchMeals(moment().format('MM-DD-YYYY'))

      })
      .catch(error => console.log('error!'));
  }
  componentWillUpdate = () => {
    //this.setState({ refresh: this.props.refresh })

    if (this.state.updateCount === 0) {
      this.setState({ updateCount: 1 })
      AsyncStorage.getItem(Constant.USERINFO)
        .then(req => JSON.parse(req))
        .then(json => {
          this.setState({ userInfo: json, })
          if (this.state.selectedDate !== null) {
            this.setState({ selectedDate: moment(this.state.selectedDate).format('MM-DD-YYYY') })

          }


        })
        .catch(error => console.log('error!'));

    }


  }

  fetchMeals = (dateFormate) => {

    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }
    const params = {
      date: dateFormate
    }

    ApiService.api.post(ApiService.myMeal, params, { headers: headers })
      .then((response) => {
        //this.setState({ foodData: [] })
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {

            if (response.data.data !== undefined && response.data.data !== null) {

              this.setState({ foodData: response.data.data.length !== 0 ? response.data.data : [] })

            }
          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigationProps)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })
  }

  _DateSelected = (date) => {
    let dateFormate = moment(date).format("MM-DD-YYYY")

    this.setState({ selectedDate: dateFormate, foodData: null })
    //alert(dateFormate)
    this.fetchMeals(dateFormate)
  }

  handleFeedOptionLocal = (key, index, type) => {
    //this.props._DeleteFeed(id + index)

    //alert(key + " && " + index)
    if (key !== null && key !== '') {
      if (key === 0) {
        this._deleteFoodItem(type, index)
      } else if (key === 1) {
        this._EditFoodItem(type, index)
      }
    }

  }

  _deleteFoodItem = (type, index) => {
    let newArray = { ...this.state.foodData }
    switch (type) {
      case 1:

        let item = newArray.breakfast
        const id = item[index].id;
        const newFood = [...item.slice(0, index), ...item.slice(index + 1)]
        newArray.breakfast = newFood;
        this.setState({ foodData: newArray })
        this.networkCallForDeleteMeal(id);
        break

      case 2:

        let item1 = newArray.lunch
        const id1 = item1[index].id;
        const newFood1 = [...item1.slice(0, index), ...item1.slice(index + 1)]
        newArray.lunch = newFood1;
        this.setState({ foodData: newArray })
        this.networkCallForDeleteMeal(id1);
        break

      case 3:

        let item2 = newArray.tea_time
        const id2 = item2[index].id;
        const newFood2 = [...item2.slice(0, index), ...item2.slice(index + 1)]
        newArray.tea_time = newFood2;
        this.setState({ foodData: newArray })
        this.networkCallForDeleteMeal(id2);
        break

      case 4:

        let item3 = newArray.snack
        const id3 = item3[index].id;
        const newFood3 = [...item3.slice(0, index), ...item3.slice(index + 1)]
        newArray.snack = newFood3;
        this.setState({ foodData: newArray })
        this.networkCallForDeleteMeal(id3);
        break

      case 5:

        let item4 = newArray.dinner
        const id4 = item4[index].id;
        const newFood4 = [...item4.slice(0, index), ...item4.slice(index + 1)]
        newArray.dinner = newFood4;
        this.setState({ foodData: newArray })
        this.networkCallForDeleteMeal(id4);
        break

      default:
        break
    }
  }

  _EditFoodItem = (type, index) => {
    let newArray = { ...this.state.foodData }
    switch (type) {
      case 1:
        this.props.navigationProps.navigate('MealNutrition', {
          title: "Edit Breakfast",
          type: 1,
          food_id: newArray.breakfast[index].food_id,
          item_id: newArray.breakfast[index].id,
        })
        break

      case 2:
        this.props.navigationProps.navigate('MealNutrition', {
          title: "Edit Lunch",
          type: 2,
          food_id: newArray.lunch[index].food_id,
          item_id: newArray.lunch[index].id,
        })
        break

      case 3:
        this.props.navigationProps.navigate('MealNutrition', {
          title: "Edit Snacks",
          type: 3,
          food_id: newArray.snack[index].food_id,
          item_id: newArray.snack[index].id,
        })
        break

      case 3:
        this.props.navigationProps.navigate('MealNutrition', {
          title: "Edit Dinner",
          type: 3,
          food_id: newArray.dinner[index].food_id,
          item_id: newArray.dinner[index].id,
        })
        break

      default:
        break
    }
  }


  networkCallForDeleteMeal = (id) => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    ApiService.api.get(ApiService.delete_meal + id, { headers: headers })
      .then((response) => {

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + error);
      })
  }

  render() {
    let index = 0
    const popupData = [
      { key: index++, label: 'Delete Meal' },
      //{ key: index++, label: 'Edit Meal' },
    ];
    return (
      <View style={styles.container}>
        <CalendarStrip
          calendarAnimation={{ type: 'sequence', duration: 30 }}
          daySelectionAnimation={{ type: 'border', duration: 200, borderWidth: 1, borderHighlightColor: 'black', backgroundColor: Colors.primaryColor }}
          style={{ height: 80, paddingTop: 0, paddingBottom: 0 }}
          calendarHeaderStyle={{ color: 'black' }}
          calendarColor={'#fff'}
          showMonth={false}
          dateNumberStyle={{ color: 'black' }}
          dateNameStyle={{ color: 'black' }}
          dateMonthStyle={{ color: 'black' }}
          highlightDateNumberStyle={{ color: 'black' }}
          highlightDateNameStyle={{ color: 'black' }}
          disabledDateNameStyle={{ color: 'black' }}
          //datesWhitelist={datesWhitelist}
          disabledDateNumberStyle={{ color: 'black' }}
          iconContainer={{ flex: 0.1 }}
          onDateSelected={date => { this._DateSelected(date) }}
        />

        {(this.state.foodData === undefined || this.state.foodData === null) ?
          (<Loader />)
          : (
            <ScrollView style={styles.dataWrapper} showsVerticalScrollIndicator={false}>
              <View style={{ margin: 24 }}>
                <View style={styles.menu_layout}>
                  <Text style={[{ flex: 1, fontSize: 14, color: Colors.primaryColor }, font_style.font_bold]}>Meal 1</Text>

                  <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.plus_btn_view}
                    onPress={() => {
                      this.props.navigationProps.navigate('AddMeal', {
                        title: "Add Meal",
                        type: 1,
                        date: this.state.selectedDate
                      })
                    }}>
                    <Image source={require('./../assets/images/icons/plus_3x.png')}
                      style={{ width: 20, height: 20, tintColor: Colors.primaryColor, alignSelf: 'flex-end' }} />
                  </TouchableOpacity>

                </View>
                {(this.state.foodData.breakfast !== undefined && this.state.foodData.breakfast !== null && this.state.foodData.breakfast.length !== 0) &&
                  (<View style={styles.box_layout}>
                    <View style={{
                      flex: 1, flexDirection: 'row', height: 50,
                      backgroundColor: Colors.dark_gray,
                      borderTopLeftRadius: 10,
                      borderTopRightRadius: 10,
                      alignItems: 'center'
                    }} >
                      <Text style={[styles.headText, font_style.font_bold, { flex: 1, }]}>Name</Text>
                      <Text style={[styles.headText, font_style.font_bold, { flex: 1 }]}>Calories</Text>
                    </View>

                    {
                      this.state.foodData.breakfast.map((rowData, index) => (
                        <ModalSelector
                          cancelText="Cancel"
                          touchableActiveOpacity={1}
                          data={popupData}
                          onChange={option => { this.handleFeedOptionLocal(option.key, index, 1) }}
                          overlayStyle={{
                            justifyContent: 'flex-end',
                            width: '100%',
                            marginBottom: -30,
                          }}
                          key={index}
                        >
                          <TouchableOpacity
                            //onLongPress={() => { this._deleteFoodItem(1, index) }}
                            style={[styles.row, { alignItems: 'center' }, index % 2 ? { backgroundColor: '#f5f5f6' } : { backgroundColor: '#ffff', }, index === this.state.foodData.breakfast.length - 1 && { borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }]} >
                            <Text style={[styles.text, font_style.font_medium, { flex: 1, }]}>{rowData.food_name}</Text>
                            <Text style={[styles.text, font_style.font_medium, { flex: 1 }]}>{rowData.quantity !== undefined && rowData.quantity !== null ? parseInt(rowData.quantity) : rowData.quantity}</Text>
                          </TouchableOpacity>

                        </ModalSelector>

                      ))
                    }
                  </View>
                  )}


                <View style={[styles.menu_layout, { marginTop: 16, }]}>
                  <Text style={[{ flex: 1, fontSize: 14, color: Colors.primaryColor }, font_style.font_bold]}>Meal 2</Text>
                  <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.plus_btn_view}
                    onPress={() => {
                      this.props.navigationProps.navigate('AddMeal', {
                        title: "Add Meal",
                        type: 2,
                        date: this.state.selectedDate
                      })
                    }}
                  >
                    <Image source={require('./../assets/images/icons/plus_3x.png')}
                      style={{ width: 20, height: 20, tintColor: Colors.primaryColor, alignSelf: 'flex-end' }} />
                  </TouchableOpacity>

                </View>

                {(this.state.foodData.lunch !== undefined && this.state.foodData.lunch !== null && this.state.foodData.lunch.length !== 0) &&
                  (<View style={styles.box_layout}>
                    <View style={{
                      flex: 1, flexDirection: 'row', height: 50,
                      backgroundColor: Colors.dark_gray,
                      borderTopLeftRadius: 10,
                      borderTopRightRadius: 10,
                      alignItems: 'center'
                    }} >
                      <Text style={[styles.headText, font_style.font_bold, { flex: 1, }]}>Name</Text>
                      <Text style={[styles.headText, font_style.font_bold, { flex: 1 }]}>Calories</Text>
                    </View>

                    {

                      this.state.foodData.lunch.map((rowData, index) => (
                        <ModalSelector
                          cancelText="Cancel"
                          touchableActiveOpacity={1}
                          data={popupData}
                          onChange={option => { this.handleFeedOptionLocal(option.key, index, 2) }}
                          overlayStyle={{
                            justifyContent: 'flex-end',
                            width: '100%',
                            marginBottom: -30,
                          }}
                          key={index}
                        >
                          <TouchableOpacity
                            style={[styles.row, { alignItems: 'center' }, index % 2 ? { backgroundColor: '#f5f5f6' } : { backgroundColor: '#ffff', }, index === this.state.foodData.lunch.length - 1 && { borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }]} >
                            <Text style={[styles.text, font_style.font_medium, { flex: 1, }]}>{rowData.food_name}</Text>
                            <Text style={[styles.text, font_style.font_medium, { flex: 1 }]}>{rowData.quantity !== undefined && rowData.quantity !== null ? parseInt(rowData.quantity) : rowData.quantity}</Text>
                          </TouchableOpacity>
                        </ModalSelector>
                      ))
                    }
                  </View>
                  )}

                <View style={[styles.menu_layout, { marginTop: 16 }]}>
                  <Text style={[{ flex: 1, fontSize: 14, color: Colors.primaryColor }, font_style.font_bold]}>Meal 3</Text>
                  <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.plus_btn_view}
                    onPress={() => {
                      this.props.navigationProps.navigate('AddMeal', {
                        title: "Add Meal",
                        type: 3,
                        date: this.state.selectedDate
                      })
                    }}>
                    <Image source={require('./../assets/images/icons/plus_3x.png')}
                      style={{ width: 20, height: 20, tintColor: Colors.primaryColor, alignSelf: 'flex-end' }} />
                  </TouchableOpacity>

                </View>

                {(this.state.foodData.tea_time !== undefined && this.state.foodData.tea_time !== null && this.state.foodData.tea_time.length !== 0) &&
                  (<View style={styles.box_layout}>
                    <View style={{
                      flex: 1, flexDirection: 'row', height: 50,
                      backgroundColor: Colors.dark_gray,
                      borderTopLeftRadius: 10,
                      borderTopRightRadius: 10,
                      alignItems: 'center'
                    }} >
                      <Text style={[styles.headText, font_style.font_bold, { flex: 1, }]}>Name</Text>
                      <Text style={[styles.headText, font_style.font_bold, { flex: 1 }]}>Calories</Text>
                    </View>

                    {

                      this.state.foodData.tea_time.map((rowData, index) => (
                        <ModalSelector
                          cancelText="Cancel"
                          touchableActiveOpacity={1}
                          data={popupData}
                          onChange={option => { this.handleFeedOptionLocal(option.key, index, 3) }}
                          overlayStyle={{
                            justifyContent: 'flex-end',
                            width: '100%',
                            marginBottom: -30,
                          }}
                          key={index}
                        >
                          <TouchableOpacity style={[styles.row, { alignItems: 'center' }, index % 2 ? { backgroundColor: '#f5f5f6' } : { backgroundColor: '#ffff', }, index === this.state.foodData.tea_time.length - 1 && { borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }]} >
                            <Text style={[styles.text, font_style.font_medium, { flex: 1, }]}>{rowData.food_name}</Text>
                            <Text style={[styles.text, font_style.font_medium, { flex: 1 }]}>{rowData.quantity !== undefined && rowData.quantity !== null ? parseInt(rowData.quantity) : rowData.quantity}</Text>
                          </TouchableOpacity>
                        </ModalSelector>
                      ))
                    }
                  </View>
                  )}

                <View style={[styles.menu_layout, { marginTop: 16 }]}>
                  <Text style={[{ flex: 1, fontSize: 14, color: Colors.primaryColor }, font_style.font_bold]}>Meal 4</Text>
                  <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.plus_btn_view}
                    onPress={() => {
                      this.props.navigationProps.navigate('AddMeal', {
                        title: "Add Meal",
                        type: 4,
                        date: this.state.selectedDate
                      })
                    }}>
                    <Image source={require('./../assets/images/icons/plus_3x.png')}
                      style={{ width: 20, height: 20, tintColor: Colors.primaryColor, alignSelf: 'flex-end' }} />
                  </TouchableOpacity>
                </View>

                {(this.state.foodData.snack !== undefined && this.state.foodData.snack !== null && this.state.foodData.snack.length !== 0) &&
                  (<View style={styles.box_layout}>
                    <View style={{
                      flex: 1, flexDirection: 'row', height: 50,
                      backgroundColor: Colors.dark_gray,
                      borderTopLeftRadius: 10,
                      borderTopRightRadius: 10,
                      alignItems: 'center'
                    }} >
                      <Text style={[styles.headText, font_style.font_bold, { flex: 1, }]}>Name</Text>
                      <Text style={[styles.headText, font_style.font_bold, { flex: 1 }]}>Calories</Text>
                    </View>

                    {

                      this.state.foodData.snack.map((rowData, index) => (
                        <ModalSelector
                          cancelText="Cancel"
                          touchableActiveOpacity={1}
                          data={popupData}
                          onChange={option => { this.handleFeedOptionLocal(option.key, index, 4) }}
                          overlayStyle={{
                            justifyContent: 'flex-end',
                            width: '100%',
                            marginBottom: -30,
                          }}
                          key={index}
                        >
                          <TouchableOpacity style={[styles.row, { alignItems: 'center' }, index % 2 ? { backgroundColor: '#f5f5f6' } : { backgroundColor: '#ffff', }, index === this.state.foodData.snack.length - 1 && { borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }]} >
                            <Text style={[styles.text, font_style.font_medium, { flex: 1, }]}>{rowData.food_name}</Text>
                            <Text style={[styles.text, font_style.font_medium, { flex: 1 }]}>{rowData.quantity !== undefined && rowData.quantity !== null ? parseInt(rowData.quantity) : rowData.quantity}</Text>
                          </TouchableOpacity>
                        </ModalSelector>
                      ))
                    }
                  </View>
                  )}

                <View style={[styles.menu_layout, { marginTop: 16 }]}>
                  <Text style={[{ flex: 1, fontSize: 14, color: Colors.primaryColor }, font_style.font_bold]}>Meal 5</Text>
                  <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.plus_btn_view}
                    onPress={() => {
                      this.props.navigationProps.navigate('AddMeal', {
                        title: "Add Meal",
                        type: 5,
                        date: this.state.selectedDate
                      })
                    }}>
                    <Image source={require('./../assets/images/icons/plus_3x.png')}
                      style={{ width: 20, height: 20, tintColor: Colors.primaryColor, alignSelf: 'flex-end' }} />
                  </TouchableOpacity>
                </View>

                {(this.state.foodData.dinner !== undefined && this.state.foodData.dinner !== null && this.state.foodData.dinner.length !== 0) &&
                  (<View style={styles.box_layout}>
                    <View style={{
                      flex: 1, flexDirection: 'row', height: 50,
                      backgroundColor: Colors.dark_gray,
                      borderTopLeftRadius: 10,
                      borderTopRightRadius: 10,
                      alignItems: 'center'
                    }} >
                      <Text style={[styles.headText, font_style.font_bold, { flex: 1, }]}>Name</Text>
                      <Text style={[styles.headText, font_style.font_bold, { flex: 1 }]}>Calories</Text>
                    </View>

                    {

                      this.state.foodData.dinner.map((rowData, index) => (
                        <ModalSelector
                          cancelText="Cancel"
                          touchableActiveOpacity={1}
                          data={popupData}
                          onChange={option => { this.handleFeedOptionLocal(option.key, index, 5) }}
                          overlayStyle={{
                            justifyContent: 'flex-end',
                            width: '100%',
                            marginBottom: -30,
                          }}
                          key={index}
                        >
                          <TouchableOpacity style={[styles.row, { alignItems: 'center' }, index % 2 ? { backgroundColor: '#f5f5f6' } : { backgroundColor: '#ffff', }, index === this.state.foodData.dinner.length - 1 && { borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }]} >
                            <Text style={[styles.text, font_style.font_medium, { flex: 1, }]}>{rowData.food_name}</Text>
                            <Text style={[styles.text, font_style.font_medium, { flex: 1 }]}>{rowData.quantity !== undefined && rowData.quantity !== null ? parseInt(rowData.quantity) : rowData.quantity}</Text>
                          </TouchableOpacity>
                        </ModalSelector>
                      ))
                    }
                  </View>
                  )}

                {/* <View style={[styles.menu_layout, { marginTop: 16 }]}>
                  <Text style={[{ flex: 1, fontSize: 14, color: Colors.primaryColor }, font_style.font_bold]}>Water</Text>
                  <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.plus_btn_view}
                    onPress={() => { this.props.navigationProps.navigate('Water') }}>
                    <Image source={require('./../assets/images/icons/plus_3x.png')}
                      style={{ width: 20, height: 20, tintColor: Colors.primaryColor, alignSelf: 'flex-end' }} />
                  </TouchableOpacity>

                </View>

                {(this.state.foodData.water !== undefined && this.state.foodData.water !== null && this.state.foodData.water !== "") &&
                  (<View style={styles.box_layout}>
                    <View style={{
                      flex: 1, flexDirection: 'row', height: 50,
                      backgroundColor: Colors.dark_gray,
                      borderTopLeftRadius: 10,
                      borderTopRightRadius: 10,
                      alignItems: 'center'
                    }} >
                      <Text style={[styles.headText, font_style.font_bold, { flex: 1, }]}>Name</Text>
                      <Text style={[styles.headText, font_style.font_bold, { flex: 1 }]}>Quality</Text>
                    </View>


                    <View style={[styles.row, { alignItems: 'center', backgroundColor: '#ffff', borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }]} >
                      <Text style={[styles.text, font_style.font_medium, { flex: 1, }]}>Water</Text>
                      <Text style={[styles.text, font_style.font_medium, { flex: 1 }]}>{this.state.foodData.water.water} ml</Text>
                    </View>


                  </View>
                  )} */}



              </View>

            </ScrollView>

          )}

      </View>

    )
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color
  },
  header: {
    height: 50,
    backgroundColor: Colors.dark_gray,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,

  },
  headText: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 14
  },
  text: {
    textAlign: 'center',
    color: Colors.dark_gray,
    fontSize: 14

  },
  dataWrapper: {
    marginTop: -1
  },
  row: {
    flexDirection: 'row',
    height: 40,
    backgroundColor: '#fff',
  },

  TouchableOpacityStyle: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 20,
  },

  FloatingButtonStyle: {
    resizeMode: 'contain',
    width: 50,
    height: 50,
    //
  },
  box_layout: {
    shadowColor: 'black',
    shadowOffset: { height: 1, width: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 10,
    elevation: 20,
  },

  menu_layout: {
    width: '100%',
    flexDirection: 'row',
    marginBottom: 8,
    justifyContent: 'center',
    alignItems: 'center'
  },
  plus_btn_view: {
    width: 56, height: 32, justifyContent: 'center',
    // paddingEnd: 24
  }

});