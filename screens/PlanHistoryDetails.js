import React, { Component } from 'react'
import { ScrollView, StyleSheet, Image, Text, Dimensions, TouchableOpacity, ImageBackground, AsyncStorage } from 'react-native';
import { View } from 'native-base';
import HeaderLeft from '../components/HeaderLeft';
import Constant from '../constants/Constant';
import Colors from '../constants/Colors';
import Carousel from 'react-native-snap-carousel';
import { textHeader, font_style } from '../components/styles';
import { FlatGrid } from 'react-native-super-grid';
import HeaderBackground from '../components/HeaderBackground';
import HeaderRight from '../components/HeaderRight';
import HeaderBackLeft from '../components/HeaderBackLeft';
import Loader from '../components/Loader';
import { SafeAreaView } from 'react-navigation';
import { Ionicons } from '@expo/vector-icons';
import Utils from '../constants/Utils';
import ApiService from '../webservice/ApiService';
import DataNotFound from '../components/DataNotFound';
import moment from 'moment'
import { FlatList } from 'react-native-gesture-handler';
import { WebView } from 'react-native-webview';

const horizontalMargin = 10;
const sliderWidth = Dimensions.get('window').width;
const slideWidth = sliderWidth - 80;
const itemWidth = slideWidth + horizontalMargin * 2;
const itemHeight = 200;

export default class PlanHistoryDetails extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.plan_history_details}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,

  });

  state = {
    data: null,
    planId: ''
  }

  componentDidMount() {
    _this = this
    const planId = this.props.navigation.getParam('planId', null);

    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        var headers = {
          'X-Access-Token': json.access_key,
        }

        ApiService.api.get(ApiService.planHistoryDetails + planId, { headers: headers })
          .then((response) => {
            this.setState({ data: {} })
            if (response !== null && response.data != null) {
              if (response.data.status === Constant.SUCCESS) {
                this.setState({
                  data: response.data.data != null ? response.data.data : {},

                })

              } else if (response.data.status === Constant.WARNING) {
                if (response.data.session !== undefined && response.data.session !== null) {
                  if (response.data.session === false) {
                    Utils.toastShow(response.data.message)
                    Utils._signOutAsync(this.props.navigation)
                  }
                }
              }
            }

          })
          .catch(function (error) {
            this.setState({ data: {} })
            Utils._SentryReport(error)
            console.log("error " + JSON.stringify(error.response));
          })

      })
      .catch(error => console.log('error!'));
  }

  _SelectPlan = (type) => {
    var params = ''
    let current_date = moment().format('MM-DD-YYYY')
    this.setState({ spinner: true })
    if (type === 1) {
      const item = this.state.entries[this.state.selectedIndex]
      params = {
        plan_id: this.state.data.plan_id,
        current_date: current_date,
        type: type
      }
    } else {
      params = {
        current_date: current_date,
        type: type,
        price: this.state.data.price,
        quantity: this.state.data.quantity,
        title: this.state.data.title,
        description: this.state.data.description
      }
    }

    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        var headers = {
          'X-Access-Token': json.access_key,
        }
        ApiService.api.post(ApiService.joinPlan, params, { headers: headers })
          .then((response) => {
            this.setState({ spinner: false })
            Utils.toastShow(response.data.message)
            if (response !== null && response.data != null) {
              if (response.data.status === Constant.SUCCESS) {

                this.props.navigation.navigate('Membership')

              } else if (response.data.status === Constant.WARNING) {
                if (response.data.session !== undefined && response.data.session !== null) {
                  if (response.data.session === false) {
                    Utils.toastShow(response.data.message)
                    Utils._signOutAsync(this.props.navigation)
                  }

                }
              }
            }
          })
          .catch(function (error) {
            this.setState({ spinner: false })
            Utils._SentryReport(error)
            console.log("error " + JSON.stringify(error.response));
          })

      })
      .catch(error => console.log('error!' + JSON.stringify(error.response)));
  }

  ShowAlert = (item, index) => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }
    let newArray = this.state.data

    newArray.auto_renewal = newArray.auto_renewal === "active" ? 'inactive' : 'active'

    this.setState({ data: newArray })
    ApiService.api.get(ApiService.planSetting, { headers: headers })
      .then((response) => {

        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {

          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })
  }

  render() {
    if (this.state.data === undefined || this.state.data === null) {
      return (<Loader />)
    } else {
      return (
        <View style={styles.container}>

          {/* <ScrollView style={{
            backgroundColor: Colors.white_color,
            borderRadius: 5,
            flex: 1
          }}> */}
          <View style={[styles.status, this.state.data.status === 'active' ? { backgroundColor: Colors.green, } : { backgroundColor: Colors.light_gray }]}>
            <Text style={[styles.status_txt, font_style.font_medium, this.state.data.status === 'active' ? { color: Colors.white_color, } : { color: Colors.black_color }]}>{this.state.data.status === 'active' ? 'Active' : 'Expired'}</Text>
          </View>
          <View style={{ flex: 1, padding: 16, marginTop: 8 }}>

            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
              <Text style={[{ fontSize: 35, }, font_style.font_bold]}>{this.state.data.sub_title !== undefined && this.state.data.sub_title !== null ? '$' + this.state.data.price : this.state.data.title}</Text>
              {this.state.data.sub_title !== undefined && this.state.data.sub_title !== null && (
                <View style={{ alignSelf: 'flex-end' }}>
                  <Text style={[{ fontSize: 18, }, font_style.font_bold]}>/{this.state.data.sub_title}</Text>
                </View>)}


            </View>
            {/* <Text style={[{ color: Colors.gray_shade, fontSize: 14, marginTop: 24 }, font_style.font_medium,]}>${this.state.data.price} per sessions</Text> */}

            <Text style={[{ color: Colors.gray_shade, fontSize: 14, marginTop: 8 }, font_style.font_medium,]}>Price : ${this.state.data.price}</Text>
            {this.state.data.quantity !== undefined && this.state.data.quantity !== null &&
              <Text style={[{ color: Colors.gray_shade, fontSize: 14, marginTop: 8 }, font_style.font_medium,]}>Session Quantity : {this.state.data.quantity === '-1' ? 'Unlimited' : this.state.data.quantity}</Text>
            }
            {this.state.data.remanning_quantity !== undefined && this.state.data.remanning_quantity !== null && this.state.data.remanning_quantity >= 0 &&
              <Text style={[{ color: Colors.gray_shade, fontSize: 14, marginTop: 8 }, font_style.font_medium,]}>Remaining Sessions : {this.state.data.remanning_quantity === '-1' ? 'Unlimited' : this.state.data.remanning_quantity}</Text>
            }

            {this.state.data.start_date !== undefined && this.state.data.start_date !== null &&
              <Text style={[{ color: Colors.gray_shade, fontSize: 14, marginTop: 8 }, font_style.font_medium,]}>Start Date : {this.state.data.start_date}</Text>
            }
            {this.state.data.end_date !== undefined && this.state.data.end_date !== null &&
              <Text style={[{ color: Colors.gray_shade, fontSize: 14, marginTop: 8 }, font_style.font_medium,]}>End Date : {this.state.data.end_date}</Text>
            }

            {/* {this.state.data.status === 'active' && (
              <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                  <Text style={[styles.title, font_style.font_medium]}>Plan Auto Renewal</Text>
                </View>
                <TouchableOpacity onPress={() => { this.ShowAlert() }}>
                  <Image source={this.state.data.auto_renewal === "active" ? require('../assets/images/yes_3x.png') : require('../assets/images/no_3x.png')} style={{ width: 78, height: 38 }} resizeMode="contain"></Image>
                </TouchableOpacity>
              </View>
            )} */}


            {this.state.data.description !== undefined && this.state.data.description !== null && (
              <View style={{ flex: 1, }}>
                <Text style={[{ fontSize: 14, color: Colors.black_color, marginBottom: 8, marginTop: 14 }, font_style.font_medium,]}>Membership details</Text>
                <WebView
                  style={{}}
                  originWhitelist={['*']}
                  source={{
                    html: `<!doctype html><html lang="en"> <head> <meta charset="utf-8"> <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> <body style="font-size: 20px;">${this.state.data.description}</body></html>`
                  }}
                />
              </View>
            )}


          </View>

          {/* </ScrollView> */}

          {/* {this.state.data.status === 'inactive' && (
            <View style={styles.bottom_layout}>
              <View style={{ flexDirection: 'row', height: 48 }}>
                <View style={{ flex: 1, backgroundColor: Colors.white_color, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={[{ color: Colors.primaryColor, fontSize: 14, }, font_style.font_medium]}>${this.state.data.price}</Text>
                </View>
                <TouchableOpacity
                  onPress={() => { this._SelectPlan(this.state.data.plan_type) }}
                  style={{ flex: 1, backgroundColor: Colors.primaryColor, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={[{ color: Colors.white_color, fontSize: 14, }, font_style.font_medium]}>Proceed</Text>
                </TouchableOpacity>
              </View>
            </View>
          )} */}

          <SafeAreaView />
        </View>
      )
    }

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white_color,
  },

  TouchableOpacityStyle: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 20,
    bottom: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  status: {

    borderBottomLeftRadius: 26 / 2,
    borderTopLeftRadius: 26 / 2,
    alignItems: 'flex-end',
    justifyContent: 'center',
    height: 26,
    position: 'absolute',
    right: 0,
    top: 8
  },
  status_txt: {
    color: '#fff',
    fontSize: 14,
    paddingEnd: 8,
    paddingStart: 8
  },
  bottom_layout: {
    justifyContent: 'flex-end', shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  }
})