import React, { Component } from 'react'
import { ScrollView, StyleSheet, Image, Text, TouchableOpacity, AsyncStorage } from 'react-native';
import { View } from 'native-base';
import HeaderLeft from '../components/HeaderLeft';
import { textHeader, font_style, buttonStyle } from '../components/styles';
import HeaderBackground from '../components/HeaderBackground';
import Colors from '../constants/Colors';
import Constant from '../constants/Constant';
import { TextInput } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker'
import moment from 'moment'
import ModalSelector from 'react-native-modal-selector'
import Utils from '../constants/Utils';
import ApiService from '../webservice/ApiService';
import HeaderBackLeft from '../components/HeaderBackLeft';
import Spinner from 'react-native-loading-spinner-overlay';
import CustomProgressBar from '../components/CustomProgressBar';

export default class AddWaterScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.add_water}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,
    headerRight: <TouchableOpacity
      onPress={() => { _this._AddWater() }}
      style={textHeader.rightIcon}
      activeOpacity={1}>
      <Image source={require('./../assets/images/icons/tick_3x.png')}
        resizeMode="contain" style={{ width: 20, height: 20, alignSelf: 'flex-end' }} />
    </TouchableOpacity>,
  });

  state = {
    userInfo: '',
    water: "0",
    spinner: false
  }

  componentDidMount = () => {
    _this = this
  }

  _AddWater = () => {
    this.setState({ spinner: true })
    var params = {
      water: this.state.water,
      date: moment().format('MM-DD-YYYY')
    }

    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        var headers = {
          'X-Access-Token': json.access_key,
        }
        ApiService.api.post(ApiService.add_water, params, { headers: headers })
          .then((response) => {
            this.setState({ spinner: false })
            this.props.navigation.navigate("Meal")
          })
          .catch(function (error) {
            //alert('Error')
            this.setState({ spinner: false })
            console.log("error " + JSON.stringify(error.response));
          })

      })
      .catch(error => console.log('error!' + JSON.stringify(error.response)));

  }

  increaseWater = (water) => {
    let value = parseInt(this.state.water) + parseInt(water)
    this.setState({ water: value.toString() })
  }

  render() {

    return (
      <View style={styles.container}>

        <CustomProgressBar spinner={this.state.spinner} />

        <View style={{
          flexDirection: 'row',
          width: 200,
        }}>

          <TextInput style={[styles.txt_style, font_style.font_medium]}
            placeholder="0"
            keyboardType={'numeric'}
            onChangeText={(water) => this.setState({ water })}
            value={this.state.water}
          />
          <Text style={[{ paddingStart: 16, alignItems: 'center' }, styles.txt_style, font_style.font_medium]}>ml</Text>
        </View>




        <View style={{
          flexDirection: 'row',
          paddingBottom: 8,
          marginTop: 24,
          height: 46, alignItems: 'center'
        }}>
          <TouchableOpacity style={{ flex: 1, borderColor: Colors.light_gray, borderWidth: 1, height: 46, alignItems: 'center', justifyContent: 'center' }} onPress={() => { this.increaseWater(200) }} activeOpacity={1}>
            <Text style={[{ textAlign: 'center' }, , styles.txt_style, font_style.font_medium]}>+200 ml</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ flex: 1, borderColor: Colors.light_gray, borderWidth: 1, height: 46, alignItems: 'center', justifyContent: 'center', marginEnd: 16, marginStart: 16 }} onPress={() => { this.increaseWater(100) }} activeOpacity={1}>
            <Text style={[{ textAlign: 'center' }, styles.txt_style, font_style.font_medium]}>+100 ml</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ flex: 1, borderColor: Colors.light_gray, borderWidth: 1, height: 46, alignItems: 'center', justifyContent: 'center' }} onPress={() => { this.increaseWater(250) }} activeOpacity={1}>
            <Text style={[{ textAlign: 'center' }, styles.txt_style, font_style.font_medium]}>+250 ml</Text>
          </TouchableOpacity>


        </View>

      </View>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
    padding: 24,
    alignItems: 'center'
  },
  avatar: {
    width: 46,
    height: 46,
  },
  header: {
    alignItems: 'center',
  },
  txt_style: {
    fontSize: 14,
    color: Colors.dark_gray,
    width: 100,
    borderBottomColor: Colors.light_gray,
    borderBottomWidth: 1,
  },
})
