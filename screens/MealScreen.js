import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Text, Dimensions, Platform, TouchableOpacity, Image, AsyncStorage } from 'react-native';
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import HeaderLeft from '../components/HeaderLeft'
import HeaderRight from '../components/HeaderRight'
import Constant from '../constants/Constant'
import { textHeader, font_style } from '../components/styles'
import Colors from '../constants/Colors';
import { Icon } from 'react-native-elements';
import CalendarStrip from '../components/calender/CalendarStrip';
import MyMealScreen from './MyMealScreen';
import DietPlanScreen from './DietPlanScreen';
import HeaderBackground from '../components/HeaderBackground';
import axios from 'axios';
import ApiService from '../webservice/ApiService';
import Utils from '../constants/Utils';
import NutritionalGuidelineScreen from './NutritionalGuidelineScreen';
import moment from 'moment';
import { connect } from 'react-redux';
import { fetchIntake } from '../redux/action';
const screenWidth = Math.round(Dimensions.get('window').width);

let count = 0
class MealScreen extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.meal}`.toUpperCase()}</Text>,
    headerLeft: <HeaderLeft navigationProps={navigation} />,
    headerRight: <TouchableOpacity
      style={textHeader.rightIcon}
      activeOpacity={1}
      onPress={() => {
        navigation.navigate('MealsTime')
      }}>
      <Image source={require('../assets/images/icons/clock.png')}
        style={{ width: 20, height: 20, alignSelf: 'flex-end', tintColor: Colors.white_color }} />
    </TouchableOpacity>,
  });
  constructor(props) {
    super(props);
    this.state = {
      tableHead: ['Name', 'Quantity'],
      tableHeadWater: ['Name', 'Quantity'],
      selectedTab: 0,
      refresh: 'refresh',
      userInfo: '',
      intakeData: null,
    }
  }

  componentDidMount() {
    this.setState({ selectedTab: this.props.navigation.state.params === undefined || this.props.navigation.state.params.tab === undefined ? 0 : this.props.navigation.state.params.tab })
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        this.fetchIntake()

      })
      .catch(error => console.log('error!'));
  }

  setTab(tab) {
    this.setState({ selectedTab: tab });
  }

  fetchIntake = () => {

    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }
    const params = {
      date: moment().format('MM-DD-YYYY')
    }


    ApiService.api.post(ApiService.cal_intake, params, { headers: headers })
      .then((response) => {

        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            console.log('intakeData', response.data.data)
            if (response.data.data !== undefined && response.data.data !== null) {
              //this.setState({ intakeData: response.data.data })
              this.props.fetch_intake(response.data.data)
            }
          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("intake error " + JSON.stringify(error.response));
      })
  }

  componentWillReceiveProps = () => {
    //this.fetchIntake()
    this.setState({ selectedTab: this.props.navigation.state.params === undefined || this.props.navigation.state.params.tab === undefined ? 0 : this.props.navigation.state.params.tab })

  }

  render() {
    const state = this.state;

    const element = (cellData, cellIndex) => (

      <View key={cellIndex} style={{
        width: '100%',
        height: '100%',
        borderLeftColor: Colors.light_gray,
        borderLeftWidth: 0.5,
        marginTop: -1,
        justifyContent: 'center'
      }}>
        <Text style={[styles.text, font_style.font_medium]}>{cellData}</Text>
      </View>

    );
    return (
      <View style={styles.container}>

        {this.props.data !== undefined && this.props.data !== null && this.props.data.fetchIntake !== null &&
          (<View style={{ backgroundColor: Colors.white_color, padding: 20 }}>
            <Text style={[font_style.font_medium, { fontSize: 16 }]} >Calories Remaining</Text>
            <View style={{ flexDirection: 'row', marginTop: 8 }}>
              <View style={{ flex: 1, alignItems: 'center' }}>
                <Text style={[font_style.font_medium, { fontSize: 14 }]}>{this.props.data.fetchIntake.calories_per_day !== undefined && this.props.data.fetchIntake.calories_per_day !== null ? parseInt(this.props.data.fetchIntake.calories_per_day) : this.props.data.fetchIntake.calories_per_day}</Text>
                <Text style={[font_style.font_medium, { fontSize: 14, color: Colors.dark_gray }]}>Goal</Text>
              </View>

              <Text style={{ flex: .5 }}>-</Text>
              <View style={{ flex: 1, alignItems: 'center', }}>
                <Text style={[font_style.font_medium, { fontSize: 14 }]}>{this.props.data.fetchIntake.taken_calories !== undefined && this.props.data.fetchIntake.taken_calories !== null ? parseInt(this.props.data.fetchIntake.taken_calories) : this.props.data.fetchIntake.taken_calories}</Text>
                <Text style={[font_style.font_medium, { fontSize: 14, color: Colors.dark_gray }]}>Food</Text>
              </View>

              <Text style={{ flex: .5 }}>=</Text>

              <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text style={[font_style.font_medium, { fontSize: 18, color: Colors.primaryColor }]}>{parseInt(this.props.data.fetchIntake.calories_per_day) - parseInt(this.props.data.fetchIntake.taken_calories)}</Text>
                <Text style={[font_style.font_medium, { fontSize: 14, color: Colors.dark_gray }]}>Remaining</Text>
              </View>
            </View>
          </View>
          )}

        <View style={{
          flexDirection: 'row', height: 46,
          borderBottomColor: Colors.light_gray,
          borderBottomWidth: 1,
        }}>

          <TouchableOpacity onPress={() => this.setTab(0)} style={{ width: '49%', alignItems: 'center', justifyContent: 'center', }}>

            <View style={{ alignSelf: 'center', flexDirection: 'row', }}>
              <Image source={require('../assets/images/icons/meal.png')} style={[this.state.selectedTab === 0 ? { tintColor: Colors.primaryColor } : { tintColor: Colors.dark_gray }, { width: 22, height: 22, marginRight: 12 }]} resizeMode="contain" />
              <Text style={[this.state.selectedTab === 0 ? { color: Colors.primaryColor } : { color: Colors.dark_gray }, { fontSize: 19, }]}>{'My Meals'.toUpperCase()}</Text>
            </View>

          </TouchableOpacity>
          <View style={{
            borderLeftColor: Colors.light_gray,
            borderLeftWidth: 1,
            marginTop: 6,
            marginBottom: 6
          }}>
          </View>
          <TouchableOpacity onPress={() => this.setTab(1)} style={{ width: '49%', alignItems: 'center', justifyContent: 'center', }}>

            <View style={{ alignSelf: 'center', flexDirection: 'row', }}>
              <Image source={require('../assets/images/icons/diet_plan_3x.png')} style={[this.state.selectedTab === 1 ? { tintColor: Colors.primaryColor } : { tintColor: Colors.dark_gray }, { width: 22, height: 22, marginRight: 12 }]} resizeMode="contain" />
              <Text style={[this.state.selectedTab === 1 ? { color: Colors.primaryColor } : { color: Colors.dark_gray }, { fontSize: 19, }]}>{'My Totals '.toUpperCase()}</Text>
            </View>

          </TouchableOpacity>
        </View>

        {/* {this.renderScreen(this.state.selectedTab)} */}

        {this.state.selectedTab === 0
          ?
          (<MyMealScreen navigationProps={this.props.navigation} />)
          :
          (<NutritionalGuidelineScreen navigationProps={this.props.navigation} screen={Constant.meal} />)
        }

      </View >
    )
  }
}

const mapStateToProps = state => {
  return {
    data: state.lists
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetch_intake: (item) => {
      dispatch(fetchIntake(item))
    },
  }

}
export default connect(mapStateToProps, mapDispatchToProps)(MealScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color
  },
  header: {
    height: 50,
    backgroundColor: Colors.dark_gray,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,

  },
  headText: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 14
  },
  text: {
    textAlign: 'center',
    color: Colors.dark_gray,
    fontSize: 14

  },
  dataWrapper: {
    marginTop: -1
  },
  row: {
    flexDirection: 'row',
    height: 40,
    backgroundColor: '#fff',
  },

  TouchableOpacityStyle: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 20,
  },

  FloatingButtonStyle: {
    resizeMode: 'contain',
    width: 50,
    height: 50,
    //
  },
  box_layout: {
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: 1, width: 1 },
        shadowOpacity: 0.2,
        shadowRadius: 10,
      },
      android: {
        elevation: 20,
      },
    }),
  },

  menu_layout: {
    width: '100%',
    flexDirection: 'row',
    marginBottom: 8
  },

});