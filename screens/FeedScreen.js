import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
  AsyncStorage,
  Dimensions
} from 'react-native';
import FeedCard from '../components/FeedCard';
import { Icon } from 'react-native-elements';
import HeaderLeft from '../components/HeaderLeft';
import HeaderFeedRight from '../components/HeaderFeedRight';
import Constant from '../constants/Constant';
import { textHeader, font_style, buttonStyle } from '../components/styles';
import ApiService from '../webservice/ApiService';
import Colors from '../constants/Colors';
import HeaderBackground from '../components/HeaderBackground';
import Loader from '../components/Loader'
import { connect } from 'react-redux';
import DataNotFound from '../components/DataNotFound';
import Utils from '../constants/Utils';
import Modal from 'react-native-modalbox';
import {
  updateCount, fetchFeedList, fetchFeedDetails
} from '../redux/action';

class FeedScreen extends React.Component {

  static navigationOptions = ({ navigation }) => {

    return {
      headerBackground: <HeaderBackground />,
      headerStyle: textHeader.header_style,
      headerTitle: <Text style={textHeader.header}>{`${Constant.feed}`.toUpperCase()}</Text>,
      headerLeft: <HeaderLeft navigationProps={navigation} />,
      headerRight: <HeaderFeedRight navigationProps={navigation} />,
    }
  }

  state = {
    access_key: '',
    userInfo: {},
    loading: false,
  }

  async componentDidMount() {


    //
    //this.props.navigation.setParams({ notification_count: "0", store_count: 0 });
    var userInfo = JSON.parse(await AsyncStorage.getItem(Constant.USERINFO));
    console.log('userInfo', userInfo)
    this.setState({ userInfo });
    this.showLoader()
    this._FetchFeeds()

    this._CheckPlanStatus()
    this.interval = setInterval(() => {
      this._CheckPlanStatus()
    }, 300000);
  }

  _CheckPlanStatus = () => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    console.log('check plan status')
    ApiService.api.get(ApiService.planStatus, { headers: headers })
      .then((response) => {
        console.log('plan_status', response.data);
        if (response !== null && response.data != null && response.data.status === Constant.WARNING) {
          this.refs.modal3.open()
        }
      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error _FetchSessionPrice" + JSON.stringify(error));
      })
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }



  // componentWillReceiveProps = () => {

  //   console.log('feed componentWillReceiveProps')
  //   //this.setState({ allFeed: null })
  //   AsyncStorage.getItem(Constant.USERINFO)
  //     .then(req => JSON.parse(req))
  //     .then(json => {
  //       this._FetchFeeds()
  //       //this._NotificationCount()
  //     })
  //     .catch(error => console.log('feed error!', error.response));
  // }


  _FetchFeeds = () => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    ApiService.api.get(ApiService.fetchAllFeeds, { headers: headers })
      .then((response) => {
        this.hideLoader()
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            this.props.fetchFeedList(response.data.data)

          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        //alert('Error')
        console.log("error _FetchFeeds " + JSON.stringify(error.response));

        Utils._SentryReport(error)

      })
  }


  showLoader = () => {
    this.setState({ loading: true });
  }

  hideLoader = () => {
    this.setState({ loading: false });
  }

  _LikeFeed = (index) => {
    //alert("Like: " + index)
    const newArray = [...this.props.lists.feed_list];
    if (newArray[index].user_like !== null && newArray[index].user_like !== '' && newArray[index].user_like === Constant.user_like) {
      newArray[index].likes = newArray[index].likes - 1;
      newArray[index].user_like = Constant.user_unlike
    } else {
      newArray[index].likes = newArray[index].likes + 1;
      newArray[index].user_like = Constant.user_like
    }

    var params = {
      user_id: this.state.userInfo.id,
      feed_id: newArray[index].feed.id
    }



    ApiService.api.post(ApiService.likeDislike, params)
      .then((response) => {
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        //alert('Error')
        console.log("_LikeFeed error " + error);
        Utils._SentryReport(error)
      })

    this.props.fetchFeedList(newArray)

  }

  _CommentFeed = (index, id) => {
    // alert("Comment: " + index + "  " + id)
    this.props.navigation.navigate("Comment",
      {
        user_id: this.state.userInfo.id,
        feed_id: id,
        access_key: this.state.userInfo.access_key,
      }
    )
  }

  _FeedDetails = (index, id) => {
    // alert("Comment: " + index + "  " + id)
    this.props.fetchFeedDetails(null)
    this.props.navigation.navigate("FeedDetails",
      {
        feed_id: id,
      }
    )
  }
  _DeleteFeed = (index) => {
    //alert("delete post: " + index)
    var params = {}

    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }
    const temArray = [...this.props.lists.feed_list]
    const newallFeed = [...this.props.lists.feed_list.slice(0, index), ...this.props.lists.feed_list.slice(index + 1)]
    this.props.fetchFeedList(newallFeed)

    ApiService.api.post(ApiService.deleteFeed + temArray[index].feed.id, params, { headers: headers })
      .then((response) => {

        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            Utils.toastShow(response.data.message)
          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        //alert('Error')
        console.log("error " + error);
        Utils._SentryReport(error)
      })
  }

  _EditFeed = (index) => {
    this.props.navigation.navigate("AddFeed",
      {
        feedData: this.props.lists.feed_list[index],
        editFeed: 'yes',
        //_UpdateData: () => this._UpdateData(),
        title: Constant.edit_feed
      }
    )
  }

  membershipModal = () => {
    return (
      <Modal style={[styles.modal, { height: 400, width: Dimensions.get('screen').width - 40, borderRadius: 25, }]}
        position={"center"}
        ref={"modal3"}
        backdropColor="rgba(0,0,255,1.00)"
        backdropOpacity={0.8}
        animationIn="fadeIn"
        animationOut="fadeOut"
        hideModalContentWhileAnimating={true}
        coverScreen={true}
        backdropTransitionOutTiming={0}
      >

        <View style={{ backgroundColor: Colors.white_color, borderRadius: 25, }}  >

          <View style={{ height: 54, alignItems: 'flex-end', }}>
            <TouchableOpacity style={{ height: 54, width: 54, alignItems: 'flex-end', justifyContent: 'center', }}
              onPress={() => this.refs.modal3.close()}>
              <View style={{ width: 20, height: 20, borderRadius: 20 / 2, borderWidth: 2, borderColor: '#FF135B', marginEnd: 13 }}
                onPress={() => this.refs.modal3.close()}>

                <View style={{ alignSelf: 'center', justifyContent: 'center', width: 18, height: 18, justifyContent: 'center', alignItems: 'center' }}>
                  <Icon
                    name='ios-close'
                    type='ionicon'
                    color='#FF135B'
                    size={18}
                  />
                </View>

              </View>
            </TouchableOpacity>

          </View>
          <View style={{ height: 180, width: Dimensions.get('screen').width - 40 }} >
            <Image style={{ flex: 1, width: undefined, height: undefined }}
              source={require('../assets/images/icons/memberhip_img_3x.png')}
            />
          </View>

          <Text style={[{
            fontSize: 16, color: 'rgba(51,51,64,0.8)',
            marginStart: 28, marginEnd: 28,
            marginTop: 24,
            textAlign: 'center'
          }, font_style.font_Book]}>Become a Member
            {/* Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, */}
          </Text>

          <View style={{ marginStart: 28, marginEnd: 28 }}>
            <TouchableOpacity
              onPress={() => { this.refs.modal3.close(), this.props.navigation.navigate('Membership') }}
              activeOpacity={1}
              style={[buttonStyle.auth_btn, { backgroundColor: Colors.primaryColor, marginTop: 32, }]}
              disabled={this.state.disableBtn}>
              <Text style={[buttonStyle.loginText, { color: Colors.white_color }]}>{'Get Now'.toUpperCase()}</Text>
            </TouchableOpacity>
          </View>

        </View>

      </Modal>
    )
  }

  render() {

    return (
      <View style={styles.container}>
        {this.membershipModal()}
        {this.state.loading ? <Loader /> :
          this.props.lists.feed_list.length === 0 ?
            <DataNotFound /> :

            <FeedCard
              userInfo={this.state.userInfo}
              feeds={this.props.lists.feed_list}
              _LikeFeed={this._LikeFeed}
              _CommentFeed={this._CommentFeed}
              _FeedDetails={this._FeedDetails}
              _DeleteFeed={this._DeleteFeed}
              _EditFeed={this._EditFeed}
              navigationProps={this.props.navigation} />}



      </View>
    )
  }

}

//state map
const mapStateToProps = state => {
  return {
    counts: state.counts,
    lists: state.lists
  }
}

const mapDispatchToProps = dispatch => {
  return {
    updateCounts: (item) => {
      dispatch(updateCount(item))
    },

    fetchFeedList: (item) => {
      dispatch(fetchFeedList(item))
    },
    fetchFeedDetails: (item) => {
      dispatch(fetchFeedDetails(item))
    },
  }

}
export default connect(mapStateToProps, mapDispatchToProps)(FeedScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color
  },
  contentContainer: {
    paddingTop: 10,
  },

  list: {
    backgroundColor: "#E6E6E6",
  },
  top_layout: {
    paddingEnd: 20,
    width: 32,
    height: 32,
    alignItems: 'flex-end',
    justifyContent: 'center'
  }

});
