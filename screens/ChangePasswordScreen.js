import React from 'react';
import {
  ScrollView, StyleSheet, View, Image, Dimensions,
  Text, TouchableOpacity, Platform, Keyboard, AsyncStorage,
  KeyboardAvoidingView
} from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import { TextInput } from 'react-native-gesture-handler';
import { textInput, buttonStyle, text_color } from '../components/styles';
import Colors from '../constants/Colors';
import { Icon } from 'react-native-elements'
import Utils from '../constants/Utils';
import ApiService from '../webservice/ApiService';
import Constant from '../constants/Constant';
import Spinner from 'react-native-loading-spinner-overlay';
import CustomProgressBar from '../components/CustomProgressBar';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import HeaderLeft from '../components/HeaderLeft';

const deviceWidth = Dimensions.get('window').width;

export default class ChangePasswordScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {
    oldPassword: '',
    password: '',
    cpassword: '',
    visible_old_password: true,
    visible_password: true,
    visible_cpassword: true,
    disableBtn: false,
    spinner: false,
    offset: 0,
    bgColor: Colors.primaryColor
  }

  _ResetPassword = () => {

    if (this.state.oldPassword !== null && this.state.oldPassword !== '') {

      if (this.state.password !== null && this.state.password !== '') {
        if (this.state.password.length >= 6) {
          if (this.state.password === this.state.cpassword) {
            this.setState({ disableBtn: true })
            this.networkCall()
          } else {
            Utils.toastShow('Password & Confirm Password should be same')
          }

        } else {
          Utils.toastShow('Password should be at least 6 characters')
        }

      } else {
        Utils.toastShow('New password required')
      }
    } else {
      Utils.toastShow('Old password required')
    }
  }

  networkCall = () => {
    this.setState({ spinner: true })
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {

        const user = {
          old_password: this.state.oldPassword,
          new_password: this.state.password,
        };

        var headers = {
          'X-Access-Token': json.access_key,
        }

        ApiService.api.post(ApiService.change_password, user, { headers: headers })
          .then((response) => {
            this.setState({ spinner: false })
            if (response !== null && response.data != null) {
              this.setState({ disableBtn: false })
              if (response.data.status !== undefined && response.data.status === Constant.TRUE) {
                Utils.toastShow(response.data.message)
                this.setState({
                  oldPassword: '',
                  password: '',
                  cpassword: '',
                  visible_old_password: true,
                  visible_password: true,
                  visible_cpassword: true,
                })
                //this.props.navigation.navigate('Main')
              } else if (response.data.status !== undefined && response.data.status === Constant.FALSE) {
                Utils.toastShow(response.data.message)
                if (response.data.session !== undefined && response.data.session !== null) {
                  if (response.data.session === false) {
                    Utils._signOutAsync(this.props.navigation)
                  }
                }
              }
            } else {
              this.setState({ disableBtn: false })
              Utils.toastShow(Constant.message)
            }

          })
          .catch(function (error) {
            console.log("error " + error);
            Utils._SentryReport(error)
            this.setState({ disableBtn: false, spinner: false })
            Utils.toastShow(Constant.message)
          })

      })
      .catch((error) => {
        console.log('error!')
        Utils._SentryReport(error)
        this.setState({ disableBtn: false, spinner: false })
      });
  }

  onScroll = (event) => {
    var currentOffset = event.nativeEvent.contentOffset.y;
    var direction = currentOffset > this.state.offset ? 'down' : 'up';
    this.setState({
      bgColor: currentOffset > this.state.offset ? Colors.white_color : Colors.primaryColor
    })
    this.offset = currentOffset;
    console.log(direction);
  }

  render() {
    return (
      <KeyboardAwareScrollView enableAutomaticScroll enableOnAndroid={true}
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={{
          flexGrow: 1,
          flexDirection: 'row',
          flexWrap: 'wrap',
          width: '100%',
          //height: '100%',
          backgroundColor: Colors.white_color
        }}
        scrollEnabled={true}
        keyboardShouldPersistTaps='handled'
        onScroll={event => this.onScroll(event)}
        style={{ backgroundColor: this.state.bgColor }}
        ref={ref => this.scrollView = ref}
      >
        <View onPress={() => { Keyboard.dismiss() }} activeOpacity={1} style={styles.container}>
          <CustomProgressBar spinner={this.state.spinner} />
          <View style={{
            width: deviceWidth,
            height: deviceWidth,
            justifyContent: 'flex-end',
            marginTop: -50,

          }} >
            <Image style={{
              width: '100%',
              height: undefined,
              aspectRatio: 1,
            }}
              source={require('../assets/images/group_reset_3x.png')}
            />
            <Text style={{
              position: 'absolute',
              justifyContent: 'flex-end',
              alignItems: 'center',
              paddingBottom: 100,
              alignSelf: 'center',
              fontSize: 25,
              color: '#FAFAFA',
              textAlign: 'center',
              fontFamily: 'futura-book',
            }}>{'Change \npassword'.toUpperCase()}</Text>
          </View>

          <View style={{
            position: 'absolute',
            justifyContent: 'center',
          }}>
            <View style={{ paddingTop: 40, }}>
              <HeaderLeft navigationProps={this.props.navigation} />
            </View>

            {/* <TouchableOpacity
              onPress={() => {
                console.log(this.props.navigation.navigate('Drawer'));
                // this.props.navigation.navigate('Main')
              }}
              style={{
                paddingTop: 48,
                width: 24, height: 24, alignItems: 'flex-start',
                padding: 20,
              }}>
              <Image source={require('./../assets/images/icons/arrow-right_3x.png')} style={{ width: 11, height: 20, }} />
            </TouchableOpacity> */}
          </View>
          <View style={styles.mainContainer}>


            <View style={[styles.textInputMain, { marginTop: 20, }]}>
              <TextInput
                style={[styles.inputStyle, {}]}
                onChangeText={(oldPassword) => this.setState({ oldPassword })}
                value={this.state.oldPassword}
                placeholder="Enter old password"
                placeholderTextColor={Colors.dark_gray}
                secureTextEntry={this.state.visible_old_password}
                password={this.state.visible_old_password}
              />
              <TouchableOpacity style={styles.view_password} onPress={() => this.setState({ visible_old_password: this.state.visible_old_password === false ? true : false })}  >
                <Image source={this.state.visible_old_password === false ? require('../assets/images/icons/view_password_3x.png') : require('../assets/images/icons/eye.png')} style={[styles.image]} />
              </TouchableOpacity>
            </View>


            <View style={styles.textInputMain}>
              <TextInput
                style={[styles.inputStyle,]}
                onChangeText={(password) => this.setState({ password })}
                value={this.state.password}
                placeholder="Enter new password"
                placeholderTextColor={Colors.dark_gray}
                secureTextEntry={this.state.visible_password}
                password={this.state.visible_password}
              />
              <TouchableOpacity style={styles.view_password} onPress={() => this.setState({ visible_password: this.state.visible_password === false ? true : false })}  >
                <Image source={this.state.visible_password === false ? require('../assets/images/icons/view_password_3x.png') : require('../assets/images/icons/eye.png')} style={[styles.image]} />
              </TouchableOpacity>
            </View>


            <View style={styles.textInputMain}>
              <TextInput
                style={[styles.inputStyle,]}
                onChangeText={(cpassword) => this.setState({ cpassword })}
                value={this.state.cpassword}
                placeholder="Enter confirm password"
                placeholderTextColor={Colors.dark_gray}
                secureTextEntry={this.state.visible_cpassword}
                password={this.state.visible_cpassword}
                onFocus={() => {
                  this.scrollView.scrollToEnd()
                }}
              />
              <TouchableOpacity style={styles.view_password} onPress={() => this.setState({ visible_cpassword: this.state.visible_cpassword === false ? true : false })}  >
                <Image source={this.state.visible_cpassword === false ? require('../assets/images/icons/view_password_3x.png') : require('../assets/images/icons/eye.png')} style={[styles.image]} />
              </TouchableOpacity>
            </View>

            <TouchableOpacity
              disabled={this.state.disableBtn}
              activeOpacity={1}
              style={[buttonStyle.primaryBtn, { marginBottom: 10 }]}
              underlayColor='#fff'
              onPress={() => { Keyboard.dismiss(), this._ResetPassword() }}>
              <Text style={buttonStyle.btnText}>{'Reset Password'.toUpperCase()}</Text>
            </TouchableOpacity>

          </View>

        </View>

      </KeyboardAwareScrollView>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: '100%',
    height: '100%',
    backgroundColor: Colors.white_color
  },

  mainContainer: {
    padding: 24,
    width: '100%',
    flexDirection: 'column',

  },


  textInputMain: {
    flexDirection: 'row',
    borderRadius: 54 / 2,
    borderWidth: 1,
    borderColor: Colors.light_gray,
    width: '100%',
    height: 54,
    marginTop: 6,
    justifyContent: 'center',
    marginTop: 20,

  },

  image: {
    width: deviceWidth,
    height: deviceWidth / 1.5
  },
  inputStyle: {
    flex: 1,
    paddingStart: 16,
    alignSelf: 'center',
    height: 62,
    paddingTop: 4,
    textAlign: 'center',
    fontFamily: 'futura-book',
    fontSize: 14,
  },
  view_password: { position: 'absolute', right: 0, height: 48, width: 48, justifyContent: 'center', alignSelf: 'center', alignItems: 'flex-end', paddingEnd: 16, },
  image: {
    width: 18,
    height: 14,
    tintColor: Colors.dark_gray
  },
});
