import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage,
  Keyboard
} from 'react-native';
import { Icon } from 'react-native-elements'
import { TextInput, ScrollView } from 'react-native-gesture-handler';
import { textInput, buttonStyle, text_color, textHeader, font_style } from '../components/styles';
import Colors from '../constants/Colors';
import Constant from '../constants/Constant';
import HeaderRight from '../components/HeaderRight';
import HeaderBackground from '../components/HeaderBackground';
import HeaderBackLeft from '../components/HeaderBackLeft';
import { Searchbar } from 'react-native-paper';
import { Container, Content, Card, CardItem, Thumbnail, Left, Body, Right, ListItem } from 'native-base'
import ApiService from '../webservice/ApiService';
import Loader from '../components/Loader';
import { Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';

export default class WorkoutListScreen extends Component {

  static navigationOptions = ({ navigation }) => ({

    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.add_workout}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,
    //headerRight: <HeaderRight navigationProps={navigation} screen="AddMeal" />,
  });

  state = {
    search: '',
    workoutList: null,
    dataBackup: null,
    search_result: null,
    FoodAPIData: '',
    secretTokenData: null,
    userInfo: null,
  };

  componentDidMount = () => {

    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        this.fetchSearchHistory()

      })
      .catch(error => console.log('error!', error));
  }



  fetchSearchHistory = () => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }
    ApiService.api.get(ApiService.getWorkoutName, { headers: headers })
      .then((response) => {

        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            this.setState({
              workoutList: response.data.data != null ? response.data.data : [],
              dataBackup: response.data.data != null ? response.data.data : []
            })

          } else if (response.data.status === Constant.WARNING) {

          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })
  }

  _WorkoutSelect = (data) => {
    this.props.navigation.navigate('AddWorkout', {
      data: data,
      title: data.exercise,
      date: this.props.navigation.state.params.date
    })
  }

  setSearchText(event) {

    let searchText = this.state.search
    let data = this.state.dataBackup
    searchText = searchText.trim().toLowerCase();
    data = data.filter(l => {
      return l.exercise.toLowerCase().match(searchText);
    });
    this.setState({
      workoutList: data
    });
  }

  _getMetaData = (value) => {
    const splitArray = value.split("-");
    const meta = splitArray[0].replace("Per", "")
    return meta.trim()
  }

  _getValue = (value) => {
    let splitArray = value.split("-");
    splitArray = splitArray[1].split("|");
    // splitArray.forEach(element => {
    //   const pair = element.split(":")
    //   const title = pair[0].replace(/[^a-zA-Z0-9_,\s]/g, '')
    //   const value = pair[1].replace(/[^a-zA-Z0-9_,\s]/g, '')

    // });
    meta = splitArray[0].replace("Calories:", "")
    meta = meta.replace("kcal", "")
    return meta.trim()
  }

  searchTextChange = (search) => {
    this.setState({ search: search });
    if (search === '') {
      let data = this.state.dataBackup
      this.setState({ workoutList: data });
    }
  }
  render() {

    const { search } = this.state;
    return (
      <TouchableOpacity onPress={() => { Keyboard.dismiss() }} activeOpacity={1} style={styles.container} >
        <View style={{ flexDirection: 'row', width: '100%', backgroundColor: '#fff' }}>
          <View style={styles.container} >
            <Searchbar
              transparent
              placeholder="Search"
              onChangeText={search => { this.searchTextChange(search) }}
              value={this.state.search}
              style={{
                elevation: 0,
                shadowOpacity: 0,
                borderBottomWidth: 0,
              }}
              onClear={search => this.searchTextChange('')}
              onSubmitEditing={search => { this.setSearchText(search) }}
            />
          </View>


        </View>

        <Container style={{ backgroundColor: Colors.bg_color, }} >
          <Content >
            {(this.state.workoutList === undefined || this.state.workoutList === null) ?
              (<Loader />)
              : (
                <View>
                  {this.state.workoutList.map((item, index) => {
                    return (
                      <View key={index} >
                        <TouchableOpacity onPress={() => { this._WorkoutSelect(item) }}
                        >
                          <Card transparent style={{

                          }} >
                            <CardItem style={{
                              backgroundColor: Colors.bg_color,
                              borderBottomColor: Colors.light_gray,
                              borderBottomWidth: 1,
                            }}>

                              <Text style={[font_style.font_medium, styles.meta_txt]}>{item.exercise}</Text>

                            </CardItem>
                          </Card>
                        </TouchableOpacity>
                      </View>
                    )
                  })}
                </View>
              )}

          </Content>



        </Container>

        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.TouchableOpacityStyle}
          onPress={() => {
            this.props.navigation.navigate('CreateWorkout', {
              // _UpdateData: this._UpdateData(),
              date: this.props.navigation.state.params.date
            })
          }}
        >
          <View
            style={{
              width: 50,
              height: 50,
              borderRadius: 50 / 2,
              alignItems: 'center',
              justifyContent: 'center',
              marginBottom: 16,
              backgroundColor: Colors.primaryColor
            }}>
            <Ionicons name="md-add" size={32} color='#fff' />
          </View>
        </TouchableOpacity>

      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,

  },
  icon_view: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingEnd: 20,
    alignItems: 'center'
  },
  meta_txt: {
    color: Colors.dark_gray,
    fontSize: 14,
  },
  sub_meta_txt: {
    color: Colors.meta_color,
    fontSize: 12,
    marginTop: 6
  },
  TouchableOpacityStyle: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 20,
  },

});