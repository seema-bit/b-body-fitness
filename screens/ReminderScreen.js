import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';
import Colors from '../constants/Colors';
import Constant from '../constants/Constant';
import HeaderRight from '../components/HeaderRight';
import HeaderBackground from '../components/HeaderBackground';
import HeaderBackLeft from '../components/HeaderBackLeft';
import { textInput, buttonStyle, text_color, textHeader, font_style } from '../components/styles';
import { Switch } from 'react-native-switch';
import HeaderLeft from '../components/HeaderLeft';


export default class ReminderScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.class_reminders}`.toUpperCase()}</Text>,
    headerLeft: <HeaderLeft navigationProps={navigation} />,
    //headerRight: <HeaderRight navigationProps={navigation} />,
  });

  state = {
    SwitchOnValueHolder: false,
    SwitchOnValueHolder1: true,
  }

  ShowAlert = (value) => {
    if (value == true) {
      this.setState({ SwitchOnValueHolder: false })
    }
    else {
      this.setState({ SwitchOnValueHolder: true })
    }
  }

  ShowAlert1 = (value) => {
    if (value == true) {
      this.setState({ SwitchOnValueHolder1: false })
    }
    else {
      this.setState({ SwitchOnValueHolder1: true })
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={[styles.heading, font_style.font_bold]}>CLASSES</Text>
        <View style={{ flexDirection: 'row', }}>
          <View style={{ flex: 1 }}>
            <Text style={[styles.class_name, font_style.font_medium]}>Yoga</Text>
            <Text style={[styles.class_time, font_style.font_medium]}>10:00 am</Text>
          </View>
          <TouchableOpacity onPress={() => { this.ShowAlert(this.state.SwitchOnValueHolder) }}>
            <Image source={this.state.SwitchOnValueHolder ? require('../assets/images/yes_3x.png') : require('../assets/images/no_3x.png')} style={{ width: 78, height: 38 }}></Image>
          </TouchableOpacity>
        </View>

        <View style={{ flexDirection: 'row', marginTop: 30 }}>
          <View style={{ flex: 1 }}>
            <Text style={[styles.class_name, font_style.font_medium]}>GYM</Text>
            <Text style={[styles.class_time, font_style.font_medium]}>07:00 pm</Text>
          </View>
          <TouchableOpacity onPress={() => { this.ShowAlert1(this.state.SwitchOnValueHolder1) }}>
            <Image source={this.state.SwitchOnValueHolder1 ? require('../assets/images/yes_3x.png') : require('../assets/images/no_3x.png')} style={{ width: 78, height: 38 }}></Image>
          </TouchableOpacity>
        </View>

      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
    padding: 20,

  },
  heading: {
    fontSize: 16,
    color: '#413F40',
    marginBottom: 20,
  },
  class_name: {
    fontSize: 16,
    color: Colors.dark_gray
  },
  class_time: {
    fontSize: 16,
    color: '#A4A4A4',
    paddingTop: 4

  }
})