import React from 'react';
import { ScrollView, StyleSheet, Image, Text, TouchableOpacity, AsyncStorage, Platform, ImageBackground } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import HeaderLeft from '../components/HeaderLeft';
import HeaderRight from '../components/HeaderRight';
import Colors from '../constants/Colors';
import { View } from 'native-base';
import Constant from '../constants/Constant';
import { textHeader, font_style, buttonStyle } from '../components/styles';
import HeaderBackground from '../components/HeaderBackground';
import { Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from 'native-base'
import moment from 'moment';
import Loader from '../components/Loader';
import ApiService from '../webservice/ApiService';
import { Searchbar } from 'react-native-paper';
import Utils from '../constants/Utils';
import DataNotFound from '../components/DataNotFound';

export default class ContestScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.contest}`.toUpperCase()}</Text>,
    headerLeft: <HeaderLeft navigationProps={navigation} />,
    headerRight: <TouchableOpacity
      onPress={() => { navigation.navigate('ContestWinners') }}
      style={textHeader.rightIcon}>
      <Image source={require('./../assets/images/icons/trophy_3x.png')}
        resizeMode="contain" style={{ width: 20, height: 20, tintColor: '#fff', alignSelf: 'flex-end' }} />
    </TouchableOpacity>,
  });

  state = {
    data: null,
    search: '',
    userInfo: '',
    allContest: null
  }
  componentDidMount = () => {

    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })

        this._FetchContests()

      })
      .catch(error => console.log('error!'));

  }

  _FetchContests = () => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    ApiService.api.get(ApiService.contest, { headers: headers })
      .then((response) => {
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            this.setState({
              allContest: response.data.data != null ? response.data.data : [],

            })
          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })
  }
  _RedirectToRegister = (status, item) => {
    if (status !== true) {
      this.props.navigation.navigate('ContestRegister', {
        data: item
      })
    }

  }

  componentWillReceiveProps = () => {

    this._FetchContests()

  }

  render() {
    if (this.state.allContest === undefined || this.state.allContest === null) {
      return (<Loader />)
    } else {

      return (
        <View style={styles.container}>
          {/* <View style={{
            height: 58,
            borderBottomColor: Colors.light_gray,
            borderBottomWidth: 1,
            backgroundColor: '#fff'
          }}>
            <Searchbar
              transparent
              placeholder="Search"
              onChangeText={search => { this.setState({ search: search }) }}
              value={this.state.search}
              style={{
                backgroundColor: 'yellow',
                elevation: 0,
                shadowOpacity: 0,
                borderBottomWidth: 0,
                flex: 1,
              }}
              onSubmitEditing={search => { this._SearchData(search) }}
            />
          </View> */}
          {this.state.allContest.length === 0
            ?
            (<DataNotFound />)
            :
            (<Container style={{ backgroundColor: Colors.bg_color, paddingTop: 10 }} >
              <Content >
                {this.state.allContest.map((item, index) => {
                  return (
                    <TouchableOpacity
                      onPress={() => {
                        this.props.navigation.navigate('ContestDetails', {
                          contestId: item.id
                        })
                      }}
                      key={item.id}
                      activeOpacity={1}>

                      <Card style={{
                        flex: 0,
                        borderBottomColor: Colors.light_gray,
                        borderBottomWidth: 1,
                      }} transparent>
                        <CardItem style={{ borderRadius: 4, backgroundColor: Colors.bg_color }}>

                          {item.image ? (<ImageBackground source={require('../assets/images/thumbnail.png')}
                            style={{ width: 45, height: 45, borderRadius: 45 / 2 }}
                            imageStyle={{ borderRadius: 45 / 2 }}>
                            <Thumbnail source={{ uri: item.image }} style={{ width: 45, height: 45, borderRadius: 45 / 2 }} />
                          </ImageBackground>) :
                            (<Image source={require('../assets/images/thumbnail.png')}
                              style={{ width: 45, height: 45, borderRadius: 45 / 2 }} />)
                          }

                          <Body style={{ marginStart: 16 }}>
                            <Text style={[font_style.font_medium, { color: Colors.dark_gray, fontSize: 16, textTransform: 'capitalize' }]}>{item.name}</Text>
                            <View style={{ flexDirection: 'row' }}>
                              <Text style={[font_style.font_medium, styles.meta_txt, { flex: 1 }]}>Start Date : {item.start_date}</Text>
                              <Text style={[font_style.font_medium, styles.meta_txt, { alignItems: 'flex-end' }]}>End Date : {item.end_date}</Text>
                            </View>

                          </Body>

                        </CardItem>

                        <CardItem style={{ borderRadius: 4, backgroundColor: Colors.bg_color }}>
                          <Body>
                            <Text style={[styles.details, font_style.font_light, { textTransform: 'capitalize' }]} numberOfLines={3}>{item.description}</Text>
                            <TouchableOpacity
                              activeOpacity={1}
                              onPress={() => {

                                this._RedirectToRegister(item.user_contest, item)
                              }}
                              style={[buttonStyle.auth_btn, styles.btn_register]}
                              underlayColor='#fff'
                              disabled={this.state.disableBtn}>
                              {item.user_contest ?
                                (<Text style={[buttonStyle.loginText, { color: Colors.dark_gray }]}>{'REGISTERED'.toUpperCase()}</Text>)
                                :
                                (<Text style={buttonStyle.loginText}>{'REGISTER NOW'.toUpperCase()}</Text>)
                              }

                            </TouchableOpacity>
                          </Body>
                        </CardItem>
                      </Card>
                    </TouchableOpacity>
                  )
                })}
              </Content>
            </Container>
            )
          }

        </View>

      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },
  meta_txt: {
    color: '#A4A4A4',
    fontSize: 12,
    marginTop: 4
  },
  txt_style: {
    fontSize: 14,
    color: Colors.dark_gray,
  },
  details: {
    color: Colors.dark_gray,
    fontSize: 14
  },
  btn_register: {
    height: 46,
    shadowColor: 'black',
    shadowOffset: { height: 1, width: 0 },
    shadowOpacity: 0.4,
    shadowRadius: 10,
    elevation: 3
  },
})