import React from 'react';
import { Image, Platform, ScrollView, StyleSheet, TouchableOpacity, View, AsyncStorage, ImageBackground, Text, Dimensions } from 'react-native';
import HeaderLeft from '../components/HeaderLeft';
import HeaderRight from '../components/HeaderRight';
import moment from 'moment';
import { textInput, buttonStyle, text_color, textHeader } from '../components/styles';
import Colors from '../constants/Colors';
import Constant from '../constants/Constant';
import { font_style } from '../components/styles';
import HeaderBackground from '../components/HeaderBackground';
import Loader from '../components/Loader';
import ApiService from '../webservice/ApiService';
import Utils from '../constants/Utils';
import HeaderBackLeft from '../components/HeaderBackLeft';
import CustomProgressBar from '../components/CustomProgressBar';
import { Ionicons } from '@expo/vector-icons';

let datesWhitelist = [{
  start: moment(),
}];
let datesBlacklist = [moment().add(1, 'days')]; // 1 day disabled



export default class ClassDetails extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.class_details}`.toUpperCase()}</Text>,
    headerLeft: <TouchableOpacity
      onPress={() => { _this._Redirect() }} style={textHeader.leftIcon}>
      <Image source={require('./../assets/images/icons/arrow-right_3x.png')} style={{ width: 11, height: 20, alignSelf: 'flex-start' }} />
    </TouchableOpacity>,
  });

  state = {
    data: null,
    selectedDate: null,
    userInfo: '',
    classId: '',
    spinner: false,
  }


  _Redirect = () => {
    const redirect = this.props.navigation.getParam('redirect', null);
    if (redirect !== undefined && redirect !== null && redirect === 'notification') {
      this.props.navigation.navigate("Notification")
    } else {
      this.props.navigation.navigate("Class", {
        refresh: 'yes',
      })
    }

  }

  componentDidMount() {
    const classId = this.props.navigation.getParam('classId', null);
    if (classId !== null) {
      this.setState({ classId: classId })
    }
    _this = this

    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json, data: null })

        this._ClassDetails()

      })
      .catch(error => console.log('feed error!', error.response));
  }

  _ClassDetails = () => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    ApiService.api.get(ApiService.class_details + this.state.classId, { headers: headers })
      .then((response) => {
        this.setState({ data: {} })
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            this.setState({ data: response.data.data != null ? response.data.data : {} })
          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })
  }

  _JoinClass = () => {

    if (this.state.data.past_class !== undefined && this.state.data.past_class !== null && this.state.data.past_class === 'inactive') {
      Utils.toastShow(this.state.data.msg)
      // if (this.state.data.class_join === "true") {
      //   Utils.toastShow('You can not change past class status')
      // } else {
      //   Utils.toastShow('You can not join past class')
      // }

    } else {
      if (this.state.data.past_class !== undefined && this.state.data.past_class !== null && this.state.data.past_class === 'active') {

        if (this.state.data.class_join === "true") {
          if (this.state.data.class_active_for_user !== undefined && this.state.data.class_active_for_user !== null && this.state.data.class_active_for_user === 'false') {
            //Utils.toastShow('You can not change selection after 24 hours of join class')
            Utils.toastShow(this.state.data.msg)
          } else {
            this._JoinClassforNetwork()
          }
        } else {
          this._JoinClassforNetwork()
        }


      } else {
        this._JoinClassforNetwork()
      }

    }

  }

  _JoinClassforNetwork = () => {
    this.setState({ spinner: true })
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }
    const params = {
      join_date: moment().format("MM-DD-YYYY HH:mm:ss"),
      class_id: this.state.data.id
    }


    ApiService.api.post(ApiService.joinClass, params, { headers: headers })
      .then((response) => {
        this.setState({ spinner: false })
        if (response !== null && response.data != null) {
          Utils.toastShow(response.data.message)
          if (response.data.status === Constant.SUCCESS) {
            //this.setState({ data: response.data })

            let newArray = this.state.data
            if (newArray.class_join === "false") {
              newArray.class_join = "true"
              newArray.class_active_for_user = "true"
            } else {
              newArray.class_join = "false"
            }

            this.setState({ data: newArray })


          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })
  }

  closeBanner = () => {

    this.setState({ data: null })
  }

  render() {

    //const { data } = this.state.data
    if (this.state.data === undefined || this.state.data === null) {
      return (<Loader />)
    } else {
      return (
        <View style={styles.container}>

          <CustomProgressBar spinner={this.state.spinner} />
          <ScrollView>
            <View style={{ backgroundColor: Colors.white_color, borderRadius: 5, margin: 20 }}>

              <ImageBackground source={require('../assets/images/thumbnail.png')} style={{ height: 200, width: '100%' }}>
                {this.state.data.image && (<Image source={{ uri: this.state.data.image }}
                  style={{ height: 200, width: '100%', flex: 1, borderRadius: 4 }} resizeMode="cover" />)}

              </ImageBackground>

              <View style={{ padding: 8 }}>
                <View style={{ flexDirection: 'row', }}>
                  <View style={{ flex: 1 }}>
                    <Text style={[font_style.font_bold, { fontSize: 16, marginBottom: 8 }]}>{this.state.data.exercise_type}</Text>
                    {/* <Text style={[font_style.font_bold, styles.meta_txt, { color: Colors.primaryColor, marginBottom: 8, fontSize: 16 }]}>${this.state.data.per_class_price !== undefined && this.state.data.per_class_price !== null ? this.state.data.per_class_price : 0}</Text> */}
                    <Text style={[{ fontSize: 14, marginBottom: 8 }, font_style.font_medium]}>Class Time : {this.state.data.time_from} to {this.state.data.time_to}</Text>
                  </View>
                  <TouchableOpacity
                    onPress={() => { this._JoinClass() }}
                    style={[styles.joinButton, this.state.data.class_join === 'true' ? { backgroundColor: 'green', } : { backgroundColor: Colors.light_gray },]}>
                    <Image source={require('./../assets/images/icons/tick_3x.png')}
                      resizeMode="contain" style={[{ width: 20, height: 20, }, this.state.data.class_join === 'true' ? { tintColor: '#fff', } : { tintColor: Colors.dark_gray }]} resizeMode="contain" />
                  </TouchableOpacity>

                </View>

                <Text style={[{ fontSize: 14, marginBottom: 8 }, font_style.font_medium]}>Class Date : {this.state.data.date}</Text>
                <Text style={[{ fontSize: 14, marginBottom: 8 }, font_style.font_medium]}>Details :</Text>
                <Text style={[font_style.font_medium, { fontSize: 14, color: 'rgba(65,63,64,0.7)', marginBottom: 8 }]}>{this.state.data.description}</Text>

              </View>

            </View>

          </ScrollView>

          {this.state.data !== null && this.state.data.banner_show !== null && this.state.data.banner_show === true && (
            <View style={{ padding: 16, backgroundColor: Colors.primaryColor, position: 'absolute', width: Dimensions.get('screen').width, top: 0 }}>

              <View style={{ paddingTop: 16, paddingBottom: 16, }}>
                <Text style={[font_style.font_medium, { fontSize: 16, color: Colors.white_color }]}>{this.state.data.banner_title}</Text>
                <Text style={[font_style.font_medium, { fontSize: 14, color: Colors.white_color }]}>{this.state.data.banner_description}</Text>

              </View>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => { this.closeBanner() }}
                style={{
                  position: 'absolute', alignItems: 'center', justifyContent: 'center',
                  right: 0, width: 56, height: 48, top: 8
                }}>
                <Ionicons name="md-close" size={28} color='#fff' />

              </TouchableOpacity>
            </View>

          )}
        </View>
      )
    }

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  meta_txt: { color: 'rgba(90,91,93,0.7)', fontSize: 12, marginTop: 4 },
  joinButton: {
    width: 56,
    height: 32,
    borderRadius: 32 / 2,
    justifyContent: 'center',
    alignItems: 'center'
  }

});
