import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Text, Dimensions, Platform, TouchableOpacity, Image, AsyncStorage, ImageBackground } from 'react-native';
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import HeaderLeft from '../components/HeaderLeft'
import HeaderRight from '../components/HeaderRight'
import Constant from '../constants/Constant'
import { textHeader, font_style } from '../components/styles'
import Colors from '../constants/Colors';
import CalendarStrip from '../components/calender/CalendarStrip';
import HeaderBackground from '../components/HeaderBackground';
import Loader from '../components/Loader';
import ApiService from '../webservice/ApiService';
import moment from 'moment';
import DataNotFound from '../components/DataNotFound';
import { Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';
import CustomProgressBar from '../components/CustomProgressBar';

import { Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from 'native-base'

const screenWidth = Math.round(Dimensions.get('window').width);




export default class TeamWorkoutSingle extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.team_workout}`.toUpperCase()}</Text>,
    headerLeft: <TouchableOpacity
      onPress={() => {
        navigation.navigate("Workout", {
          refresh: 'yes',
        })
      }} style={textHeader.leftIcon}>
      <Image source={require('./../assets/images/icons/arrow-right_3x.png')} style={{ width: 11, height: 20, alignSelf: 'flex-start' }} />
    </TouchableOpacity>,
  });

  state = {
    tableHead: ['No. of Sets', 'Weight', 'Reps'],
    widthArr: [screenWidth / 3.3, screenWidth / 3.3, screenWidth / 3.3],
    data: null,
    selectedDate: null,
    userInfo: '',
    loading_status: false,
    updateCount: 0,
    selectedTab: 0,
    workoutId: ''
  }


  componentDidMount = () => {
    const workoutId = this.props.navigation.getParam('id', null);

    this.setState({ workoutId: workoutId })

    //this.refreshEvent = evEventsent.subscribe('RefreshList', () => this.refresh());
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {

        this.setState({ userInfo: json, selectedDate: moment().format('MM-DD-YYYY') })
        if (this.state.workoutId !== undefined && this.state.workoutId !== null && this.state.workoutId !== '') {
          this._FetchTeamWorkout(moment().format('MM-DD-YYYY'));
        }

      })
      .catch(error => console.log('error!'));

  }

  componentWillReceiveProps = () => {
    this.setState({ data: null })
    this._FetchTeamWorkout(this.state.selectedDate)
  }

  _FetchTeamWorkout = (dateFormate) => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    //this.setState({ allClasses: [] })
    ApiService.api.get(ApiService.singleWorkout + this.state.workoutId, { headers: headers })
      .then((response) => {
        this.setState({ loading_status: false })
        if (response !== null && response.data != null) {

          if (response.data.status === Constant.SUCCESS && response.data.data !== null) {


            let element1 = response.data.data[0]
            let data_set = [];
            element1.data_sets.forEach(element => {
              let newArray = [element.sets, element.weight_per_rep !== null && element.weight_per_rep !== '' ? element.weight_per_rep + " kg" : '-', element.repetitions]
              data_set = [...data_set, newArray]
            });
            element1.data_set = data_set

            this.setState({ data: element1 })


          } else if (response.data.status === Constant.WARNING) {

          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })
  }

  _DateSelected = (date) => {
    let dateFormate = moment(date).format("MM-DD-YYYY")

    this.setState({ selectedDate: dateFormate, })
    this.setState({ loading_status: true })
    this._FetchTeamWorkout(dateFormate)
  }



  setTab(tab) {
    this.setState({ selectedTab: tab });
    //this.renderScreen(tab)
  }



  render() {
    state = this.state

    const element = (cellData, cellIndex) => (

      <View key={cellIndex} style={[{
        width: '100%',
        height: '100%',
        borderBottomColor: Colors.light_gray,
        borderBottomWidth: 1,
        marginTop: -1,
        justifyContent: 'center',
      }, cellIndex === 1 && {
        borderRightColor: Colors.light_gray,
        borderRightWidth: 0.5,
        borderLeftColor: Colors.light_gray,
        borderLeftWidth: 0.5,
      }]}>
        <Text style={[styles.text, font_style.font_medium]} numberOfLines={1} >{cellData}</Text>
      </View>

    );

    if (this.state.data === undefined || this.state.data === null) {
      return (<Loader />)
    } else {
      return (

        <View style={styles.container}>

          <CustomProgressBar spinner={this.state.spinner} />
          <ScrollView>
            <View style={{ backgroundColor: Colors.white_color, borderRadius: 5, margin: 20 }}>

              <ImageBackground source={require('../assets/images/thumbnail.png')} style={{ height: 200, width: '100%' }}>
                {this.state.data.image && (<Image source={{ uri: this.state.data.image }}
                  style={{ height: 200, width: '100%', flex: 1, borderRadius: 4 }} resizeMode="cover" />)}

              </ImageBackground>

              <View style={{ padding: 8 }}>
                <View style={{ flexDirection: 'row', }}>
                  <View style={{ flex: 1 }}>
                    <Text style={[font_style.font_bold, { fontSize: 16, marginBottom: 8 }]}>{this.state.data.title}</Text>
                    <Text style={[{ fontSize: 14, marginBottom: 8 }, font_style.font_medium]}>Workout Time : {this.state.data.time_from} to {this.state.data.time_to}</Text>
                  </View>

                </View>

                <Text style={[{ fontSize: 14, marginBottom: 8 }, font_style.font_medium]}>Workout Date : {this.state.data.date}</Text>
                <Text style={[{ fontSize: 14, marginBottom: 8 }, font_style.font_medium]}>Details :</Text>
                <Text style={[font_style.font_medium, { fontSize: 14, color: 'rgba(65,63,64,0.7)', marginBottom: 8 }]}>{this.state.data.description}</Text>

              </View>
              <View >

                <Table borderStyle={{ borderColor: 'transparent' }}>
                  <Row data={this.state.tableHead} flexArr={[1, 1, 1]} style={styles.header} textStyle={[styles.headText, font_style.font_bold]} />
                </Table>

                <Table borderStyle={{ borderColor: 'transparent' }} style={{ borderRadius: 100 }}>
                  {
                    this.state.data.data_set.map((rowData, index) => (
                      <TableWrapper key={index} style={styles.row} style={[styles.row,]} >
                        {
                          rowData.map((cellData, cellIndex) => (
                            <Cell key={cellIndex} data={element(cellData, cellIndex)} textStyle={[styles.text, font_style.font_medium]} style={cellIndex === 0 ? { flex: 1 } : { flex: 1 }} />
                          ))
                        }
                      </TableWrapper>
                    ))
                  }
                </Table>

              </View>

            </View>

          </ScrollView>


        </View>
      )
    }



  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color
  },
  header: {
    height: 50,
    backgroundColor: Colors.dark_gray,

  },
  headText: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 14
  },
  text: {
    textAlign: 'center',
    color: Colors.dark_gray,
    fontSize: 14

  },
  dataWrapper: {
    marginTop: -1
  },
  row: {
    flexDirection: 'row',
    height: 40,
    backgroundColor: '#fff',
  },

  TouchableOpacityStyle: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 20,
  },

  FloatingButtonStyle: {
    resizeMode: 'contain',
    width: 50,
    height: 50,
    //
  },

});