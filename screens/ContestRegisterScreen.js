import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage,
  Keyboard,
} from 'react-native';
import { Icon } from 'react-native-elements'
import { TextInput, ScrollView } from 'react-native-gesture-handler';
import { textInput, buttonStyle, text_color, textHeader, font_style } from '../components/styles';
import Colors from '../constants/Colors';
import Constant from '../constants/Constant';
import HeaderBackLeft from '../components/HeaderBackLeft';
import HeaderRight from '../components/HeaderRight';
import HeaderProfileRight from '../components/HeaderProfileRight';

import PopupDialog, { DialogContent, DialogButton, DialogTitle } from 'react-native-popup-dialog';
import ModalSelector from 'react-native-modal-selector'
import ApiService from '../webservice/ApiService';
import Utils from '../constants/Utils';
import HeaderBackground from '../components/HeaderBackground';
import Spinner from 'react-native-loading-spinner-overlay';
import moment from 'moment';
import CustomProgressBar from '../components/CustomProgressBar';

export default class ContestRegisterScreen extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.contest_register}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,

  });

  state = {
    userDetails: '',
    name: '',
    email: '',
    mobile: '',
    access_key: '',
    contestData: '',
    disableBtn: false,
    spinner: false
  }

  componentDidMount = () => {
    const contest_data = this.props.navigation.getParam('data', null);
    this.setState({ contestData: contest_data })
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({
          userDetails: json,
          name: json.name !== 'null' && json.name !== null ? json.name : '',
          email: json.email !== 'null' && json.email !== null ? json.email : '',
          mobile: json.mobile !== 'null' && json.mobile !== null ? json.mobile : '',
          access_key: json.access_key,
        })


      })
      .catch(error => console.log('error!'));
  }
  _RegisterNow = () => {

    let msg = '';
    if (this.state.name !== null && this.state.name !== '') {
      if (this.state.email !== null && this.state.email !== '') {
        if (Utils._emailValidate(this.state.email)) {
          if (this.state.mobile !== null && this.state.mobile !== '') {
            if (this.state.mobile.length >= 10) {
              this.networkCall()
            } else {
              msg = 'Enter valid mobile number'
              Utils.toastShow(msg)
            }

          } else {
            msg = 'Mobile number required'
            Utils.toastShow(msg)
          }

        } else {
          msg = 'Email is not correct'
          Utils.toastShow(msg)
        }

      } else {
        msg = 'Email required'
        Utils.toastShow(msg)
      }
    } else {
      msg = 'Name required'
      Utils.toastShow(msg)
    }
  }

  networkCall = () => {
    this.setState({ spinner: true })
    const params = {
      contest_id: this.state.contestData !== undefined ? this.state.contestData.id : 0,
      date: moment().format("MM-DD-YYYY"),
      name: this.state.name,
      email_id: this.state.email,
      phone: this.state.mobile,
    }

    var headers = {
      'X-Access-Token': this.state.userDetails.access_key,
    }

    ApiService.api.post(ApiService.joinContest, params, { headers: headers })
      .then((response) => {
        this.setState({ disableBtn: false, spinner: false })
        if (response !== null && response.data != null) {

          this.props.navigation.navigate("Contest",
            { refresh: 'yes' })
          if (response.data.status === Constant.SUCCESS) {
            Utils.toastShow(response.data.message)
          } else if (response.data.status === Constant.WARNING) {
            Utils.toastShow(response.data.message)
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils.toastShow(Constant.message)
        Utils._SentryReport(error)
        this.setState({ disableBtn: false, spinner: false })
        console.log("error " + error);
      })
  }


  render() {

    return (

      <ScrollView style={styles.container} keyboardShouldPersistTaps="always">
        <CustomProgressBar spinner={this.state.spinner} />

        <View style={styles.container}>
          <View style={styles.bodyContent}>
            <Text style={[styles.title, font_style.font_medium]}>{'To register in this contest,Please fill the form given below'.toUpperCase()}</Text>
            <View style={styles.textInputMain}>
              <TextInput
                style={styles.inputStyle}
                onChangeText={(name) => this.setState({ name })}
                value={this.state.name}
                placeholder="Enter Name"
                placeholderTextColor={Colors.dark_gray}
              />
            </View>

            <View style={styles.textInputMain}>
              <TextInput
                style={styles.inputStyle}
                onChangeText={(email) => this.setState({ email })}
                value={this.state.email}
                placeholder="Enter Email"
                placeholderTextColor={Colors.dark_gray}
                autoCapitalize='none'
              />
            </View>

            <View style={styles.textInputMain}>
              <TextInput
                style={styles.inputStyle}
                onChangeText={(mobile) => this.setState({ mobile })}
                value={this.state.mobile}
                placeholder="Enter Phone Number"
                placeholderTextColor={Colors.dark_gray}
                keyboardType="numeric"
                maxLength={10}
              />
            </View>

            <TouchableOpacity
              disabled={this.state.disableBtn}
              onPress={() => { Keyboard.dismiss(), this._RegisterNow() }}
              style={[buttonStyle.login_btn, { marginTop: 20 }]}
              underlayColor='gray'>
              <Text style={buttonStyle.btnText}>{'Register Now'.toUpperCase()}</Text>
            </TouchableOpacity>

            <View></View>
            <Text style={[styles.terms, font_style.font_Book]}>By clicking on "Register Now" button, it mean you are agree with our{" "}
              <Text onPress={() => { this.props.navigation.navigate("TermsConditions") }} style={[styles.terms, font_style.font_Book, { textDecorationLine: 'underline' }]}>Terms and Conditions</Text>

            </Text>


          </View>

        </View>
      </ScrollView>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },
  title: {
    fontSize: 20,
    color: Colors.dark_gray,
    textAlign: 'center'
  },

  name: {
    fontSize: 22,
    fontWeight: '600',
  },

  bodyContent: {
    flex: 1,
    margin: 24,
    flexDirection: 'row',
    flexWrap: 'wrap'

  },
  cross_icon_view: {
    width: 32,
    height: 32,
    position: 'absolute',
    backgroundColor: 'white',
    borderRadius: 50,
    backgroundColor: 'white',
    shadowOffset: { width: 1, height: 1, },
    shadowColor: 'black',
    shadowOpacity: .4,
    marginTop: 90,
    marginLeft: 60,
    left: 150,
    top: 0,
    zIndex: 10,
    alignSelf: 'center',
    justifyContent: 'center'
  },
  text_style: {
    marginTop: 16,
    color: Colors.txt_gray,
    paddingStart: 16,
    fontFamily: 'futura-medium',
    fontWeight: 'bold',

  },
  textInputMain: {
    flexDirection: 'row',
    borderRadius: 54,
    borderWidth: 1,
    borderColor: Colors.light_gray,
    width: '100%',
    height: 54,
    marginTop: 20,
    justifyContent: 'center'

  },
  inputStyle: {
    flex: 1,
    fontFamily: 'futura-medium',
    alignSelf: 'center',
    textAlign: 'center'

  },
  inputStyleNew: {
    flex: 1,
    paddingStart: 16,
    fontFamily: 'futura-medium',
  },
  edit_icon_view: {
    width: 32,
    height: 32,
    borderRadius: 50,
    backgroundColor: 'white',
    shadowOffset: { width: 0, height: 1, },
    shadowColor: 'black',
    shadowOpacity: .4,
    elevation: 3,
    alignSelf: 'center',
    marginRight: 10,
    justifyContent: 'center'
  },
  terms: {
    marginTop: 32,
    fontSize: 14,
    color: Colors.dark_gray,
    textAlign: 'center'
  }
});