import React, { Component } from 'react'
import { ScrollView, StyleSheet, Image, Text, TouchableOpacity, ActivityIndicator, ImageBackground, AsyncStorage } from 'react-native';
import { View, Toast } from 'native-base';
import HeaderBackLeft from '../components/HeaderBackLeft';
import Constant from '../constants/Constant';
import Colors from '../constants/Colors';
import { textInput, buttonStyle, font_style, textHeader } from '../components/styles';
import HeaderBackground from '../components/HeaderBackground';
import ApiService from '../webservice/ApiService';
import Loader from '../components/Loader';
import { WebView } from 'react-native-webview';
import { SafeAreaView } from 'react-navigation';
import Utils from '../constants/Utils';
import { CheckBox } from 'react-native-elements'
import { Col } from 'react-native-table-component';
import CustomProgressBar from '../components/CustomProgressBar';

export default class AgreeTerms extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.term_condition}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,

  });

  state = {
    checked: false,
    spinner: false,
    disableBtn: false,
  }


  ActivityIndicatorLoadingView() {
    //making a view to show to while loading the webpage
    return (
      <View style={[styles.container1, styles.horizontal]}>
        <ActivityIndicator size="small" color={Colors.primaryColor} />
      </View>

    );
  }

  _AgreeTerms = () => {
    if (this.state.checked) {
      this.networkCall()
    } else {
      Utils.toastShow('You must agree to our Terms & Conditions and Privacy Policy to sign up.')
    }
  }
  networkCall = () => {
    this.setState({ spinner: true })
    const user = {
      device_token: this.props.navigation.state.params.device_token,
      name: this.props.navigation.state.params.name,
      username: this.props.navigation.state.params.username,
      email: this.props.navigation.state.params.email,
      password: this.props.navigation.state.params.password,
      latitude: this.props.navigation.state.params.latitude,
      longitude: this.props.navigation.state.params.longitude,
      gym_id: this.props.navigation.state.params.gym_id,
      fcm_token: this.props.navigation.state.params.fcm_token

    };

    console.log('user', user)

    ApiService.api.post(ApiService.signup, user)
      .then((response) => {
        if (response !== null && response.data != null) {
          this.setState({ disableBtn: false, spinner: false })
          if (response.data.status === Constant.TRUE) {
            AsyncStorage.setItem(Constant.LOGGEDIN, 'true');
            AsyncStorage.setItem(Constant.USERINFO, JSON.stringify(response.data.data));
            // this.props.navigation.navigate('AddCard', {
            //   from: 'signup'
            // })
            this.props.navigation.navigate('BMR', {
              from: 'signup'
            })
          } else if (response.data.status === Constant.FALSE) {
            this.setState({ disableBtn: false, spinner: false })
            Utils.toastShow(response.data.message)
          } else {
            this.setState({ disableBtn: false, spinner: false })
            Utils.toastShow(Constant.message)
          }
        } else {
          this.setState({ disableBtn: false, spinner: false })
          Utils.toastShow(Constant.message)
        }

      })
      .catch(function (error) {
        console.log("sign error : " + error);
        this.setState({ disableBtn: false, spinner: false })
        Utils.toastShow(Constant.message)
        Utils._SentryReport(error)
      })
  }


  render() {
    return (
      <View style={styles.container}>

        <CustomProgressBar spinner={this.state.spinner} />
        <WebView
          source={{ uri: ApiService.BASE_URL + ApiService.condition_url }}
          style={[styles.webview_style]}
          renderLoading={this.ActivityIndicatorLoadingView}
          startInLoadingState={true}
        />



        <View style={{
          justifyContent: 'flex-end', backgroundColor: Colors.white_color, shadowColor: "#000",
          shadowOffset: {
            width: 1,
            height: 0,
          },
          shadowOpacity: 0.22,
          shadowRadius: 2.22,

          elevation: 3,
        }}>
          <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingEnd: 24, paddingStart: 4 }}>
            <CheckBox
              style={{ backgroundColor: 'yellow' }}
              checkedColor={Colors.primaryColor}
              uncheckedColor={Colors.icon_color}
              checked={this.state.checked}
              onPress={() => { this.setState({ checked: this.state.checked === true ? false : true }) }}
            />

            <Text style={[{ fontSize: 14, color: Colors.dark_gray, marginStart: 8, flex: 1 }, font_style.font_medium]}>
              {`I have read and agree to the Terms & Conditions.`}
            </Text>
          </View>
          <View style={{ marginStart: 24, marginEnd: 24, marginBottom: 10 }}>
            <TouchableOpacity
              onPress={() => this._AgreeTerms()}
              activeOpacity={1}

              style={[buttonStyle.auth_btn, { backgroundColor: Colors.primaryColor, marginTop: 4, }]}
              disabled={this.state.disableBtn}>
              <Text style={[buttonStyle.loginText, { color: Colors.white_color }]}>{'Accept'.toUpperCase()}</Text>
            </TouchableOpacity>
          </View>

          <SafeAreaView />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white_color,
  },
  webview_style: {
    flex: 1,
    backgroundColor: Colors.white_color,
    marginStart: 8, marginEnd: 8
  },
  image: {
    width: '100%', height: 150,
    alignItems: 'center'
  },
  row: {

    flexDirection: 'row',
    marginTop: 10
  },
  txt_value: {
    flex: .8, fontSize: 14,
    color: Colors.dark_gray
  },
  txt_key: {
    flex: .4,
    fontSize: 14,
    color: Colors.dark_gray
  },
  ///loader
  container1: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',

  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  }
})