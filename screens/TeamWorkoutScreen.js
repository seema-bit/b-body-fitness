import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Text, Dimensions, Platform, TouchableOpacity, Image, AsyncStorage } from 'react-native';
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import HeaderLeft from '../components/HeaderLeft'
import HeaderRight from '../components/HeaderRight'
import Constant from '../constants/Constant'
import { textHeader, font_style } from '../components/styles'
import Colors from '../constants/Colors';
import CalendarStrip from '../components/calender/CalendarStrip';
import HeaderBackground from '../components/HeaderBackground';
import Loader from '../components/Loader';
import ApiService from '../webservice/ApiService';
import moment from 'moment';
import DataNotFound from '../components/DataNotFound';
import { Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';

import { Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from 'native-base'

const screenWidth = Math.round(Dimensions.get('window').width);




export default class TeamWorkoutScreen extends Component {

  state = {
    tableHead: ['No. of Sets', 'Weight', 'Reps'],
    widthArr: [screenWidth / 3.3, screenWidth / 3.3, screenWidth / 3.3],
    allWorkout: null,
    selectedDate: null,
    userInfo: '',
    loading_status: false,
    updateCount: 0,
    selectedTab: 0,
  }


  componentDidMount = () => {
    //this.refreshEvent = evEventsent.subscribe('RefreshList', () => this.refresh());
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {

        this.setState({ userInfo: json, selectedDate: moment().format('MM-DD-YYYY') })
        this._FetchTeamWorkout(moment().format('MM-DD-YYYY'));

      })
      .catch(error => console.log('error!'));

  }

  componentWillReceiveProps = () => {
    this.setState({ allWorkout: null })
    this._FetchTeamWorkout(this.state.selectedDate)
  }

  _FetchTeamWorkout = (dateFormate) => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    const params = {
      date: dateFormate
    }

    //this.setState({ allClasses: [] })
    ApiService.api.post(ApiService.teamworkout, params, { headers: headers })
      .then((response) => {
        this.setState({ allWorkout: [], loading_status: false })
        if (response !== null && response.data != null) {

          if (response.data.status === Constant.SUCCESS) {

            if (response.data.data !== undefined && response.data.data !== null && response.data.data.length !== 0) {

              this.setState({ allWorkout: response.data.data })

            } else {
              this.setState({ allWorkout: [] })
            }
          } else if (response.data.status === Constant.WARNING) {

          }
        }

      })
      .catch(function (error) {
        console.log("error " + JSON.stringify(error.response));
        Utils._SentryReport(error)
      })
  }

  _DateSelected = (date) => {
    let dateFormate = moment(date).format("MM-DD-YYYY")

    this.setState({ selectedDate: dateFormate, })
    this.setState({ loading_status: true })
    this._FetchTeamWorkout(dateFormate)
  }



  setTab(tab) {
    this.setState({ selectedTab: tab });
    //this.renderScreen(tab)
  }



  render() {
    state = this.state

    const element = (cellData, cellIndex) => (

      <View key={cellIndex} style={[{
        width: '100%',
        height: '100%',

        marginTop: -1,
        justifyContent: 'center'
      }, cellIndex === 1 && {
        borderRightColor: Colors.light_gray,
        borderRightWidth: 0.5,
        borderLeftColor: Colors.light_gray,
        borderLeftWidth: 0.5,
      }]}>
        <Text style={[styles.text, font_style.font_medium, cellIndex === 0 && { textAlign: 'left', paddingStart: 8, paddingEnd: 8 }]} numberOfLines={1} >{cellData}</Text>
      </View>

    );
    return (

      <View style={styles.container}>

        <CalendarStrip
          calendarAnimation={{ type: 'sequence', duration: 30 }}
          daySelectionAnimation={{ type: 'border', duration: 200, borderWidth: 1, borderHighlightColor: 'black', backgroundColor: Colors.primaryColor }}
          style={{ height: 80, paddingTop: 0, paddingBottom: 0 }}
          calendarHeaderStyle={{ color: 'black' }}
          calendarColor={'#fff'}
          showMonth={false}
          dateNumberStyle={{ color: 'black' }}
          dateNameStyle={{ color: 'black' }}
          dateMonthStyle={{ color: 'black' }}
          highlightDateNumberStyle={{ color: 'black' }}
          highlightDateNameStyle={{ color: 'black' }}
          disabledDateNameStyle={{ color: 'black' }}
          //datesWhitelist={datesWhitelist}
          disabledDateNumberStyle={{ color: 'black' }}
          iconContainer={{ flex: 0.1 }}
          onDateSelected={date => { this._DateSelected(date) }}
        />


        {this.state.loading_status ?
          (<View style={{ flex: 1 }}>
            <Loader />
          </View>)
          :
          (<View style={{ flex: 1 }}>{
            this.state.allWorkout === undefined || this.state.allWorkout === null ? (
              <Loader />
            ) :
              this.state.allWorkout.length === 0 ?
                (<DataNotFound />)
                :
                (

                  <ScrollView>
                    {this.state.allWorkout.map((item, index) => {
                      return (
                        <View key={index}>
                          <TouchableOpacity
                            style={{ borderRadius: 4 }}
                            onPress={() => {
                              this.props.navigationProps.navigate('TeamWorkoutSingle', {
                                id: item.id
                              })
                            }}
                          >
                            <Card style={{ flex: 0, marginStart: 16, marginEnd: 16, }} transparent>
                              <CardItem style={{ borderRadius: 5 }}>
                                <View style={{ flexDirection: 'row' }}>
                                  <Thumbnail source={{ uri: item.image }} style={{ height: 63, width: 63, borderRadius: 63 / 2 }} />
                                </View>

                                <View style={{ flex: 1, marginStart: 16, marginEnd: 16 }}>
                                  <Text style={[font_style.font_bold, { color: Colors.dark_gray, fontSize: 14 }]}>{item.title}</Text>
                                  {/* <Text style={[font_style.font_bold, styles.meta_txt, { color: Colors.primaryColor }]}>${item.per_class_price !== undefined && item.per_class_price !== null ? item.per_class_price : 0}</Text> */}
                                  <Text style={[font_style.font_medium, styles.meta_txt]}>By {item.admin_name}</Text>
                                  <Text style={[font_style.font_medium, styles.meta_txt, { color: Colors.dark_gray }]}>{item.time_from} to {item.time_to}</Text>
                                </View>

                                <TouchableOpacity
                                  //onPress={() => { this._JoinClass(item, index) }}
                                  style={[styles.joinButton,]}>
                                  <Image source={require('./../assets/images/icons/tick_3x.png')}
                                    resizeMode="contain" style={[{ width: 20, height: 20, },]} resizeMode="contain" />
                                </TouchableOpacity>

                              </CardItem>
                            </Card>
                          </TouchableOpacity>

                        </View>

                      )
                    }
                    )}


                  </ScrollView>

                )

          }

          </View>)}

      </View >
    )
    // }

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color
  },
  header: {
    height: 50,
    backgroundColor: Colors.dark_gray,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,

  },
  headText: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 14
  },
  text: {
    textAlign: 'center',
    color: Colors.dark_gray,
    fontSize: 14

  },
  dataWrapper: {
    marginTop: -1
  },
  row: {
    flexDirection: 'row',
    height: 40,
    backgroundColor: '#fff',
  },

  TouchableOpacityStyle: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 20,
  },

  FloatingButtonStyle: {
    resizeMode: 'contain',
    width: 50,
    height: 50,
    //
  },

});