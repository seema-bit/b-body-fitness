import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
  Dimensions,
  AsyncStorage,
  //Modal
} from 'react-native';
import HeaderLeft from '../components/HeaderLeft';
import HeaderRight from '../components/HeaderRight';
import moment from 'moment';

import { textInput, buttonStyle, text_color, textHeader } from '../components/styles';
import Colors from '../constants/Colors';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base'
import Constant from '../constants/Constant';
import { font_style } from '../components/styles';
import HeaderBackground from '../components/HeaderBackground';
import Loader from '../components/Loader';
import ApiService from '../webservice/ApiService';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button'
import Modal from "react-native-modal";
import DataNotFound from '../components/DataNotFound';
import Utils from '../constants/Utils';

export default class MyOrdersScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.my_order}`.toUpperCase()}</Text>,
    headerLeft: <HeaderLeft navigationProps={navigation} />,
    //headerRight: <HeaderRight navigationProps={navigation} />,
  });

  state = {
    allProducts: null,
    temp_time: {
      index: 0,
      value: "month",
      name: 'Last 1 Month'
    },
    time: {
      index: 0,
      value: "month",
      name: 'Last 1 Month'
    },
    visibleModal: false,
    userInfo: {}
  }

  componentWillReceiveProps() {
    this._fetchOrder(this.state.time)
  }

  componentWillMount() {
    this.setState({
      allProducts: null,
      temp_time: {
        index: 0,
        value: "month",
        name: 'Last 1 Month'
      },
      time: {
        index: 0,
        value: "month",
        name: 'Last 1 Month'
      },
    })
  }


  componentDidMount = () => {
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        this._fetchOrder(this.state.time)


      })
      .catch(error => console.log('error!'));

  }

  onTimeSelect(index, value) {
    let name = this.setName("1", index)
    this.setState({
      temp_time: { index: index, value: value, name: name }
    })

  }

  _fetchOrder = (time) => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    const params = {
      "time_period": time.value
    }
    ApiService.api.post(ApiService.orderHistory, params, { headers: headers })
      .then((response) => {
        this.setState({ allProducts: [] })
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            console.log('response.data.data', response.data.data)
            this.setState({
              allProducts: response.data.data != null ? response.data.data : [],

            })
          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })
  }

  saveValue = () => {

    this.setState({ time: this.state.temp_time, visibleModal: false })

    this._fetchOrder(this.state.temp_time)
  }

  setName = (type, index) => {
    switch (index) {
      case 0:
        return "Last 1 Month"

      case 1:
        return "Last 3 Months"

      case 2:
        return "Last 6 Months"

      case 3:
        return "1 Year"

      case 4:
        return "All"


      default:
        return "Last 1 Month"
    }
  }

  render() {
    if (this.state.allProducts === undefined || this.state.allProducts === null) {
      return (<Loader />)
    } else {
      return (
        <View style={styles.container}>

          <Modal isVisible={this.state.visibleModal}
            // animationIn="fadeIn"
            animationOut="slideOutDown"
            animationOutTiming={1000}
            backdropTransitionOutTiming={0}
            onBackButtonPress={() => { this.setState({ visibleModal: false }) }}
            onBackdropPress={() => { this.setState({ visibleModal: false }) }}
          >
            <View style={{ backgroundColor: '#fff' }}>
              <Text style={[{ marginStart: 24, marginTop: 24, fontSize: 16, marginBottom: 16 }, font_style.font_medium]}>Time Filter</Text>

              <RadioGroup
                style={{ marginStart: 8, justifyContent: 'center' }}
                selectedIndex={this.state.time.index}
                onSelect={(index, value) => this.onTimeSelect(index, value)}
              >

                <RadioButton value={'month'}>
                  <Text>Last 1 Month</Text>
                </RadioButton>

                <RadioButton value={'3_months'}>
                  <Text>Last 3 Months</Text>
                </RadioButton>

                <RadioButton value={'6_months'}>
                  <Text>Last 6 Months</Text>
                </RadioButton>

                <RadioButton value={'year'}>
                  <Text>Last 1 Year</Text>
                </RadioButton>

                <RadioButton value={'all'}>
                  <Text>All</Text>
                </RadioButton>
              </RadioGroup>



              <View style={styles.btn_view}>
                <TouchableOpacity
                  style={{ marginEnd: 24 }}
                  onPress={() => { this.setState({ visibleModal: false }) }}>
                  <Text style={{ color: Colors.primaryColor }}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.saveValue()}>
                  <Text style={{ color: Colors.primaryColor }}>Ok</Text>
                </TouchableOpacity>
              </View>

            </View>



          </Modal>



          <View style={{
            flexDirection: 'row', height: 52,
            borderBottomColor: Colors.light_gray,
            borderBottomWidth: 1,
            backgroundColor: '#fff'
          }}>

            <TouchableOpacity style={{ width: '50%', justifyContent: 'center', }}>

              <View style={{ flexDirection: 'row', }}>

                <Text style={[{ color: Colors.dark_gray, fontSize: 16, marginStart: 20 }, font_style.font_medium]}>{this.state.time.name}</Text>
              </View>

            </TouchableOpacity>
            <TouchableOpacity style={{ width: '50%', alignItems: 'flex-end', justifyContent: 'center', }}
              onPress={() => { this.setState({ visibleModal: true }) }}>

              <View style={{ flexDirection: 'row', }}>
                <Image source={require('../assets/images/icons/filter_3x.png')} style={[{ width: 22, height: 22, marginRight: 20 }]} />

              </View>

            </TouchableOpacity>
          </View>
          {this.state.allProducts.length === 0
            ?
            (<DataNotFound />)
            : (
              <Container style={{ backgroundColor: Colors.bg_color, paddingTop: 10 }} >

                <Content>
                  {this.state.allProducts.map((item, index) => {
                    return (
                      <TouchableOpacity
                        style={{ marginStart: 16, marginEnd: 16, }}
                        key={index}
                        onPress={() => {
                          this.props.navigation.navigate('OrderDetails', {
                            orderId: item.id
                          })
                        }}
                      >
                        <Card style={{ flex: 0, }} >
                          <CardItem style={{ borderRadius: 4 }}>
                            <View style={{
                              alignItems: 'center',
                              justifyContent: 'center',
                            }}>
                              <Image style={styles.image} source={{ uri: item.product_image }} style={{
                                height: 65,
                                width: 65,
                              }} />
                            </View>
                            <Body style={{
                              justifyContent: 'center', marginStart: 8
                            }}>
                              <Text style={[font_style.font_bold, { color: Colors.feed_gray, fontSize: 14 }]}>{item.product_name}</Text>
                              <Text style={[font_style.font_medium, styles.meta_txt]}>Ordered on {item.order_date}</Text>
                            </Body>
                            <Right>
                              <Image source={require('../assets/images/icons/right_arrow_3x.png')} style={{ marginEnd: 8 }} />
                            </Right>
                          </CardItem>
                        </Card>
                      </TouchableOpacity>

                    )
                  })}
                </Content>
              </Container>
            )}
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  meta_txt: { color: Colors.feed_gray, fontSize: 12, marginTop: 4 },
  btn_view: {
    flexDirection: 'row',
    height: 46, alignItems: 'flex-end',
    justifyContent: 'flex-end',
    marginEnd: 24,
    marginBottom: 24
  },

});
