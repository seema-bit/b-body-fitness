import React, { Component } from 'react'
import { ScrollView, StyleSheet, Image, Text, Dimensions, TouchableOpacity, ImageBackground, AsyncStorage } from 'react-native';
import { View } from 'native-base';
import HeaderLeft from '../components/HeaderLeft';
import Constant from '../constants/Constant';
import Colors from '../constants/Colors';
import Carousel from 'react-native-snap-carousel';
import { textHeader, font_style } from '../components/styles';
import { FlatGrid } from 'react-native-super-grid';
import HeaderBackground from '../components/HeaderBackground';
import HeaderRight from '../components/HeaderRight';
import HeaderBackLeft from '../components/HeaderBackLeft';
import Loader from '../components/Loader';
import { SafeAreaView } from 'react-navigation';
import { Ionicons } from '@expo/vector-icons';
import Utils from '../constants/Utils';
import ApiService from '../webservice/ApiService';
import DataNotFound from '../components/DataNotFound';
import moment from 'moment'
import { FlatList } from 'react-native-gesture-handler';
import { WebView } from 'react-native-webview';
import { LinearGradient } from 'expo-linear-gradient';

const horizontalMargin = 10;
const sliderWidth = Dimensions.get('window').width;
const slideWidth = sliderWidth - 80;
const itemWidth = slideWidth + horizontalMargin * 2;
const itemHeight = 200;

const webViewScript = `
  <script>
          setTimeout(function () {
            window.ReactNativeWebView.postMessage(document.documentElement.scrollHeight)
          }, 500)
        </script>`;

export default class Discounts extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.discounts}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,

  });

  constructor(props) {
    super(props);
    this.state = {
      webheight: 60,
      entries: null,
    }
  }


  componentDidMount() {
    _this = this
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        var headers = {
          'X-Access-Token': json.access_key,
        }

        ApiService.api.get(ApiService.allDiscount, { headers: headers })
          .then((response) => {
            this.setState({ entries: [] })
            if (response !== null && response.data != null) {
              if (response.data.status === Constant.SUCCESS) {
                this.setState({
                  entries: response.data.data != null ? response.data.data : [],
                  //package_price: response.data.data[0].price
                })


              } else if (response.data.status === Constant.WARNING) {
                if (response.data.session !== undefined && response.data.session !== null) {
                  if (response.data.session === false) {
                    Utils.toastShow(response.data.message)
                    Utils._signOutAsync(this.props.navigation)
                  }
                }
              }
            }

          })
          .catch(function (error) {
            Utils._SentryReport(error)
            console.log("error " + JSON.stringify(error.response));
          })

      })
      .catch(error => console.log('error!'));
  }

  _setWebHeight = (height, index) => {

    let newArray = [...this.state.entries]

    newArray[index].webviewHeight = height

    this.setState({ entries: newArray })

  }

  render() {

    if (this.state.entries === undefined || this.state.entries === null) {
      return (<Loader />)
    } else {
      if (this.state.entries.length === 0) {
        return (<DataNotFound />)
      } else {
        return (
          <View style={styles.container}>
            <View style={{ flex: 1 }}>
              <ScrollView>
                <Text style={[{ fontSize: 14, marginTop: 8, paddingStart: 20, paddingEnd: 20, textAlign: 'center' }, font_style.font_medium,]}>These discounts must be requested in person</Text>

                <FlatList
                  style={{ padding: 20 }}
                  showsVerticalScrollIndicator={false}
                  data={this.state.entries}
                  renderItem={({ item, index }) =>
                    <View style={{
                      backgroundColor: Colors.white_color,
                      borderRadius: 5,
                      padding: 16,
                      marginBottom: 16
                    }}>

                      {/* <View style={{ alignItems: 'center' }}>
                    <Text style={[{ color: Colors.white_color, fontSize: 20, }, font_style.font_medium,]}>{`Get`.toUpperCase()}</Text>
                    <Text style={[{ color: Colors.white_color, fontSize: 25, }, font_style.font_bold]}>{item.discount}%<Text style={[{ color: Colors.white_color, fontSize: 16, }, font_style.font_medium]}> OFF</Text></Text>

                  </View> */}
                      <View style={{ alignItems: 'center', justifyContent: 'center', }}>
                        <Text style={[{ fontSize: 20, }, font_style.font_bold]}>{item.title}</Text>

                      </View>

                      <View style={{ flex: 1 }}>
                        <Text style={[{ fontSize: 2, color: Colors.white_color }, font_style.font_medium,]}>discount</Text>
                        <WebView style={{ height: item.webviewHeight }}
                          automaticallyAdjustContentInsets={false}
                          scrollEnabled={false}
                          originWhitelist={['*']}
                          source={{
                            html: `<!doctype html><html lang="en"> <head> <meta charset="utf-8"> <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, shrink-to-fit=no"></head><body style="padding:0;
    margin:0;"
    scroll="no" >
                ${item.description}${webViewScript} <style>
   ul {
  padding:2;
    margin:0;
}
span {
  font-size: 16px!important;

}
</style></body></html>`
                          }}
                          onMessage={event => {
                            webviewHeight = parseInt(event.nativeEvent.data)
                            this._setWebHeight(webviewHeight, index)
                            //this.setState({ webheight: parseInt(event.nativeEvent.data) });


                          }}
                          javaScriptEnabled={true}
                          injectedJavaScript={webViewScript}
                          domStorageEnabled={true}
                        ></WebView>


                      </View>

                    </View>
                  }
                  keyExtractor={item => item.id}
                />
              </ScrollView>

              <TouchableOpacity
                activeOpacity={0.7}
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: 50 / 2,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: Colors.primaryColor
                }}
                onPress={() => {
                  this.props.navigation.navigate('ContactUs', {
                    content: 'Inquiry for discounts'
                  })
                }}
                style={styles.TouchableOpacityStyle}>

                <Image source={require('../assets/images/icons/question.png')}
                  style={{ width: 24, height: 24, alignSelf: 'center', tintColor: Colors.white_color }} resizeMode="contain" />

              </TouchableOpacity>

            </View>

            <SafeAreaView />
          </View>
        )
      }
    }


  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },

  TouchableOpacityStyle: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 20,
    bottom: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
})