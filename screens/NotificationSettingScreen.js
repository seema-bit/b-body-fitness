import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';
import Colors from '../constants/Colors';
import Constant from '../constants/Constant';
import HeaderRight from '../components/HeaderRight';
import HeaderBackground from '../components/HeaderBackground';
import HeaderBackLeft from '../components/HeaderBackLeft';
import { textInput, buttonStyle, text_color, textHeader, font_style } from '../components/styles';
import { Switch } from 'react-native-switch';
import HeaderLeft from '../components/HeaderLeft';
import ApiService from '../webservice/ApiService';
import Utils from '../constants/Utils';

export default class NotificationSettingScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.settings}`.toUpperCase()}</Text>,
    headerLeft: <HeaderLeft navigationProps={navigation} />,
    //headerRight: <HeaderRight navigationProps={navigation} />,
  });

  state = {
    SwitchOnValueHolder: false,
    SwitchOnValueHolder1: true,
    data: [
      { id: 1, title: 'Someone likes my status', status: "0", value: 'like_post' },
      { id: 2, title: 'Someone comments on my status', status: "0", value: 'comment_post' },
      { id: 3, title: 'Someone reply one of my comments', status: "0", value: 'reply_post' },
      { id: 4, title: 'Class Reminder', status: "0", value: 'class_reminder' },
    ],
    userInfo: '',
    notification: false
  }

  componentDidMount = () => {
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        let newArray = this.state.data
        if (json.notification_setting !== null) {
          if (json.notification_setting.like_post === "1") {
            newArray[0].status = "1"
          } else {
            newArray[0].status = "0"
          }

          if (json.notification_setting.comment_post === "1") {

            newArray[1].status = "1"
          } else {
            newArray[1].status = "0"
          }

          if (json.notification_setting.reply_post === "1") {
            newArray[2].status = "1"
          } else {
            newArray[2].status = "0"
          }

          if (json.notification_setting.class_reminder === "1") {
            newArray[3].status = "1"
          } else {
            newArray[3].status = "0"
          }

          this.setState({ data: newArray })


        } else {
          const noti_obj = {
            "class_reminder": "0",
            "comment_post": "0",
            "like_post": "0",
            "reply_post": "0"
          }

          let userData = this.state.userInfo
          userData.notification_setting = noti_obj

          AsyncStorage.setItem(Constant.USERINFO, JSON.stringify(userData));
        }
      })
      .catch(error => console.log('error!'));
  }

  ShowAlert = (index) => {
    let newArray = [...this.state.data]
    if (this.state.data[index].status == "0") {
      newArray[index].status = "1"
      this.setState({ data: newArray })
    } else {
      newArray[index].status = "0"
      this.setState({ data: newArray })
    }

    let params = {}
    let notification = this.state.userInfo.notification_setting
    if (notification === undefined || notification === null) {
      const noti_obj = {
        "class_reminder": "0",
        "comment_post": "0",
        "like_post": "0",
        "reply_post": "0"
      }

      let userData = this.state.userInfo
      userData.notification_setting = noti_obj
      notification = noti_obj

      AsyncStorage.setItem(Constant.USERINFO, JSON.stringify(userData));
    }
    this.state.data.forEach(element => {
      const value = element.value
      params[value] = element.status;
      notification[value] = element.status;
    });


    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    let userData = this.state.userInfo
    userData.notification_setting = notification

    AsyncStorage.setItem(Constant.USERINFO, JSON.stringify(userData));

    ApiService.api.post(ApiService.notificationSetting, params, { headers: headers })
      .then((response) => {
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {

          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })

  }

  _CheckStatusNotificationSetting = () => {

    this.setState({ notification: this.state.notification ? false : true })
  }

  render() {
    return (
      <View style={styles.container}>
        {/* <TouchableOpacity style={[{
          flexDirection: 'row', paddingTop: 16, paddingBottom: 16,
          borderBottomColor: Colors.light_gray,
          borderBottomWidth: 1,
        }]}
          activeOpacity={1}
          onPress={() => { this.props.navigation.navigate('Frenchise') }}>
          <View style={{ flex: 1 }}>
            <Text style={[styles.title, font_style.font_medium, { marginStart: 20, marginEnd: 20 }]}>Franchise</Text>
          </View>

          <Image source={require('../assets/images/icons/right_arrow_3x.png')} style={{ marginEnd: 20 }} />
        </TouchableOpacity> */}

        <TouchableOpacity style={[{
          flexDirection: 'row', paddingTop: 16, paddingBottom: 16,
          borderBottomColor: Colors.light_gray,
          borderBottomWidth: 1,
        }]}
          activeOpacity={1}
          onPress={() => {
            this.props.navigation.navigate('BMR', {
              title: "Add BreakFast",
              from: 'setting'
            })
          }}>
          <View style={{ flex: 1 }}>
            <Text style={[styles.title, font_style.font_medium, { marginStart: 20, marginEnd: 20 }]}>{Constant.bmr_calculation}</Text>
          </View>

          <Image source={require('../assets/images/icons/right_arrow_3x.png')} style={{ marginEnd: 20 }} />
        </TouchableOpacity>

        <View>
          <TouchableOpacity style={[{
            flexDirection: 'row', paddingTop: 16, paddingBottom: 16,
            borderBottomColor: Colors.light_gray,
            borderBottomWidth: 1,
          }]}
            activeOpacity={1}
            onPress={() => { this._CheckStatusNotificationSetting() }}>
            <View style={{ flex: 1 }}>
              <Text style={[styles.title, font_style.font_medium, { marginStart: 20 }]}>Push notification and Reminder</Text>
            </View>

            <Image source={this.state.notification ? require('../assets/images/icons/arrow_down_3x.png') : require('../assets/images/icons/right_arrow_3x.png')} style={[this.state.notification ? { marginEnd: 20, width: 12, height: 7 } : { marginEnd: 20, width: 7, height: 12 }]} resizeMode="contain" />

          </TouchableOpacity>
          {this.state.notification && (<View style={{ backgroundColor: Colors.bg_color }}>
            <Text style={[styles.heading, font_style.font_bold]}>Send me a push notification when</Text>
            {this.state.data.map((item, index) => {
              return (<View style={[{
                flexDirection: 'row', marginRight: 20, marginLeft: 20, paddingTop: 16, paddingBottom: 16,
              }, index < this.state.data.length - 1 && {
                borderBottomColor: Colors.light_gray,
                borderBottomWidth: 1,
              }]}
                key={index}>
                <View style={{ flex: 1 }}>
                  <Text style={[styles.title, font_style.font_medium]}>{item.title}</Text>
                </View>
                <TouchableOpacity onPress={() => { this.ShowAlert(index) }}>
                  <Image source={item.status === "0" ? require('../assets/images/yes_3x.png') : require('../assets/images/no_3x.png')} style={{ width: 78, height: 38 }} resizeMode="contain"></Image>
                </TouchableOpacity>
              </View>
              )
            })
            }
          </View>
          )}

        </View>

        {/* <View style={[{
          flexDirection: 'row', paddingTop: 16, paddingBottom: 16,
          marginStart: 20, marginEnd: 20,
          borderBottomColor: Colors.light_gray,
          borderBottomWidth: 1,
        }]}>
          <View style={{ flex: 1 }}>
            <Text style={[styles.title, font_style.font_medium]}>Class Reminder</Text>
          </View>

          <Image source={require('../assets/images/icons/arrow_down_3x.png')} style={{ width: 8, height: 8, tintColor: '#797979', alignItems: 'flex-end' }} resizeMode="contain" />
        </View> */}



      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',

  },
  heading: {
    fontSize: 16,
    color: '#413F40',
    margin: 20
  },
  title: {
    fontSize: 16,
    color: Colors.dark_gray
  },
  class_time: {
    fontSize: 16,
    color: '#A4A4A4',
    paddingTop: 4

  }
})