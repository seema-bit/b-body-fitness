import React, { Component } from 'react'
import { ScrollView, StyleSheet, Image, Text, TouchableOpacity, AsyncStorage, Platform, Linking } from 'react-native';
import { View } from 'native-base';
import HeaderLeft from '../components/HeaderLeft';
import MapView, { Marker, Polyline } from 'react-native-maps';
import Colors from '../constants/Colors';
import Constant from '../constants/Constant';
import { textHeader, font_style } from '../components/styles';
import HeaderRight from '../components/HeaderRight';
import HeaderBackground from '../components/HeaderBackground';
import ApiService from '../webservice/ApiService';
import MapViewDirections from 'react-native-maps-directions';
import Utils from '../constants/Utils';
import Constants from 'expo-constants';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import openMap, { createOpenLink } from 'react-native-open-maps';
import { Button } from 'react-native-paper';
import { MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';

let marker = null



export default class NearByGymScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.gym_near_me}`.toUpperCase()}</Text>,
    headerLeft: <HeaderLeft navigationProps={navigation} />,
    //headerRight: <HeaderRight navigationProps={navigation} />,
  });

  state = {
    markers: [],
    marker: {},
    mapRegion: null,
    hasLocationPermissions: false,
    locationResult: null,
    location: null,
    origin: null,
    destination: null
  }

  _goToYosemite() {

    const openYosemiteZoomedOut = {
      ...this.state.destination,
      navigate_mode: 'preview',
      travelType: 'drive',
      end: Platform.OS === 'ios' ? `geo:${this.state.destination.latitude},${this.state.destination.longitude}` : `${this.state.destination.latitude},${this.state.destination.longitude}`,
      query: `geo:${this.state.destination.latitude},${this.state.destination.longitude}`
    };

    openMap(openYosemiteZoomedOut)

  }

  //const openYosemiteZoomedOut = createOpenLink({ ...yosemite, zoom: 30 });
  onRegionChange(region) {
    this.setState({ region });
  }

  componentWillMount = () => {
    if (Platform.OS === 'android' && !Constants.isDevice) {
      this.setState({
        errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
      });
    } else {
      this._getLocationAsync();
    }
  }

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    } else {
      this.setState({ hasLocationPermissions: true });
    }



    if (this.state.hasLocationPermissions) {
      let location = await Location.getCurrentPositionAsync({});
      if (location !== null && location !== undefined) {
        let location = await Location.getCurrentPositionAsync({});

        console.log('location', location)
        this.setState({
          mapRegion: {
            latitude: location.coords.latitude,
            longitude: location.coords.longitude,
            latitudeDelta: 0.04,
            longitudeDelta: 0.05,
          },
          origin: {
            latitude: location.coords.latitude,
            longitude: location.coords.longitude,
          }
        });
      }

    }
  };


  componentDidMount() {

    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })

        this._FetchSingleGym()


      })
      .catch(error => console.log('error!', error));

  }

  _getLocation = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      Utils.toastShow('Permission to access location was denied')
      this.setState({
        locationResult: 'Permission to access location was denied',
      });
    } else {
      this.setState({ hasLocationPermissions: true });
    }

  };


  _FetchSingleGym = () => {
    ApiService.api.get(ApiService.single_gym)
      .then((response) => {
        if (response !== null && response.status != null) {
          if (response.data.status === Constant.SUCCESS) {
            const singleData = response.data.data;
            const jsonData = {
              coordinate: {
                latitude: parseFloat(singleData.latitude),
                longitude: parseFloat(singleData.longitude),
              },
              mobile: singleData.mobile,
              address: singleData.address
            }

            this.setState({
              destination: {
                // latitude: 33.6684774,
                // longitude: -117.7644671,
                latitude: parseFloat(response.data.data.latitude),
                longitude: parseFloat(response.data.data.longitude),
              },
              marker: jsonData
            })
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error _FetchSingleGym " + JSON.stringify(error.response));
      })
  }


  _FetchData = () => {
    this.setState({ markers: [] })
    ApiService.api.get(ApiService.fetchGyms)
      .then((response) => {
        if (response !== null && response.status != null) {
          if (response.data.status === Constant.SUCCESS) {

            let newArray = []
            response.data.data.forEach(singleData => {

              if (singleData.latitude !== null && singleData.longitude !== null && singleData.latitude !== '' && singleData.longitude !== '') {
                const jsonData = {
                  coordinate: {
                    latitude: parseFloat(singleData.latitude),
                    longitude: parseFloat(singleData.longitude),
                  },
                  mobile: singleData.mobile,
                  address: singleData.address + ", " + singleData.city + ", " + singleData.country
                }
                newArray.push(jsonData)
              }


            });

            this.setState({ markers: newArray })



            //this.setState({ data: response.data.data != null ? response.data.data : [] })

          } else {

          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })
  }

  showGymData = (data) => {
    marker = data
  }

  _phoneCall = () => {

    let phoneNumber = Constant.mobile_number;
    console.log('phoneNumber', phoneNumber)
    if (Platform.OS !== 'android') {
      phoneNumber = `telprompt:${Constant.mobile_number}`;
    }
    else {
      phoneNumber = `tel:${Constant.mobile_number}`;
    }

    Linking.openURL(phoneNumber);

  }

  render() {
    const element = (data) => (
      <View style={{ backgroundColor: 'white', padding: 4 }}>
        <View style={{ flexDirection: 'row' }}>
          <Text style={[{ fontSize: 14, color: '#000' }, font_style.font_medium]}>Add:</Text>
          <Text style={[{ flex: 1, fontSize: 14, color: Colors.dark_gray, }, font_style.font_medium]}>{data.address}</Text>
        </View>
        <View style={{ flexDirection: 'row', marginTop: 4 }}>
          <Text style={[{ fontSize: 14, color: '#000' }, font_style.font_medium]}>Phone:</Text>
          <Text style={[{ flex: 1, fontSize: 14, color: Colors.dark_gray }, font_style.font_medium]}>+1 {data.mobile}</Text>
        </View>
        {/* <Text>{this.props.title}</Text> */}
      </View>
    );

    return (
      <View>
        <MapView
          style={{ width: '100%', height: '100%' }}
          region={this.state.mapRegion}
        //onRegionChange={this.onRegionChange}
        >

          {this.state.origin !== null &&
            <MapView.Marker
              coordinate={this.state.origin}>
              {/* <MyMarkerView coordinate={destination} />
            <MapView.Callout style={{ width: 200, flex: 1 }} /> */}
            </MapView.Marker>
          }

          {this.state.destination !== null &&
            <MapView.Marker
              coordinate={this.state.destination}
              //pinColor={Colors.green}
              icon={require('../assets/images/icons/pin2.png')}
            // onPress={() => this._goToYosemite()}
            >
              {Platform.OS == 'ios' ? <MyMarkerView coordinate={this.state.destination} /> : null}
              <MapView.Callout style={{ width: 200, flex: 1 }} onPress={() => {
                this._goToYosemite()
              }}>
                {element(this.state.marker)}
              </MapView.Callout>
            </MapView.Marker>
          }


          {(this.state.origin !== null && this.state.destination !== null) &&

            // <Polyline

            //   coordinates={[
            //     this.state.origin,
            //     this.state.destination]
            //   }
            //   strokeColor={Colors.primaryColor} // fallback for when `strokeColors` is not supported by the map-provider
            //   strokeWidth={3}
            //   geodesic={true}
            //   lineDashPhase={2}

            // />
            <MapViewDirections
              mode="WALKING"
              onReady={result => {
                this.setState({ origin: result.coordinates[0], destination: result.coordinates[result.coordinates.length - 1] })
              }}
              origin={this.state.origin}
              destination={this.state.destination}
              apikey={"AIzaSyCZuDdWc2zIWJlaDH-h8ylxXzAgdlZyM4w"}
              strokeWidth={3}
              strokeColor={Colors.primaryColor}
            />
          }

        </MapView>
        {/* <MapView
          region={this.state.region}
          onRegionChange={this.onRegionChange}
        >
          {this.state.markers.map(marker => (
            <Marker
              key={marker.coordinate.latitude + marker.title}
              coordinate={marker.coordinate}
              title={marker.title}
              description={marker.description}
            />
          ))}
        </MapView> */}
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => { this._phoneCall() }}
          style={[styles.TouchableOpacityStyle, { bottom: 80 }]}>
          <View

            style={{
              width: 50,
              height: 50,
              borderRadius: 50 / 2,
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
              marginBottom: 16,
              backgroundColor: Colors.primaryColor
            }}>

            <MaterialIcons name="call" size={32} color='#fff' />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => { Linking.openURL(`mailto:${Constant.email}`) }}
          style={styles.TouchableOpacityStyle}>
          <View

            style={{
              width: 50,
              height: 50,
              borderRadius: 50 / 2,
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
              marginBottom: 16,
              backgroundColor: Colors.primaryColor
            }}>

            <MaterialCommunityIcons name="email-outline" size={32} color='#fff' />
          </View>
        </TouchableOpacity>

      </View>
    )
  }
}

class MyMarkerView extends React.Component {
  render() {
    //const identifier = this.props.navigation.getPerams("identifier", null)
    return (
      <Image source={require('../assets/images/icons/pin3.png')} style={{ height: 36, width: 36, marginBottom: 32, }} resizeMode="contain"></Image>
    );
  }
}

class MyCalloutView extends React.Component {
  state = {
    markerData: null
  }
  componentDidMount() {
    const dataJson = this.props.data
    this.setState({ markerData: dataJson })
  }
  render() {

    return (
      <View style={{ backgroundColor: 'white', padding: 4 }}>
        <View style={{ flexDirection: 'row' }}>
          <Text style={[{ fontSize: 14, color: '#000' }, font_style.font_medium]}>Add:</Text>
          <Text style={[{ fontSize: 14, color: Colors.dark_gray, }, font_style.font_medium]}>{marker}</Text>
        </View>
        <View style={{ flexDirection: 'row', marginTop: 4 }}>
          <Text style={[{ fontSize: 14, color: '#000' }, font_style.font_medium]}>Phone:</Text>
          <Text style={[{ fontSize: 14, color: Colors.dark_gray }, font_style.font_medium]}>+1 </Text>
        </View>
        {/* <Text>{this.props.title}</Text> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },
  meta_txt: {
    color: Colors.dark_gray,
    fontSize: 14,
    marginTop: 4,

  },
  circle: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2,
    backgroundColor: '#fff',
    borderColor: Colors.primaryColor,
    borderWidth: 4,
    justifyContent: 'center',


  },
  pinText: {
    color: Colors.primaryColor,
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 20,
    marginBottom: 10,
  },
  TouchableOpacityStyle: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 20,
  },
})
