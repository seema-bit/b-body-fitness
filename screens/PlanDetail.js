import React, { Component } from 'react'
import { ScrollView, StyleSheet, Image, Text, Dimensions, TouchableOpacity, ImageBackground, AsyncStorage } from 'react-native';
import { View } from 'native-base';
import HeaderLeft from '../components/HeaderLeft';
import Constant from '../constants/Constant';
import Colors from '../constants/Colors';
import Carousel from 'react-native-snap-carousel';
import { textHeader, font_style } from '../components/styles';
import { FlatGrid } from 'react-native-super-grid';
import HeaderBackground from '../components/HeaderBackground';
import HeaderRight from '../components/HeaderRight';
import HeaderBackLeft from '../components/HeaderBackLeft';
import Loader from '../components/Loader';
import { SafeAreaView } from 'react-navigation';
import Utils from '../constants/Utils';
import ApiService from '../webservice/ApiService';
import DataNotFound from '../components/DataNotFound';
import moment from 'moment'
import { FlatList } from 'react-native-gesture-handler';
import { WebView } from 'react-native-webview';

const horizontalMargin = 10;
const sliderWidth = Dimensions.get('window').width;
const slideWidth = sliderWidth - 80;
const itemWidth = slideWidth + horizontalMargin * 2;
const itemHeight = 200;


export default class PlanDetail extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.plan_details}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,

  });

  state = {
    data: null,
    planId: '',
    package_price: 0,
    selectedIndex: 0,
    quantity: 1,
    total_price: 30,
    spinner: false,
    unit_price: 0
  }

  componentDidMount() {
    _this = this
    let planId = this.props.navigation.getParam('planId', null);
    planId = planId === null || planId === '' ? 0 : planId

    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        var headers = {
          'X-Access-Token': json.access_key,
        }

        ApiService.api.get(ApiService.planDetails + planId, { headers: headers })
          .then((response) => {
            this.setState({ data: {} })
            if (response !== null && response.data != null) {
              if (response.data.status === Constant.SUCCESS) {
                this.setState({
                  data: response.data.data != null ? response.data.data : {},
                  unit_price: response.data.data.price
                })


              } else if (response.data.status === Constant.WARNING) {
                if (response.data.session !== undefined && response.data.session !== null) {
                  if (response.data.session === false) {
                    Utils.toastShow(response.data.message)
                    Utils._signOutAsync(this.props.navigation)
                  }
                }
              }
            }

          })
          .catch(function (error) {
            this.setState({ data: {} })
            Utils._SentryReport(error)
            console.log("error " + JSON.stringify(error.response));
          })

      })
      .catch(error => console.log('error!'));
  }


  _SelectPlan = (type) => {
    var params = ''
    let current_date = moment().format('MM-DD-YYYY')
    this.setState({ spinner: true })
    if (type === 1) {
      params = {
        plan_id: this.state.id,
        current_date: current_date,
        type: type
      }
    } else {
      params = {
        current_date: current_date,
        type: type,
        price: this.state.data.price,
        quantity: this.state.quantity,
        title: this.state.data.title,
        description: this.state.data.description
      }
    }


    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        var headers = {
          'X-Access-Token': json.access_key,
        }
        ApiService.api.post(ApiService.joinPlan, params, { headers: headers })
          .then((response) => {
            this.setState({ spinner: false })
            Utils.toastShow(response.data.message)
            if (response !== null && response.data != null) {
              if (response.data.status === Constant.SUCCESS) {

                this.props.navigation.navigate('Membership')

              } else if (response.data.status === Constant.WARNING) {
                if (response.data.session !== undefined && response.data.session !== null) {
                  if (response.data.session === false) {
                    Utils.toastShow(response.data.message)
                    Utils._signOutAsync(this.props.navigation)
                  }

                }
              }
            }
          })
          .catch(function (error) {
            this.setState({ spinner: false })
            Utils._SentryReport(error)
            console.log("error " + JSON.stringify(error.response));
          })

      })
      .catch(error => console.log('error!' + JSON.stringify(error.response)));
  }


  _ManageSessions(quantity) {
    let newData = this.state.data
    newData.price = quantity * this.state.unit_price
    this.setState({ quantity: quantity, data: newData })
  }

  render() {
    if (this.state.data === undefined || this.state.data === null) {
      return (<Loader />)
    } else {
      return (
        <View style={styles.container}>

          {this.state.data.plan_type == 1
            ? (
              // <ScrollView style={{ backgroundColor: Colors.white_color, borderRadius: 5, padding: 16, }}>


              <View style={{ flex: 1, height: Dimensions.get('screen').height, backgroundColor: Colors.white_color, padding: 16 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>

                  <Text style={[{ fontSize: 35, }, font_style.font_bold]}>${this.state.data.price}</Text>
                  {this.state.data.sub_title !== undefined && this.state.data.sub_title !== null && (
                    <View style={{ alignSelf: 'flex-end' }}>
                      <Text style={[{ fontSize: 18, }, font_style.font_bold]}>/{this.state.data.sub_title}</Text>
                    </View>)}


                </View>
                <Text style={[{ fontSize: 14, marginTop: 8, textAlign: 'center' }, font_style.font_medium,]}>(${this.state.data.value_range} per sessions)</Text>

                {this.state.data.description !== undefined && this.state.data.description !== null && (
                  <View style={{ flex: 1, marginTop: 24 }} >
                    <Text style={[{ fontSize: 14, color: Colors.black_color, marginBottom: 8 }, font_style.font_medium,]}>Membership details</Text>
                    <WebView
                      style={{ flex: 1 }}
                      originWhitelist={['*']}
                      source={{
                        html: `<!doctype html><html lang="en"> <head> <meta charset="utf-8"> <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> <body style="font-size: 20px;">
                ${this.state.data.description}</body></html>`
                      }}
                    />
                  </View>
                )}
              </View>

              // </ScrollView>
            )
            : (
              <View style={{ flex: 1, height: Dimensions.get('screen').height }}>
                <View style={{ margin: 20, flex: 1 }}>
                  <View style={{ height: 150, backgroundColor: Colors.white_color, borderRadius: 5, padding: 20 }}>
                    <Text style={[{ fontSize: 16, marginBottom: 4 }, font_style.font_medium]}>Sessions</Text>
                    <Text style={[{ fontSize: 16, marginBottom: 20 }, font_style.font_Book]}>({this.state.data.title})</Text>
                    <View style={{ justifyContent: 'center', flexDirection: 'row', alignItems: 'center' }}>
                      <TouchableOpacity
                        onPress={() => { this._ManageSessions(this.state.quantity === 1 ? this.state.quantity : this.state.quantity - 1) }}
                        activeOpacity={1}
                        style={styles.icon_view}
                      >

                        <Image source={require('../assets/images/icons/decrease_3x.png')}
                          style={{ width: 18, height: 18, alignSelf: 'center' }} />
                      </TouchableOpacity>
                      <Text style={{ width: 60, textAlign: 'center' }}>{this.state.quantity}</Text>

                      <TouchableOpacity
                        onPress={() => { this._ManageSessions(this.state.quantity + 1) }}
                        activeOpacity={1}
                        style={styles.icon_view}
                      >
                        <Image source={require('../assets/images/icons/increase_3x.png')}
                          style={{ width: 18, height: 18, alignSelf: 'center' }} />

                      </TouchableOpacity>


                    </View>
                  </View>
                </View>

              </View>
            )}



          <View style={styles.bottom_layout}>
            <View style={{ flexDirection: 'row', height: 48 }}>
              <View style={{ flex: 1, backgroundColor: Colors.white_color, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={[{ color: Colors.primaryColor, fontSize: 14, }, font_style.font_medium]}>${this.state.data.price}</Text>
              </View>
              <TouchableOpacity
                onPress={() => { this._SelectPlan(this.state.data.plan_type) }}
                style={{ flex: 1, backgroundColor: Colors.primaryColor, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={[{ color: Colors.white_color, fontSize: 14, }, font_style.font_medium]}>Proceed</Text>
              </TouchableOpacity>
            </View>
          </View>
          <SafeAreaView />
        </View>
      )
    }

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },

  TouchableOpacityStyle: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 20,
    bottom: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  status: {

    borderBottomLeftRadius: 26 / 2,
    borderTopLeftRadius: 26 / 2,
    alignItems: 'flex-end',
    justifyContent: 'center',
    height: 26,
    position: 'absolute',
    right: 0,
    top: 20
  },
  status_txt: {
    color: '#fff',
    fontSize: 14,
    paddingEnd: 8,
    paddingStart: 8
  },
  bottom_layout: {
    justifyContent: 'flex-end', shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  }
})