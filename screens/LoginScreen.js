import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  AsyncStorage,
  Dimensions,
  TouchableHighlight,
  Keyboard,
  KeyboardAvoidingView,
  Platform
} from 'react-native';
import { TextInput, ScrollView } from 'react-native-gesture-handler';
import Constant from '../constants/Constant';
import ApiService from '../webservice/ApiService';
import { Icon } from 'react-native-elements';
import { textInput, buttonStyle, font_style } from '../components/styles';
import Colors from '../constants/Colors';
import { Expo, Notifications } from 'expo';
import Utils from '../constants/Utils';
import * as firebase from 'firebase';
import Spinner from 'react-native-loading-spinner-overlay';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import * as Facebook from 'expo-facebook';
import CustomProgressBar from '../components/CustomProgressBar';
import Constants from 'expo-constants';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';


const fbId = '360277767960071';


export default class LoginScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {
    username: '',
    password: '',
    visible_password: true,
    device_token: '',
    userId: '',
    userInfo: {},
    mapRegion: null,
    hasLocationPermissions: false,
    locationResult: null,
    disableBtn: false,
    fbdisableBtn: false,
    expoToken: '',
    notification: null,
    title: 'Hello World',
    body: 'Say something!',
    spinner: false
  }

  componentDidMount() {
    this._getLocationAsync()
    this.registerForPushNotifications()
  }

  async registerForPushNotifications() {
    const { status } = await Permissions.getAsync(Permissions.NOTIFICATIONS);


    if (status !== 'granted') {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      if (status !== 'granted') {
        return;
      }
    }

    const expoToken = await Notifications.getExpoPushTokenAsync();

    this.subscription = Notifications.addListener(this.handleNotification);

    this.setState({
      expoToken,
    });
    this.writeUserData(expoToken)
  }

  handleNotification = notification => {
    this.setState({
      notification,
    });
  };

  writeUserData(token) {
    firebase.database().ref('/users/').push({
      token: token,
    }).then((data) => {
      //success callback
    }).catch((error) => {
      //error callback
      console.log('error ', error)
    })
  }

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        locationResult: 'Permission to access location was denied',
      });
    } else {
      this.setState({ hasLocationPermissions: true });
    }

    if (this.state.hasLocationPermissions) {
      let location = await Location.getCurrentPositionAsync({});
      if (location !== null && location !== undefined) {
        // Center the map on the location we just fetched.
        this.setState({ locationResult: JSON.stringify(location), mapRegion: { latitude: location.coords.latitude, longitude: location.coords.longitude, latitudeDelta: 0.0922, longitudeDelta: 0.0421 } });
      }

    }

  };


  storeData = async (user_id) => {
    try {
      await AsyncStorage.setItem('id', user_id)
    } catch (e) {
      console.log('AsyncStorage Error: ' + error.message);
    }
  }

  _SignIn = () => {

    if (this.state.username) {
      if (this.state.password) {
        this.setState({ disableBtn: true })
        this.networkCall()
      } else {
        Utils.toastShow("Password required");
      }
    } else {
      Utils.toastShow("Username or Email required");
    }
  }

  _FbLogIn = async () => {
    console.log('facebook login')
    //Utils.toastShow('Facebook Login');

    try {
      await Facebook.initializeAsync(fbId);
      const {
        type,
        token
      } = await Facebook.logInWithReadPermissionsAsync(fbId, {
        permissions: ["public_profile", "email"],

      });
      if (type === "success") {
        //Utils.toastShow('Facebook loggin succesfully');
        // Get the user's name using Facebook's Graph API
        const response = await fetch(`https://graph.facebook.com/me?access_token=${token}&fields=id,name,picture.type(large),email`);
        const info = await response.json();

        if (info !== null && info.name !== null && info.email != null) {
          this.setState({ userInfo: info, token: token })

          let profile = info.picture != null && info.picture.data !== null && info.picture.data.url !== null ? info.picture.data.url : null

          const params = {
            email: info.email,
            name: info.name,
            image: profile,
            type: Constant.FACEBOOK,
            device_token: Constants.deviceId,
            latitude: this.state.mapRegion === null ? '' : this.state.mapRegion.latitude,
            longitude: this.state.mapRegion === null ? '' : this.state.mapRegion.longitude,
            gym_id: 8,
            fcm_token: this.state.expoToken
          }
          console.log('params', params)
          this.facebooUserNetworkCall(params);
          //this.props.navigation.navigate('Facebook', params)
        } else {
          this.props.navigation.navigate('SignUp', { userDetail: info })
        }

      } else {
        Utils.toastShow(Constant.message);
      }
    } catch (error) {
      console.log('facebook error', error)
    }


  }

  facebooUserNetworkCall = (params) => {
    this.setState({ spinner: true, fbdisableBtn: true })
    ApiService.api.post(ApiService.login, params)
      .then((response) => {
        this.setState({ spinner: false, fbdisableBtn: false })
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            AsyncStorage.setItem(Constant.LOGGEDIN, 'true');
            AsyncStorage.setItem(Constant.USERINFO, JSON.stringify(response.data.data));
            if (response.data.data !== undefined && response.data.data !== null && response.data.data.bmi_calculate !== undefined && response.data.data.bmi_calculate === false) {
              this.props.navigation.navigate('BMR', {

                from: 'login'
              })
            } else {
              this.props.navigation.navigate('Main')
            }

            // this.props.navigation.navigate('AddCard', {

            //   from: 'login'
            // })
          } else if (response.data.status === Constant.WARNING) {
            //alert(response.data.message)
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
              } else {
                this.props.navigation.navigate('Facebook', params)
              }
            } else {
              this.props.navigation.navigate('Facebook', params)
            }

          }
        }

      })
      .catch(function (error) {
        // this.setState({ fbdisableBtn: false })
        Utils.toastShow(Constant.message)
        Utils._SentryReport(error)
      })

  }


  networkCall = () => {
    this.setState({ spinner: true })
    const user = {
      device_token: Constants.deviceId,
      email: this.state.username,
      password: this.state.password,
      latitude: this.state.mapRegion === null ? '' : this.state.mapRegion.latitude,
      longitude: this.state.mapRegion === null ? '' : this.state.mapRegion.longitude,
      fcm_token: this.state.expoToken

    };

    ApiService.api.post(ApiService.login, user)
      .then((response) => {
        this.setState({ spinner: false })
        if (response !== null && response.data != null) {
          this.setState({ disableBtn: false })
          if (response.data.status === Constant.TRUE) {
            AsyncStorage.setItem(Constant.LOGGEDIN, 'true');
            Utils._FoodApiTokenGenerate()

            AsyncStorage.setItem(Constant.USERINFO, JSON.stringify(response.data.data))
              .then(() => {
                console.log('It was saved successfully')
              })
              .catch(() => {
                console.log('There was an error saving the product')
              })
            //AsyncStorage.setItem(Constant.USERINFO, response.data.data.toString());
            Utils.toastShow(response.data.message)


            if (response.data.data !== undefined && response.data.data !== null && response.data.data.bmi_calculate !== undefined && response.data.data.bmi_calculate === false) {
              this.props.navigation.navigate('AddCard', {

                from: 'login'
              })
            } else {
              this.props.navigation.navigate('Main')
            }

          } else if (response.data.status === Constant.FALSE) {
            this.setState({ spinner: false })
            Utils.toastShow(response.data.message)
          } else {
            this.setState({ spinner: false })
            Utils.toastShow(Constant.message)
          }
        } else {
          this.setState({ disableBtn: false })
          Utils.toastShow(Constant.message)
        }

      })
      .catch(function (error) {

        this.setState({ disableBtn: false, spinner: false })
        console.log("error " + error);
        Utils.toastShow(Constant.message)
        Utils._SentryReport(error)
      })
  }

  _RedirectToSignUp = () => {
    this.setState({ username: '', password: '' })
    this.props.navigation.navigate('SignUp')
  }

  _RedirectToForgotPassword = () => {
    this.setState({ username: '', password: '' })
    this.props.navigation.navigate('ForgotPassword')
  }

  render() {
    return (
      <ImageBackground source={require('../assets/images/bg.png')} style={{ width: '100%', height: '100%' }}
      >

        <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', }} behavior="padding" enabled keyboardVerticalOffset={0}>

          <CustomProgressBar spinner={this.state.spinner} />

          <ScrollView
            contentContainerStyle={{ flexGrow: 1, justifyContent: 'space-between', flexDirection: 'column' }}
            keyboardShouldPersistTaps='handled'
          >
            <View style={styles.container}>


              <View style={styles.main_container}>
                <View style={styles.iconstyle}>
                  <Image source={require('../assets/images/signin_logo.png')} style={{ width: 220, height: 120, marginBottom: 50 }} resizeMode="contain" />
                </View>
                <Text style={[styles.sign_in_txt, font_style.font_Book]}>{'Sign In'.toUpperCase()}</Text>
                <TextInput
                  style={[textInput.auth_textInput]}
                  onChangeText={(username) => this.setState({ username })}
                  value={this.state.username}
                  placeholder="Username or Email"
                  placeholderTextColor="#fff"
                  autoCapitalize='none'
                  autoCompleteType="off"
                />
                <View style={{ width: '100%', justifyContent: 'center', marginTop: 20 }}>
                  <TextInput
                    style={[textInput.auth_textInput,]}
                    onChangeText={(password) => this.setState({ password })}
                    value={this.state.password}
                    placeholder="Enter your password"
                    placeholderTextColor="#fff"
                    secureTextEntry={this.state.visible_password}
                    password={this.state.visible_password}
                    autoCompleteType="off"
                  />
                  <TouchableOpacity style={styles.view_password} onPress={() => this.setState({ visible_password: this.state.visible_password === false ? true : false })}  >
                    <Image source={this.state.visible_password === false ? require('../assets/images/icons/view_password_3x.png') : require('../assets/images/icons/eye.png')} style={[styles.image]} />
                  </TouchableOpacity>
                </View>
                <Text style={styles.forgot_pass}
                  onPress={() => { Keyboard.dismiss(), this._RedirectToForgotPassword() }}
                >Forgot Password?</Text>

                <TouchableOpacity
                  onPress={() => { Keyboard.dismiss(), this._SignIn() }}
                  style={buttonStyle.auth_btn}
                  underlayColor='gray'
                  activeOpacity={1}
                  disabled={this.state.disableBtn}
                >
                  <Text style={buttonStyle.loginText}>{'Sign In'.toUpperCase()}</Text>
                </TouchableOpacity>
                <Text style={[{ color: 'white', marginTop: 16, marginBottom: 8, fontSize: 12 }, font_style.font_medium]}>Or Sign Up</Text>
                <View style={{
                  height: 70,
                }}>
                  <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap', width: '100%' }}>
                    <TouchableOpacity
                      style={styles.facebookScreenButton}
                      underlayColor='#fff'
                      onPress={() => { Keyboard.dismiss(), this._FbLogIn() }}
                      activeOpacity={1}
                    //disabled={this.state.fbdisableBtn}
                    >
                      <View
                        style={{
                          alignSelf: 'center',
                          marginRight: 4,
                        }}>
                        <Icon
                          name='facebook'
                          type='font-awesome'
                          color='white'
                          size={14}
                        />
                      </View>

                      <Text style={[styles.loginText, { fontFamily: 'futura-medium', marginStart: 16 }]}>{'facebook'.toUpperCase()}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      activeOpacity={1}
                      style={styles.accountScreenButton}
                      onPress={() => { Keyboard.dismiss(), this._RedirectToSignUp() }}
                      underlayColor='#fff'>
                      <Text style={[styles.loginText, { fontFamily: 'futura-medium', fontWeight: 'bold', }]}>{'Create Account'.toUpperCase()}</Text>
                    </TouchableOpacity>

                  </View>
                </View>
              </View>

            </View>

          </ScrollView>


        </KeyboardAvoidingView>
      </ImageBackground >


    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    alignItems: 'center',
    padding: 24,

    flex: 1, justifyContent: 'flex-end'
  },
  iconstyle: {

    justifyContent: 'flex-end',
  },
  main_container: {
    //flex: 1,
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  loginScreenButton: {
    marginRight: 40,
    marginLeft: 40,
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: Colors.shades_blue,
    borderRadius: 4,
    width: '100%',
    height: 54,
    justifyContent: 'center',
  },

  facebookScreenButton: {
    marginTop: 10,
    padding: 10,
    backgroundColor: Colors.shades_blue,
    borderRadius: 50,
    height: 54,
    justifyContent: 'center',
    flex: 1,
    marginRight: 16,
    flexDirection: 'row'
  },

  accountScreenButton: {
    flex: 1,
    marginTop: 10,
    padding: 10,
    backgroundColor: Colors.shades_blue,
    borderRadius: 50,
    height: 54,
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginLeft: 16,
  },
  loginText: {
    color: '#fff',
    textAlign: 'center',
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: 12
  },
  forgot_pass: {
    color: 'white',
    marginTop: 16,
    alignSelf: 'flex-end',
    textDecorationLine: 'underline',
    marginBottom: 16,
    fontFamily: 'futura-book',
  },
  sign_in_txt: {
    color: 'white',
    marginTop: 10,
    marginBottom: 10,
    textAlign: 'center',
    fontSize: 25,
  },
  view_password: { position: 'absolute', right: 0, height: 48, width: 48, justifyContent: 'center', alignSelf: 'center', top: 13, alignItems: 'flex-end', paddingEnd: 16 },
  image: {
    width: 18,
    height: 14,
    tintColor: Colors.white_color
  },
});