import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  AsyncStorage,
  Dimensions,
  Keyboard,
  KeyboardAvoidingView
} from 'react-native';
import { TextInput, ScrollView } from 'react-native-gesture-handler';
import Constant from '../constants/Constant';
import ApiService from '../webservice/ApiService';
import { Icon } from 'react-native-elements';
import { textInput, buttonStyle, font_style } from '../components/styles';
import { Notifications } from 'expo';
import Constants from 'expo-constants';
import * as Location from 'expo-location';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';

import Utils from '../constants/Utils';
import * as firebase from 'firebase';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Spinner from 'react-native-loading-spinner-overlay';
import CustomProgressBar from '../components/CustomProgressBar';
import Colors from '../constants/Colors';

const fbId = '';

export default class SignUpScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {
    name: '',
    username: '',
    email: '',
    password: '',
    cpassword: '',
    visible_password: true,
    visible_cpassword: true,
    userId: '',
    userInfo: {},
    token: '',
    mapRegion: null,
    hasLocationPermissions: false,
    locationResult: null,
    disableBtn: false,
    expoToken: '',
    notification: null,
    title: 'Hello World',
    body: 'Say something!',
    spinner: false,
    scrollViewPosition: 0
  }

  componentDidMount() {
    this._getLocationAsync();
    this.registerForPushNotifications()
  }

  async registerForPushNotifications() {
    const { status } = await Permissions.getAsync(Permissions.NOTIFICATIONS);


    if (status !== 'granted') {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      if (status !== 'granted') {
        return;
      }
    }

    const expoToken = await Notifications.getExpoPushTokenAsync();

    this.subscription = Notifications.addListener(this.handleNotification);

    this.setState({
      expoToken,
    });
    this.writeUserData(expoToken)
  }

  handleNotification = notification => {
    this.setState({
      notification,
    });
  };

  writeUserData(token) {
    firebase.database().ref('/users/').push({
      token: token,
    }).then((data) => {
      //success callback
      console.log('data ', data)
    }).catch((error) => {
      //error callback
      console.log('error ', error)
    })
  }

  _handleMapRegionChange = mapRegion => {
    console.log(mapRegion);
    this.setState({ mapRegion });
  };


  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        locationResult: 'Permission to access location was denied',
      });
    } else {
      this.setState({ hasLocationPermissions: true });
    }

    if (this.state.hasLocationPermissions) {
      let location = await Location.getCurrentPositionAsync({});
      if (location !== null && location !== undefined) {
        // Center the map on the location we just fetched.
        this.setState({ locationResult: JSON.stringify(location), mapRegion: { latitude: location.coords.latitude, longitude: location.coords.longitude, latitudeDelta: 0.0922, longitudeDelta: 0.0421 } });
      }

    }
  };

  storeData = async (user_id) => {
    try {
      await AsyncStorage.setItem('id', user_id)
    } catch (e) {
      console.log('AsyncStorage Error: ' + error.message);
    }
  }

  _SignUp = () => {
    let msg = '';
    if (this.state.name !== null && this.state.name !== '') {
      if (this.state.username !== null && this.state.username !== '') {
        if (this.state.email !== null && this.state.email !== '') {
          if (this._emailValidate(this.state.email)) {
            if (this.state.password !== null && this.state.password !== '') {
              if (this.state.password.length >= 6) {
                if (this.state.cpassword !== null && this.state.cpassword !== '') {
                  if (this.state.password === this.state.cpassword) {

                    this.props.navigation.navigate('CompleteSignUp', {
                      device_token: Constants.deviceId,
                      name: this.state.name,
                      username: this.state.username,
                      email: this.state.email,
                      password: this.state.password,
                      latitude: this.state.mapRegion === null ? '' : this.state.mapRegion.latitude,
                      longitude: this.state.mapRegion === null ? '' : this.state.mapRegion.longitude,
                      gym_id: 8,
                      fcm_token: this.state.expoToken
                    })
                  } else {
                    msg = 'Password & confirm password should be same'
                    Utils.toastShow(msg)
                  }
                } else {
                  msg = 'Confirm password required'
                  Utils.toastShow(msg)
                }
              } else {
                msg = 'Password should be at least 6 characters'
                Utils.toastShow(msg)
              }

            } else {
              msg = 'Password required'
              Utils.toastShow(msg)
            }

          } else {
            msg = 'Email is not correct'
            Utils.toastShow(msg)
          }

        } else {
          msg = 'Email required'
          Utils.toastShow(msg)
        }
      } else {
        msg = 'Username required'
        Utils.toastShow(msg)
      }
    } else {
      msg = 'Name required'
      Utils.toastShow(msg)
    }

  }

  _emailValidate = (text) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      console.log("Email is Not Correct");

      this.setState({ email: text })
      return false;
    } else {
      this.setState({ email: text, error_msg: '' })
      console.log("Email is Correct");
      return true;
    }
  }



  render() {
    let viewHeight = 0
    return (
      <ImageBackground source={require('../assets/images/bg.png')} style={{ width: '100%', height: '100%' }}
      >

        <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', paddingBottom: 20, backgroundColor: 'transparent' }} behavior="padding" enabled keyboardVerticalOffset={0} >
          {/* <KeyboardAvoidingView behavior="padding" enabled> */}
          <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{
              flexGrow: 1, justifyContent: 'space-between',
              flexDirection: 'column',

            }}
            ref={ref => this.scrollView = ref}
            keyboardShouldPersistTaps='handled'
          >
            <CustomProgressBar spinner={this.state.spinner} />
            <View style={styles.main_container}>
              <Text style={[styles.sign_in_txt, font_style.font_Book]}>{'Sign Up'.toUpperCase()}</Text>
              <TextInput
                style={[textInput.auth_textInput, styles.top_margin]}
                onChangeText={(name) => this.setState({ name })}
                value={this.state.name}
                placeholder="Name"
                placeholderTextColor="#fff"
                autoCompleteType="off"
              />

              <TextInput
                style={[textInput.auth_textInput, styles.top_margin]}
                onChangeText={(username) => this.setState({ username })}
                value={this.state.username}
                placeholder="Username"
                placeholderTextColor="#fff"
                autoCapitalize='none'
                autoCompleteType="off"
              />
              <TextInput
                style={[textInput.auth_textInput, styles.top_margin]}
                onChangeText={(email) => this.setState({ email })}
                value={this.state.email}
                placeholder="Email"
                placeholderTextColor="#fff"
                autoCapitalize='none'
                autoCompleteType="off" />

              <View style={{ width: '100%', justifyContent: 'center' }}>
                <TextInput
                  style={[textInput.auth_textInput, styles.top_margin]}
                  onChangeText={(password) => this.setState({ password })}
                  value={this.state.password}
                  placeholder="Password"
                  placeholderTextColor="#fff"
                  secureTextEntry={this.state.visible_password}
                  password={this.state.visible_password}
                  autoCompleteType="off" />
                <TouchableOpacity style={styles.view_password} onPress={() => this.setState({ visible_password: this.state.visible_password === false ? true : false })}  >
                  <Image source={this.state.visible_password === false ? require('../assets/images/icons/view_password_3x.png') : require('../assets/images/icons/eye.png')} style={[styles.image]} />
                </TouchableOpacity>
              </View>

              <View style={{ width: '100%', justifyContent: 'center' }}>
                <TextInput
                  style={[textInput.auth_textInput, styles.top_margin]}
                  onChangeText={(cpassword) => this.setState({ cpassword })}
                  value={this.state.cpassword}
                  placeholder="Confirm Password"
                  placeholderTextColor="#fff"
                  secureTextEntry={this.state.visible_cpassword}
                  password={this.state.visible_cpassword}
                  autoCompleteType="off"
                />

                <TouchableOpacity style={styles.view_password} onPress={() => this.setState({ visible_cpassword: this.state.visible_cpassword === false ? true : false })}  >
                  <Image source={this.state.visible_cpassword === false ? require('../assets/images/icons/view_password_3x.png') : require('../assets/images/icons/eye.png')} style={[styles.image]} />
                </TouchableOpacity>

              </View>

              <TouchableOpacity
                activeOpacity={1}
                onPress={() => { Keyboard.dismiss(), this._SignUp() }}
                style={[buttonStyle.auth_btn, styles.top_margin]}
                underlayColor='#fff'
              >
                <Text style={buttonStyle.loginText}>{'Sign Up'.toUpperCase()}</Text>
              </TouchableOpacity>
              <Text style={{ color: 'white', marginTop: 16, marginBottom: 8, fontFamily: 'futura-medium', fontSize: 14 }}>Already have an account?</Text>
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => { Keyboard.dismiss(), this.props.navigation.navigate('Login') }}
                style={buttonStyle.login_btn}
                underlayColor='#fff'>
                <Text style={[buttonStyle.btnText]}>{'Sign In'.toUpperCase()}</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
          {/* </KeyboardAvoidingView> */}
        </KeyboardAvoidingView>
      </ImageBackground >


    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    flex: 1,
    flexDirection: 'column',
    //alignItems: 'center',
  },
  top_margin: {
    marginBottom: 10
  },
  iconstyle: {
    flex: 1,
    marginTop: 10,
    justifyContent: 'center',
  },
  main_container: {
    marginTop: 20,
    flex: 1,
    padding: 24,
    width: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },

  sign_in_txt: {
    color: 'white',
    marginTop: 10,
    marginBottom: 10,
    textAlign: 'center',
    fontSize: 25,
  },
  view_password: { position: 'absolute', right: 16 },
  image: {
    width: 18,
    height: 14,
    tintColor: Colors.white_color
  },
});