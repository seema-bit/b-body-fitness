import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage,
  KeyboardAvoidingView
} from 'react-native';
import { Icon } from 'react-native-elements'
import { TextInput, ScrollView } from 'react-native-gesture-handler';
import { textInput, buttonStyle, text_color, textHeader } from '../components/styles';
import Colors from '../constants/Colors';
import Constant from '../constants/Constant';
import HeaderLeft from '../components/HeaderLeft';
import HeaderRight from '../components/HeaderRight';
import HeaderProfileRight from '../components/HeaderProfileRight';

import PopupDialog, { DialogContent, DialogButton, DialogTitle } from 'react-native-popup-dialog';
import ModalSelector from 'react-native-modal-selector'
import ApiService from '../webservice/ApiService';
import Utils from '../constants/Utils';
import HeaderBackground from '../components/HeaderBackground';
import Spinner from 'react-native-loading-spinner-overlay';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CustomProgressBar from '../components/CustomProgressBar';
import HeaderBackLeft from '../components/HeaderBackLeft';

export default class EnqueryScreen extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.inquiry}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,

  });

  state = {
    userDetails: '',
    name: '',
    email: '',
    mobile: '',
    message: '',
    access_key: '',
    disableBtn: false,
    spinner: false
  }

  _ProfileUpdate = () => {

    // var params = new FormData();
    // params.append('name', this.state.name);
    // params.append('email', this.state.email);
    // params.append('mobile', this.state.mobile);
    // params.append('message', this.state.message);

    let msg = '';
    if (this.state.name !== null && this.state.name !== '') {
      if (this.state.email !== null && this.state.email !== '') {
        if (Utils._emailValidate(this.state.email)) {
          if (this.state.mobile !== null && this.state.mobile !== '') {
            if (this.state.mobile.length >= 10) {
              if (this.state.message !== null && this.state.message !== '') {
                this.setState({ disableBtn: true })
                this.networkCall()
              } else {
                msg = 'Message required'
                Utils.toastShow(msg)
              }
            } else {
              msg = 'Enter valid mobile number'
              Utils.toastShow(msg)
            }

          } else {
            msg = 'Mobile number required'
            Utils.toastShow(msg)
          }

        } else {
          msg = 'Email is not correct'
          Utils.toastShow(msg)
        }

      } else {
        msg = 'Email required'
        Utils.toastShow(msg)
      }
    } else {
      msg = 'Name required'
      Utils.toastShow(msg)
    }
  }

  networkCall = () => {
    this.setState({ spinner: true })
    const params = {
      name: this.state.name,
      email: this.state.email,
      mobile: this.state.mobile,
      message: this.state.message,
    }


    console.log('params : ' + JSON.stringify(params))

    ApiService.api.post(ApiService.contact_us, params)
      .then((response) => {
        console.log("response data : " + JSON.stringify(response.data))
        this.setState({ disableBtn: false, spinner: false })
        if (response !== null && response.data != null) {
          console.log(response.data.data)
          Utils.toastShow(response.data.message)
          if (response.data.status === Constant.SUCCESS) {

            this.setState({
              name: '',
              email: '',
              mobile: '',
              message: '',
            })
          } else if (response.data.status === Constant.WARNING) {
            this.setState({ disableBtn: false })
            Utils.toastShow(response.data.message)
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils._signOutAsync(this.props.navigation)
              }
            }

          }
        }

      })
      .catch(function (error) {
        Utils.toastShow(Constant.message)
        Utils._SentryReport(error)
        this.setState({ disableBtn: false, spinner: false })
        console.log("error " + error);
      })
  }


  render() {

    return (

      <KeyboardAwareScrollView enableAutomaticScroll enableOnAndroid
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={styles.container}
        scrollEnabled={true}
        keyboardShouldPersistTaps='handled' >
        <ScrollView style={styles.container}
          contentContainerStyle={{
            flexGrow: 1, justifyContent: 'space-between',
            flexDirection: 'column',


          }}>
          <CustomProgressBar spinner={this.state.spinner} />
          <View style={styles.container}>

            <View style={styles.bodyContent}>

              <Text style={styles.text_style}>Enquery</Text>

              <View style={styles.textInputMain}>
                <Text style={styles.inputStyle}>Enquery for Membership Plan</Text>
              </View>

              <Text style={styles.text_style}>Message</Text>


              <View style={[{

                borderWidth: 1,
                borderColor: Colors.light_gray,
                width: '100%',
                height: 46,
                padding: 16,
                marginTop: 6,
                borderRadius: 5, height: 100, justifyContent: 'flex-start'
              }]}>
                <TextInput
                  style={[styles.inputStyleNew]}
                  onChangeText={(message) => this.setState({ message })}
                  value={this.state.message}
                  placeholder="Type Message"
                  placeholderTextColor={Colors.dark_gray}
                  multiline={true}
                />
              </View>

              <TouchableOpacity
                disabled={this.state.disableBtn}
                onPress={() => this._ProfileUpdate()}
                style={[buttonStyle.login_btn, { marginBottom: 20 }]}
                underlayColor='gray'>
                <Text style={buttonStyle.btnText}>{'Submit'.toUpperCase()}</Text>
              </TouchableOpacity>

            </View>

          </View>
        </ScrollView>
      </KeyboardAwareScrollView>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,

  },
  header: {
    backgroundColor: Colors.bg_color,
    padding: 10,
    alignItems: 'center',
  },

  avatar: {
    width: 110,
    height: 110,
    borderRadius: 57,
    borderWidth: 2,
    borderColor: "white",
    marginBottom: 10,
  },
  name: {
    fontSize: 22,
    fontWeight: '600',
  },
  bodyContent: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 10,
  },

  bodyContent: {
    paddingStart: 24,
    paddingEnd: 24,
    flexDirection: 'row',
    flexWrap: 'wrap'

  },
  cross_icon_view: {
    width: 32,
    height: 32,
    position: 'absolute',
    backgroundColor: 'white',
    borderRadius: 50,
    backgroundColor: 'white',
    shadowOffset: { width: 1, height: 1, },
    shadowColor: 'black',
    shadowOpacity: .4,
    marginTop: 90,
    marginLeft: 60,
    left: 150,
    top: 0,
    zIndex: 10,
    alignSelf: 'center',
    justifyContent: 'center'
  },
  text_style: {
    marginTop: 16,
    color: Colors.txt_gray,
    paddingStart: 16,
    fontFamily: 'futura-medium',
    fontWeight: 'bold',

  },
  textInputMain: {
    flexDirection: 'row',
    borderRadius: 50,
    borderWidth: 1,
    borderColor: Colors.light_gray,
    width: '100%',
    height: 46,
    marginTop: 6,

  },
  inputStyle: {
    flex: 1,
    paddingStart: 16,
    fontFamily: 'futura-medium',
    alignSelf: 'center'

  },
  inputStyleNew: {
    fontSize: 16,
    fontFamily: 'futura-medium',
  },
  edit_icon_view: {
    width: 32,
    height: 32,
    borderRadius: 50,
    backgroundColor: 'white',
    shadowOffset: { width: 1, height: 1, },
    shadowColor: 'black',
    shadowOpacity: .4,
    alignSelf: 'center',
    marginRight: 10,
    justifyContent: 'center'
  },
});