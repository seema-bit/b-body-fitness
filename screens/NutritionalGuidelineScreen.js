import React from 'react';
import { ScrollView, StyleSheet, Image, Text, TouchableOpacity, AsyncStorage } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import HeaderLeft from '../components/HeaderLeft';
import HeaderRight from '../components/HeaderRight';
import Colors from '../constants/Colors';
import { View } from 'native-base';
import Constant from '../constants/Constant';
import { textHeader, font_style } from '../components/styles';
import HeaderBackground from '../components/HeaderBackground';
import { Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from 'native-base'
import ApiService from '../webservice/ApiService';
import HeaderBackLeft from '../components/HeaderBackLeft';
import Loader from '../components/Loader';
import Utils from '../constants/Utils';

export default class NutritionalGuidelineScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.calorie_goal}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,

  });

  state = {
    data: null
  }

  componentDidMount = () => {
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        var headers = {
          'X-Access-Token': json.access_key,
        }

        ApiService.api.get(ApiService.userBMI, { headers: headers })
          .then((response) => {
            this.setState({ data: [] })
            if (response !== null && response.data != null) {
              if (response.data.status === Constant.SUCCESS) {
                this.setState({
                  data: response.data.data != null ? response.data.data : [],
                })


              } else if (response.data.status === Constant.WARNING) {
                if (response.data.session !== undefined && response.data.session !== null) {
                  if (response.data.session === false) {
                    Utils.toastShow(response.data.message)
                    Utils._signOutAsync(this.props.navigation)
                  }
                }
              }
            }

          })
          .catch(function (error) {
            Utils._SentryReport(error)
            console.log("error " + JSON.stringify(error.response));
          })

      })
      .catch(error => console.log('error!'));
  }
  render() {

    if (this.state.data === undefined || this.state.data === null) {
      return (<Loader />)
    } else {
      return (
        <View style={styles.container}>
          <Text style={[styles.default_txt, font_style.font_medium]}>Default Goal</Text>
          <View style={styles.second_container}>
            <TouchableOpacity

              style={styles.row_view}
              activeOpacity={1}>
              <View style={styles.startView}>
                <Text style={[font_style.font_Book, styles.meta_txt]}>Calories</Text>

              </View>

              <Text style={[font_style.font_Book, styles.meta_txt, styles.endView]}>{this.state.data.calories_per_day}</Text>

            </TouchableOpacity>
            <TouchableOpacity
              style={styles.row_view}
              activeOpacity={1}>
              <View style={styles.startView}>
                <Text style={[font_style.font_Book, styles.meta_txt]}>Remaining Calories</Text>
              </View>

              <Text style={[font_style.font_Book, styles.meta_txt, styles.endView]}>{this.state.data.remanning}</Text>

            </TouchableOpacity>
            <TouchableOpacity
              style={styles.row_view}
              activeOpacity={1}>
              <View style={styles.startView}>
                <Text style={[font_style.font_Book, styles.meta_txt]}>Carbohydrates</Text>
                <Text style={[font_style.font_Book, styles.meta_txt, { marginStart: 8 }]}>{parseFloat(this.state.data.carbohydrates_gram).toFixed(0)}g</Text>

              </View>

              <Text style={[font_style.font_Book, styles.meta_txt, styles.endView]}>{this.state.data.carbohydrates_per}%</Text>

            </TouchableOpacity>


            <TouchableOpacity
              style={styles.row_view}
              activeOpacity={1}>
              <View style={styles.startView}>
                <Text style={[font_style.font_Book, styles.meta_txt]}>Protein</Text>
                <Text style={[font_style.font_Book, styles.meta_txt, { marginStart: 8 }]}>{parseFloat(this.state.data.protein_gram).toFixed(0)}g</Text>
              </View>

              <Text style={[font_style.font_Book, styles.meta_txt, styles.endView]}>{this.state.data.protein_per}%</Text>

            </TouchableOpacity>
            <TouchableOpacity
              style={styles.row_view}
              activeOpacity={1}>
              <View style={styles.startView}>
                <Text style={[font_style.font_Book, styles.meta_txt]}>Fat</Text>
                <Text style={[font_style.font_Book, styles.meta_txt, { marginStart: 8 }]}>{parseFloat(this.state.data.fat_gram).toFixed(0)}g</Text>

              </View>

              <Text style={[font_style.font_Book, styles.meta_txt, styles.endView]}>{this.state.data.fat_per}%</Text>

            </TouchableOpacity>
          </View>

        </View>
      );
    }

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },
  second_container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  meta_txt: {
    fontSize: 14,
    color: Colors.dark_gray
  },
  default_txt: {
    color: Colors.dark_gray,
    marginRight: 20,
    marginLeft: 20,
    marginTop: 16,
    marginBottom: 16,
    fontSize: 14
  },
  row_view: {
    flexDirection: 'row',
    paddingTop: 16,
    paddingBottom: 16,
    borderBottomColor: Colors.light_gray,
    borderBottomWidth: 1,
  },
  startView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginLeft: 20
  },
  endView: {
    alignItems: 'flex-end',
    justifyContent: 'center',
    color: Colors.primaryColor,
    marginRight: 20,
  }
});
