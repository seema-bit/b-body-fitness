import React from 'react';
import { ScrollView, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import HeaderLeft from '../components/HeaderLeft';
import HeaderRight from '../components/HeaderRight';
import Colors from '../constants/Colors';
import { View } from 'native-base';
import Constant from '../constants/Constant';
import { textHeader, font_style } from '../components/styles';
import HeaderBackground from '../components/HeaderBackground';
import { Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from 'native-base'
import ApiService from '../webservice/ApiService';
import Utils from '../constants/Utils';

export default class FAQScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.faq}`.toUpperCase()}</Text>,
    headerLeft: <HeaderLeft navigationProps={navigation} />,
    //headerRight: <HeaderRight navigationProps={navigation} />,
  });

  state = {
    data: []
  }

  componentDidMount = () => {
    ApiService.api.get(ApiService.faq)
      .then((response) => {
        this.setState({ disableBtn: false })
        if (response !== null && response.data != null) {

          if (response.data.status === Constant.SUCCESS) {
            this.setState({ data: response.data.data })

          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        //Utils.toastShow(Constant.message)
        Utils._SentryReport(error)
        console.log("error " + error);
      })
  }

  render() {
    return (
      <View style={styles.container}>
        {this.state.data.map((item, index) => {
          return (
            <TouchableOpacity style={{
              flexDirection: 'row', marginRight: 20, marginLeft: 20, paddingTop: 16, paddingBottom: 16, borderBottomColor: Colors.light_gray,
              borderBottomWidth: 1,
            }}
              activeOpacity={1}
              onPress={() => {
                this.props.navigation.navigate("FAQAnswer",
                  {
                    data: item
                  }
                )
              }}>
              <Text style={[font_style.font_Book, styles.meta_txt, { flex: 1, justifyContent: 'center' }]}>{item.question}</Text>
              <Image source={require('../assets/images/icons/right_arrow_3x.png')} style={{ alignItems: 'flex-end', justifyContent: 'center' }} />
            </TouchableOpacity>
          )
        })}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },
  meta_txt: {
    fontSize: 16,
    color: Colors.dark_gray
  }
});
