import React, { Component } from 'react'
import { ScrollView, StyleSheet, Image, Text, Dimensions, TouchableOpacity, ImageBackground, AsyncStorage, Platform, Linking } from 'react-native';
import { View } from 'native-base';
import HeaderLeft from '../components/HeaderLeft';
import Constant from '../constants/Constant';
import Colors from '../constants/Colors';
import Carousel from 'react-native-snap-carousel';
import { textHeader, font_style } from '../components/styles';
import { FlatGrid } from 'react-native-super-grid';
import HeaderBackground from '../components/HeaderBackground';
import HeaderRight from '../components/HeaderRight';
import HeaderBackLeft from '../components/HeaderBackLeft';
import Loader from '../components/Loader';
import { SafeAreaView } from 'react-navigation';
import { LinearGradient } from 'expo-linear-gradient'
import { Ionicons } from '@expo/vector-icons';
import Utils from '../constants/Utils';
import ApiService from '../webservice/ApiService';
import DataNotFound from '../components/DataNotFound';
import moment from 'moment'
import { WebView } from 'react-native-webview';
import CustomProgressBar from '../components/CustomProgressBar';

const horizontalMargin = 10;
const sliderWidth = Dimensions.get('window').width;
const slideWidth = sliderWidth - 80;
const itemWidth = slideWidth + horizontalMargin * 2;
const itemHeight = 200;
const arrow_down = require('../assets/images/icons/arrow_down_3x.png')

var script = `<script>
          setTimeout(function () {
            window.ReactNativeWebView.postMessage(document.documentElement.scrollHeight)
          }, 500)
        </script>`;
const style = `<style>
#arrowDown {
    position:fixed;
    bottom: 0;
    left: 46%;
    font-size: 30px;
    line-height: 1.1;
    color: #0000FF;
}

#scrollLoc{
  overflow: hidden;
   position: fixed;
    bottom: 0px;
    right: 0;
}
</style>`

const scrollScript = `
var fullHeight = document.documentElement.scrollHeight;
var height = window.pageYOffset;
var lastScroll = 0;

window.onscroll=function(){

  var height = window.pageYOffset;
  height = height+250
  var x = document.getElementById("arrowDown");
  if (fullHeight < height) {
        x.style.display = "none";
    } else {
x.style.display = "block";
        if(height>300){
          x.style.opacity = "0.4";
        }else{
          x.style.opacity = "1";
        }

        let currentScroll = document.documentElement.scrollTop || document.body.scrollTop; // Get Current Scroll Value

      if (currentScroll > 0 && lastScroll <= currentScroll){
        lastScroll = currentScroll;
        x.style.transform = "rotate(0deg)";
      }else{
        lastScroll = currentScroll;
        x.style.transform = "rotate(180deg)";
        if(currentScroll <10){
x.style.transform = "rotate(0deg)";
      }
      }


  }

};true;
`

const scrollTopDown = ``


export default class MembershipPlanScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.membership_plan}`.toUpperCase()}</Text>,
    headerLeft: <HeaderLeft navigationProps={navigation} />,
    //   <TouchableOpacity
    //   onPress={() => { navigation.navigate("Feed") }}
    //   style={textHeader.leftIcon}>
    //   <Image source={require('./../assets/images/icons/arrow-right_3x.png')}
    //     resizeMode="contain" style={{ width: 20, height: 20, alignSelf: 'flex-start' }} />
    // </TouchableOpacity>,
    headerRight: <TouchableOpacity
      onPress={() => navigation.navigate('PlanHistory')}
      style={textHeader.rightIcon}>
      <Image source={require('./../assets/images/icons/plan_history1.png')}
        resizeMode="contain" style={{ width: 16, height: 20, tintColor: Colors.white_color, alignSelf: 'flex-end' }} />
    </TouchableOpacity>

  });

  constructor(props) {
    super(props);
    this.state = {
      entries: null,
      selectedTab: 0,
      package_price: 0,
      selectedIndex: 0,
      quantity: 1,
      total_price: 30,
      spinner: false,
      sessionData: null,
      sub_title: '',
      webheight: 100,
      showLoadMore: true
    }
  }

  componentDidMount() {
    _this = this
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        this._FetchAllPlans()
        this._FetchSessionPrice()



      })
      .catch(error => console.log('error!'));
  }

  _FetchAllPlans = () => {

    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }
    ApiService.api.get(ApiService.getAllPlans, { headers: headers })
      .then((response) => {
        this.setState({ entries: [] })
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            this.setState({
              entries: response.data.data != null ? response.data.data : [],
              package_price: response.data.data[0].price,
              sub_title: response.data.data[0].sub_title
            })


          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error _FetchAllPlans" + JSON.stringify(error.response));
      })
  }

  _FetchSessionPrice = () => {

    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }
    ApiService.api.get(ApiService.sessionPrice, { headers: headers })
      .then((response) => {
        this.setState({ sessionData: '' })
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            this.setState({
              sessionData: response.data.data != null ? response.data.data : {},
            })


          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error _FetchSessionPrice" + JSON.stringify(error.response));
      })
  }

  setTab(tab) {
    this.setState({ selectedTab: tab });
  }

  _PickPackage = (item, index) => {
  }

  _SelectPlan = (type) => {
    if (type === 1) {
      Linking.openURL(this.state.entries[this.state.selectedIndex].link);

    } else {
      this.props.navigation.navigate('ContactUs', {
        content: 'Inquiry for membership plan'
      })
    }

    // var params = ''
    // let current_date = moment().format('MM-DD-YYYY')
    // this.setState({ spinner: true })
    // if (type === 1) {
    //   console.log(this.state.selectedIndex)
    //   const item = this.state.entries[this.state.selectedIndex]
    //   params = {
    //     plan_id: item.id,
    //     current_date: current_date,
    //     type: type
    //   }
    // } else {
    //   params = {
    //     current_date: current_date,
    //     type: type,
    //     price: this.state.total_price,
    //     quantity: this.state.quantity,
    //     title: this.state.sessionData.title,
    //     description: this.state.sessionData.description
    //   }
    // }

    // AsyncStorage.getItem(Constant.USERINFO)
    //   .then(req => JSON.parse(req))
    //   .then(json => {
    //     console.log(json.access_key)
    //     this.setState({ userInfo: json })
    //     var headers = {
    //       'X-Access-Token': json.access_key,
    //     }
    //     ApiService.api.post(ApiService.joinPlan, params, { headers: headers })
    //       .then((response) => {
    //         this.setState({ spinner: false })
    //         console.log("join plans : " + JSON.stringify(response.data))
    //         Utils.toastShow(response.data.message)
    //         if (response !== null && response.data != null) {
    //           if (response.data.status === Constant.SUCCESS) {



    //           } else if (response.data.status === Constant.WARNING) {
    //             if (response.data.session !== undefined && response.data.session !== null) {
    //               if (response.data.session === false) {
    //                 Utils.toastShow(response.data.message)
    //                 Utils._signOutAsync(this.props.navigation)
    //               }
    //               // } else {
    //               //   if (!this.state.spinner) {
    //               //     alert(response.data.message)
    //               //   } else {
    //               //     Utils.toastShow(response.data.message)
    //               //   }

    //             }
    //           }
    //         }
    //       })
    //       .catch(function (error) {
    //         this.setState({ spinner: false })
    //         console.log("error " + JSON.stringify(error.response));
    //       })

    //   })
    //   .catch(error => console.log('error!' + JSON.stringify(error.response)));
  }

  _setItemHeight = (height) => {

    let newArray = []
    this.state.entries.forEach(element => {
      element.layoutHeight = height
      element.showLoadMore = true
      newArray = [...newArray, element]

    });

    this.setState({ entries: newArray })

  }


  _setWebHeight = (height, index) => {

    let newArray = [...this.state.entries]

    newArray[index].webviewHeight = height

    this.setState({ entries: newArray })

  }


  _setButtonVisibility = (index) => {
    let newArray = [...this.state.entries]

    newArray[index].showLoadMore = false

    this.setState({ entries: newArray })
  }

  _ManageSessions(quantity) {
    this.setState({ quantity: quantity, total_price: quantity * 30 })
  }

  _Packages = () => {
    if (this.state.entries === undefined || this.state.entries === null) {
      return (<Loader />)
    } else {
      if (this.state.entries.length === 0) {
        return (<DataNotFound />)
      } else {
        return (
          <View style={{ flex: 1 }}>
            <Carousel
              data={this.state.entries}
              renderItem={({ item, index }) => {

                let layoutHeight = 0;
                let webviewHeight = 0;

                const runFirst = `<script>
                  function scrollWin() {
                    window.scrollTo(0, 500);
                    alart()
                }
                </script>`


                return (
                  <View key={index} style={[styles.carousal_style, { alignItems: 'center' }]}>

                    <View
                      style={{
                        width: '100%',
                        height: '90%',

                      }}>
                      <ImageBackground source={require('../assets/images/membership_bg.png')}
                        style={{ width: '100%', height: 150, borderRadius: 10, alignItems: 'center', justifyContent: 'center' }}
                        imageStyle={{ borderRadius: 10 }}>

                        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'flex-end' }}>


                          <Text style={[{ color: Colors.white_color, fontSize: 35, }, font_style.font_bold]}>${item.price}</Text>
                          {item.sub_title !== undefined && item.sub_title !== null && (
                            <View style={{ alignSelf: 'flex-end' }}>
                              <Text style={[{ color: Colors.white_color, fontSize: 18, }, font_style.font_bold]}>{item.sub_title}</Text>
                            </View>)}


                        </View>
                        <Text style={[{ color: Colors.white_color, fontSize: 14, marginTop: 16, marginBottom: 8, marginStart: 24, marginEnd: 24, textAlign: 'center' }, font_style.font_bold,]}>{item.title}</Text>
                        {item.value_range !== undefined && item.value_range !== null && item.value_range !== '' && item.value_range !== '0' && <Text style={[{ color: Colors.white_color, fontSize: 14, marginTop: 24 }, font_style.font_medium,]}>${item.value_range} Per Sessions</Text>}
                        {item.quantity !== undefined && item.quantity !== null && item.quantity !== '' && item.quantity !== "0" &&
                          < Text style={[{ color: Colors.white_color, fontSize: 14, marginTop: 8 }, font_style.font_medium,]}>Session Quantity : {item.quantity === '-1' ? 'Unlimited' : item.quantity}</Text>
                        }
                      </ImageBackground>

                      <View style={styles.membership}>
                        <Text style={[{ fontSize: 14, color: Colors.black_color, marginBottom: 8 }, font_style.font_medium,]}>Membership details</Text>

                        <View
                          style={[{ flex: 1, },]}
                          onLayout={(event) => {
                            var { x, y, width, height } = event.nativeEvent.layout;
                            //alert(event.nativeEvent.layout.height)

                            layoutHeight = parseInt(event.nativeEvent.layout.height)
                            this._setItemHeight(layoutHeight)

                          }} >
                          <WebView
                            showsVerticalScrollIndicator={item.showLoadMore !== undefined && item.showLoadMore !== false ? false : true}
                            style={{ flex: 1, }}

                            //bounces={false}
                            scrollEnabled={item.showLoadMore !== undefined && item.showLoadMore !== false ? false : true}
                            injectedJavaScript={scrollScript}
                            onMessage={event => {
                              //alert(event.nativeEvent.data);
                              webviewHeight = parseInt(event.nativeEvent.data)
                              this._setWebHeight(webviewHeight, index)
                              //this.setState({ webheight: parseInt(event.nativeEvent.data) });


                            }}
                            source={{
                              html: `<!doctype html><html lang="en"> <head> <meta charset="utf-8"> <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no"></head> <body style="padding:0;
    margin:0;${item.showLoadMore !== undefined && item.showLoadMore !== false && 'overflow: hidden'}">
                ${item.description}

                 <span id="arrowDown" style="${item.showLoadMore !== undefined && item.showLoadMore !== false && 'display : none'}">&#8964;</span>

${style}
               ${script}
               ${scrollTopDown}

                </body>

</html>` }}
                          />

                          {item.layoutHeight < item.webviewHeight &&

                            <View>
                              {item.showLoadMore !== undefined && item.showLoadMore !== false &&
                                <TouchableOpacity
                                  onPress={() => this._setButtonVisibility(index)}>

                                  <Text style={[{ fontSize: 14, color: Colors.black_color, marginTop: 8, textAlign: 'center', justifyContent: 'flex-end', }, font_style.font_medium,]}>View More</Text>

                                </TouchableOpacity>
                              }

                            </View>
                          }


                        </View>


                      </View>
                    </View>
                  </View>
                );
              }}
              sliderWidth={sliderWidth}
              itemWidth={itemWidth}
              inactiveSlideOpacity={1}
              inactiveSlideScale={1}
              firstItem={this.state.selectedIndex}
              onSnapToItem={slideIndex => this.setState({ selectedIndex: slideIndex, package_price: this.state.entries[slideIndex].price, sub_title: this.state.entries[slideIndex].sub_title })}
            />
            <View style={{ justifyContent: 'flex-end' }}>
              <View style={{ flexDirection: 'row', height: 48 }}>
                <View style={{ flex: 1, backgroundColor: Colors.white_color, justifyContent: 'center', paddingStart: 16, alignItems: 'center' }}>
                  <Text style={[{ color: Colors.primaryColor, fontSize: 14, }, font_style.font_medium]}>${this.state.package_price}</Text>
                  <Text style={[{ fontSize: 14, }, font_style.font_medium]}>For {this.state.sub_title === 'Unlimited' ? '1 Month' : this.state.sub_title}</Text>
                </View>
                <TouchableOpacity
                  onPress={() => { this._SelectPlan(1) }}
                  style={{ flex: 1, backgroundColor: Colors.primaryColor, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                  <Text style={[{ color: Colors.white_color, fontSize: 14, }, font_style.font_medium]}>Purchase Plan</Text>
                  <Image source={require('../assets/images/icons/proceed.png')} style={{ width: 20, height: 16, marginStart: 2 }} resizeMode="contain" ></Image>
                </TouchableOpacity>
              </View>
            </View>

          </View>
        )
      }

    }

  }

  _Classess = () => {
    if (this.state.sessionData === undefined || this.state.sessionData === null) {
      return (<Loader />)
    } else {
      return (
        <View style={{ flex: 1, height: Dimensions.get('screen').height }}>
          <View style={{ margin: 20, flex: 1 }}>
            <View style={{ backgroundColor: Colors.white_color, borderRadius: 5, padding: 20 }}>
              <Text style={[{ fontSize: 16, marginBottom: 4 }, font_style.font_medium]}>Sessions(A la Carte)</Text>
              <Text style={[{ fontSize: 16, marginBottom: 20 }, font_style.font_Book]}>({this.state.sessionData.title})</Text>
              <Text style={[{ fontSize: 16, marginBottom: 20 }, font_style.font_Book]}>Total : {this.state.quantity} * $30 = ${this.state.total_price}</Text>
              <View style={{ justifyContent: 'center', flexDirection: 'row', alignItems: 'center' }}>
                <TouchableOpacity
                  onPress={() => { this._ManageSessions(this.state.quantity === 1 ? this.state.quantity : this.state.quantity - 1) }}
                  activeOpacity={1}
                  style={styles.icon_view}
                >

                  <Image source={require('../assets/images/icons/decrease_3x.png')}
                    style={{ width: 18, height: 18, alignSelf: 'center' }} />
                </TouchableOpacity>
                <Text style={{ width: 60, textAlign: 'center' }}>{this.state.quantity}</Text>

                <TouchableOpacity
                  onPress={() => { this._ManageSessions(this.state.quantity + 1) }}
                  activeOpacity={1}
                  style={styles.icon_view}
                >
                  <Image source={require('../assets/images/icons/increase_3x.png')}
                    style={{ width: 18, height: 18, alignSelf: 'center' }} />

                </TouchableOpacity>


              </View>
            </View>
          </View>


          <View style={{ justifyContent: 'flex-end' }}>
            <View style={{ flexDirection: 'row', height: 48 }}>
              <View style={{ flex: 1, backgroundColor: Colors.white_color, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={[{ color: Colors.primaryColor, fontSize: 14, }, font_style.font_medium]}>Total : ${this.state.total_price}</Text>
              </View>
              <TouchableOpacity
                onPress={() => { this._SelectPlan(2) }}
                style={{ flex: 1, backgroundColor: Colors.primaryColor, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                <Text style={[{ color: Colors.white_color, fontSize: 14, }, font_style.font_medium]}>Inquiry</Text>
                <Image source={require('../assets/images/icons/proceed.png')} style={{ width: 20, height: 16, marginStart: 2 }} resizeMode="contain" ></Image>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      )
    }

  }

  render() {
    return (
      <View style={styles.container}>
        <CustomProgressBar spinner={this.state.spinner} />
        <View style={{ flex: 1 }}>
          <View style={{
            flexDirection: 'row', height: 46,
            borderBottomColor: Colors.light_gray,
            borderBottomWidth: 1,
            backgroundColor: '#fff'
          }}>

            <TouchableOpacity onPress={() => this.setTab(0)} style={{ width: '49%', alignItems: 'center', justifyContent: 'center', }}>

              <View style={{ alignSelf: 'center', flexDirection: 'row', }}>
                <Text style={[this.state.selectedTab === 0 ? { color: Colors.primaryColor } : { color: Colors.dark_gray }, { fontSize: 19, }]}>Packages</Text>
              </View>

            </TouchableOpacity>
            <View style={{
              borderLeftColor: Colors.light_gray,
              borderLeftWidth: 1,
              marginTop: 6,
              marginBottom: 6
            }}>
            </View>
            <TouchableOpacity onPress={() => this.setTab(1)} style={{ width: '49%', alignItems: 'center', justifyContent: 'center', }}>

              <View style={{ alignSelf: 'center', flexDirection: 'row', }}>
                <Text style={[this.state.selectedTab === 1 ? { color: Colors.primaryColor } : { color: Colors.dark_gray }, { fontSize: 19, }]}>Classes</Text>
              </View>

            </TouchableOpacity>
          </View>

          {this.state.selectedTab === 0
            ?
            (this._Packages())
            :
            (this._Classess())
          }
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => { this.props.navigation.navigate('Discounts') }}
            style={styles.TouchableOpacityStyle}>
            <View

              style={{
                width: 50,
                height: 50,
                borderRadius: 50 / 2,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: Colors.primaryColor
                //marginBottom: 100
              }}>
              <Image source={require('../assets/images/icons/tag.png')}
                style={{ width: 24, height: 24, alignSelf: 'center', tintColor: Colors.white_color }} resizeMode="contain" />

            </View>
          </TouchableOpacity>


        </View>


        <SafeAreaView />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },
  carousal_style: {
    flex: 1,
    margin: 5,

  },
  triangle: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: 16,
    borderRightWidth: 16,
    borderBottomWidth: 16,
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: '#fff',

  },
  triangleDown: {
    transform: [
      { rotate: '180deg' }
    ]
  },
  gridView: {
    marginTop: 10,
    marginBottom: 10,
    flex: 1,
  },
  img_view: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2
  },
  TouchableOpacityStyle: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 20,
    bottom: 60,
  },
  icon_view: {
    width: 32, height: 32,
    justifyContent: 'center',
  },
  membership: {
    padding: 8, flex: 1, borderRadius: 5,
    paddingBottom: 8,
    backgroundColor: Colors.white_color, margin: 8, marginStart: 12, marginEnd: 12,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  }
})