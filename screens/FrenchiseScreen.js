import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage,
  KeyboardAvoidingView,
  Dimensions,
  TextInput,
  Platform,
  Keyboard,
} from 'react-native';
import { Icon } from 'react-native-elements'
import { ScrollView } from 'react-native-gesture-handler';
import { textInput, buttonStyle, text_color, textHeader, font_style } from '../components/styles';
import Colors from '../constants/Colors';
import Constant from '../constants/Constant';
import HeaderLeft from '../components/HeaderLeft';
import HeaderRight from '../components/HeaderRight';
import HeaderProfileRight from '../components/HeaderProfileRight';

import { Asset } from 'expo-asset';
import * as ImageManipulator from 'expo-image-manipulator';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';

import PopupDialog, { DialogContent, DialogButton, DialogTitle } from 'react-native-popup-dialog';
import ModalSelector from 'react-native-modal-selector'
import ApiService from '../webservice/ApiService';
import Utils from '../constants/Utils';
import HeaderBackground from '../components/HeaderBackground';
import Spinner from 'react-native-loading-spinner-overlay';
import HeaderBackLeft from '../components/HeaderBackLeft';
import { Dropdown } from 'react-native-material-dropdown';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CustomProgressBar from '../components/CustomProgressBar';
import { SafeAreaView } from 'react-navigation';
import * as WebBrowser from 'expo-web-browser';
import ModalDropdown from 'react-native-modal-dropdown';

export default class FrenchiseScreen extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.franchise}`.toUpperCase()}</Text>,
    headerLeft: <HeaderLeft navigationProps={navigation} />,
    // headerLeft: <TouchableOpacity
    //   onPress={() => { _this.props.navigation.navigate('NotificationSetting') }}
    //   style={textHeader.leftIcon}>
    //   <Image source={require('./../assets/images/icons/arrow-right_3x.png')} style={{ width: 11, height: 20, alignSelf: 'flex-start' }} />
    // </TouchableOpacity>,

  });

  state = {
    userInfo: '',
    access_key: '',
    disableBtn: false,
    spinner: false,
    gym_data: [],
    state_value: [
      'Alabama',
      'Alaska',
      'Arizona',
      'Arkansas',
      'California',
      'Colorado',
      'Connecticut',
      'Delaware',
      'Florida',
      'Georgia',
      'Hawaii',
      'Idaho',
      'Illinois',
      'Indiana',
      'Iowa',
      'Kansas',
      'Kentucky',
      'Louisiana',
      'Maine',
      'Maryland',
      'Massachusetts',
      'Michigan',
      'Minnesota',
      'Mississippi',
      'Missouri',
      'Montana',
      'Nebraska',
      'Nevada',
      'New Hampshire',
      'New Jersey',
      'New Mexico',
      'New York',
      'North Carolina',
      'North Dakota', 'Ohio',
      'Oklahoma',
      'Oregon',
      'Pennsylvania',
      'Rhode Island',
      'South Carolina',
      'South Dakota',
      'Texas',
      'Utah',
      'Vermont',
      'Virginia',
      'Washington',
      'West Virginia',
      'Wisconsin',
      'Wyoming'
    ],
    time_call: [
      'Morning',
      'Afternoon',
      'Evening'
    ],
    net_worth: [
      'Under $100,000',
      '$100,000 - $250,000',
      '$250,000 - $500,000',
      '$500,000 - $1,000,000',
      'Over $1,000,000'
    ],
    net_worth: [
      'Under $100,000',
      '$100,000 - $250,000',
      '$250,000 - $500,000',
      '$500,000 - $1,000,000',
      'Over $1,000,000'
    ],
    cash_available: [
      'Under $150,000',
      '$150,000 - $250,000',
      '$250,000 - $500,000',
      '$500,000 - $1,000,000',
      'Over $1,000,000'
    ],
    know_about: [
      'Advertisment',
      'Drive By / Signage',
      'Internet',
      'Internet Search',
      'News Story',
      'Other'
    ],
    full_name: '',
    address: '',
    email: '',
    country: '',
    state: '',
    city: '',
    zipcode: '',
    time_to_call: '',
    current_net_worth: '',
    cash_available_for_investment: '',
    here_about_us: '',
    website: '',
    location: '',
    linkedin: '',
    comment: '',
    phone: '',
    gym: '',
    upload_file: null,
    result: null,
    attachment: '',
    scrollViewPosition: 0,
    scrollView_pos: 0

  }

  componentDidMount = () => {
    this._FetchData()
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })


      })
      .catch(error => console.log('error!', error));

    this.setState({
      state: this.state.state_value[0].value,
      time_to_call: this.state.time_call[0].value,
      current_net_worth: this.state.net_worth[0].value,
      cash_available_for_investment: this.state.cash_available[0].value,
      here_about_us: this.state.know_about[0].value,
    })
  }
  _FetchData = () => {
    ApiService.api.get(ApiService.fetchGyms)
      .then((response) => {
        if (response !== null && response.status != null) {
          if (response.data.status === Constant.SUCCESS) {

            response.data.data.forEach(element => {
              let gymData = {
                id: element.id,
                name: element.name,
                admin_id: element.admin_id,
                gym_name: element.gym_name,
                value: element.gym_name,
              }

              this.setState({ gym_data: [...this.state.gym_data, gymData] })
            });

          } else if (response.data.status === Constant.FALSE) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })
  }

  _ProfileUpdate = () => {

    let msg = '';
    if (this.state.full_name !== null && this.state.full_name.trim() !== '') {
      if (this.state.email !== null && this.state.email.trim() !== '') {
        if (Utils._emailValidate(this.state.email.trim())) {
          if (this.state.phone !== null && this.state.phone.trim() !== '') {
            if (this.state.phone.length >= 10) {
              if (this.state.address !== null && this.state.address.trim() !== '') {
                if (this.state.city !== null && this.state.city.trim() !== '') {
                  if (this.state.website !== null && this.state.website.trim() !== '') {
                    if (Utils.is_url(this.state.website.trim())) {
                      this._Validation()
                    } else {
                      msg = 'Enter valid website link'
                      Utils.toastShow(msg)
                    }
                  } else {
                    this._Validation()

                  }

                } else {
                  msg = 'City required'
                  Utils.toastShow(msg)
                }
              } else {
                msg = 'Address required'
                Utils.toastShow(msg)
              }

            } else {
              msg = 'Enter valid phone number'
              Utils.toastShow(msg)
            }

          } else {
            msg = 'Phone number required'
            Utils.toastShow(msg)
          }

        } else {
          msg = 'Email is not correct'
          Utils.toastShow(msg)
        }

      } else {
        msg = 'Email required'
        Utils.toastShow(msg)
      }
    } else {
      msg = 'Name required'
      Utils.toastShow(msg)
    }
  }

  _Validation = () => {
    if (this.state.linkedin !== null && this.state.linkedin.trim() !== '') {
      if (Utils.is_url(this.state.linkedin.trim())) {
        if (this.state.comment !== null && this.state.comment !== '') {
          this.setState({ disableBtn: true })
          this.networkCall()
        } else {
          msg = 'Comment required'
          Utils.toastShow(msg)
        }
      } else {
        msg = 'Enter valid linkedin profile'
        Utils.toastShow(msg)
      }
    } else {
      if (this.state.comment !== null && this.state.comment !== '') {
        this.setState({ disableBtn: true })
        this.networkCall()
      } else {
        msg = 'Comment required'
        Utils.toastShow(msg)
      }
    }
  }
  networkCall = () => {

    const image_name = Utils.handleClick()
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }
    this.setState({ spinner: true })

    var params = new FormData();
    params.append('full_name', this.state.full_name);
    params.append('address', this.state.address);
    params.append('email', this.state.email);
    params.append('state', this.state.state);
    params.append('country', this.state.country);
    params.append('city', this.state.city);
    params.append('zipcode', this.state.zipcode);
    params.append('time_to_call', this.state.time_to_call);
    params.append('current_net_worth', this.state.current_net_worth);
    params.append('cash_available_for_investment', this.state.cash_available_for_investment);
    params.append('here_about_us', this.state.here_about_us);
    params.append('website', this.state.website);
    params.append('location', this.state.location);

    params.append('linkedin', this.state.linkedin);
    params.append('comment', this.state.comment);
    params.append('phone', this.state.phone);
    params.append('gym_id', this.state.gym);

    if (this.state.upload_file !== null) {
      params.append('upload_file',
        { uri: this.state.upload_file, name: `${image_name}` + '.jpg', type: 'image/jpg' });
    }


    ApiService.api.post(ApiService.frenchise, params, { headers: headers })
      .then((response) => {
        this.setState({ disableBtn: false, spinner: false })
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.TRUE) {
            Utils.toastShow(response.data.message)
            this.setState({
              country: this.state.state_value[0].value,
              time_to_call: this.state.time_call[0].value,
              current_net_worth: this.state.net_worth[0].value,
              cash_available_for_investment: this.state.cash_available[0].value,
              here_about_us: this.state.know_about[0].value,
              full_name: '',
              address: '',
              email: '',
              city: '',
              zipcode: '',
              website: '',
              location: '',
              linkedin: '',
              comment: '',
              phone: '',
              attachment: ''
            })
          } else if (response.data.status === Constant.FALSE) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }

          //this.props.navigation.navigate('NotificationSetting')
        } else {

          Utils.toastShow(Constant.message)
        }

      })
      .catch(function (error) {
        Utils.toastShow(Constant.message)
        Utils._SentryReport(error)
        //this.setState({ disableBtn: false, spinner: false })
      })
  }

  onChangeTextPress(value) {
  }

  _pickImageFromGallery = async () => {

    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

    if (status === 'granted') {
      let result = await ImagePicker.launchImageLibraryAsync({

        mediaTypes: 'All'
      });


      if (!result.cancelled) {
        this.setState({ upload_file: result.uri, visible: false });
        try {
          const words = result.uri.split('/');
          this.setState({ attachment: words[words.length - 1] })
        } catch (error) {

        }
      }

    }

  };
  _handlePressButtonAsync = async () => {
    let result = await WebBrowser.openBrowserAsync(ApiService.pdfUrl);
    this.setState({ result });
  };

  // componentWillMount() {
  //   console.log('componentWillMount')
  // }

  render() {

    return (

      <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', backgroundColor: Colors.bg_color, paddingBottom: 10 }} behavior="padding" enabled keyboardVerticalOffset={Platform.OS === 'ios' ? 100 : 110} >

        <ScrollView style={styles.container} ref={ref => this.scrollView = ref}
          // onScroll={(event) => {
          //   this.setState({ scrollViewPosition: event.nativeEvent.contentOffset.y })
          // }}
          keyboardShouldPersistTaps="handled"
        >
          <CustomProgressBar spinner={this.state.spinner} />

          <View style={styles.container}>
            <View style={styles.bodyContent}>
              <Text style={styles.text_style}>Name</Text>
              <View style={styles.textInputMain}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(full_name) => this.setState({ full_name })}
                  value={this.state.full_name}
                  placeholder="Name"
                  placeholderTextColor={Colors.dark_gray}
                />
              </View>

              <Text style={styles.text_style}>Email</Text>
              <View style={styles.textInputMain}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(email) => this.setState({ email })}
                  value={this.state.email}
                  placeholder="Enter Email"
                  placeholderTextColor={Colors.dark_gray}
                  autoCapitalize='none'
                />
              </View>

              <Text style={styles.text_style}>Phone Number</Text>

              <View style={styles.textInputMain}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(phone) => this.setState({ phone })}
                  value={this.state.phone}
                  placeholder="Enter Phone Number"
                  placeholderTextColor={Colors.dark_gray}
                  keyboardType="numeric"
                  maxLength={10}
                />
              </View>

              <Text style={styles.text_style}>Address</Text>
              <View style={styles.textInputMain}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(address) => this.setState({ address })}
                  value={this.state.address}
                  placeholder="Address"
                  placeholderTextColor={Colors.dark_gray}
                />
              </View>

              <Text style={styles.text_style}>City</Text>
              <View style={styles.textInputMain}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(city) => this.setState({ city })}
                  value={this.state.city}
                  placeholder="City"
                  placeholderTextColor={Colors.dark_gray}
                />
              </View>

              <Text style={styles.text_style}>State</Text>

              {/* <View style={[styles.textInputMain, {
                justifyContent: 'center',
                alignItems: 'center',

              }]}>
                <Dropdown
                  containerStyle={{
                    width: '100%',
                    alignSelf: 'center',
                    paddingTop: 12,
                    paddingStart: 16, paddingEnd: 16,
                  }}
                  pickerStyle={styles.pickerStyle}
                  inputContainerStyle={{ borderBottomColor: 'transparent', justifyContent: 'center' }}
                  dropdownOffset={{ top: 0, bottom: -10 }}
                  dropdownMargins={{ min: 0, max: 0 }}
                  data={this.state.state_value}
                  value={this.state.state_value[0].value}
                  onChangeText={(value) => { this.setState({ state: value }) }}
                />
              </View> */}
              <View style={[styles.textInputMain, {
                justifyContent: 'center',
                alignItems: 'center',
              }]}>
                <ModalDropdown
                  options={this.state.state_value}
                  textStyle={styles.custom_dropdown_textStyle}
                  defaultValue={this.state.state_value[0]}
                  style={styles.custom_dropdown_style}
                  dropdownTextStyle={styles.custom_dropdownTextStyle}
                  dropdownStyle={styles.custom_dropdownStyle}
                  renderSeparator={() => <View style={{ borderWidth: 0 }} />}
                  onSelect={(idx, value) => { this.setState({ state: value }) }}
                />
                <View style={{ position: 'absolute', right: 20, width: 20, height: 20, justifyContent: 'center' }}>
                  <Image source={require('../assets/images/icons/down.png')} style={{ width: 10, height: 10, alignSelf: 'center', tintColor: Colors.dark_gray }} />
                </View>

              </View>


              <Text style={styles.text_style}>Zip Code</Text>
              <View style={styles.textInputMain}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(zipcode) => this.setState({ zipcode })}
                  value={this.state.zipcode}
                  placeholder="Zipcode"
                  placeholderTextColor={Colors.dark_gray}
                />
              </View>

              <Text style={styles.text_style}>Country</Text>
              <View style={styles.textInputMain}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(country) => this.setState({ country })}
                  value={this.state.country}
                  placeholder="Country"
                  placeholderTextColor={Colors.dark_gray}
                />
              </View>

              <Text style={styles.text_style}>Best time to call</Text>
              {/* <View style={[styles.textInputMain, {
                justifyContent: 'center',
                alignItems: 'center',

              }]}>
                <Dropdown
                  containerStyle={{
                    width: '100%',
                    alignSelf: 'center',
                    paddingTop: 12,
                    paddingStart: 16, paddingEnd: 16,
                  }}
                  pickerStyle={styles.pickerStyle}
                  inputContainerStyle={{ borderBottomColor: 'transparent' }}
                  dropdownOffset={{ top: 0, bottom: -10 }}
                  dropdownMargins={{ min: 0, max: 0 }}
                  data={this.state.time_call}
                  value={this.state.time_call[0].value}
                  onChangeText={(value) => { this.setState({ time_to_call: value }) }}
                />

              </View> */}

              <View style={[styles.textInputMain, {
                justifyContent: 'center',
                alignItems: 'center',
              }]}>
                <ModalDropdown
                  options={this.state.time_call}
                  textStyle={styles.custom_dropdown_textStyle}
                  defaultValue={this.state.time_call[0]}
                  style={styles.custom_dropdown_style}
                  dropdownTextStyle={styles.custom_dropdownTextStyle}
                  dropdownStyle={[styles.custom_dropdownStyle, { height: 120 }]}
                  renderSeparator={() => <View style={{ borderWidth: 0 }} />}
                  onSelect={(idx, value) => { this.setState({ time_to_call: value }) }}
                />
                <View style={{ position: 'absolute', right: 20, width: 20, height: 20, justifyContent: 'center' }}>
                  <Image source={require('../assets/images/icons/down.png')} style={{ width: 10, height: 10, alignSelf: 'center', tintColor: Colors.dark_gray }} />
                </View>

              </View>

              <Text style={styles.text_style}>Current Net Worth</Text>

              {/* <View style={[styles.textInputMain, {
                justifyContent: 'center',
                alignItems: 'center',

              }]}>
                <Dropdown
                  containerStyle={{
                    width: '100%',
                    alignSelf: 'center',
                    paddingTop: 12,
                    paddingStart: 16, paddingEnd: 16,
                  }}
                  pickerStyle={styles.pickerStyle}
                  inputContainerStyle={{ borderBottomColor: 'transparent' }}
                  dropdownOffset={{ top: 0, bottom: -10 }}
                  dropdownMargins={{ min: 0, max: 0 }}
                  data={this.state.net_worth}
                  value={this.state.net_worth[0].value}
                  onChangeText={(value) => { this.setState({ current_net_worth: value }) }}
                />
              </View> */}

              <View style={[styles.textInputMain, {
                justifyContent: 'center',
                alignItems: 'center',
              }]}>
                <ModalDropdown
                  options={this.state.net_worth}
                  textStyle={styles.custom_dropdown_textStyle}
                  defaultValue={this.state.net_worth[0]}
                  style={styles.custom_dropdown_style}
                  dropdownTextStyle={styles.custom_dropdownTextStyle}
                  dropdownStyle={styles.custom_dropdownStyle}
                  renderSeparator={() => <View style={{ borderWidth: 0 }} />}
                  onSelect={(idx, value) => { this.setState({ current_net_worth: value }) }}
                />
                <View style={{ position: 'absolute', right: 20, width: 20, height: 20, justifyContent: 'center' }}>
                  <Image source={require('../assets/images/icons/down.png')} style={{ width: 10, height: 10, alignSelf: 'center', tintColor: Colors.dark_gray }} />
                </View>

              </View>


              <Text style={styles.text_style}>Cash Available for Investment</Text>

              {/* <View style={[styles.textInputMain, {
                justifyContent: 'center',
                alignItems: 'center',

              }]}>
                <Dropdown
                  containerStyle={{
                    width: '100%',
                    alignSelf: 'center',
                    paddingTop: 12,
                    paddingStart: 16, paddingEnd: 16,
                  }}
                  pickerStyle={styles.pickerStyle}
                  inputContainerStyle={{ borderBottomColor: 'transparent' }}
                  dropdownOffset={{ top: 0, bottom: -10 }}
                  dropdownMargins={{ min: 0, max: 0 }}
                  data={this.state.cash_available}
                  value={this.state.cash_available[0].value}
                  onChangeText={(value) => { this.setState({ cash_available_for_investment: value }) }}
                />
              </View> */}

              <View style={[styles.textInputMain, {
                justifyContent: 'center',
                alignItems: 'center',
              }]}>
                <ModalDropdown
                  options={this.state.cash_available}
                  textStyle={styles.custom_dropdown_textStyle}
                  defaultValue={this.state.cash_available[0]}
                  style={styles.custom_dropdown_style}
                  dropdownTextStyle={styles.custom_dropdownTextStyle}
                  dropdownStyle={styles.custom_dropdownStyle}
                  renderSeparator={() => <View style={{ borderWidth: 0 }} />}
                  onSelect={(idx, value) => { this.setState({ cash_available_for_investment: value }) }}
                />
                <View style={{ position: 'absolute', right: 20, width: 20, height: 20, justifyContent: 'center' }}>
                  <Image source={require('../assets/images/icons/down.png')} style={{ width: 10, height: 10, alignSelf: 'center', tintColor: Colors.dark_gray }} />
                </View>

              </View>

              <Text style={styles.text_style}>How Did You Hear About Us</Text>



              {/* <View style={[styles.textInputMain, {
                justifyContent: 'center',
                alignItems: 'center',

              }]}>
                <Dropdown
                  containerStyle={{
                    width: '100%',
                    alignSelf: 'center',
                    paddingTop: 12,
                    paddingStart: 16, paddingEnd: 16,
                  }}
                  pickerStyle={styles.pickerStyle}
                  inputContainerStyle={{
                    borderBottomColor: 'transparent',
                  }}
                  dropdownOffset={{ top: 0, bottom: -10 }}
                  dropdownMargins={{ min: 0, max: 0 }}
                  data={this.state.know_about}
                  value={this.state.know_about[0].value}
                  onChangeText={(value) => { this.setState({ here_about_us: value }) }}
                />
              </View> */}

              <View style={[styles.textInputMain, {
                justifyContent: 'center',
                alignItems: 'center',
              }]}>
                <ModalDropdown
                  options={this.state.know_about}
                  textStyle={styles.custom_dropdown_textStyle}
                  defaultValue={this.state.know_about[0]}
                  style={styles.custom_dropdown_style}
                  dropdownTextStyle={styles.custom_dropdownTextStyle}
                  dropdownStyle={styles.custom_dropdownStyle}
                  renderSeparator={() => <View style={{ borderWidth: 0 }} />}
                  onSelect={(idx, value) => { this.setState({ here_about_us: value }) }}
                />
                <View style={{ position: 'absolute', right: 20, width: 20, height: 20, justifyContent: 'center' }}>
                  <Image source={require('../assets/images/icons/down.png')} style={{ width: 10, height: 10, alignSelf: 'center', tintColor: Colors.dark_gray }} />
                </View>

              </View>


              <Text style={styles.text_style}>Website</Text>
              <View style={styles.textInputMain}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(website) => this.setState({ website })}
                  value={this.state.website}
                  placeholder="Website"
                  placeholderTextColor={Colors.dark_gray}
                  autoCapitalize='none'
                />
              </View>

              <Text style={styles.text_style}>Social Media</Text>
              <View style={styles.textInputMain}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(linkedin) => this.setState({ linkedin })}
                  value={this.state.linkedin}
                  placeholder="Social Media"
                  placeholderTextColor={Colors.dark_gray}
                  autoCapitalize='none'
                />
              </View>

              <Text style={styles.text_style}>Comment</Text>

              <View style={[{

                borderWidth: 1,
                borderColor: Colors.light_gray,
                width: '100%',
                marginTop: 6,
                borderRadius: 5, height: 100, justifyContent: 'flex-start',
                justifyContent: 'flex-start',

              }]}
                onLayout={({ nativeEvent: { layout: { x, y, width, height } } }) => {
                  this.setState({
                    scrollViewPosition: y
                  });

                }}>
                <TextInput
                  onChange={(event) => {
                    // onContentSizeChange doesn't work on Android, use onChange instead https://github.com/facebook/react-native/issues/11692
                    //this.setState({ height: event.nativeEvent.contentSize.height });
                  }}
                  style={[styles.inputStyleNew, {
                    textAlignVertical: 'top',
                  }, Platform.OS === 'ios' ? { flex: 1, } : { height: 105 }]}
                  onChangeText={(comment) => this.setState({ comment })}
                  value={this.state.comment}
                  placeholder="Type Comment"
                  placeholderTextColor={Colors.dark_gray}
                  multiline={true}
                  autoCorrect={false}
                  onFocus={() => {
                    this.scrollView.scrollTo({ x: 0, y: Platform.OS === 'ios' ? this.state.scrollViewPosition - 240 : this.state.scrollViewPosition + 30, animated: true });
                  }}

                />
              </View>




              <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 16 }}>
                <Text style={[styles.text_style, { paddingTop: 6, paddingBottom: 6, alignSelf: 'center', marginTop: 0 }]}>Attach Profile Photo</Text>

                <TouchableOpacity
                  onPress={() => { this._pickImageFromGallery() }}>
                  <Text style={{ borderColor: Colors.light_gray, borderWidth: 1, borderRadius: 10, padding: 6, marginStart: 8 }}>Choose Photo</Text>
                </TouchableOpacity>

              </View>
              <Text numberOfLines={1} style={[styles.text_style, { paddingTop: 6, paddingBottom: 6, alignSelf: 'center', marginTop: 0 }]}>{this.state.attachment}</Text>


              <TouchableOpacity
                activeOpacity={1}
                disabled={this.state.disableBtn}
                onPress={() => { Keyboard.dismiss(), this._ProfileUpdate() }}
                style={[buttonStyle.login_btn, { marginBottom: 20 }]}
                underlayColor='gray'>
                <Text style={buttonStyle.btnText}>{'Submit'.toUpperCase()}</Text>
              </TouchableOpacity>

              {/* <TouchableOpacity
                onPress={this._handlePressButtonAsync}
                style={{ marginBottom: 20 }}>
                <Text style={[styles.text_style, { color: Colors.primaryColor, textAlign: 'center' }]}>Download the franchise application</Text>
              </TouchableOpacity> */}

            </View>

          </View>

        </ScrollView>
        <SafeAreaView />
      </KeyboardAvoidingView>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },
  header: {
    backgroundColor: Colors.bg_color,
    padding: 10,
    alignItems: 'center',
  },

  avatar: {
    width: 110,
    height: 110,
    borderRadius: 57,
    borderWidth: 2,
    borderColor: "white",
    marginBottom: 10,
  },
  name: {
    fontSize: 22,
    fontWeight: '600',
  },
  bodyContent: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 10,
  },

  bodyContent: {
    paddingStart: 24,
    paddingEnd: 24,
    flexDirection: 'row',
    flexWrap: 'wrap'

  },
  cross_icon_view: {
    width: 32,
    height: 32,
    position: 'absolute',
    backgroundColor: 'white',
    borderRadius: 50,
    backgroundColor: 'white',
    shadowOffset: { width: 1, height: 1, },
    shadowColor: 'black',
    shadowOpacity: .4,
    marginTop: 90,
    marginLeft: 60,
    left: 150,
    top: 0,
    zIndex: 10,
    alignSelf: 'center',
    justifyContent: 'center'
  },
  text_style: {
    marginTop: 16,
    color: Colors.txt_gray,
    paddingStart: 16,
    fontFamily: 'futura-medium',

  },
  textInputMain: {
    flexDirection: 'row',
    borderRadius: 46 / 2,
    borderWidth: 1,
    borderColor: Colors.light_gray,
    width: '100%',
    height: 46,
    marginTop: 6,
    justifyContent: 'center'

  },
  inputStyle: {
    flex: 1,
    paddingStart: 16,
    fontFamily: 'futura-medium',
    alignSelf: 'center',
    height: 56,
  },
  inputStyleNew: {
    paddingStart: 16,
    paddingTop: 16,
    fontFamily: 'futura-medium',
  },
  edit_icon_view: {
    width: 32,
    height: 32,
    borderRadius: 50,
    backgroundColor: 'white',
    shadowOffset: { width: 1, height: 1, },
    shadowColor: 'black',
    shadowOpacity: .4,
    alignSelf: 'center',
    marginRight: 10,
    justifyContent: 'center'
  },
  pickerStyle: {
    width: Dimensions.get('window').width - 48,
    paddingStart: 20,
    marginStart: 24, marginEnd: 24, marginTop: 60
  },
  custom_dropdown_style: {
    justifyContent: 'center',
    width: '100%', height: 48,
    justifyContent: 'center', alignSelf: 'center'
  },
  custom_dropdownTextStyle: {
    fontSize: 14,
    alignSelf: 'center',
    backgroundColor: '#fff',
    width: '100%',
    paddingStart: 16
  },
  custom_dropdownStyle: {
    width: Dimensions.get('window').width - 48,
    padding: 4

  },
  custom_dropdown_textStyle: {
    fontSize: 14, height: 48,
    justifyContent: 'center',
    paddingTop: 16, paddingStart: 20,
    fontFamily: 'futura-medium',
    color: Colors.feed_gray,
  },
});