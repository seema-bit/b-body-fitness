import React, { Component } from 'react';
import { Image, TouchableOpacity, ImageBackground, StyleSheet, Dimensions, AsyncStorage, Platform, Share, StatusBar, ScrollView, Keyboard } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Left, Body, Right, View, Icon } from 'native-base';
import Colors from '../constants/Colors';
import { font_style, textHeader } from '../components/styles';
import Constant from '../constants/Constant';
import ModalSelector from 'react-native-modal-selector'
import moment from 'moment'
import Modal from 'react-native-modalbox';
import ImageZoom from 'react-native-image-pan-zoom';
import { SafeAreaView } from 'react-navigation';
import { FlatList } from 'react-native-gesture-handler';
import ApiService from '../webservice/ApiService';
import Utils from '../constants/Utils';
import Loader from '../components/Loader';
import HeaderBackLeft from '../components/HeaderBackLeft';
import HeaderBackground from '../components/HeaderBackground';
import { Video } from 'expo-av'
//import VideoPlayer from 'expo-video-player'
import { MaterialIcons } from '@expo/vector-icons';
import {
  updateCount, fetchFeedList, fetchFeedDetails
} from '../redux/action';
import { connect } from 'react-redux';

import { ScreenOrientation } from 'expo';
import VideoPlayer from '../components/ExpoVideoPlayer/dist/VideoPlayer';

class FeedDetails extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.feed_details}`.toUpperCase()}</Text>,
    headerLeft: <TouchableOpacity
      onPress={() => {
        (navigation.getParam('redirect', null) !== undefined && navigation.getParam('redirect', null) !== null && navigation.getParam('redirect', null) === 'notification') ?
          navigation.navigate("Notification")
          :
          navigation.navigate("Feed", {
            refresh: 'yes'
          })
      }} style={textHeader.leftIcon}>
      <Image source={require('./../assets/images/icons/arrow-right_3x.png')} style={{ width: 11, height: 20, alignSelf: 'flex-start' }} />
    </TouchableOpacity>
  });

  state = {
    feedData: {},
    userData: null,
    name: '',
    image: null,
    comment_count: 0,
    userInfo: '',
    userLikes: null,
    image_enlarge: '',
    refreshing: false,
    report_category: [],
    selected_category: null,
    feed_id: null,
    report_text: '',
    report_status: null,
  }

  _Redirect = () => {
    const redirect = this.props.navigation.getParam('redirect', null);
    if (redirect !== undefined && redirect !== null && redirect === 'notification') {
      this.props.navigation.navigate("Notification")
    } else {
      this.props.navigation.navigate("Feed", {
        refresh: 'yes'
      })
    }

  }

  componentDidMount() {

    _this = this
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json, })
        this._FetchFeeds()

      })
      .catch(error => console.log('feed error!', error.response));
  }

  componentWillReceiveProps = () => {
    _this = this
    //this.setState({ allFeed: null })
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json, })
        if (this.props.lists.feed_details === undefined || this.props.lists.feed_details === null) {
          this._FetchFeeds()
        }
      })
      .catch(error => console.log('feed error!', error.response));
  }


  _FetchFeeds = () => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    const feed_id = this.props.navigation.getParam('feed_id', '');
    console.log(feed_id, 'feed_id')
    ApiService.api.get(ApiService.feedDetails + feed_id, { headers: headers })
      .then((response) => {
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            this.props.fetchFeedDetails(response.data.data)

          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })
  }

  _LikeFeed = () => {
    //alert("Like: " + index)
    const newArray = this.props.lists.feed_details;
    if (newArray.user_like !== null && newArray.user_like !== '' && newArray.user_like === Constant.user_like) {
      newArray.likes = newArray.likes - 1;
      newArray.user_like = Constant.user_unlike
    } else {
      newArray.likes = newArray.likes + 1;
      newArray.user_like = Constant.user_like
    }


    this.props.fetchFeedDetails(newArray)
    //this.setState({ feedData: newArray })

    var params = {
      user_id: this.state.userInfo.id,
      feed_id: newArray.id
    }

    this.props.lists.feed_list.forEach((element, index) => {

      if (element.id === newArray.id) {
        element.user_like = newArray.user_like
        element.likes = newArray.likes

        console.log('element', element)
        const newallFeed = [...this.props.lists.feed_list.slice(0, index), element, ...this.props.lists.feed_list.slice(index + 1)]
        this.props.fetchFeedList(newallFeed)
        //break
      }
    });

    ApiService.api.post(ApiService.likeDislike, params)
      .then((response) => {
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + error);
      })

  }

  _CommentFeed = () => {
    this.props.navigation.navigate("Comment",
      {
        user_id: this.state.userInfo.id,
        feed_id: this.props.lists.feed_details.id,
        access_key: this.state.userInfo.access_key,
        redirect: 'FeedDetails'
      }
    )
  }

  _FeedDetails = (index, id) => {
    this.props.navigation.navigate("FeedDetails",
      {
        feed_id: id,
      }
    )
  }
  _DeleteFeed = () => {
    //alert("delete post: " + index)
    var params = {}

    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    this.props.lists.feed_list.forEach((element, index) => {

      if (element.id === this.props.lists.feed_details.id) {
        const newallFeed = [...this.props.lists.feed_list.slice(0, index), ...this.props.lists.feed_list.slice(index + 1)]
        this.props.fetchFeedList(newallFeed)
        //break
      }
    });

    this.props.fetchFeedDetails(null)

    ApiService.api.post(ApiService.deleteFeed + this.props.lists.feed_details.id, params, { headers: headers })
      .then((response) => {

        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            Utils.toastShow(response.data.message)

            this._Redirect()
          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + error);
      })
  }

  _EditFeed = () => {

    let createJson = this.props.lists.feed_details
    createJson.feed = {}
    createJson.feed.image = createJson.image
    createJson.feed.content = createJson.content
    createJson.feed.id = createJson.id
    //this.setState({ feedData: createJson })
    this.props.navigation.navigate("AddFeed",
      {
        feedData: this.props.lists.feed_details,
        editFeed: 'yes',
        redirect: 'feed_details',
        //_UpdateData: () => this._UpdateData(),
        title: Constant.edit_feed
      }
    )
  }

  postReport = (selected_category) => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    if (!selected_category && !this.state.feed_id) return;


    this.setState({ loading: true });

    var params = {
      category_id: selected_category,
      feed_id: this.state.feed_id,
      user_id: this.state.userInfo.id,
      additional_info: this.state.report_text
    }
    console.log(params);
    ApiService.api.post(ApiService.postReport, params, { headers: headers })
      .then((response) => {
        this.setState({ report_category: [], loading: false })
        if (response !== null && response.data != null) {
          console.log(response.data);
          if (response.data.status === Constant.SUCCESS) {
            this.setState({ report_status: response.data });

          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        this.setState({ selected_category: null, report_text: "", feed_id: null, loading: false })
        console.log("error " + JSON.stringify(error.response));


      })
  }

  handleFeedOptionLocal = (key) => {

    if (key !== null && key !== '') {
      if (key === 0) {
        this._DeleteFeed()
      } else if (key === 1) {
        this._EditFeed()
      }
    }

  }

  enlargeImage = (image) => {
    this.setState({ image_enlarge: image })
    this.refs.modal1.open()
  }

  renderModal() {
    return (
      <Modal
        backButtonClose={true}
        swipeToClose={true}
        coverScreen={true}
        backdropPressToClose={true}
        animationIn="fadeIn"
        animationOut="fadeOut"
        hideModalContentWhileAnimating={true}
        animationInTiming={1500}
        animationOutTiming={1500}
        backdropTransitionOutTiming={0}
        position={"center"} ref={"modal1"} style={{ margin: 0, }}  >
        <StatusBar barStyle="dark-content" />
        <View>

          <ImageZoom cropWidth={Dimensions.get('window').width}
            cropHeight={Dimensions.get('window').height}
            imageWidth={Dimensions.get('window').width}
            imageHeight={Dimensions.get('window').width}>
            <Image style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').width }}
              source={{ uri: this.state.image_enlarge }} />
          </ImageZoom>
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => this.refs.modal1.close()}
            activeOpacity={1}
            style={styles.icon_view}
          >

            <Image source={require('../assets/images/icons/close_3x.png')}
              style={{ width: 20, height: 20, tintColor: Colors.black_color, }} />
          </TouchableOpacity>
        </View>
      </Modal>
    )
  }

  renderVideoModal() {
    return (
      <Modal
        backButtonClose={true}
        swipeToClose={true}
        coverScreen={true}
        backdropPressToClose={true}
        animationIn="fadeIn"
        animationOut="fadeOut"
        hideModalContentWhileAnimating={true}
        animationInTiming={1500}
        animationOutTiming={1500}
        backdropTransitionOutTiming={0}
        position={"center"} ref={"modal2"} style={{ margin: 0, }}  >
        <StatusBar barStyle="dark-content" />
        <View style={{ flex: 1, }}>

          <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>

            <VideoPlayer
              height={250}
              videoProps={{
                shouldPlay: true,
                resizeMode: Video.RESIZE_MODE_CONTAIN,
                source: {
                  uri: this.props.lists.feed_details.image
                },
                isMuted: false,

                playThroughEarpieceAndroid: true,
              }}

              sliderColor={Colors.dark_gray}
              showControlsOnLoad={true}
              showFullscreenButton={false}
              isPortrait={this.state.portrait}
              playFromPositionMillis={0}
              switchToLandscape={() => this.changeScreenOrientation("0")}
              switchToPortrait={() => this.changeScreenOrientation("1")}
              playFromPositionMillis={0}
            />

          </View>
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => this.refs.modal2.close()}
            activeOpacity={1}
            style={styles.icon_view}
          >

            <Image source={require('../assets/images/icons/close_3x.png')}
              style={{ width: 20, height: 20, tintColor: Colors.black_color, }} />
          </TouchableOpacity>
        </View>
      </Modal>

    )
  }

  renderReportModal() {
    return (
      <Modal
        backButtonClose={true}
        transparent={true}
        swipeToClose={false}
        coverScreen={true}
        backdrop
        backdropPressToClose={true}
        animationIn="fadeIn"
        animationOut="fadeOut"
        hideModalContentWhileAnimating={true}
        animationInTiming={1500}
        animationOutTiming={1500}
        backdropTransitionOutTiming={0}
        position={"center"} ref={"reportModal"} style={{ margin: 0, backgroundColor: 'transparent', justifyContent: 'center' }}  >
        {/* <SafeAreaView /> */}
        {/* <StatusBar barStyle="dark-content" /> */}
        <ScrollView keyboardShouldPersistTaps='handled' style={{ flex: 1 }} contentContainerStyle={{ flex: 1 }} >
          <TouchableOpacity style={{ flex: 1 }} onPress={() => { this.refs.reportModal.close(); this.setState({ selected_category: null, feed_id: null, report_text: "", report_status: null, }) }} />

          <View style={{ width: '90%', alignSelf: 'center', alignItems: 'center', height: this.state.report_status ? 200 : 400, borderRadius: 20, paddingBottom: 20, backgroundColor: '#fff', justifyContent: 'center' }}>
            <View style={{ backgroundColor: '#fff', alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: 100, height: 100, borderRadius: 50, marginTop: -50 }}>
              <Image style={{ width: 50, height: 50 }} resizeMode="contain" source={require('../assets/images/report_img.png')} />
            </View>

            <Text style={[font_style.font_bold, { fontSize: 20, color: '#626467' }]}>{this.state.report_status ? this.state.report_status.message : "Report Feed"}</Text>

            {this.state.report_status ? null : <Text style={[font_style.font_medium, { width: '80%', fontSize: 12, marginTop: 5, marginBottom: 10, textAlign: 'center', color: '#7b7e83' }]}>Help us understand what's happening? What's wrong with this Feed?</Text>}

            {this.state.report_status ? (<View style={{ width: '80%', flex: 1, justifyContent: 'center' }}>
              <View style={{ width: '100%', marginTop: 20, justifyContent: 'center', }}>
                <TouchableOpacity
                  onPress={() => {
                    Keyboard.dismiss()
                    if (this.state.report_status) {
                      this.refs.reportModal.close();
                      this.setState({ report_status: null, selected_category: null, feed_id: null, report_text: '' });
                    } else {
                      this.postReport(this.state.selected_category);
                    }
                  }}
                  activeOpacity={1}
                  style={[styles.auth_btn, { backgroundColor: Colors.primaryColor, marginTop: 0, marginBottom: 20 }]}
                  disabled={this.state.disableBtn}>
                  <Text style={[styles.loginText, { color: Colors.white_color }]}>{this.state.report_status ? 'Ok' : 'Report Feed'}</Text>
                </TouchableOpacity>
              </View>
            </View>
            ) : null}

            {(this.state.selected_category != null && this.state.selected_category == 7 && !this.state.report_status) ?
              (<View style={{ width: '80%', flex: 1, }}>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                  <TextInput
                    style={{ height: 40, borderColor: Colors.primaryColor, borderBottomWidth: 1 }}
                    onChangeText={text => { this.setState({ report_text: text }) }}
                    value={this.state.report_text}
                    placeholder={"Additional Info (Optional)"}
                    onSubmitEditing={() => {
                      this.postReport(this.state.selected_category)
                    }}
                  />
                </View>

                <TouchableOpacity onPress={() => { this.setState({ selected_category: null }) }} style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                  {/* <Image source={require('../assets/images/icons/arrow-right_3x.png')} style={{ width: 20, height: 20, alignSelf: 'flex-start', tintColor: Colors.primaryColor }} /> */}
                  <Ionicons name='ios-arrow-back' size={20} color={Colors.primaryColor} />
                  <Text style={[font_style.font_medium, { marginLeft: 10, color: Colors.primaryColor, fontSize: 14 }]}>Go Back</Text>
                </TouchableOpacity>

                <View style={{ width: '100%', marginTop: 20, alignSelf: 'baseline' }}>
                  <TouchableOpacity
                    onPress={() => {
                      Keyboard.dismiss()
                      if (this.state.report_status) {
                        this.refs.reportModal.close();
                        this.setState({ report_status: null, selected_category: null, feed_id: null, report_text: '' });
                      } else {
                        this.postReport(this.state.selected_category);
                      }
                    }}
                    activeOpacity={1}
                    style={[styles.auth_btn, { backgroundColor: Colors.primaryColor, marginTop: 0, marginBottom: 20 }]}
                    disabled={this.state.disableBtn}>
                    <Text style={[styles.loginText, { color: Colors.white_color }]}>{this.state.report_status ? 'Ok' : 'Report Feed'}</Text>
                  </TouchableOpacity>
                </View>
              </View>
              )
              :
              (
                this.state.report_status ? null : <View style={{ flex: 1, width: '100%', }}>
                  <FlatList
                    showsVerticalScrollIndicator={false}
                    data={this.state.report_category}
                    //onRefresh={this._handleRefresh}
                    refreshing={this.state.refreshing}
                    onEndReachedThreshold={0.5}
                    //onEndReached={this._handleLoadMore}
                    showsVerticalScrollIndicator
                    ItemSeparatorComponent={() => <View style={{ width: '100%', height: 1, backgroundColor: '#ccc' }} />}
                    keyExtractor={(item, index) => {
                      return index.toString();
                    }}
                    ListEmptyComponent={() => <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                      <Loader />
                    </View>}
                    contentContainerStyle={{ backgroundColor: Colors.white_color }}
                    renderItem={({ item, index }) => (
                      <TouchableOpacity style={{ flexDirection: 'row', padding: 10, paddingLeft: 15, paddingRight: 15, alignItems: 'center' }}
                        onPress={() => {

                          this.postReport(item.id);

                        }}>
                        <ImageBackground source={require('../assets/images/admin.jpg')}
                          style={{ width: 30, height: 30 }}>
                          <Image source={{ uri: item.image }} style={{ width: 30, height: 30, backgroundColor: '#fff' }} />
                        </ImageBackground>
                        <Text style={[font_style.font_medium, { fontSize: 12, marginLeft: 10, color: '#7b7e83' }]}>{item.name}</Text>
                      </TouchableOpacity>
                    )}
                  />
                </View>
              )}

          </View>
          <TouchableOpacity style={{ flex: 1 }} onPress={() => { this.refs.reportModal.close(); this.setState({ selected_category: null, feed_id: null, report_text: "", report_status: null, }) }} />
        </ScrollView>
        {this.state.loading ? <View style={{ top: 0, bottom: 0, left: 0, right: 0, position: 'absolute', backgroundColor: '#00000040', alignItems: 'center', justifyContent: 'center' }}><Loader /></View> : null}
        {/* <SafeAreaView /> */}
      </Modal>
    )
  }

  renderLikeModal() {
    return (
      <Modal
        backButtonClose={true}
        swipeToClose={false}
        coverScreen={true}
        backdropPressToClose={true}
        animationIn="fadeIn"
        animationOut="fadeOut"
        hideModalContentWhileAnimating={true}
        animationInTiming={1500}
        animationOutTiming={1500}
        backdropTransitionOutTiming={0}
        position={"center"} ref={"likeModal"} style={{ margin: 0, backgroundColor: '#00000040', }}  >
        <SafeAreaView />
        <StatusBar barStyle="dark-content" />
        <View style={{ flex: 1, marginTop: 80, backgroundColor: '#fff', borderTopLeftRadius: 20, borderTopRightRadius: 20 }}>
          {this.state.userLikes === undefined || this.state.userLikes === null ?
            (<Loader />)
            :
            (
              <View>

                <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 20 }}>
                  <Text style={[font_style.font_medium, { position: 'absolute', width: '100%', fontSize: 16, textAlign: 'center', flex: 1 }]}>LIKES</Text>
                  <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => this.refs.likeModal.close()}
                    activeOpacity={1}
                    style={styles.like_icon_view}
                  >
                    <Image source={require('../assets/images/icons/arrow-right_3x.png')}
                      resizeMode='contain'
                      style={{ width: 22, height: 22, tintColor: Colors.black_color, justifyContent: 'center' }} />
                  </TouchableOpacity>
                </View>
                <FlatList
                  showsVerticalScrollIndicator={false}
                  data={this.state.userLikes}
                  //onRefresh={this._handleRefresh}
                  refreshing={this.state.refreshing}
                  onEndReachedThreshold={0.5}
                  //onEndReached={this._handleLoadMore}
                  keyExtractor={(item, index) => {
                    return index.toString();
                  }}
                  contentContainerStyle={{ backgroundColor: Colors.white_color }}
                  renderItem={({ item, index }) => (
                    <Card style={{
                      flex: 0,
                      borderBottomColor: Colors.light_gray,
                      borderBottomWidth: 1,
                      marginStart: 16, marginEnd: 16,
                    }} key={index} transparent>

                      <CardItem>
                        <Left>
                          <ImageBackground source={require('../assets/images/admin.jpg')}
                            style={styles.img_view}
                            imageStyle={styles.imageStyle}>
                            <TouchableOpacity
                              activeOpacity={1}
                              onPress={() => this.enlargeImage(this.props.lists.feed_details.image)}>
                              <Image source={{ uri: this.props.lists.feed_details.image }} style={styles.img_view} />
                            </TouchableOpacity>

                          </ImageBackground>

                          <Body>
                            <Text style={[font_style.font_medium, { fontSize: 14, textTransform: 'capitalize' }]}>{this.props.lists.feed_details.name}</Text>
                          </Body>
                        </Left>


                      </CardItem>
                    </Card>
                  )}
                />


              </View>
            )}

        </View>
        <SafeAreaView />
      </Modal>
    )
  }

  getUserLikes = () => {
    if (this.props.lists.feed_details.likes > 0) {
      this.refs.likeModal.open()
      this._FetchUserLikes()
    }
  }

  _FetchUserLikes = () => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    ApiService.api.get(ApiService.getUserLikes + "feed/" + this.props.lists.feed_details.id + "/" + 0, { headers: headers })
      .then((response) => {
        this.setState({ userLikes: [] })
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            this.setState({ userLikes: response.data.data != null ? response.data.data : [] })
          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        //alert('Error')
        this.setState({ userLikes: [] })
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })
  }


  _SharePost = async () => {
    try {
      const result = await Share.share({
        ...Platform.select({
          ios: {
            message: this.props.lists.feed_details.content !== null && this.props.lists.feed_details.content !== undefined ? this.props.lists.feed_details.content : 'B Body Fitness',
            url: this.props.lists.feed_details.image,
          },
          android: {
            message: this.props.lists.feed_details.content !== null && this.props.lists.feed_details.content !== undefined ? this.props.lists.feed_details.content : 'B Body Fitness' + ' \n' + this.props.lists.feed_details.image
          }
        }),
        title: 'B Body Fitness'
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType

        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed

      }
    } catch (error) {
      Utils.toastShow(error.message);
    }

  }

  playVideo = (url) => {
    this.refs.modal2.open()
  }

  getReportCategory = () => {
    this.refs.reportModal.open()
    // console.log(item.feed.id,this.state.userInfo.id)
    this.setState({ feed_id: this.props.lists.feed_details.id });
    this.fetchReportCategory()
  }

  fetchReportCategory = () => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    ApiService.api.get(ApiService.getReportCategory, { headers: headers })
      .then((response) => {
        this.setState({ report_category: [] })
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            this.setState({ report_category: response.data.data != null ? response.data.data : [] })
          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        this.setState({ report_category: [] })
        console.log("error " + JSON.stringify(error.response));


      })
  }

  render() {

    let index = 0
    const popupData = [
      { key: index++, label: 'Delete Feed' },
      { key: index++, label: 'Edit Feed' },
    ];
    if (this.props.lists.feed_details === undefined || this.props.lists.feed_details === null) {
      return (<Loader />)
    } else {
      return (
        < Container >
          {this.renderModal()}
          {this.renderLikeModal()}
          {this.renderVideoModal()}
          {this.renderReportModal()}
          <Content>
            <Card style={{ flex: 0 }} key={index} transparent>
              <CardItem>
                <Left>
                  {this.props.lists.feed_details.image !== undefined && this.props.lists.feed_details.image !== null && this.props.lists.feed_details.image !== ""
                    ?
                    (<ImageBackground source={require('../assets/images/admin.jpg')}
                      style={styles.img_view}
                      imageStyle={styles.imageStyle}>
                      <TouchableOpacity
                        activeOpacity={1}
                        onPress={() => this.enlargeImage(this.props.lists.feed_details.image)}>
                        <Image source={{ uri: this.props.lists.feed_details.image }} style={styles.img_view} />
                      </TouchableOpacity>

                    </ImageBackground>)
                    :
                    (<Image source={require('../assets/images/admin.jpg')} style={styles.img_view} />)
                  }

                  <Body>
                    <Text style={[font_style.font_medium, { fontSize: 14, }]}>{this.props.lists.feed_details.name}</Text>
                    <Text style={[font_style.font_medium, { fontSize: 12, color: 'rgba(90,91,93,0.7)', marginTop: 4 }]}>{moment(this.props.lists.feed_details.created_at).format('DD MMM')} at {moment(this.props.lists.feed_details.created_at).format('hh:mm A')} </Text>
                  </Body>
                </Left>

                {(this.state.userInfo.id !== null && this.state.userInfo.id !== 'undefind' && this.state.userInfo.id == this.props.lists.feed_details.user_id) &&
                  (<Right>

                    <ModalSelector
                      cancelText="Cancel"
                      touchableActiveOpacity={1}
                      data={popupData}
                      onChange={option => { this.handleFeedOptionLocal(option.key) }}
                      overlayStyle={{
                        justifyContent: 'flex-end',
                        width: '100%',
                        marginBottom: -30,
                      }}

                    >
                      <TouchableOpacity activeOpacity={1}>
                        <Image source={require('../assets/images/icons/dots_3x.png')}
                          style={{ width: 20, height: 20, tintColor: Colors.red_shade }}
                          resizeMode="contain"
                        />
                      </TouchableOpacity>

                    </ModalSelector>


                  </Right>
                  )
                }
              </CardItem>

              <CardItem>
                <Text style={[font_style.font_medium, { fontSize: 14, color: 'rgba(65,63,64,0.7)' }]}>{this.props.lists.feed_details.content}</Text>
              </CardItem>
              <CardItem>
                <Body>

                  <ImageBackground source={require('../assets/images/thumbnail.png')} style={{ height: 200, width: '100%' }}>
                    <TouchableOpacity activeOpacity={1}
                      onPress={() => { this.props.lists.feed_details.type === 'video' ? this.playVideo(this.props.lists.feed_details.image) : this.enlargeImage(this.props.lists.feed_details.image) }} style={{ height: 200, width: '100%', }}>

                      {this.props.lists.feed_details.type === 'video' ?
                        <View style={{ height: 200, width: '100%', flex: 1, justifyContent: 'center' }}>
                          <Video
                            style={{ height: 200, width: '100%', flex: 1, borderRadius: 4 }}
                            source={{ uri: this.props.lists.feed_details.image }}
                            usePoster
                            shouldPlay={false}
                            resizeMode="cover"
                          />
                          <View style={styles.play_icon}>

                            <MaterialIcons name="play-circle-outline" size={48} color={Colors.black_color} />
                          </View>
                        </View>
                        : <Image source={{ uri: this.props.lists.feed_details.image }}
                          style={{ height: 200, width: '100%', flex: 1, borderRadius: 4 }} resizeMode="cover" />}
                    </TouchableOpacity>
                  </ImageBackground>


                </Body>
              </CardItem>
              <View style={{ flexDirection: 'row', marginTop: -8, marginLeft: 13, marginRight: 13 }}>

                <View style={{ flex: 1.2, }}>
                  <View style={{ flexDirection: 'row', }}>
                    <TouchableOpacity
                      activeOpacity={1}
                      style={{ flexDirection: 'row', }}
                      onPress={() => this.getUserLikes()}>
                      <View
                        style={{ height: 42, width: 24, justifyContent: 'center' }}
                      >
                        <Image source={require('../assets/images/icons/like_filled.png')} resizeMode="contain" style={{ width: 22, height: 22, }} />

                      </View>
                      <View
                        activeOpacity={1}
                        style={{ justifyContent: 'center', marginLeft: 5 }}>
                        <Text note style={[font_style.font_medium, { fontSize: 14, color: 'rgba(65,63,64,0.7)', textAlign: 'center', alignSelf: 'center' }]}>{this.props.lists.feed_details.likes < 2 ? 'Like' : 'Likes'} {this.props.lists.feed_details.likes}</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={{ flex: 1, }}>
                  <TouchableOpacity transparent
                    activeOpacity={1}
                    style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', }}
                    onPress={() => this._CommentFeed()}>
                    <View
                      style={{ height: 42, width: 24, justifyContent: 'center' }}
                    >
                      <Image source={require('../assets/images/icons/comment_filled.png')} resizeMode="contain" style={{ width: 22, height: 22 }} />
                    </View>
                    <Text note style={[font_style.font_medium, { fontSize: 14, marginLeft: 5, color: 'rgba(65,63,64,0.7)', textAlign: 'center', }]}>{this.props.lists.feed_details.comments_count < 2 ? 'Comment' : 'Comments'} {this.props.lists.feed_details.comments_count}</Text>
                  </TouchableOpacity>
                </View>
              </View>

              <View style={{ flexDirection: 'row', marginTop: -8 }}>
                <View style={{ marginRight: 10 }}>
                  <View style={{ flexDirection: 'row', }}>
                    <TouchableOpacity
                      activeOpacity={1}
                      style={{ paddingStart: 16, }}
                      onPress={() => this._LikeFeed()}>
                      <View
                        style={{ height: 42, width: 24, justifyContent: 'center', }}
                      >
                        {this.props.lists.feed_details.user_like !== null && this.props.lists.feed_details.user_like !== '' && this.props.lists.feed_details.user_like === Constant.user_like
                          ?
                          (<Image source={require('../assets/images/icons/liked_3x.png')} resizeMode="contain" style={{ width: 18, height: 18, }} />)
                          :
                          (<Image source={require('../assets/images/icons/like_3x.png')} resizeMode="contain" style={{ width: 18, height: 18 }} />)
                        }

                      </View>
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={{}}>
                  <TouchableOpacity transparent
                    activeOpacity={1}
                    style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}
                    onPress={() => this._CommentFeed(index, this.props.lists.feed_details.id)}>
                    <View
                      style={{ height: 42, width: 24, justifyContent: 'center' }}
                    >
                      <Image source={require('../assets/images/icons/comment_new.png')} resizeMode="contain" style={{ width: 18, height: 18 }} />
                    </View>
                    {/* <Text note style={[font_style.font_medium, { fontSize: 14, color: 'rgba(65,63,64,0.7)', textAlign: 'center', }]}>{this.props.lists.feed_details.comments_count} {this.props.lists.feed_details.comments_count < 2 ? 'Comment' : 'Comments'} </Text> */}
                  </TouchableOpacity>
                </View>

                <View style={{ flex: 1 }}></View>

                <View style={{ paddingEnd: 10 }}>
                  <TouchableOpacity transparent
                    activeOpacity={1}
                    style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end' }}
                    onPress={() => this._SharePost()}>
                    <View style={{ height: 42, width: 24, justifyContent: 'center' }} >
                      <Image source={require('../assets/images/icons/share_3x.png')} resizeMode="contain" style={{ width: 18, height: 16, /*tintColor: Colors.primaryColor*/ }} />
                    </View>
                    {/* <Text note style={[font_style.font_medium, { fontSize: 14, color: 'rgba(65,63,64,0.7)', textAlign: 'center', }]}>Share</Text> */}
                  </TouchableOpacity>
                </View>
                {(this.props.lists.feed_details.user.id !== null && this.props.lists.feed_details.user.id !== 'undefind' && this.props.lists.feed_details.user.id === this.state.userInfo.id) ? null :
                  <View style={{ paddingEnd: 16 }}>
                    <TouchableOpacity transparent
                      activeOpacity={1}
                      style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end' }}
                      onPress={() => { this.getReportCategory() }}>
                      <View style={{ height: 42, width: 24, justifyContent: 'center' }} >
                        <Image source={require('../assets/images/icons/report.png')} resizeMode="contain" style={{ width: 20, height: 20, }} />
                      </View>
                      {/* <Text note style={[font_style.font_medium, { fontSize: 14, color: 'rgba(65,63,64,0.7)', textAlign: 'center', }]}>Share</Text> */}
                    </TouchableOpacity>
                  </View>}

              </View>

            </Card>

          </Content>

        </Container >

      )
    }

  }

}

//state map
const mapStateToProps = state => {
  return {
    lists: state.lists,
    details: state.details,
  }
}

const mapDispatchToProps = dispatch => {
  return {

    fetchFeedList: (item) => {
      dispatch(fetchFeedList(item))
    },

    fetchFeedDetails: (item) => {
      dispatch(fetchFeedDetails(item))
    },
  }

}
export default connect(mapStateToProps, mapDispatchToProps)(FeedDetails)


const styles = StyleSheet.create({
  img_view: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2
  },

  imageStyle: { borderRadius: 50 / 2 },
  play_icon: {
    width: 48,
    height: 48,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection: 'column',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  icon_view: {
    //backgroundColor: Colors.primaryColor,
    width: 48, height: 48,
    position: 'absolute',
    top: Platform.OS === 'ios' ? 30 : 10,
    right: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },

  like_icon_view: {
    //backgroundColor: Colors.primaryColor,
    width: 48, height: 48,
    right: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  auth_btn: {
    marginTop: 16,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#fff',
    borderRadius: 50,
    width: '100%',
    height: 54,
    justifyContent: 'center',

  },
  loginText: {
    color: Colors.primaryColor,
    textAlign: 'center',
    alignSelf: 'center',
    textAlign: 'center',
    fontFamily: 'futura-bold',
    fontSize: 14
  },
})