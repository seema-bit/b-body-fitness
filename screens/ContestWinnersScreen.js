import React from 'react';
import { ScrollView, StyleSheet, Image, Text, TouchableOpacity, AsyncStorage, Platform, ImageBackground } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import HeaderBackLeft from '../components/HeaderBackLeft';
import HeaderRight from '../components/HeaderRight';
import Colors from '../constants/Colors';
import { View } from 'native-base';
import Constant from '../constants/Constant';
import { textHeader, font_style, buttonStyle } from '../components/styles';
import HeaderBackground from '../components/HeaderBackground';
import { Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from 'native-base'
import moment from 'moment';
import Loader from '../components/Loader';
import ApiService from '../webservice/ApiService';
import { Searchbar } from 'react-native-paper';
import DataNotFound from '../components/DataNotFound';
import Utils from '../constants/Utils';

export default class ContestWinnersScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.contest_winner}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,
  });

  state = {
    data: null,
    search: '',
    userInfo: '',
    allContest: null
  }
  componentDidMount = () => {
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        var headers = {
          'X-Access-Token': json.access_key,
        }

        ApiService.api.get(ApiService.contest_winners, { headers: headers })
          .then((response) => {
            if (response !== null && response.data != null) {
              if (response.data.status === Constant.SUCCESS) {
                this.setState({
                  allContest: response.data.data != null ? response.data.data : [],

                })
              } else if (response.data.status === Constant.WARNING) {
                if (response.data.session !== undefined && response.data.session !== null) {
                  if (response.data.session === false) {
                    Utils.toastShow(response.data.message)
                    Utils._signOutAsync(this.props.navigation)
                  }
                }
              }
            }

          })
          .catch(function (error) {
            Utils._SentryReport(error)
            console.log("error " + JSON.stringify(error.response));
          })

      })
      .catch(error => console.log('error!'));

  }

  _RedirectToRegister = (status, item) => {
    if (status !== true) {
      this.props.navigation.navigate('ContestRegister', {
        data: item
      })
    }

  }

  render() {
    if (this.state.allContest === undefined || this.state.allContest === null) {
      return (<Loader />)
    } else {

      return (
        <View style={styles.container}>

          {this.state.allContest.length === 0
            ?
            (<DataNotFound />)
            :
            (<Container style={{ backgroundColor: Colors.bg_color, paddingTop: 10 }} >
              <Content >
                {this.state.allContest.map((item, index) => {
                  return (
                    <TouchableOpacity
                      onPress={() => {
                        this.props.navigation.navigate('ContestDetails', {
                          contestId: item.data.id
                        })
                      }}
                      key={index}
                      activeOpacity={1}>

                      <Card style={{
                        flex: 0,
                        borderBottomColor: Colors.light_gray,
                        borderBottomWidth: 1,
                      }} transparent>
                        <CardItem style={{ borderRadius: 4, backgroundColor: Colors.bg_color }}>

                          {item.data.image ?
                            (<ImageBackground source={require('../assets/images/thumbnail.png')}
                              style={{ width: 45, height: 45, borderRadius: 45 / 2 }}
                              imageStyle={{ borderRadius: 45 / 2 }}>
                              <Thumbnail source={{ uri: item.data.image }} style={{ width: 45, height: 45, borderRadius: 45 / 2 }} />
                            </ImageBackground>)
                            :
                            (<Image source={require('../assets/images/thumbnail.png')} style={{ width: 45, height: 45, borderRadius: 45 / 2 }} />)
                          }

                          <Body style={{ marginStart: 16 }}>
                            <Text style={[font_style.font_medium, { color: Colors.dark_gray, fontSize: 16, textTransform: 'capitalize' }]}>{item.data.name}</Text>
                            {/* <Text style={[font_style.font_medium, { color: Colors.dark_gray, fontSize: 16, textTransform: 'capitalize' }]}>{item.data.description}</Text> */}
                            <View style={{ flexDirection: 'row' }}>
                              <Text style={[font_style.font_medium, styles.meta_txt, { flex: 1 }]}>Start Date : {item.data.start_date}</Text>
                              <Text style={[font_style.font_medium, styles.meta_txt, { alignItems: 'flex-end' }]}>End Date : {item.data.end_date}</Text>
                            </View>

                          </Body>

                        </CardItem>

                        <CardItem style={{ borderRadius: 4, backgroundColor: Colors.bg_color }}>
                          <Text>Winner : {item.winner.name}</Text>
                        </CardItem>
                      </Card>
                    </TouchableOpacity>
                  )
                })}
              </Content>
            </Container>
            )
          }

        </View>

      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },
  meta_txt: {
    color: '#A4A4A4',
    fontSize: 12,
    marginTop: 4
  },
  txt_style: {
    fontSize: 14,
    color: Colors.dark_gray,
  },
  details: {
    color: Colors.dark_gray,
    fontSize: 14
  },
  btn_register: {
    height: 46,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: 1, width: 0 },
        shadowOpacity: 0.2,
        shadowRadius: 10,
      },
      android: {
        elevation: 4,
      },
    }),
  },
})