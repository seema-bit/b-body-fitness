import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  Button,
  View,
  ImageBackground,
  AsyncStorage,
} from 'react-native';
import Constant from '../constants/Constant';
import Utils from '../constants/Utils';

export default class SplashScreen extends React.Component {
  static navigationOptions = {
    header: null
  };
  constructor() {
    super();
    this.state = {

      isVisible: true,

    }
  }

  Hide_Splash_Screen = async () => {

    this.setState({
      isVisible: false

    });

    const userLoggedIn = await AsyncStorage.getItem(Constant.LOGGEDIN);
    const userDetails = await AsyncStorage.getItem(Constant.USERINFO);

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(userLoggedIn && userDetails !== null ? 'Main' : 'Login');
    if (userLoggedIn && userDetails !== null) {
      Utils._FoodApiTokenGenerate()
    }

  }

  componentDidMount = async () => {

    this.setState({
      isVisible: false

    });

    const userLoggedIn = await AsyncStorage.getItem(Constant.LOGGEDIN);
    const userDetails = await AsyncStorage.getItem(Constant.USERINFO);

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(userLoggedIn && userDetails !== null ? 'Main' : 'Login');
    if (userLoggedIn && userDetails !== null) {
      Utils._FoodApiTokenGenerate()
    }

  }


  render() {
    return (
      <View />
    );
  }
}