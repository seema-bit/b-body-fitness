import React, { Component } from 'react'
import { ScrollView, StyleSheet, Image, Text, Dimensions, TouchableOpacity, ImageBackground } from 'react-native';
import { View } from 'native-base';
import HeaderLeft from '../components/HeaderLeft';
import Constant from '../constants/Constant';
import Colors from '../constants/Colors';
import Carousel from 'react-native-snap-carousel';
import { textHeader, font_style } from '../components/styles';
import { FlatGrid } from 'react-native-super-grid';
import HeaderBackground from '../components/HeaderBackground';
import HeaderRight from '../components/HeaderRight';
import HeaderBackLeft from '../components/HeaderBackLeft';
import Loader from '../components/Loader';

const horizontalMargin = 10;
const sliderWidth = Dimensions.get('window').width;
const slideWidth = sliderWidth - 80;
const itemWidth = slideWidth + horizontalMargin * 2;
const itemHeight = 200;

export default class PhotoProgressScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.progress}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,
    //headerRight: <HeaderRight navigationProps={navigation} screen='PhotoProgress' />,

  });

  state = {
    entries: [
    ],
    selectedTab: 0,
  }

  componentDidMount() {
    const fetchData = this.props.navigation.getParam('data', null)
    if (fetchData !== undefined && fetchData !== null) {
      this.setState({ entries: fetchData })

    }
  }
  setTab(tab) {
    this.setState({ selectedTab: tab });
    //this.renderScreen(tab)
  }

  _renderItem({ item, index }) {
    return (
      <View key={index} style={[styles.carousal_style, { alignItems: 'center' }]}>

        <View style={{
          width: '100%',
          backgroundColor: '#fff',
          padding: 10,
          height: 380,

        }}>
          <ImageBackground source={require('../assets/images/thumbnail.png')}
            style={{ width: '100%', height: 360, }}>
            <Image source={{ uri: item.image }} style={{ width: '100%', height: 360, }} />
          </ImageBackground>


        </View>
        <View style={[styles.triangle, styles.triangleDown, { alignItems: 'center' }]} />

        <View style={{ alignItems: 'center', marginTop: 30 }}>
          {(item.current_date !== null && item.current_date !== undefined && item.current_date !== '' && item.current_date !== 'undefined') &&
            <Text style={[{ fontSize: 22, color: Colors.dark_gray }, font_style.font_medium]}>{item.current_date}</Text>
          }

          {(item.weight !== null && item.weight !== undefined && item.weight !== '' && item.weight !== 'undefined') ?
            (<Text style={[{ fontSize: 22, color: Colors.dark_gray }, font_style.font_medium]}>{item.weight} kg</Text>)
            :
            (<Text style={[{ fontSize: 22, color: Colors.dark_gray }, font_style.font_medium]}>0 kg</Text>)
          }


        </View>
      </View>
    );
  }

  render() {


    return (
      <View style={styles.container}>
        <View style={{
          flexDirection: 'row', height: 46,
          borderBottomColor: Colors.light_gray,
          borderBottomWidth: 1,
          backgroundColor: '#fff'
        }}>

          <TouchableOpacity onPress={() => this.setTab(0)} style={{ width: '49%', alignItems: 'center', justifyContent: 'center', }}>

            <View style={{ alignSelf: 'center', flexDirection: 'row', }}>
              <Image source={require('../assets/images/icons/large_view_3x.png')} style={[this.state.selectedTab === 0 ? { tintColor: Colors.primaryColor } : { tintColor: Colors.dark_gray }, { width: 22, height: 22, marginRight: 12 }]} />
              <Text style={[this.state.selectedTab === 0 ? { color: Colors.primaryColor } : { color: Colors.dark_gray }, { fontSize: 19, }]}>Large View</Text>
            </View>

          </TouchableOpacity>
          <View style={{
            borderLeftColor: Colors.light_gray,
            borderLeftWidth: 1,
            marginTop: 6,
            marginBottom: 6
          }}>
          </View>
          <TouchableOpacity onPress={() => this.setTab(1)} style={{ width: '49%', alignItems: 'center', justifyContent: 'center', }}>

            <View style={{ alignSelf: 'center', flexDirection: 'row', }}>
              <Image source={require('../assets/images/icons/grid_view_3x.png')} style={[this.state.selectedTab === 1 ? { tintColor: Colors.primaryColor } : { tintColor: Colors.dark_gray }, { width: 22, height: 22, marginRight: 12 }]} />
              <Text style={[this.state.selectedTab === 1 ? { color: Colors.primaryColor } : { color: Colors.dark_gray }, { fontSize: 19, }]}>Grid View</Text>
            </View>

          </TouchableOpacity>
        </View>

        {this.state.selectedTab === 0
          ?
          (<Carousel
            data={this.state.entries}
            renderItem={this._renderItem}
            sliderWidth={sliderWidth}
            itemWidth={itemWidth}
            inactiveSlideOpacity={1}
            inactiveSlideScale={1}
          />)
          :
          (<FlatGrid
            itemDimension={130}
            items={this.state.entries}
            style={styles.gridView}
            renderItem={({ item, index }) => (
              <TouchableOpacity >
                <View style={[styles.itemContainer, { backgroundColor: '#fff', padding: 8 }]}>

                  <ImageBackground source={require('../assets/images/thumbnail.png')}
                    style={{
                      width: '100%',
                      height: 200,

                    }}>
                    <Image source={{ uri: item.image }} style={{
                      width: '100%',
                      height: 200,

                    }} />
                  </ImageBackground>

                </View>
              </TouchableOpacity>
            )}
          />)
        }

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },
  carousal_style: {
    flex: 1,
    margin: 10,
    backgroundColor: Colors.bg_color,

  },
  triangle: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: 16,
    borderRightWidth: 16,
    borderBottomWidth: 16,
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: '#fff',

  },
  triangleDown: {
    transform: [
      { rotate: '180deg' }
    ]
  },
  gridView: {
    marginTop: 10,
    marginBottom: 10,
    flex: 1,
  },
  img_view: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2
  },
})