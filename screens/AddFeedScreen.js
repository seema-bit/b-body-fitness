import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage,
  Keyboard,
  KeyboardAvoidingView,
  Dimensions,
  SafeAreaView,
  Platform,
  StatusBar
} from 'react-native';
import { Icon } from 'react-native-elements'
import { TextInput, ScrollView } from 'react-native-gesture-handler';
import { textInput, buttonStyle, text_color, textHeader, font_style, modalSelector } from '../components/styles';
import Colors from '../constants/Colors';
import Constant from '../constants/Constant';
import HeaderLeft from '../components/HeaderLeft';
import HeaderRight from '../components/HeaderRight';
import HeaderProfileRight from '../components/HeaderProfileRight';

import { Video } from 'expo-av';
import PopupDialog, { DialogContent, DialogButton, DialogTitle } from 'react-native-popup-dialog';
import ModalSelector from 'react-native-modal-selector'
import ApiService from '../webservice/ApiService';
import Utils from '../constants/Utils';
import HeaderBackground from '../components/HeaderBackground';
import HeaderBackLeft from '../components/HeaderBackLeft';
import Spinner from 'react-native-loading-spinner-overlay';
import CustomProgressBar from '../components/CustomProgressBar';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
//import VideoPlayer from 'expo-video-player'
import Modal from 'react-native-modalbox';
import { ScreenOrientation } from 'expo';
import { Ionicons, FontAwesome, MaterialIcons } from '@expo/vector-icons';
import { duration } from 'moment';
import { Camera } from 'expo-camera'
import ImageCrop from "./../components/ImageCropper/ImageCrop";
import * as Random from 'expo-random';
import * as MediaLibrary from 'expo-media-library';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { fetchFeedList } from '../redux/action';
import { Col } from 'native-base';
import VideoPlayer from '../components/ExpoVideoPlayer/dist/VideoPlayer';

const { width } = Dimensions.get("window");
const rows = 3;
const cols = 3;
const widthGrid = (width / cols);
const heightGrid = (width / rows);

class AddFeedScreen extends Component {

  static navigationOptions = ({ navigation }) => ({
    // headerBackground: <HeaderBackground />,
    // headerStyle: textHeader.header_style,
    // headerTitle: <Text style={textHeader.header}>{`${navigation.state.params.title}`.toUpperCase()}</Text>,
    // headerLeft: <TouchableOpacity
    //   onPress={() => {
    //     navigation.state.params.redirect !== undefined &&
    //       navigation.state.params.redirect !== null &&
    //       navigation.state.params.redirect !== '' &&
    //       navigation.state.params.redirect === 'feed_details'
    //       ? (navigation.navigate("FeedDetails", {
    //         refresh: 'yes'
    //       })) :
    //       (navigation.dispatch(NavigationActions.navigate({
    //         routeName: 'Feed',
    //         action: NavigationActions.navigate({ routeName: 'AddFeed' }),
    //       })))

    //   }}
    //   style={textHeader.leftIcon}>
    //   <Image source={require('./../assets/images/icons/arrow-right_3x.png')}
    //     resizeMode="contain" style={{ width: 20, height: 20, alignSelf: 'flex-start' }} />
    // </TouchableOpacity>,
    header: null
    // headerRight: <TouchableOpacity
    //   onPress={() => {
    //     _this !== null ? _this._AddFeed() : navigation.navigate("Feed")
    //   }}
    //   style={textHeader.rightIcon}>
    //   <Image source={require('./../assets/images/icons/tick_3x.png')}
    //     resizeMode="contain" style={{ width: 20, height: 20, alignSelf: 'flex-end' }} />
    // </TouchableOpacity>,
  });

  constructor(props) {
    super(props)
    this.state = {
      cameraIsRecording: false,
      permissionsGranted: false,
      bcolor: 'red',
      text: '',
      image: '',
      visible: false,
      random: 'qwewqrwerw',
      feedData: '',
      imageUpdate: false,
      spinner: false,
      focusText: true,
      uri: null,
      mute: false,
      fullScreen: false,
      shouldPlay: true,
      portrait: true,
      showCustomCamera: false,
      timer: null,
      minutes_Counter: '00',
      seconds_Counter: '00',
      startDisable: false,
      isDoneRecording: false,
      videoRecordFile: null,
      height: width,
      width,
      newImage: null,
      userInfo: '',
      disableBtn: false,
      offset: 0,
      bgColor: Colors.primaryColor

    }
    this.camera = undefined
    this.state = { permissionsGranted: false, bcolor: 'red' }
    this.takeFilm = this.takeFilm.bind(this)

  }


  async componentDidMount() {
    //this.start();
    _this = this;

    var userInfo = JSON.parse(await AsyncStorage.getItem(Constant.USERINFO));
    this.setState({ userInfo });

    let feedData = this.props.navigation.getParam('feedData', '');
    const randomBytes = await Random.getRandomBytesAsync(16);
    this.setState({ random: randomBytes })
    this.setState({ image: '' })
    if (feedData !== null && feedData !== undefined && feedData !== '') {
      this.setState({
        feedData: feedData,
        text: feedData.feed.content,
        image: feedData.feed.image
      })
    }

    let cameraResponse = await Permissions.askAsync(Permissions.CAMERA)
    if (cameraResponse.status == 'granted') {
      let audioResponse = await Permissions.askAsync(Permissions.AUDIO_RECORDING);
      if (audioResponse.status == 'granted') {
        this.setState({ permissionsGranted: true });
      }
    }
  }


  componentWillUnmount() {
    _this = this
    clearInterval(this.state.timer);
  }

  _FetchFeeds = () => {
    console.log('_FetchFeeds')
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    ApiService.api.get(ApiService.fetchAllFeeds, { headers: headers })
      .then((response) => {
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            this.props.fetchFeedList(response.data.data)
            console.log('feed_list', JSON.stringify(this.props.list))

          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        //alert('Error')
        Utils._SentryReport(error)
        console.log("error _FetchFeeds " + JSON.stringify(error.response));
      })
  }


  onButtonStart = () => {

    let timer = setInterval(() => {

      var num = (Number(this.state.seconds_Counter) + 1).toString(),
        count = this.state.minutes_Counter;

      if (Number(this.state.seconds_Counter) == 59) {
        count = (Number(this.state.minutes_Counter) + 1).toString();
        num = '00';
      }

      this.setState({
        minutes_Counter: count.length == 1 ? '0' + count : count,
        seconds_Counter: num.length == 1 ? '0' + num : num
      });
    }, 1000);
    this.setState({ timer });

    //this.setState({ startDisable: true })
  }


  onButtonStop = () => {
    clearInterval(this.state.timer);
    this.setState({ startDisable: false })
  }


  onButtonClear = () => {
    this.setState({
      timer: null,
      minutes_Counter: '00',
      seconds_Counter: '00',
    });
  }

  _pickImageFromGallery = async () => {

    this.setState({ newImage: null })
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

    if (status === 'granted') {
      try {
        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.All,
          //allowsEditing: true,
          //aspect: [16, 9],
        });


        if (!result.cancelled) {
          if (result.type === 'image') {
            this.setState({ newImage: result.uri })
            this.refs.crop_modal.open()
          } else {
            this.setState({ image: result.uri, uri: result, visible: false, imageUpdate: true });
          }

        } else {
        }
      } catch (error) {
        console.log('catch error', error)
      }

    } else {

    }

    //this.handleClick()

  };

  _pickImageFromCamera = async () => {
    this.setState({ newImage: null })
    const { status } = await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL, Permissions.AUDIO_RECORDING);

    if (status === 'granted') {
      let result = await ImagePicker.launchCameraAsync(
        {
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
          //allowsEditing: true,
          aspect: [16, 9]
        })

      if (!result.cancelled) {
        this.setState({ image: result.uri, uri: result, visible: false, imageUpdate: true });
        if (result.type === 'image') {
          this.setState({ newImage: result.uri })
          this.refs.crop_modal.open()
        } else {
          this.setState({ image: result.uri, uri: result, visible: false, imageUpdate: true });
        }
      }

    } else {
      Utils.toastShow('Please check camera permission')
    }
  }

  handleClick() {
    const min = 1, max = 100;
    const rand = min + Math.random() * (max - min);
    this.setState({ random: rand });
  }

  showPopup = () => {
    this.setState({ visible: true });
  }

  takeFilm() {
    if (this.camera) {
      //this.onButtonStart
      this.camera.recordAsync(
        { quality: '4:3' }
      )
        .then(data => (this.setState({ videoRecordFile: data.uri })))
    }
  }


  toggleRecording = () => {
    if (this.state.cameraIsRecording) {
      this._RecordingStop();
    } else {
      this.startRecording();
    }
  }

  startRecording() {
    this.setState({ cameraIsRecording: true, isDoneRecording: false })
    this.takeFilm();
    //this.onButtonStart()
  }

  _RecordingStop() {
    this.setState({ cameraIsRecording: false, isDoneRecording: true })
    //this.onButtonStop
    this.camera.stopRecording();
  }

  CancelVideoRecording() {
    this.refs.camera_modal.close()
    this.setState({ cameraIsRecording: false, isDoneRecording: false, videoRecordFile: null })
  }

  _UploadVideoFromCamera = () => {
    return (
      <Modal
        backButtonClose={true}
        swipeToClose={true}
        coverScreen={true}
        backdropPressToClose={true}
        animationIn="fadeIn"
        animationOut="fadeOut"
        hideModalContentWhileAnimating={true}
        animationInTiming={1500}
        animationOutTiming={1500}
        backdropTransitionOutTiming={0}
        position={"center"} ref={"camera_modal"} style={{ margin: 0, }}  >

        <View style={{ flex: 1 }}>

          {this.state.isDoneRecording ? (
            <View style={{
              flex: 1, backgroundColor: Colors.black_color,
              justifyContent: 'center', height: 200
            }}>
              <VideoPlayer
                videoProps={{
                  shouldPlay: false,
                  resizeMode: Video.RESIZE_MODE_CONTAIN,
                  source: {
                    uri: this.state.videoRecordFile
                  },
                  isMuted: false,

                  playThroughEarpieceAndroid: true,
                }}
                errorCallback={error => console.error('Error: ', error.message, error.type, error.obj)}
                sliderColor={Colors.dark_gray}
                showControlsOnLoad={true}
                showFullscreenButton={false}
                isPortrait={this.state.portrait}
                playFromPositionMillis={0}
                switchToLandscape={() => this.changeScreenOrientation("0")}
                switchToPortrait={() => this.changeScreenOrientation("1")}
                playFromPositionMillis={0}
              />
            </View>

          ) :
            (<View style={{ flex: 1 }}>
              <Camera ref={ref => this.camera = ref} style={{ flex: 1 }} ></Camera>
            </View>
            )}

          <View style={styles.footer}>
            <TouchableOpacity
              style={styles.left}
              onPress={() => { this.CancelVideoRecording() }}
            >
              <Ionicons name="md-close" size={28} />
            </TouchableOpacity>


            <TouchableOpacity
              style={[styles.recordButton, styles.center]}
              onPress={() => { this.toggleRecording() }}
            >
              {this.state.cameraIsRecording ? (
                <MaterialIcons name="stop" size={48}
                  color={Colors.recordVideo.recording}

                />
              ) : (
                  <View style={styles.record} />
                )}
            </TouchableOpacity>



            {/* {this.state.cameraIsRecording && (
            <TouchableOpacity
              style={styles.left}
              onPress={this._RecordingStop()}
            >
              <Text style={styles.cancel}> Cancel </Text>
            </TouchableOpacity>)}

          <TouchableOpacity
            style={[styles.recordButton, styles.center]}
            onPress={this.startRecording()}
          >
            {this.state.cameraIsRecording ? (
              <Ionicons name="md-add" size={32}
                color={Colors.recordVideo.recording}

              />
            ) : (
                <View style={styles.record} />
              )}
          </TouchableOpacity>
*/}
            <View style={styles.right}>
              {/* {this.state.cameraIsRecording && (
                <Text style={styles.duration}>{this.state.minutes_Counter} : {this.state.seconds_Counter}</Text>


              )} */}
              {this.state.isDoneRecording && (
                <TouchableOpacity style={{
                  width: 40,
                  height: 40,
                  borderRadius: 40 / 2,
                  borderColor: Colors.black_color,
                  borderWidth: 1,
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
                  onPress={() => {
                    this.refs.camera_modal.close()
                    this.setState({
                      isDoneRecording: false,
                      image: this.state.videoRecordFile,
                      uri: { uri: this.state.videoRecordFile, type: 'video' },
                      videoRecordFile: null,
                      cameraIsRecording: false
                    })
                  }}>
                  <Ionicons name="md-checkmark" size={28}

                  />
                </TouchableOpacity>
              )}
            </View>

          </View>
          <SafeAreaView />
        </View>

      </Modal>
    )
  }

  showImageSelectDialog = (key) => {
    if (key !== null && key !== '') {
      if (key === 0) {
        this._pickImageFromCamera()
      } else if (key === 2) {
        this._pickImageFromGallery()
      } else if (key === 1) {
        //this.setState({ showCustomCamera: true })
        this.refs.camera_modal.open()
      }
    }
  }


  async _StoreVideoImageInGallery(image, name) {

    const asset = await MediaLibrary.createAssetAsync(image);
    const album = await MediaLibrary.getAlbumAsync('BBody');
    if (album === null) {
      let albumCreated = await MediaLibrary.createAlbumAsync(
        'BBody',
        asset,
        false
      );
      if (albumCreated) {
        this.getAssetFromAlbum(albumCreated);
      } else {
        //handle album creation error
      }

    } else {
      let assetAdded = await MediaLibrary.addAssetsToAlbumAsync(
        [asset],
        album,
        false
      );
      if (assetAdded === true) {
        this.getAssetFromAlbum(album);
      } else {
        // handle asset add error
      }
    }

  }

  _AddFeed = () => {
    if (this.state.text !== undefined && this.state.text !== null && this.state.text !== '') {

      if (this.state.image !== null && this.state.image !== '') {
        this.setState({ spinner: true })
        //this.handleClick()
        const min = 1, max = 100;
        const rand = min + Math.random() * (max - min);
        AsyncStorage.getItem(Constant.USERINFO)
          .then(req => JSON.parse(req))
          .then(json => {
            var params = new FormData();
            params.append('user_id', json.id);
            params.append('content', this.state.text);

            let ext = this.state.image.split(".");

            if (this.state.image !== null || this.state.imageUpdate === true) {
              params.append('image',
                { uri: this.state.image, name: `${rand}` + '.' + `${ext[ext.length - 1]}`, type: `image/jpg` });
            } else {
              params.append('image')
            }
            if (this.state.uri !== undefined && this.state.uri !== null) {
              this._StoreVideoImageInGallery(this.state.image, `${rand}` + '.' + `${ext[ext.length - 1]}`)
            }

            var headers = {
              'X-Access-Token': json.access_key,
            }


            let editFeed = this.props.navigation.getParam('editFeed', '')

            let ApiCall = ''

            if (editFeed !== null && editFeed !== undefined && editFeed !== '') {
              ApiCall = ApiService.api.post(ApiService.editFeed + this.state.feedData.feed.id, params, { headers: headers })
            } else {
              ApiCall = ApiService.api.post(ApiService.addFeed, params, { headers: headers })
            }
            ApiCall.then((response) => {
              this.setState({ spinner: false })
              if (response !== null && response.data != null) {
                Utils.toastShow(response.data.message)
                if (response.data.status === Constant.TRUE) {
                  Utils.toastShow(response.data.message)

                  if (this.props.navigation.state.params.redirect !== undefined &&
                    this.props.navigation.state.params.redirect !== null &&
                    this.props.navigation.state.params.redirect !== '' &&
                    this.props.navigation.state.params.redirect === 'feed_details') {
                    this.props.navigation.navigate("FeedDetails", {
                      refresh: 'yes'
                    })
                  } else {
                    this.props.navigation.navigate("Feed", {
                      refresh: 'yes'
                    })
                  }

                  if (editFeed !== null && editFeed !== undefined && editFeed !== '') {
                    this.props.lists.feed_list.forEach((element, index) => {
                      if (element.feed.id === this.state.feedData.feed.id) {
                        element.feed.content = this.state.text
                        if (this.state.uri !== undefined && this.state.uri !== null) {
                          element.feed.image = response.data.data.image
                          element.feed.type = this.state.uri.type
                        }

                        const newallFeed = [...this.props.lists.feed_list.slice(0, index), element, ...this.props.lists.feed_list.slice(index + 1)]
                        this.props.fetchFeedList(newallFeed)
                      }
                    });
                  } else {
                    this._FetchFeeds();
                  }

                } else if (response.data.status === Constant.WARNING) {
                  //alert(response.data.message)
                  if (response.data.session !== undefined && response.data.session !== null) {
                    if (response.data.session === false) {
                      Utils._signOutAsync(this.props.navigation)
                    }
                  }
                }
              }

            })
              .catch(function (error) {
                this.setState({ spinner: false })
                //alert('Error')
                console.log("error " + JSON.stringify(error.response));
                Utils._SentryReport(error)
              })
          })
          .catch(error1 => {
            console.log('error!', error1.response)
            Utils._SentryReport(error1)
          });

      } else {
        Utils.toastShow("Please upload image/video")
      }
    } else {
      Utils.toastShow("Content required")
    }


  }

  _CloseKeyboard = () => {
    this.setState({ focusText: true })
    Keyboard.dismiss()
  }


  renderVideoModal() {
    return (
      <Modal
        backButtonClose={true}
        swipeToClose={true}
        coverScreen={true}
        backdropPressToClose={true}
        animationIn="fadeIn"
        animationOut="fadeOut"
        hideModalContentWhileAnimating={true}
        animationInTiming={1500}
        animationOutTiming={1500}
        backdropTransitionOutTiming={0}
        position={"center"} ref={"modal2"} style={{ margin: 0, }}  >
        <StatusBar barStyle="dark-content" />
        <View style={{ flex: 1, }}>
          <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>

            <VideoPlayer
              height={250}
              hideControlsTimerDuration={6000}
              videoProps={{
                shouldPlay: true,
                resizeMode: Video.RESIZE_MODE_CONTAIN,
                source: {
                  uri: this.state.image
                },
                isMuted: false,

                playThroughEarpieceAndroid: true,
              }}
              errorCallback={error => console.error('Error: ', error.message, error.type, error.obj)}
              sliderColor={Colors.dark_gray}
              showControlsOnLoad={true}
              showFullscreenButton={false}
              isPortrait={this.state.portrait}
              playFromPositionMillis={0}
              switchToLandscape={() => this.changeScreenOrientation("0")}
              switchToPortrait={() => this.changeScreenOrientation("1")}
              playFromPositionMillis={0}
            />

          </View>
          <TouchableOpacity
            onPress={() => this.refs.modal2.close()}
            activeOpacity={1}
            style={styles.icon_view}
          >

            <Image source={require('../assets/images/icons/close_3x.png')}
              style={{ width: 20, height: 20, tintColor: Colors.black_color, }} />
          </TouchableOpacity>
        </View>
      </Modal>

    )
  }

  capture() {
    this.refs.cropper.crop().then(result => {

      this.setState({ image: result.uri, uri: result, visible: false, imageUpdate: true });
      this.refs.crop_modal.close();
      //this.setState({ newImage: null })
    });

  }

  _CropImage() {
    return (
      <Modal
        backButtonClose={true}
        swipeToClose={false}
        coverScreen={true}
        backdropPressToClose={true}
        animationIn="fadeIn"
        animationOut="fadeOut"
        hideModalContentWhileAnimating={true}
        animationInTiming={1500}
        animationOutTiming={1500}
        backdropTransitionOutTiming={0}
        position={"center"} ref={"crop_modal"} style={{ margin: 0, }}  >
        <View style={styles.crop_container}>
          <SafeAreaView />
          <View style={{ width: width, height: 48, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity
              style={[textHeader.leftIcon, { flex: 1, justifyContent: 'center' }]}
              onPress={() => this.refs.crop_modal.close()}
            >

              <Image source={require('./../assets/images/icons/arrow-right_3x.png')} style={{ width: 11, height: 20, justifyContent: 'center' }} />
            </TouchableOpacity>
            <Text style={[styles.done, { textAlign: 'center', color: 'white', flex: 1, }]} >Edit Photo</Text>
            <TouchableOpacity
              style={{ flex: 1, alignItems: 'flex-end', paddingEnd: 20 }}
              onPress={() => {
                this.capture();
              }}
            >
              <Text style={styles.done}>Done</Text>
            </TouchableOpacity>
          </View>

          <View style={{ justifyContent: 'center', flex: 1, alignItems: 'center' }}>

            {this.state.newImage !== null && <ImageCrop
              style={{ width: this.state.height, height: this.state.height, alignItems: 'center', backgroundColor: 'blue' }}
              ref={"cropper"}
              image={this.state.newImage}
              cropHeight={Dimensions.get('screen').width}
              cropWidth={Dimensions.get('screen').width}
            />}

            <View style={styles.sectionContainer} pointerEvents='none'>
              <View style={styles.boxContainer} />
              <View style={styles.boxContainer} />
              <View style={styles.boxContainer} />
              <View style={styles.boxContainer} />
              <View style={styles.boxContainer} />
              <View style={styles.boxContainer} />
              <View style={styles.boxContainer} />
              <View style={styles.boxContainer} />
              <View style={styles.boxContainer} />
            </View>
          </View>

        </View>
      </Modal>
    )
  }


  changeScreenOrientation = (type) => {
    if (type === '0') {
      ScreenOrientation.allowAsync(ScreenOrientation.Orientation.LANDSCAPE);
      this.setState({ portrait: false })
    } else {
      ScreenOrientation.allowAsync(ScreenOrientation.Orientation.PORTRAIT);
      this.setState({ portrait: true })
    }


  }
  playVideo = (url) => {
    this.refs.modal2.open()
  }

  onScroll = (event) => {
    var currentOffset = event.nativeEvent.contentOffset.y;
    var direction = currentOffset > this.state.offset ? 'down' : 'up';
    console.log('currentOffset : ' + currentOffset + " && this.state.offset : " + this.state.offset)
    this.setState({
      bgColor: currentOffset > this.state.offset ? Colors.white_color : Colors.primaryColor,
      offset: currentOffset
    })
    //this.offset = currentOffset;
    console.log(direction);
  }

  CustomHearder = () => {
    return (
      <View style={[{ backgroundColor: Colors.primaryColor, },]}>
        <SafeAreaView style={styles.adroidSafeArea} />
        <View style={[{ backgroundColor: Colors.primaryColor, justifyContent: 'center', }, textHeader.header_style]}>

          <View style={{ flexDirection: 'row', }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.state.params.redirect !== undefined &&
                  this.props.navigation.state.params.redirect !== null &&
                  this.props.navigation.state.params.redirect !== '' &&
                  this.props.navigation.state.params.redirect === 'feed_details'
                  ? (this.props.navigation.navigate("FeedDetails", {
                    refresh: 'yes'
                  })) :
                  (this.props.navigation.dispatch(NavigationActions.navigate({
                    routeName: 'Feed',
                    action: NavigationActions.navigate({ routeName: 'AddFeed' }),
                  })))

              }}
              style={[textHeader.leftIcon]}>
              <Image source={require('./../assets/images/icons/arrow-right_3x.png')}
                resizeMode="contain" style={{ width: 20, height: 20, alignSelf: 'flex-start' }} />
            </TouchableOpacity>
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <Text style={[textHeader.header, { textAlign: 'center', }]}>{`${this.props.navigation.state.params.title}`.toUpperCase()}</Text>
            </View>


          </View>
        </View>

      </View>

    )
  }
  render() {
    let index = 0;
    const data = [
      { key: index++, label: 'Take a Photo...' },
      { key: index++, label: 'Take a Video...' },
      { key: index++, label: 'Choose from Library...' },

    ];


    return (


      <View style={{ flex: 1 }}>
        {this.renderVideoModal()}
        {this._UploadVideoFromCamera()}
        {this._CropImage()}
        <CustomProgressBar spinner={this.state.spinner} />
        {this.CustomHearder()}
        <View style={{ flex: 1, backgroundColor: Colors.bg_color }}>
          <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', backgroundColor: Colors.bg_color, }} behavior="padding" enabled keyboardVerticalOffset={110} >
            <ScrollView style={styles.container}
              ref={ref => this.scrollView = ref}
              keyboardShouldPersistTaps="always"
              onScroll={event => this.onScroll(event)}
              showsVerticalScrollIndicator={false}
              scrollEventThrottle={16}
            //style={{ backgroundColor: this.state.bgColor }}
            >
              <View activeOpacity={1} style={{ flex: 1, backgroundColor: Colors.bg_color }}>
                <View style={{ flex: 1, backgroundColor: Colors.bg_color }}>

                  <View style={{ backgroundColor: Colors.primaryColor, height: 270, paddingTop: 30 }} >

                    {this.state.image !== null && this.state.image !== '' ? (
                      <View style={styles.header}>
                        {this.state.uri !== undefined && this.state.uri !== null && this.state.uri.type !== undefined && this.state.uri.type !== null && this.state.uri.type === 'video' ?
                          (<TouchableOpacity
                            onPress={() => { this.playVideo(this.state.image) }}
                            style={{ height: 200, width: '100%', flex: 1, justifyContent: 'center' }}>
                            <Video
                              style={{ height: 200, width: '100%', flex: 1, borderRadius: 25 }}
                              source={{ uri: this.state.image }}
                              usePoster
                              shouldPlay={false}
                              resizeMode="cover"
                            />
                            <View style={styles.play_icon}>

                              <MaterialIcons name="play-circle-outline" size={48} color={Colors.black_color} />
                            </View>
                          </TouchableOpacity>)
                          : (<Image source={{ uri: this.state.image }}
                            style={{ height: 200, width: '100%', flex: 1, borderRadius: 25 }} resizeMode="cover" />)
                        }


                      </View>
                    ) : (
                        <View style={styles.header}>
                          <Image source={require('../assets/images/icons/image_placeholder_3x.png')} style={{ width: 110, height: 110 }} resizeMode="contain" />
                        </View>
                      )
                    }

                  </View>

                  <View style={{ marginTop: 54, }}>
                    <View style={{ backgroundColor: Colors.white_color, height: 270, borderRadius: 5, margin: 20, marginTop: 0, padding: 16, justifyContent: 'flex-start' }}>
                      <TextInput
                        style={[{ fontSize: 16, flex: 1, textAlignVertical: 'top' }, font_style.font_medium]}
                        onChangeText={(text) => this.setState({ text })}
                        value={this.state.text}
                        multiline={true}
                        placeholder='What is on your mind?'
                        autoCapitalize='none'
                        onFocus={() => {
                          //console.log("scrollViewPosition", this.state.scrollViewPosition)
                          //this.scrollView.scrollToEnd({ animated: true });
                          this.scrollView.scrollTo({ x: 0, y: 300 });
                        }}
                      />
                    </View>
                  </View>
                </View>


                <View style={{ right: 70, left: 70, position: 'absolute', top: 243 }}>
                  <ModalSelector
                    cancelText="Cancel"
                    touchableActiveOpacity={1}
                    data={data}
                    onChange={option => { this.showImageSelectDialog(option.key) }}
                    overlayStyle={modalSelector.overlayStyle}
                    optionContainerStyle={modalSelector.optionContainerStyle}

                    optionStyle={modalSelector.optionStyle}
                    optionTextStyle={modalSelector.optionTextStyle}

                    cancelStyle={modalSelector.cancelStyle}
                    cancelTextStyle={modalSelector.cancelTextStyle}
                  >
                    <View style={[buttonStyle.auth_btn, styles.photo_video_btn]}>
                      <Text style={[{ textAlign: 'center', fontSize: 16, color: '#909193' }, font_style.font_medium]}>Add Photo/Video</Text>
                    </View>
                  </ModalSelector>
                </View>

                <View style={{ marginStart: 20, marginEnd: 20, flex: 1 }}>
                  <TouchableOpacity
                    onPress={() => { Keyboard.dismiss(), this._AddFeed() }}
                    activeOpacity={1}

                    style={[buttonStyle.auth_btn, { backgroundColor: Colors.primaryColor, marginTop: 0, marginBottom: 20 }]}
                    disabled={this.state.disableBtn}>
                    <Text style={[buttonStyle.loginText, { color: Colors.white_color }]}>{'Post'.toUpperCase()}</Text>
                  </TouchableOpacity>
                </View>


              </View>

            </ScrollView>
          </KeyboardAvoidingView>
        </View>

        <SafeAreaView />
      </View>

    )
  }
}

const mapStateToProps = state => {
  return {
    lists: state.lists
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchFeedList: (item) => {
      dispatch(fetchFeedList(item))
    },
  }

}
export default connect(mapStateToProps, mapDispatchToProps)(AddFeedScreen)


const styles = StyleSheet.create({
  container: {
    flex: 1


  },
  header: {

    marginStart: 70, marginEnd: 70,
    justifyContent: 'center',
    alignItems: 'center',
    height: 170,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: Colors.white_color,
    borderStyle: "dashed"

  },

  avatar: {
    width: 110,
    height: 110,
    borderRadius: 57,
    borderWidth: 2,
    borderColor: "white",
    marginBottom: 10,
  },
  name: {
    fontSize: 22,
    fontWeight: '600',
  },
  body: {
    flex: 1,

    paddingTop: 10,
    marginBottom: 100
  },

  bodyContent: {

    paddingStart: 24,
    paddingEnd: 24,
    flexDirection: 'row',
    flexWrap: 'wrap'

  },

  text_style: {
    marginTop: 16,
    color: Colors.txt_gray,
    paddingStart: 16,
    fontFamily: 'futura-medium',
    fontWeight: 'bold',

  },
  textInputMain: {
    flexDirection: 'row',
    borderRadius: 50,
    borderWidth: 1,
    borderColor: Colors.light_gray,
    width: '100%',
    height: 46,
    marginTop: 6,
    justifyContent: 'center',
  },
  inputStyle: {
    flex: 1,
    paddingStart: 16,
    fontFamily: 'futura-medium',
    fontWeight: 'bold',
    alignSelf: 'center'

  },
  edit_icon_view: {
    width: 32,
    height: 32,
    borderRadius: 50,
    backgroundColor: 'white',
    shadowOffset: { width: 1, height: 1, },
    shadowColor: 'black',
    shadowOpacity: .4,
    alignSelf: 'center',
    marginRight: 10,
    justifyContent: 'center'
  },
  // cross_icon_view: {
  //   width: 32,
  //   height: 32,
  //   position: 'absolute',
  //   backgroundColor: 'white',
  //   borderRadius: 32 / 2,
  //   backgroundColor: 'white',
  //   zIndex: 10,
  //   alignSelf: 'flex-end',
  //   justifyContent: 'flex-end'
  // },
  cross_icon_view: {
    width: 32,
    height: 32,
    position: 'absolute',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    bottom: 16,
    right: 16,
  },

  circle_view: {
    width: 32,
    height: 32,
    backgroundColor: 'white',
    borderRadius: 50,
    backgroundColor: 'white',
    shadowOffset: { width: 1, height: 1, },
    shadowColor: 'black',
    shadowOpacity: .4,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  play_icon: {
    width: 48,
    height: 48,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection: 'column',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  inputContainer: {
    flex: 1,
    backgroundColor: Colors.white_color,
    alignItems: 'center',
  },
  innerContainer: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  camera: {
    width: Dimensions.get('screen').width,
    height: (Dimensions.get('screen').width / 3) * 4,
  },
  absolute: {
    ...StyleSheet.absoluteFillObject,
  },
  footer: {
    backgroundColor: Colors.bg_color,
    height: 85,
    flexDirection: 'row',
    alignItems: 'center',
  },
  center: {
    position: 'absolute',
    left: (Dimensions.get('screen').width / 2) - 36,
  },
  left: {
    position: 'absolute',
    left: 20,
    width: 40,
    height: 40,
    borderRadius: 40 / 2,
    borderColor: Colors.black_color,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  right: {
    position: 'absolute',
    right: 20,
  },
  duration: {
    color: Colors.recordVideo.durationText,
    fontSize: 18,
  },
  cancel: {
    color: Colors.recordVideo.cancelText,
    fontSize: 18,
    fontWeight: '500',
  },
  cancelCross: {
    position: 'absolute',
    left: 15,
    top: 24,
  },
  recordButton: {
    height: 72,
    width: 72,
    borderWidth: 5,
    borderColor: Colors.recordVideo.recordButtonBorder,
    borderRadius: 36,
    justifyContent: 'center',
    alignItems: 'center',
  },
  record: {
    height: 56,
    width: 56,
    borderRadius: 28,
    backgroundColor: Colors.recordVideo.recording,
  },
  smallCircle: {
    height: 8,
    width: 8,
    borderRadius: 4,
    marginRight: 2,
    backgroundColor: Colors.recordVideo.recording,
  },
  inputStyle: {
    marginTop: 20,
    paddingBottom: 5,
    color: Colors.primaryColor,
    fontSize: 18,
    fontWeight: '600',
    textAlign: 'center',
    width: '90%',
    borderBottomWidth: 2,
    borderBottomColor: Colors.primaryColor,
  },
  submitText: {
    fontSize: 18,
    fontWeight: '600',
    color: Colors.white_color,
  },
  submitButton: {
    marginTop: 25,
    height: 50,
    width: Dimensions.get('screen').width / 2,
    backgroundColor: Colors.primaryColor,
    borderRadius: 25,
  },

  //crop image
  crop_container: { flex: 1, backgroundColor: 'black', paddingBottom: 20 },
  originalImage: { height: width / rows, width: width / cols, position: 'absolute', bottom: 0 },
  doneContainer: { width: 100, borderColor: 'white', borderWidth: 2, alignItems: 'center', marginTop: 20 },
  done: { fontSize: 20, color: 'white' },
  sectionContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
  boxContainer: {
    width: widthGrid,
    height: heightGrid,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'white',
    borderWidth: 0.5,
    backgroundColor: 'transparent',
  },

  photo_video_btn: {
    marginTop: 0, shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  icon_view: {
    //backgroundColor: Colors.primaryColor,
    width: 48, height: 48,
    position: 'absolute',
    top: Platform.OS === 'ios' ? 30 : 10,
    right: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  adroidSafeArea: {
    flex: 1,
    paddingTop: Platform.OS === 'android' ? 25 : 0
  }
});