import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage,
  KeyboardAvoidingView,
  Keyboard
} from 'react-native';
import { Icon } from 'react-native-elements'
import { TextInput, ScrollView } from 'react-native-gesture-handler';
import { textInput, buttonStyle, text_color, textHeader, font_style } from '../components/styles';
import Colors from '../constants/Colors';
import Constant from '../constants/Constant';
import HeaderBackLeft from '../components/HeaderBackLeft';
import HeaderRight from '../components/HeaderRight';
import HeaderProfileRight from '../components/HeaderProfileRight';

import ModalSelector from 'react-native-modal-selector'
import ApiService from '../webservice/ApiService';
import Utils from '../constants/Utils';
import HeaderBackground from '../components/HeaderBackground';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button'
import Spinner from 'react-native-loading-spinner-overlay'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CustomProgressBar from '../components/CustomProgressBar';
import moment from 'moment';
import { connect } from 'react-redux';
import { fetchIntake } from '../redux/action';

class BMRCalculationScreen extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.bmr_calculation}`.toUpperCase()}</Text>,

    headerLeft:
      <View>
        {(typeof navigation.state.params.from != "undefined" !== undefined && navigation.state.params.from !== null && navigation.state.params.from === 'setting') && (<TouchableOpacity
          onPress={() => { navigation.navigate('NotificationSetting') }}
          style={textHeader.leftIcon}>
          <Image source={require('./../assets/images/icons/arrow-right_3x.png')} style={{ width: 11, height: 20, alignSelf: 'flex-start' }} />
        </TouchableOpacity>)}
      </View>
    ,
    headerRight: <TouchableOpacity
      onPress={() => { navigation.navigate((typeof navigation.state.params.from != "undefined" !== undefined && navigation.state.params.from !== null && navigation.state.params.from === 'setting') ? 'NotificationSetting' : 'Feed') }}
      style={{
        justifyContent: 'center',
        paddingRight: 20,
      }}
      activeOpacity={1}>
      <Text style={[{ color: '#fff', }, font_style.font_medium]}>Skip</Text>
    </TouchableOpacity>,

  });

  state = {
    gender: '',
    weight: 0,
    goal_weight: 0,
    height_feet: 0,
    height_inches: 0,
    age: 0,
    calculation: '',
    selected: 0,
    spinner: false,
    disableBtn: false,
    protein: '',
    fat: '',
    carbs: '',
    userInfo: null
  }

  componentDidMount = () => {
    _this = this
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        // var feet = ''
        // var inches = ''
        // if (json.height !== 'null' && json.height !== null) {
        //   var realFeet = ((json.height) / 144);
        //   feet = Math.floor(realFeet);
        //   inches = Math.round((realFeet - feet) * 12);
        // }

        this.setState({
          userInfo: json,
          protein: json.protein !== 'null' && json.protein !== null ? json.protein : '',
          fat: json.fat !== 'null' && json.fat !== null ? json.fat : '',
          carbs: json.carbs !== 'null' && json.carbs !== null ? json.carbs : ''
          // weight: json.current_weight !== 'null' && json.current_weight !== null ? json.current_weight : '',
          // goal_weight: json.weight_goal !== 'null' && json.weight_goal !== null ? json.weight_goal : '',
          // age: json.age !== 'null' && json.age !== null ? json.age : '',
          // height_feet: feet.toString(),
          // height_inches: inches.toString(),
          // selected: json.gender !== 'null' && json.gender !== null ? json.gender : '',
          // gender: json.gender !== 'null' && json.gender !== null && json.gender === 0 ? 'male' : 'female'
        })

      })
      .catch((error) => {
        //this.setState({ disableBtn: false, spinner: false })
        console.log('error!' + JSON.stringify(error.response))
      });
  }
  // _addBodyMass = () => {
  //   let msg = '';
  //   if (this.state.selected !== null && this.state.selected !== '') {
  //     if (this.state.weight !== null && this.state.weight !== '') {
  //       if (this.state.goal_weight !== null && this.state.goal_weight !== '') {
  //         if (this.state.height_feet !== null && this.state.height_feet !== '') {
  //           if (Utils.heightValidation(this.state.height_feet)) {
  //             if (this.state.age !== null && this.state.age !== '') {
  //               this.setState({ disableBtn: true, spinner: true })
  //               this.networkCall()
  //             } else {
  //               msg = 'Age required'
  //               Utils.toastShow(msg)
  //             }
  //           } else {
  //             msg = 'Enter a valid height value'
  //             Utils.toastShow(msg)
  //           }

  //         } else {
  //           msg = 'Height required'
  //           Utils.toastShow(msg)
  //         }

  //       } else {
  //         msg = 'Goal weight required'
  //         Utils.toastShow(msg)
  //       }
  //     } else {
  //       msg = 'Current weight required'
  //       Utils.toastShow(msg)
  //     }
  //   } else {
  //     msg = 'Select gender'
  //     Utils.toastShow(msg)
  //   }

  // }

  _addInput = () => {
    if (this.state.protein !== null && this.state.protein !== '') {
      if (this.state.carbs !== null && this.state.carbs !== '') {
        if (this.state.fat !== null && this.state.fat !== '') {
          this.setState({ disableBtn: true, spinner: true })
          this.networkCall()
        } else {
          Utils.toastShow('Fat is required')
        }
      } else {
        Utils.toastShow('Carbs is required')
      }
    } else {
      Utils.toastShow('Protein is required')
    }
  }
  checkHeightInches = (height_inches) => {
    if (height_inches > 11) {
      var realFeet = (height_inches / 12);
      feet = Math.floor(realFeet);
      inches = Math.round((realFeet - feet) * 12);
      const ht_feet = this.state.height_feet !== undefined && this.state.height_feet !== null && this.state.height_feet !== '' ? this.state.height_feet : 0
      var height_ft = parseInt(ht_feet) + parseInt(feet)
      this.setState({ height_feet: height_ft.toString(), height_inches: inches.toString() })
    } else {
      this.setState({ height_inches })
    }

  }

  fetchIntake = () => {

    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }
    const params = {
      date: moment().format('MM-DD-YYYY')
    }


    ApiService.api.post(ApiService.cal_intake, params, { headers: headers })
      .then((response) => {

        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {

            if (response.data.data !== undefined && response.data.data !== null) {

              this.props.fetch_intake(response.data.data)
              console.log('fetchIntake', this.props.data.fetchIntake)
            }
          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("intake error " + JSON.stringify(error.response));
      })
  }

  networkCall = () => {

    // var height = parseInt(this.state.height_feet) * 12.000 + parseInt(this.state.height_inches)
    // this.setState({ height: height })
    // var params = {
    //   gender: this.state.selected,
    //   current_weight: this.state.weight,
    //   weight_goal: this.state.goal_weight,
    //   height: parseInt(height),
    //   age: this.state.age,

    // }
    var params = {
      protein: this.state.protein,
      fat: this.state.fat,
      carbs: this.state.carbs
    }
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    console.log('headers : ', headers)
    ApiService.api.post(ApiService.setGoal, params, { headers: headers })
      .then((response) => {
        console.log('response : ', response)
        this.setState({ disableBtn: false, spinner: false })
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            this.fetchIntake();
            const newObj = { ...this.state.userInfo }
            // newObj.age = this.state.age
            // newObj.gender = this.state.selected
            // newObj.current_weight = this.state.weight
            // newObj.weight_goal = this.state.goal_weight
            // newObj.height = this.state.height * 12.000
            newObj.protein = this.state.protein
            newObj.fat = this.state.fat
            newObj.carbs = this.state.carbs

            AsyncStorage.setItem(Constant.USERINFO, JSON.stringify(newObj));

            Utils.toastShow(response.data.message)
            const getParam = this.props.navigation.getParam('from', null);
            if (typeof getParam != "undefined" !== undefined && getParam !== null && getParam === 'setting') {
              this.props.navigation.navigate("NotificationSetting")
            } else {
              this.props.navigation.navigate("Feed")
            }


          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        //this.setState({ disableBtn: false, spinner: false })
        console.log("error " + JSON.stringify(error.response));
      })


  }

  render() {

    return (

      <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', backgroundColor: Colors.bg_color, paddingBottom: 10 }} behavior="padding" enabled keyboardVerticalOffset={100} >
        <ScrollView style={styles.container} keyboardShouldPersistTaps="always">
          <CustomProgressBar spinner={this.state.spinner} />

          <View style={[styles.container, { marginTop: 20 }]}>
            {/* <View style={{ flexDirection: 'row', }}>
              <Text style={[{
                textAlign: 'center',
                marginStart: 8,
                color: Colors.txt_gray,
                paddingStart: 16,
                fontFamily: 'futura-medium',
                alignSelf: 'center'
              }]}>Gender</Text>
              <RadioGroup
                style={{ marginStart: 8, flexDirection: 'row', alignItems: 'flex-start' }}
                selectedIndex={0}
                onSelect={(index, value) => { this.setState({ gender: value, selected: index }) }}
              >
                <RadioButton value={'male'} >
                  <Text>Male</Text>
                </RadioButton>

                <RadioButton value={'female'}>
                  <Text>Female</Text>
                </RadioButton>
              </RadioGroup>
            </View> */}

            <View style={styles.bodyContent}>
              {/* <View>

              </View>

              <Text style={styles.text_style}>Current Weight</Text>
              <View style={styles.textInputMain}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(weight) => this.setState({ weight })}
                  value={this.state.weight}
                  placeholder="Current Weight in pounds"
                  placeholderTextColor={Colors.dark_gray}
                  keyboardType={'numeric'}
                />
              </View>


              <Text style={styles.text_style}>Goal Weight</Text>
              <View style={styles.textInputMain}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(goal_weight) => this.setState({ goal_weight })}
                  value={this.state.goal_weight}
                  placeholder="Goal Weight in pounds"
                  placeholderTextColor={Colors.dark_gray}
                  keyboardType={'numeric'}
                />
              </View>

              <Text style={styles.text_style}>Height</Text>
              <View style={{ flexDirection: 'row', width: '100%' }}>
                <View style={[styles.textInputMain1, { marginEnd: 16 }]}>
                  <TextInput
                    style={styles.inputStyle1}
                    onChangeText={(height_feet) => this.setState({ height_feet })}
                    value={this.state.height_feet}
                    placeholder="Feet"
                    placeholderTextColor={Colors.dark_gray}
                    autoCapitalize='none'
                    keyboardType={'numeric'}
                    maxLength={1}
                  />
                </View>
                <View style={[styles.textInputMain1, { marginStart: 16 }]}>
                  <TextInput
                    style={styles.inputStyle1}
                    onChangeText={(height_inches) => { this.checkHeightInches(height_inches) }}
                    value={this.state.height_inches}
                    placeholder="Inches"
                    placeholderTextColor={Colors.dark_gray}
                    autoCapitalize='none'
                    keyboardType={'numeric'}
                    maxLength={2}
                  />
                </View>
              </View>

              <Text style={styles.text_style}>Age</Text>

              <View style={styles.textInputMain}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(age) => this.setState({ age })}
                  value={this.state.age}
                  placeholder="Age"
                  placeholderTextColor={Colors.dark_gray}
                  keyboardType="numeric"
                  maxLength={10}
                />
              </View> */}

              <Text style={styles.text_style}>Protein</Text>
              <View style={styles.textInputMain}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(protein) => this.setState({ protein })}
                  value={this.state.protein}
                  placeholder="Protein in grams"
                  placeholderTextColor={Colors.dark_gray}
                  keyboardType={'numeric'}
                />
              </View>

              <Text style={styles.text_style}>Carbs</Text>
              <View style={styles.textInputMain}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(carbs) => this.setState({ carbs })}
                  value={this.state.carbs}
                  placeholder="Carbs in grams"
                  placeholderTextColor={Colors.dark_gray}
                  keyboardType={'numeric'}
                />
              </View>

              <Text style={styles.text_style}>Fat</Text>
              <View style={styles.textInputMain}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(fat) => this.setState({ fat })}
                  value={this.state.fat}
                  placeholder="Fat in grams"
                  placeholderTextColor={Colors.dark_gray}
                  keyboardType={'numeric'}
                />
              </View>

              <TouchableOpacity
                activeOpacity={1}
                disabled={this.state.disableBtn}
                onPress={() => { Keyboard.dismiss(), this._addInput() }}
                style={[buttonStyle.login_btn, { marginBottom: 20 }]}
                underlayColor='gray'>
                <Text style={buttonStyle.btnText}>{'Submit'.toUpperCase()}</Text>
              </TouchableOpacity>

            </View>

          </View>
        </ScrollView>


      </KeyboardAvoidingView>

    )
  }
}

const mapStateToProps = state => {
  console.log()
  return {
    data: state.lists
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetch_intake: (item) => {
      dispatch(fetchIntake(item))
    },
  }

}
export default connect(mapStateToProps, mapDispatchToProps)(BMRCalculationScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },
  header: {
    backgroundColor: Colors.bg_color,
    padding: 10,
    alignItems: 'center',
  },

  avatar: {
    width: 110,
    height: 110,
    borderRadius: 57,
    borderWidth: 2,
    borderColor: "white",
    marginBottom: 10,
  },
  name: {
    fontSize: 22,
    fontWeight: '600',
  },
  bodyContent: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 10,
  },

  bodyContent: {
    paddingStart: 24,
    paddingEnd: 24,
    flexDirection: 'row',
    flexWrap: 'wrap'

  },
  cross_icon_view: {
    width: 32,
    height: 32,
    position: 'absolute',
    backgroundColor: 'white',
    borderRadius: 50,
    backgroundColor: 'white',
    shadowOffset: { width: 1, height: 1, },
    shadowColor: 'black',
    shadowOpacity: .4,
    marginTop: 90,
    marginLeft: 60,
    left: 150,
    top: 0,
    zIndex: 10,
    alignSelf: 'center',
    justifyContent: 'center'
  },
  text_style: {
    marginTop: 16,
    color: Colors.txt_gray,
    paddingStart: 16,
    fontFamily: 'futura-medium',
    fontWeight: 'bold',

  },
  textInputMain: {
    flexDirection: 'row',
    borderRadius: 50,
    borderWidth: 1,
    borderColor: Colors.light_gray,
    width: '100%',
    height: 46,
    marginTop: 6,
    justifyContent: 'center'

  },

  textInputMain1: {
    flex: 1,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: Colors.light_gray,
    height: 46,
    marginTop: 6,
    justifyContent: 'center',

  },
  inputStyle: {
    flex: 1,
    paddingStart: 16,
    fontFamily: 'futura-medium',
    alignSelf: 'center',
    height: 56,

  },
  inputStyle1: {
    flex: 1,
    fontFamily: 'futura-medium',
    paddingStart: 16,
    height: 56,

  },
  inputStyleNew: {
    flex: 1,
    paddingStart: 16,
    fontFamily: 'futura-medium',
  },
  edit_icon_view: {
    width: 32,
    height: 32,
    borderRadius: 50,
    backgroundColor: 'white',
    shadowOffset: { width: 1, height: 1, },
    shadowColor: 'black',
    shadowOpacity: .4,
    alignSelf: 'center',
    marginRight: 10,
    justifyContent: 'center'
  },
});