import React from 'react';
import { ScrollView, StyleSheet, Text, FlatList, View, Image, TouchableOpacity, AsyncStorage, ImageBackground } from 'react-native';
import HeaderLeft from '../components/HeaderLeft';
import HeaderRight from '../components/HeaderRight';
import Constant from '../constants/Constant';
import { textHeader, font_style } from '../components/styles';
import { FlatGrid } from 'react-native-super-grid';
import Colors from '../constants/Colors';
import HeaderBackground from '../components/HeaderBackground';
import ApiService from '../webservice/ApiService';
import Loader from '../components/Loader';
import Utils from '../constants/Utils'
import DataNotFound from '../components/DataNotFound';

const shipping = null
export default class ProductScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {};

    return {
      headerBackground: <HeaderBackground />,
      headerStyle: textHeader.header_style,
      headerTitle: <Text style={textHeader.header}>{`${Constant.products}`.toUpperCase()}</Text>,
      headerLeft: <HeaderLeft navigationProps={navigation} />,
      headerRight: <TouchableOpacity
        activeOpacity={1}
        onPress={() => {
          _this.RedirectToPage()

        }} >
        <Image source={require('./../assets/images/icons/product.png')}
          resizeMode="contain" style={{ width: 20, height: 20, tintColor: '#fff', marginEnd: 20 }} />
        {params.increaseCount !== 0 && (
          <View style={{ backgroundColor: Colors.red_color, width: 8, height: 8, borderRadius: 8 / 2, justifyContent: 'center', position: 'absolute', alignItems: 'flex-end' }} />
        )
        }
      </TouchableOpacity>,
    };
  };

  state = {

    allProducts: null,
    access_key: '',
    userInfo: {},
    shippingJson: null,
    shipping: null,
  }


  componentDidMount() {
    _this = this
  }

  componentWillUnmount() {
    _this = this
    let newArray = []
    this.setState({ allProducts: [] })
    if (this.state.allProducts !== null) {
      this.state.allProducts.forEach(element => {
        element.added = false
        this.setState({ newArray: element })
      });
      this.setState({ allProducts: newArray })
    }

  }

  componentWillMount() {

    this.props.navigation.setParams({ increaseCount: 0 });
    _this = this
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        var headers = {
          'X-Access-Token': json.access_key,
        }

        AsyncStorage.getItem(Constant.addToCart)
          .then(req => JSON.parse(req))
          .then(json1 => {

            if (json1 !== undefined && json1 !== null) {
              //this.setState({ itemCount: json1.length })
              this.props.navigation.setParams({ increaseCount: json1.length });

            } else {
              this.setState({ data: [] })
            }

            ApiService.api.get(ApiService.fetchProducts, { headers: headers })
              .then((response) => {
                //this.setState({ allProducts: [] })
                if (response !== null && response.data != null) {
                  if (response.data.status === Constant.SUCCESS) {
                    this.setState({
                      allProducts: response.data.data != null ? response.data.data : [],
                      shipping: response.data.shipping
                    })

                    if (response.data.data !== null) {
                      //this.setState({ allProducts: [] })
                      let newTemp = []
                      response.data.data.forEach((element, index) => {
                        let flag = false
                        if (json1 !== undefined && json1 !== null) {
                          json1.forEach((item, itemIndex) => {
                            if (item.product_id === element.id) {
                              flag = true
                            }
                          })
                        }

                        element.added = flag
                        newTemp = [...newTemp, element]

                      });
                      this.setState({ allProducts: newTemp })
                    }

                    //shipping = response.data.shipping
                  } else if (response.data.status === Constant.WARNING) {
                    if (response.data.session !== undefined && response.data.session !== null) {
                      if (response.data.session === false) {
                        Utils.toastShow(response.data.message)
                        Utils._signOutAsync(this.props.navigation)
                      }
                    }
                  }
                }

              })
              .catch(function (error) {
                //this.setState({ allProducts: [] })
                console.log("error " + JSON.stringify(error.response));
              })

          })
          .catch(error => console.log('error!' + JSON.stringify(error.response)));



      })
      .catch(error => console.log('error!'));
  }

  RedirectToPage = () => {
    //alert("RedirectToPage" + this.state.shipping);
    this.props.navigation.navigate('Cart', {
      shipping: this.state.shipping
    })
  }

  componentWillReceiveProps() {
    _this = this

    const refresh = this.props.navigation.getParam('refresh', null)

    AsyncStorage.getItem(Constant.addToCart)
      .then(req => JSON.parse(req))
      .then(json => {

        let count = json !== undefined && json !== null && json.length !== 0 ? json.length : 0

        let previousCount = this.props.navigation.getParam('increaseCount', 0)

        if (previousCount !== count) {
          this.props.navigation.setParams({ increaseCount: count });
          let newArray = [...this.state.allProducts]
          let newTemp = []
          newArray.forEach(element => {

            let flag = false
            if (json !== undefined && json !== null) {
              json.forEach((item, itemIndex) => {
                if (item.product_id === element.id) {
                  flag = true
                }
              })
            }

            element.added = flag
            newTemp = [...newTemp, element]
          });


          this.setState({ allProducts: newTemp })
        }
      })
      .catch(error => console.log('error!' + JSON.stringify(error.response)));



  }

  _AddToCart = (index) => {

    AsyncStorage.getItem(Constant.addToCart)
      .then(req => JSON.parse(req))
      .then(json => {
        //Utils.toastShow("Added to Cart");
        let flag = true
        let newArray = []
        if (json !== null && json !== undefined) {
          newArray = [...json]

          json.forEach((item, itemIndex) => {
            if (item.product_id === this.state.allProducts[index].id) {

              flag = false
              //break
            }
          })


          let tempData = [...this.state.allProducts]
          tempData[index].added = true

          this.setState({ allProducts: tempData })

          if (flag) {
            const count = 1
            let userJson = {
              product: this.state.allProducts[index],
              product_id: this.state.allProducts[index].id,
              admin_id: this.state.allProducts[index].admin_id,
              quantity: count,
              unit_price: parseFloat(this.state.allProducts[index].price).toFixed(2),
              price: parseFloat(this.state.allProducts[index].price).toFixed(2) * count,
            }
            newArray = [...newArray, userJson]
          }

        } else {
          let tempData = [...this.state.allProducts]
          tempData[index].added = true

          this.setState({ allProducts: tempData })
          //newArray = []
          const count = 1
          let userJson = {
            product: this.state.allProducts[index],
            admin_id: this.state.allProducts[index].admin_id,
            product_id: this.state.allProducts[index].id,
            quantity: count,
            unit_price: parseFloat(this.state.allProducts[index].price).toFixed(2),
            price: parseFloat(this.state.allProducts[index].price).toFixed(2) * count,
          }
          newArray = [...newArray, userJson]
        }

        AsyncStorage.setItem(Constant.addToCart, JSON.stringify(newArray))

        //this.setState({ itemCount: newArray.length })
        this.props.navigation.setParams({ increaseCount: newArray.length });

      })
      .catch(error => console.log('error!' + JSON.stringify(error.response)));

  }


  // productAdded = (index) => {

  //   let newArray = [...this.state.allProducts]


  //   newArray[index].added = true

  //   this.setState({ allProducts: newArray })
  // }

  render() {
    if (this.state.allProducts === undefined || this.state.allProducts === null) {
      return (<Loader />)
    } else {
      if (this.state.allProducts.length === 0) {
        return (<DataNotFound />)
      } else {
        return (
          <View style={styles.container}>
            <FlatGrid

              showsVerticalScrollIndicator={false}
              itemDimension={130}
              items={this.state.allProducts}
              style={styles.gridView}
              // staticDimension={300}
              // fixed
              // spacing={20}
              renderItem={({ item, index }) => (
                <TouchableOpacity key={index}
                  onPress={() => {
                    this.props.navigation.navigate('ProductDetails', {
                      productId: item.id,
                      shipping: this.state.shipping
                    })
                  }}
                  activeOpacity={1}
                >
                  <View style={[styles.itemContainer, { backgroundColor: '#fff' }]}>
                    <Text style={[{
                      color: Colors.primaryColor,
                      textAlign: 'left',
                      marginTop: 8,
                      marginLeft: 16,
                      marginBottom: 8
                    }, font_style.font_bold]}>${item.price}</Text>
                    <View style={{ alignItems: 'center', }}>

                      <View style={{
                        alignItems: 'center',
                        height: 120,
                        width: 120,
                        justifyContent: 'center',
                      }}>
                        <ImageBackground source={require('../assets/images/thumbnail.png')}
                          style={styles.img_view}>
                          <Image source={{ uri: item.image }} style={{
                            width: 112,
                            height: 112,

                          }} />
                        </ImageBackground>
                      </View>

                      <Text style={[{ padding: 4, fontSize: 14, color: Colors.dark_gray, textTransform: 'capitalize' }, font_style.font_medium]}>{item.name}</Text>
                      {/* <Rating
                      type='star'
                      imageSize={12} startingValue={4.5}
                      ratingImage={require('../assets/images/icons/star_3x.png')}
                      ratingBackgroundColor='gray'
                    /> */}
                      <Text style={[{ padding: 4, fontSize: 14, color: Colors.dark_gray, textTransform: 'capitalize' }, font_style.font_light]}>{item.sub_title}</Text>
                      <View style={{
                        borderStyle: 'solid',
                        borderWidth: 0.5,
                        width: '100%', borderColor: Colors.light_gray,
                        marginTop: 6,
                      }}>

                      </View>
                      <TouchableOpacity
                        style={{ padding: 8, marginTop: 4, width: '100%', }}
                        activeOpacity={1}
                        onPress={() => { this._AddToCart(index) }}>
                        <Text style={[{ fontSize: 14, color: 'rgba(90, 91, 93, 0.7)', textAlign: 'center' }, font_style.font_medium]}> {item.added !== undefined && item.added !== null && item.added === true ? 'ADDED' : 'ADD TO CART'}</Text>
                      </TouchableOpacity>
                    </View>

                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
        )
      }

    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
    paddingStart: 8,
    paddingEnd: 8
  },
  gridView: {
    marginTop: 10,
    marginBottom: 10,
    flex: 1,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,

  },
  itemName: {
    fontSize: 16,
    color: '#000',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#000',
  },
  img_view: {
    width: 112,
    height: 112,
  },

});

