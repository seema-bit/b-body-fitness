import React from 'react';
import {
  ScrollView, StyleSheet, View, Image, Dimensions, Text, TouchableOpacity, Platform,
  Keyboard
} from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import { TextInput } from 'react-native-gesture-handler';
import { textInput, buttonStyle, text_color } from '../components/styles';
import Colors from '../constants/Colors';
import { Icon } from 'react-native-elements'
import Utils from '../constants/Utils';
import ApiService from '../webservice/ApiService';
import Constant from '../constants/Constant';
import Spinner from 'react-native-loading-spinner-overlay';
import CustomProgressBar from '../components/CustomProgressBar';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

const deviceWidth = Dimensions.get('window').width;

export default class ForgotPasswordScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {
    email: '',
    disableBtn: false,
    spinner: false,
    offset: 0,
    bgColor: Colors.primaryColor
  }

  onScroll = (event) => {
    var currentOffset = event.nativeEvent.contentOffset.y;
    var direction = currentOffset > this.state.offset ? 'down' : 'up';
    this.setState({
      bgColor: currentOffset > this.state.offset ? Colors.white_color : Colors.primaryColor
    })
    this.offset = currentOffset;
    console.log(direction);
  }

  _ForgotPassword = () => {
    Keyboard.dismiss()
    if (this.state.email !== null && this.state.email !== '') {
      if (Utils._emailValidate(this.state.email)) {
        this.setState({ disableBtn: true })
        this.networkCall();
      } else {
        Utils.toastShow('Email is not correct')
      }

    } else {
      Utils.toastShow('Email required')
    }
  }

  networkCall = () => {
    this.setState({ spinner: true })
    const user = {
      email: this.state.email,
    };

    ApiService.api.post(ApiService.forgot_password, user)
      .then((response) => {

        if (response !== null && response.data != null) {
          this.setState({ disableBtn: false, spinner: false })
          if (response.data.status === Constant.TRUE) {
            Utils.toastShow(response.data.message)
            this.setState({ email: '' })
            this.props.navigation.navigate('ResetPassword')
          } else if (response.data.status === Constant.FALSE) {
            Utils.toastShow(response.data.message)
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils._signOutAsync(this.props.navigation)
              }
            }
          } else {
            Utils.toastShow(Constant.message)
          }
        } else {
          this.setState({ disableBtn: false, spinner: false })
          Utils.toastShow(Constant.message)
        }

      })
      .catch(function (error) {
        this.setState({ disableBtn: false, spinner: false })
        Utils.toastShow(Constant.message)
        Utils._SentryReport(error)
      })
  }

  render() {
    return (
      <KeyboardAwareScrollView enableAutomaticScroll enableOnAndroid
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={{ flex: 1, backgroundColor: Colors.white_color }}
        scrollEnabled={true}
        keyboardShouldPersistTaps='handled'
        onScroll={event => this.onScroll(event)}
        style={{ backgroundColor: this.state.bgColor }}>
        <TouchableOpacity onPress={() => { Keyboard.dismiss() }} activeOpacity={1} style={styles.container}>
          <CustomProgressBar spinner={this.state.spinner} />
          <View style={{
            width: deviceWidth,
            height: deviceWidth,
            justifyContent: 'flex-end',
            marginTop: -50,

          }} >
            <Image style={{
              width: '100%',
              height: undefined,
              aspectRatio: 1,
            }}
              source={require('../assets/images/group_reset_3x.png')}
            />

            <Text style={{
              position: 'absolute',
              justifyContent: 'flex-end',
              alignItems: 'center',
              paddingBottom: 100,
              alignSelf: 'center',
              fontSize: 25,
              color: '#FAFAFA',
              textAlign: 'center',
              fontFamily: 'futura-book',
            }}>{'Forgot \npassword'.toUpperCase()}</Text>
          </View>

          <View style={{
            position: 'absolute',
            justifyContent: 'center',
          }}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}
              style={{
                paddingTop: 48,
                width: 24, height: 24, alignItems: 'flex-start',
                padding: 20,
              }}>
              {/* <Icon
              style={{ alignSelf: 'start' }}
              name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'}
              type='ionicon'
              color='white'
              size={28}

            /> */}
              <Image source={require('./../assets/images/icons/arrow-right_3x.png')} style={{ width: 11, height: 20, }} />
            </TouchableOpacity>
          </View>
          <View style={styles.mainContainer}>
            <TextInput
              style={[textInput.gray_textInput, { marginTop: 20, marginBottom: 10 }]}
              onChangeText={(email) => this.setState({ email })}
              value={this.state.email}
              placeholder="Enter your email"
              placeholderTextColor={Colors.dark_gray}
              autoCapitalize='none'
            />

            <TouchableOpacity
              activeOpacity={1}
              disabled={this.state.disableBtn}
              style={[buttonStyle.primaryBtn, { marginBottom: 10 }]}
              underlayColor='#fff'
              onPress={() => this._ForgotPassword()}>
              <Text style={buttonStyle.btnText}>{'Confirm'.toUpperCase()}</Text>
            </TouchableOpacity>
            {/* <TouchableOpacity
            style={buttonStyle.lightGrayBtn}
            underlayColor='#fff'
            onPress={() => this.props.navigation.navigate('ResetPassword')}>
            <Text style={[buttonStyle.loginText, { color: Colors.dark_gray }]}>{'Reset Password'.toUpperCase()}</Text>
          </TouchableOpacity> */}

          </View>

        </TouchableOpacity>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: '100%',
    height: '100%',
  },

  mainContainer: {
    padding: 24,
    width: '100%',
  },



  image: {
    width: deviceWidth,
    height: deviceWidth / 1.5
  }
});
