import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage,
  Dimensions,
  Platform,
  ProgressBarAndroid,
  ProgressViewIOS,
} from 'react-native';
import Colors from '../constants/Colors';
import Constant from '../constants/Constant';
import HeaderRight from '../components/HeaderRight';
import HeaderBackground from '../components/HeaderBackground';
import HeaderBackLeft from '../components/HeaderBackLeft';
import { textInput, buttonStyle, text_color, textHeader, font_style } from '../components/styles';
import {
  PieChart,
} from 'react-native-chart-kit'
import * as Progress from 'react-native-progress';
import { Container, Content, Card, CardItem, Thumbnail, Left, Body, Right, ListItem } from 'native-base'
import { ScrollView } from 'react-native-gesture-handler';
import ApiService from '../webservice/ApiService';
import Loader from '../components/Loader';
import axios from "axios";
import Utils from '../constants/Utils';
import moment from 'moment';
import Spinner from 'react-native-loading-spinner-overlay';
import CustomProgressBar from '../components/CustomProgressBar';
import DatePicker from 'react-native-datepicker'
import Modal from 'react-native-modalbox';
import { TextInput } from 'react-native-gesture-handler';
import { Dropdown } from 'react-native-material-dropdown';
import { Appearance } from 'react-native-appearance';
import { connect } from 'react-redux';
import { fetchIntake } from '../redux/action';

class MealNutritionDetailsScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${navigation.state.params.title}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,
    headerRight: <TouchableOpacity
      onPress={() => { _this._OpenQuantityModel() }}
      style={textHeader.rightIcon}>
      <Image source={require('./../assets/images/icons/tick_3x.png')}
        resizeMode="contain" style={{ width: 20, height: 20, alignSelf: 'flex-end' }} />
    </TouchableOpacity>

  });

  state = {
    foodData: null,
    data: [
      { name: 'Carbs', value: 'carbohydrate', population: 37.5, color: '#C10376', legendFontColor: '#7F7F7F', legendFontSize: 15, },
      { name: 'Protein', value: 'protein', population: 25, color: '#1370EE', legendFontColor: '#7F7F7F', legendFontSize: 15 },
      //{ name: 'Calcium', population: 25, color: '#FF9036', legendFontColor: '#7F7F7F', legendFontSize: 15 },
      { name: 'Fat', value: 'fat', population: 12.5, color: '#2F2FAA', legendFontColor: '#7F7F7F', legendFontSize: 15 },
    ],
    nutrition_facts: [],
    nutritionFactVisible: false,
    progressBarProgress: 100.0,
    secretTokenData: null,
    userInfo: null,
    calories: 0,
    protein: 0,
    carbohydrate: 0,
    fat: 0,
    spinner: false,
    meal_time: moment().format('hh:mm A'),
    quantity: "1",
    tempQuantity: '1',
    facts: [{ name: 'calories', unit: 'kcal' },
    { name: 'carbohydrate', unit: 'g' },
    { name: 'protein', unit: 'g' },
    { name: 'fat', unit: 'g' },
    { name: 'saturated_fat', unit: 'g' },
    { name: 'polyunsaturated_fat', unit: 'g' },
    { name: 'monounsaturated_fat', unit: 'g' },
    { name: 'trans_fat', unit: 'g' },
    { name: 'cholesterol', unit: ' mg' },
    { name: 'sodium', unit: 'mg' },
    { name: 'potassium', unit: 'mg' },
    { name: 'fiber', unit: 'g' },
    { name: 'sugar', unit: 'g' },
    { name: 'vitamin_a', unit: '%' },
    { name: 'vitamin_c', unit: '%' },
    { name: 'calcium', unit: '%' },
    { name: 'iron', unit: '%' },],
    servingData: null,
    selectedServingData: null
  };

  componentDidMount = () => {
    _this = this

    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        AsyncStorage.getItem(Constant.FoodAPI)
          .then(req1 => JSON.parse(req1))
          .then(json1 => {

            this.setState({ secretTokenData: json1 })
            this._FoodItem()

          })
          .catch(error => {
            console.log('food data error' + JSON.stringify(error.response))
          })

      })
      .catch(error => console.log('user data error', error));
  }

  _FoodItem = () => {
    const food_id = this.props.navigation.getParam('food_id', null);
    const currentTimestamp = Date.now()
    let diff = (currentTimestamp - this.state.secretTokenData.token_time) / 1000
    if (diff < this.state.secretTokenData.expires_in) {
      var headers = {
        'X-Access-Token': this.state.userInfo.access_key,
      }
      const params = {
        search_expression: `method=food.get&format=json&food_id=${food_id}`,
        secret_token: this.state.secretTokenData.secret_token,

      }

      ApiService.api.post(ApiService.search_meal, params, { headers: headers })
        .then((response) => {

          if (response !== null && response.data != null) {
            if (response.data.status === Constant.SUCCESS) {


              if (response.data.data.food.servings !== null
                && response.data.data.food.servings.serving !== undefined
                && response.data.data.food.servings.serving !== null
              ) {
                if (response.data.data.food.servings.serving.length !== undefined && response.data.data.food.servings.serving.length !== 0) {

                  let reportData = response.data.data.food.servings.serving[0];

                  let servingArray = []
                  response.data.data.food.servings.serving.forEach(element => {
                    let newElement = element


                    const check = element.serving_description.includes(element.metric_serving_unit)
                    if (!check) {
                      newElement.serving_description = element.serving_description + " ( " + parseFloat(element.metric_serving_amount).toString() + element.metric_serving_unit + " ) "
                    }

                    newElement.value = element.serving_description
                    servingArray = [...servingArray, newElement]
                  });


                  this.setState({ foodData: response.data.data.food !== null ? response.data.data.food : {}, servingData: servingArray })
                  this.getNutritionFacts(reportData)

                } else {
                  let reportData = response.data.data.food.servings.serving;
                  const check = reportData.serving_description.includes(reportData.metric_serving_unit)
                  if (!check) {
                    reportData.serving_description = reportData.serving_description + " ( " + parseFloat(reportData.metric_serving_amount).toString() + reportData.metric_serving_unit + " ) "
                  }
                  let servingArray = []
                  reportData.value = reportData.serving_description
                  servingArray = [...servingArray, reportData]


                  this.setState({ foodData: response.data.data.food !== null ? response.data.data.food : {}, servingData: servingArray })
                  this.getNutritionFacts(reportData)
                }
              }


            } else if (response.data.status === Constant.WARNING) {
              if (response.data.session !== undefined && response.data.session !== null) {
                if (response.data.session === false) {
                  Utils.toastShow(response.data.message)
                  Utils._signOutAsync(this.props.navigation)
                }
              }
            }
          }

        })
        .catch(function (error) {
          Utils._SentryReport(error)
          console.log("food error " + JSON.stringify(error.response));
        })

    } else {
      Utils._FoodApiTokenGenerate()
      this._SearchData()
    }


  }

  _OpenQuantityModel = () => {

    if (this.state.foodData !== undefined && this.state.foodData !== null) {
      this.refs.modal1.open()
    }

  }

  _OpenServingModel = () => {

    if (this.state.servingData !== undefined && this.state.servingData !== null) {
      this.refs.modal2.open()
    }
  }


  _SaveQuantity = () => {
    this.getNutritionFacts(this.state.selectedServingData)
    this.refs.modal2.close()
  }


  _SetQuantity = () => {
    this.refs.modal1.close()
    this._AddFoodItem()
  }

  fetchIntake = () => {

    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }
    const params = {
      date: moment().format('MM-DD-YYYY')
    }


    ApiService.api.post(ApiService.cal_intake, params, { headers: headers })
      .then((response) => {

        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            console.log('intakeData', response.data.data)
            if (response.data.data !== undefined && response.data.data !== null) {
              //this.setState({ intakeData: response.data.data })
              this.props.fetch_intake(response.data.data)
            }
          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("intake error " + JSON.stringify(error.response));
      })
  }

  _AddFoodItem = () => {
    if (this.state.quantity !== null && this.state.quantity !== '') {
      if (parseFloat(this.state.quantity).toFixed(2) > 0.00) {
        this.setState({ spinner: true })
        var headers = {
          'X-Access-Token': this.state.userInfo.access_key,
        }
        if (this.state.foodData !== null && this.state.foodData.food_id !== undefined && this.state.foodData.food_id !== null) {
          const item_id = this.props.navigation.getParam('item_id', null);
          if (item_id !== undefined && item_id !== null) {
            this.props.navigation.navigate("Meal", {
              refresh: 'yes'
            })
          } else {

            const params = {
              food_id: this.state.foodData.food_id,
              type: this.props.navigation.getParam('type', ''),
              date: this.props.navigation.getParam('date', moment().format('MM-DD-YYYY')),
              food_name: this.state.foodData.food_name,
              quantity: parseFloat(this.state.quantity).toFixed(2),
              calories: this.state.calories,
              protein: this.state.protein,
              fat: this.state.fat,
              carbohydrate: this.state.carbohydrate,
              meal_time: this.state.meal_time
            }
            ApiService.api.post(ApiService.add_meal, params, { headers: headers })
              .then((response) => {
                this.setState({ spinner: false })
                if (response !== null && response.data != null) {
                  if (response.data.status === Constant.SUCCESS) {
                    Utils.toastShow(response.data.message)
                    this.fetchIntake()
                    this.props.navigation.navigate("Meal", {
                      refresh: 'yes'
                    })
                  } else if (response.data.status === Constant.WARNING) {
                    if (response.data.session !== undefined && response.data.session !== null) {
                      if (response.data.session === false) {
                        Utils._signOutAsync(this.props.navigation)
                      }
                    }
                  }
                }

              })
              .catch(function (error) {
                //alert('Error')
                this.setState({ spinner: false })
                Utils._SentryReport(error)
                console.log("error " + JSON.stringify(error.response));
              })
          }

        }
      } else {
        Utils.toastShow("Enter valid quantity")
      }
    } else {
      Utils.toastShow("Quantity required")
    }


  }

  getNutritionFacts = (reportData) => {
    const temp = this.state.foodData
    temp.servings.serving = reportData;
    this.setState({ foodData: temp })
    const newArray = ["measurement_description",
      "metric_serving_amount",
      "metric_serving_unit",
      "number_of_units",
      "serving_description",
      "serving_id",
      "serving_url",
      "value"
    ]

    let splitArray = JSON.stringify(reportData);
    splitArray = splitArray.split("\",")

    let tempArray = []
    //this.setState({ nutrition_facts: emptyArr })

    splitArray.forEach((element, index) => {
      const pair = element.split(":")



      let value1 = pair[0].replace(/[^a-zA-Z0-9_,\s]/g, '')

      let value = pair[0].replace(/[^a-zA-Z0-9_,\s]/g, '')
      let title = value.replace(/_/g, ' ');
      let item = pair[1].replace(/[^a-zA-Z0-9.\s]/g, '')


      if (title.trim() === "calories") {
        this.state.calories = item
      }
      if (title.trim() === "carbohydrate") {
        this.state.carbohydrate = item
      }
      if (title.trim() === "fat") {
        this.state.fat = item
      }
      if (title.trim() === "protein") {
        this.state.protein = item
      }

      item = item.trim()
      this.state.facts.forEach(element => {

        if (element.name === value1) {
          item = item + element.unit

        }

      });
      const check = newArray.indexOf(value.trim())
      if (check <= -1) {
        const facts = { id: index, title: title, item: item }
        tempArray = [...tempArray, facts]

      }

    })

    this.setState({ nutrition_facts: tempArray })

    let carbohydrate = parseInt(this.state.carbohydrate) * 4
    let protein = parseInt(this.state.protein) * 4
    let fat = parseInt(this.state.fat) * 9

    const total = parseInt(carbohydrate) + parseInt(protein) + parseInt(fat)
    let carbohydrate_percent = (parseInt(carbohydrate) * 100) / total
    carbohydrate_percent = Number((carbohydrate_percent).toFixed(0))

    let protein_percent = (parseInt(protein) * 100) / total
    protein_percent = Number((protein_percent).toFixed(0))

    let fat_percent = (parseInt(fat) * 100) / total
    fat_percent = Number((fat_percent).toFixed(0))

    let updateData = [...this.state.data]
    this.state.data.forEach((element, index) => {
      switch (element.value) {
        case 'carbohydrate':
          updateData[index].population = carbohydrate_percent
          break;
        case 'protein':
          updateData[index].population = protein_percent
          break;
        case 'fat':
          updateData[index].population = fat_percent

          break;

        default:
          break;
      }
    })

    this.setState({ data: updateData })

  }

  handleInput = (quantity) => {
    // var output = quantity.split('.');
    // output = output.shift() + '.' + output.join('')
    this.setState({ quantity })
  }

  _setServingSize = (item, index) => {
    this.setState({ selectedServingData: this.state.servingData[index] })
    //this.getNutritionFacts(this.state.servingData[index])
  }

  render() {

    const colorScheme = Appearance.getColorScheme();
    let bgColor;
    if (colorScheme === 'dark') {
      bgColor = '#000';
    } else {
      bgColor = '#fff';
    }

    if (this.state.foodData === undefined || this.state.foodData === null) {
      return (<Loader />)
    } else {
      return (
        <View>
          <Modal
            animationIn="fadeIn"
            animationOut="fadeOut"
            hideModalContentWhileAnimating={true}
            animationInTiming={1500}
            animationOutTiming={1500}
            backdropTransitionOutTiming={0}
            style={[styles.modal, { width: 300, height: 200 }]} position={"center"} ref={"modal2"}>

            <View style={{ margin: 24, }} >
              <Text style={[{ fontSize: 16, marginBottom: 16 }, font_style.font_medium]}>Serving Size</Text>

              <View style={{
                justifyContent: 'center',
                alignItems: 'center',
              }}>
                <Dropdown
                  containerStyle={{
                    width: '100%',
                    alignSelf: 'center',
                    paddingTop: 12,
                  }}
                  pickerStyle={styles.pickerStyle}
                  inputContainerStyle={{ borderBottomColor: 'transparent', justifyContent: 'center' }}
                  dropdownOffset={{ top: 0, bottom: -10 }}
                  dropdownMargins={{ min: 0, max: 0 }}
                  data={this.state.servingData}
                  value={this.state.servingData[0].value}
                  onChangeText={(item, index) => { this._setServingSize(item, index) }}
                />
              </View>



              <View style={styles.btn_view}>
                <TouchableOpacity
                  style={styles.cancel_btn}
                  onPress={() => this.refs.modal2.close()}>
                  <Text style={{ color: Colors.primaryColor }}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.ok_btn}
                  onPress={() => this._SaveQuantity()}>
                  <Text style={{ color: Colors.primaryColor }}>Ok</Text>
                </TouchableOpacity>
              </View>

            </View>

          </Modal>

          <Modal
            animationIn="fadeIn"
            animationOut="fadeOut"
            hideModalContentWhileAnimating={true}
            animationInTiming={1500}
            animationOutTiming={1500}
            backdropTransitionOutTiming={0}
            style={[styles.modal, { width: 300, height: 150 }]} position={"center"} ref={"modal1"}>

            <View style={{ margin: 24, }} >
              <Text style={[{ fontSize: 16, marginBottom: 16 }, font_style.font_medium]}>Quantity</Text>

              <TextInput style={[{
                width: 60,
                borderBottomColor: Colors.light_gray,
                borderBottomWidth: 1,
              }, styles.txt_style, font_style.font_medium]}
                placeholder="0.0"
                placeholderTextColor={Colors.dark_gray}
                keyboardType={'decimal-pad'}
                onChangeText={(quantity) => this.handleInput(quantity)}
                value={this.state.quantity}
              />

              <View style={styles.btn_view}>
                <TouchableOpacity
                  style={styles.cancel_btn}
                  onPress={() => this.refs.modal1.close()}>
                  <Text style={{ color: Colors.primaryColor }}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.ok_btn}
                  onPress={() => {
                    if (this.state.quantity.trim().length > 0 && parseFloat(this.state.quantity).toFixed(2) > 0.00) {
                      this._SetQuantity()
                    }
                  }}>
                  <Text style={{ color: (this.state.quantity.trim().length > 0 && parseFloat(this.state.quantity).toFixed(2) > 0.00) ? Colors.primaryColor : 'grey' }}>Ok</Text>
                </TouchableOpacity>
              </View>

            </View>

          </Modal>

          <ScrollView>
            <CustomProgressBar spinner={this.state.spinner} />
            <View style={styles.container} >
              <View style={{ backgroundColor: Colors.bg_color, padding: 20 }}>
                <Text style={[{ fontSize: 16, color: '#413F40', marginBottom: 6 }, font_style.font_bold]}>{this.state.foodData.food_name}</Text>


                <View style={{
                  flexDirection: 'row', borderBottomColor: Colors.light_gray,
                  borderBottomWidth: 1,
                  paddingBottom: 8,
                  marginTop: 8
                }}>
                  <Text style={[{ flex: 1 }, styles.txt_style, font_style.font_medium]}>Number of serving</Text>
                  <Text style={[{ alignItems: 'flex-end' }, styles.txt_style, font_style.font_medium]}>{this.state.foodData.servings.serving.number_of_units !== undefined &&
                    this.state.foodData.servings.serving.number_of_units !== null ? parseInt(this.state.foodData.servings.serving.number_of_units) : this.state.foodData.servings.serving.number_of_units}</Text>
                </View>

                <View style={{
                  flexDirection: 'row', borderBottomColor: Colors.light_gray,
                  borderBottomWidth: 1,
                  paddingBottom: 8,
                  marginTop: 8
                }}>
                  <Text style={[{ flex: 1 }, styles.txt_style, font_style.font_medium]}>Serving size</Text>
                  <TouchableOpacity style={{ alignItems: 'flex-end' }} onPress={() => { this._OpenServingModel() }}>
                    <Text style={[styles.txt_style, font_style.font_medium, { color: Colors.primaryColor }]}>{this.state.foodData.servings.serving.serving_description}</Text>

                  </TouchableOpacity>
                </View>

                <View style={{
                  flexDirection: 'row',
                  paddingBottom: 8,
                  marginTop: 8
                }}>
                  <Text style={[{ flex: 1 }, styles.txt_style, font_style.font_medium]}>Time</Text>
                  <DatePicker
                    style={{ width: 80 }}
                    date={this.state.meal_time}
                    //maxDate={this.state.endDate}
                    mode="time"
                    showIcon={false}
                    placeholder="Select Time"
                    format="hh:mm A"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        left: 0,
                        marginLeft: 0,
                        borderColor: Colors.bg_color
                      },
                      dateInput: {
                        borderColor: Colors.bg_color,
                        color: '#797979'
                      },
                      datePickerCon: {
                        backgroundColor: bgColor
                      }
                      // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => { this.setState({ meal_time: date }) }}
                  />
                </View>
              </View>


              <View style={styles.second_container}>
                <View style={styles.padding_view}>
                  <PieChart
                    data={this.state.data}
                    width={Dimensions.get('window').width}
                    height={220}
                    chartConfig={{
                      decimalPlaces: 2, // optional, defaults to 2dp
                      color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    }}
                    accessor="population"
                    backgroundColor="transparent"
                    paddingLeft="15"
                  />
                </View>

                <View style={{
                  borderBottomColor: Colors.light_gray,
                  borderBottomWidth: 1,
                }} />

                <View style={[styles.padding_view, { marginTop: 16 }]}>
                  <Text style={[styles.progressbar_heading, font_style.font_bold]}>Percent of your daily goals</Text>

                  {this.state.data.map((item, index) => {
                    return (
                      <View style={styles.progressbar_view} key={index}>
                        <Text style={{ flex: 1, }}>{item.name} {item.population}%</Text>
                        <View style={{ flex: 2, justifyContent: 'center' }}>
                          <Progress.Bar progress={item.population / 100}
                            width={null}
                            color={item.color}
                            unfilledColor={'#e8e8e8'}
                            borderWidth={0}
                          />
                        </View>

                      </View>
                    )
                  })
                  }

                </View>
                {this.state.nutritionFactVisible ?
                  (<Container style={{ backgroundColor: Colors.bg_color, }} >
                    <Content >
                      <TouchableOpacity onPress={() => { this.setState({ nutritionFactVisible: false }) }}
                        style={{
                          height: 30,
                          backgroundColor: Colors.bg_color,
                          alignSelf: 'center',
                          width: Dimensions.get('window').width,
                          justifyContent: 'center',
                          marginTop: 20,
                          alignItems: 'center'
                        }}>
                        <Text style={[font_style.font_medium, styles.meta_txt]}>Close Nutrition Facts</Text>
                      </TouchableOpacity>
                      {/* <View>
                      {this.getNutritionFacts()}
                    </View> */}
                      {this.state.nutrition_facts.map((item, index) => {
                        return (

                          <ListItem key={item.id} style={{ marginEnd: 20 }}>
                            <Left>
                              <Text style={[font_style.font_medium, styles.meta_txt, { textTransform: 'capitalize' }]}>{item.title}</Text>
                            </Left>
                            <Right>
                              <Text style={[font_style.font_medium, styles.meta_txt]}>{item.item}</Text>
                            </Right>
                          </ListItem>
                        )
                      })}
                      <CardItem style={{
                        backgroundColor: Colors.bg_color,
                        marginTop: 10, marginBottom: 20
                      }} >
                        <Text style={[{ fontSize: 12, color: Colors.dark_gray }, font_style.font_Book]}>* Percent daily values are based on 2,000 calories diet. Your daily values
may be higher or lower depending on your calories needs.</Text>
                      </CardItem>
                    </Content>
                  </Container>
                  )
                  :
                  (<TouchableOpacity
                    onPress={() => { this.setState({ nutritionFactVisible: true }) }}
                    style={{
                      height: 30,
                      backgroundColor: Colors.bg_color,

                      alignSelf: 'center',
                      width: Dimensions.get('window').width,
                      justifyContent: 'center',
                      marginTop: 20
                    }}
                  >

                    <Text style={[{ textAlign: 'center', fontSize: 14, color: Colors.dark_gray }, font_style.font_medium]}>View Nutrition Facts</Text>

                  </TouchableOpacity>
                  )
                }



                <Text style={[{ marginTop: 20, marginBottom: 30, alignSelf: 'center', fontSize: 16, color: Colors.dark_gray }, font_style.font_medium]}>Report Food</Text>
              </View>

            </View>

          </ScrollView >


        </View>
      )
    }
  }
}

const mapStateToProps = state => {
  return {
    data: state.lists
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetch_intake: (item) => {
      dispatch(fetchIntake(item))
    },
  }

}
export default connect(mapStateToProps, mapDispatchToProps)(MealNutritionDetailsScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,

  },
  txt_style: {
    fontSize: 14,
    color: Colors.dark_gray,
  },
  second_container: {
    flex: 1,
    backgroundColor: '#fff',
  },

  progressbar_heading: {
    fontSize: 16,
    color: Colors.cart_color_icon,
  },
  progressbar_view: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 10
  },

  padding_view: {
    paddingStart: 20,
    paddingEnd: 20,
  },
  meta_txt: {
    color: Colors.dark_gray,
    fontSize: 14,
  },
  btn_view: {
    marginTop: 16,
    flexDirection: 'row',
    height: 46, alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  ok_btn: { height: 46, padding: 4, paddingStart: 16, justifyContent: 'center' },
  cancel_btn: { marginEnd: 24, padding: 4, height: 46, justifyContent: 'center', },
  icon_view: {
    width: 32, height: 32,
    justifyContent: 'center',
    alignItems: 'center',

  },
  pickerStyle: {
    width: 250,
    paddingStart: 20, marginTop: 60,

    marginStart: (Dimensions.get('window').width - 250) / 2, marginEnd: (Dimensions.get('window').width - 250) / 2,

  }

});