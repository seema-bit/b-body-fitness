import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
  AsyncStorage,
  Dimensions
} from 'react-native';
import HeaderLeft from '../components/HeaderLeft';
import HeaderRight from '../components/HeaderRight';
import moment from 'moment';

import { textInput, buttonStyle, text_color, textHeader } from '../components/styles';
import Colors from '../constants/Colors';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base'
import Constant from '../constants/Constant';
import CalendarStrip from '../components/calender/CalendarStrip';
import { font_style } from '../components/styles';
import HeaderBackground from '../components/HeaderBackground';
import Loader from '../components/Loader';
import ApiService from '../webservice/ApiService';
import Spinner from 'react-native-loading-spinner-overlay';
import DataNotFound from '../components/DataNotFound';
import Utils from '../constants/Utils';
import CustomProgressBar from '../components/CustomProgressBar';
import { Ionicons } from '@expo/vector-icons';

let datesWhitelist = [{
  start: moment(),
}];
let datesBlacklist = [moment().add(1, 'days')]; // 1 day disabled



export default class ClassScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.class}`.toUpperCase()}</Text>,
    headerLeft: <HeaderLeft navigationProps={navigation} />,
    //headerRight: <HeaderRight navigationProps={navigation} />,
  });

  state = {
    allClasses: null,
    selectedDate: null,
    userInfo: '',
    updateCount: 0,
    spinner: false,
    data: null
  }

  componentDidMount = () => {
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {

        this.setState({ userInfo: json, selectedDate: moment().format('MM-DD-YYYY') })
        this._FetchClasses(moment().format('MM-DD-YYYY'));

      })
      .catch(error => console.log('error!'));

  }

  componentWillReceiveProps = () => {
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json, })
        this._FetchClasses(this.state.selectedDate)

      })
      .catch(error => console.log('error!'));
  }

  componentWillUpdate = () => {

    if (this.state.updateCount === 0) {
      this.setState({ updateCount: 1 })
      AsyncStorage.getItem(Constant.USERINFO)
        .then(req => JSON.parse(req))
        .then(json => {
          this.setState({ userInfo: json, })
          this._FetchClasses(this.state.selectedDate)


        })
        .catch(error => console.log('error!'));

    }

  }

  _FetchClasses = (dateFormate) => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    const params = {
      date: dateFormate
    }
    //this.setState({ allClasses: [] })
    ApiService.api.post(ApiService.allClasses, params, { headers: headers })
      .then((response) => {
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            this.setState({ allClasses: response.data.data != null ? response.data.data : [] })
          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })
  }

  _DateSelected = (date) => {
    let dateFormate = moment(date).format("MM-DD-YYYY")

    this.setState({ selectedDate: dateFormate, allClasses: null })
    this._FetchClasses(dateFormate)
  }

  _JoinClass = (item, index) => {
    if (item.past_class !== undefined && item.past_class !== null && item.past_class === 'inactive') {
      Utils.toastShow(item.msg)
      // if (item.class_join === "true") {
      //   Utils.toastShow('You can not change past class status')
      // } else {
      //   Utils.toastShow('You can not join past class')
      // }

    } else {
      if (item.past_class !== undefined && item.past_class !== null && item.past_class === 'active') {

        if (item.class_join === "true") {
          if (item.class_active_for_user !== undefined && item.class_active_for_user !== null && item.class_active_for_user === 'false') {
            //Utils.toastShow('You can not change selection after 24 hours of join class')
            Utils.toastShow(item.msg)
          } else {
            this._JoinClassforNetwork(item, index)
          }
        } else {
          this._JoinClassforNetwork(item, index)
        }


      } else {
        this._JoinClassforNetwork(item, index)
      }

    }

  }

  _JoinClassforNetwork = (item, index) => {
    this.setState({ spinner: true })
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }
    const params = {
      join_date: moment().format("MM-DD-YYYY HH:mm:ss"),
      class_id: item.id
    }

    ApiService.api.post(ApiService.joinClass, params, { headers: headers })
      .then((response) => {
        this.setState({ spinner: false })
        if (response !== null && response.data != null) {
          Utils.toastShow(response.data.message)
          if (response.data.status === Constant.SUCCESS) {
            this.setState({ data: response.data })
            let newArray = [...this.state.allClasses]
            if (newArray[index].class_join === "false") {
              newArray[index].class_join = "true"
              newArray[index].class_active_for_user = "true"
            } else {
              newArray[index].class_join = "false"
            }

            this.setState({ allClasses: newArray })
          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })
  }

  closeBanner = () => {

    this.setState({ data: null })
  }

  render() {

    return (
      <View style={styles.container}>

        <CustomProgressBar spinner={this.state.spinner} />
        <CalendarStrip
          calendarAnimation={{ type: 'sequence', duration: 30 }}
          daySelectionAnimation={{ type: 'border', duration: 200, borderWidth: 1, borderHighlightColor: 'black', backgroundColor: Colors.primaryColor }}
          style={{ height: 80, paddingTop: 0, paddingBottom: 0 }}
          calendarHeaderStyle={{ color: 'black' }}
          calendarColor={'#fff'}
          showMonth={false}
          dateNumberStyle={{ color: 'black' }}
          dateNameStyle={{ color: 'black' }}
          dateMonthStyle={{ color: 'black' }}
          highlightDateNumberStyle={{ color: 'black' }}
          highlightDateNameStyle={{ color: 'black' }}
          disabledDateNameStyle={{ color: 'black' }}
          //datesWhitelist={datesWhitelist}
          disabledDateNumberStyle={{ color: 'black' }}
          iconContainer={{ flex: 0.1 }}
          onDateSelected={date => { this._DateSelected(date) }}
        />
        {this.state.allClasses === undefined || this.state.allClasses === null ?
          (<Loader />) :
          (<View style={{ flex: 1 }}>
            {this.state.allClasses.length === 0 ?
              (<DataNotFound />)
              :
              (<View style={{ backgroundColor: Colors.bg_color, paddingTop: 10 }} >

                {this.state.allClasses.map((item, index) => {
                  return (
                    <TouchableOpacity
                      onPress={() => {
                        this.props.navigation.navigate('ClassDetails', {
                          classId: item.id
                        })
                      }}
                      style={{ borderRadius: 4 }} key={item.id}>
                      <Card style={{ flex: 0, marginStart: 16, marginEnd: 16, }} transparent>
                        <CardItem style={{ borderRadius: 5 }}>
                          <View style={{ flexDirection: 'row' }}>
                            <Thumbnail source={{ uri: item.image }} style={{ height: 63, width: 63, borderRadius: 63 / 2 }} />
                          </View>

                          <View style={{ flex: 1, marginStart: 16, marginEnd: 16 }}>
                            <Text style={[font_style.font_bold, { color: Colors.dark_gray, fontSize: 14 }]}>{item.exercise_type}</Text>
                            {/* <Text style={[font_style.font_bold, styles.meta_txt, { color: Colors.primaryColor }]}>${item.per_class_price !== undefined && item.per_class_price !== null ? item.per_class_price : 0}</Text> */}
                            <Text style={[font_style.font_medium, styles.meta_txt]}>By {item.admin_name}</Text>
                            <Text style={[font_style.font_medium, styles.meta_txt, { color: Colors.dark_gray }]}>{item.time_from} to {item.time_to}</Text>
                          </View>

                          <TouchableOpacity
                            onPress={() => { this._JoinClass(item, index) }}
                            style={[styles.joinButton, item.class_join === 'true' ? { backgroundColor: 'green', } : { backgroundColor: Colors.light_gray },]}>
                            <Image source={require('./../assets/images/icons/tick_3x.png')}
                              resizeMode="contain" style={[{ width: 20, height: 20, }, item.class_join === 'true' ? { tintColor: '#fff', } : { tintColor: Colors.dark_gray }]} resizeMode="contain" />
                          </TouchableOpacity>

                        </CardItem>
                      </Card>
                    </TouchableOpacity>

                  )
                })}
              </View>
              )
            }
          </View>)}

        {this.state.data !== null && this.state.data.banner_show !== null && this.state.data.banner_show === true && (
          <View style={{ padding: 16, backgroundColor: Colors.primaryColor, position: 'absolute', width: Dimensions.get('screen').width, top: 0 }}>

            <View style={{ paddingTop: 16, paddingBottom: 16, }}>
              <Text style={[font_style.font_medium, { fontSize: 16, color: Colors.white_color }]}>{this.state.data.banner_title}</Text>
              <Text style={[font_style.font_medium, { fontSize: 14, color: Colors.white_color }]}>{this.state.data.banner_description}</Text>

            </View>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => { this.closeBanner() }}
              style={{
                position: 'absolute', alignItems: 'center', justifyContent: 'center',
                right: 0, width: 56, height: 48, top: 8,
              }}>
              <Ionicons name="md-close" size={28} color='#fff' />

            </TouchableOpacity>
          </View>

        )}

      </View>
    )

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  meta_txt: { color: 'rgba(90,91,93,0.7)', fontSize: 12, marginTop: 4 },
  joinButton: {
    width: 56,
    height: 32,
    borderRadius: 32 / 2,
    justifyContent: 'center',
    alignItems: 'center'
  }

});
