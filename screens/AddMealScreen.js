import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage,
  Keyboard
} from 'react-native';
import { Icon } from 'react-native-elements'
import { TextInput, ScrollView } from 'react-native-gesture-handler';
import { textInput, buttonStyle, text_color, textHeader, font_style } from '../components/styles';
import Colors from '../constants/Colors';
import Constant from '../constants/Constant';
import HeaderRight from '../components/HeaderRight';
import HeaderBackground from '../components/HeaderBackground';
import HeaderBackLeft from '../components/HeaderBackLeft';
import { Searchbar } from 'react-native-paper';
import { Container, Content, Card, CardItem, Thumbnail, Left, Body, Right, ListItem } from 'native-base'
import ApiService from '../webservice/ApiService';
import Loader from '../components/Loader';
import Utils from '../constants/Utils';
import moment from 'moment';

export default class AddMealScreen extends Component {

  static navigationOptions = ({ navigation }) => ({

    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${navigation.state.params.title}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,
    //headerRight: <HeaderRight navigationProps={navigation} screen="AddMeal" />,
  });

  state = {
    search: '',
    history: null,

    search_result: null,
    FoodAPIData: '',
    secretTokenData: null,
    userInfo: null,
  };

  componentDidMount = () => {

    this.getSecretToken();

    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })

        this.fetchSearchHistory()

      })
      .catch(error => console.log('error!', error));
  }

  getSecretToken = () => {
    AsyncStorage.getItem(Constant.FoodAPI)
      .then(req => JSON.parse(req))
      .then(json => {

        if (json === undefined || json === null) {
          Utils._FoodApiTokenGenerate()
          this.getSecretToken()
        } else {
          this.setState({ secretTokenData: json })
        }

      })
      .catch(error => {
        console.log('error!' + JSON.stringify(error.response))
      })
  }

  fetchSearchHistory = () => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }
    ApiService.api.get(ApiService.search_history, { headers: headers })
      .then((response) => {

        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            this.setState({ history: response.data.data != null ? response.data.data : [] })

          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })
  }

  _MealDetails = (food_id) => {
    this.props.navigation.navigate('MealNutrition', {
      food_id: food_id,
      title: this.props.navigation.getParam('title', ''),
      type: this.props.navigation.getParam('type', ''),
      date: this.props.navigation.getParam('date', moment().format('MM-DD-YYYY'))
    })
  }

  _SearchData = (search) => {

    const currentTimestamp = Date.now()
    let diff = (currentTimestamp - this.state.secretTokenData.token_time) / 1000
    if (diff < this.state.secretTokenData.expires_in) {
      var headers = {
        'X-Access-Token': this.state.userInfo.access_key,
      }
      const params = {
        search_expression: `method=foods.search&format=json&search_expression=${this.state.search}`,
        secret_token: this.state.secretTokenData.secret_token,

      }

      ApiService.api.post(ApiService.search_meal, params, { headers: headers })
        .then((response) => {

          if (response !== null && response.data != null) {
            if (response.data.status === Constant.SUCCESS) {
              this.setState({ search_result: response.data.data.foods.food != null ? response.data.data.foods.food : [] })

            } else if (response.data.status === Constant.WARNING) {
              if (response.data.session !== undefined && response.data.session !== null) {
                if (response.data.session === false) {
                  Utils._signOutAsync(this.props.navigation)
                }
              }
            }
          }

        })
        .catch(function (error) {
          //alert('Error')
          Utils._SentryReport(error)
          console.log("error " + JSON.stringify(error.response));
        })

    } else {
      Utils._FoodApiTokenGenerate()
      this._SearchData(this.state.search)
    }
  }

  _getMetaData = (value) => {
    const splitArray = value.split("-");
    const meta = splitArray[0].replace("Per", "")
    return meta.trim()
  }

  _getValue = (value) => {
    let splitArray = value.split("-");
    splitArray = splitArray[1].split("|");
    // splitArray.forEach(element => {
    //   const pair = element.split(":")
    //   const title = pair[0].replace(/[^a-zA-Z0-9_,\s]/g, '')
    //   const value = pair[1].replace(/[^a-zA-Z0-9_,\s]/g, '')

    // });
    meta = splitArray[0].replace("Calories:", "")
    meta = meta.replace("kcal", "")
    return meta.trim()
  }

  render() {

    const { search } = this.state;
    return (
      <TouchableOpacity onPress={() => { Keyboard.dismiss() }} activeOpacity={1} style={styles.container} >
        <View style={{ flexDirection: 'row', width: '100%', backgroundColor: '#fff' }}>
          <View style={styles.container} >
            <Searchbar
              transparent
              placeholder="Search"
              onChangeText={search => { this.setState({ search: search }); }}
              value={search}
              style={{
                elevation: 0,
                shadowOpacity: 0,
                borderBottomWidth: 0,
              }}
              onSubmitEditing={search => { this._SearchData(search) }}
            />
          </View>

          {/* <View style={styles.icon_view}>

            <TouchableOpacity
              //onPress={() => { this.props.navigationProps.goBack() }}
              style={{ marginEnd: 18, }}>
              <Image source={require('./../assets/images/icons/location_3x.png')}
                resizeMode="contain" style={{ width: 20, height: 20, tintColor: '#413F40' }} />
            </TouchableOpacity>
            <TouchableOpacity
            //onPress={() => { this.props.navigationProps.goBack() }}
            >
              <Image source={require('./../assets/images/icons/qr_code_3x.png')}
                resizeMode="contain" style={{ width: 20, height: 20, tintColor: '#413F40' }} />
            </TouchableOpacity>
          </View> */}
        </View>

        <Container style={{ backgroundColor: Colors.bg_color, }} >
          {search === null || search === ''
            ?
            (<Content >
              <CardItem style={{ backgroundColor: Colors.bg_color }} >
                <Left>
                  <Text style={[{ fontSize: 14, color: Colors.dark_gray }, font_style.font_bold]}>History</Text>
                </Left>

              </CardItem>
              {(this.state.history === undefined || this.state.history === null) ?
                (<Loader />)
                : (
                  <View>
                    {this.state.history.map((item, index) => {
                      return (
                        <TouchableOpacity key={index} onPress={() => { this._MealDetails(item.food_id) }} activeOpacity={1}>
                          <Card transparent style={{ marginTop: -10 }} >
                            <CardItem style={{ backgroundColor: Colors.bg_color }}>
                              <Left>
                                <Text style={[font_style.font_medium, styles.meta_txt]}>{item.food_name}</Text>
                              </Left>
                              <Right>
                                <Text style={[font_style.font_medium, styles.meta_txt]}>{item.single_item}</Text>
                              </Right>
                            </CardItem>
                          </Card>
                        </TouchableOpacity>

                      )
                    })}
                  </View>
                )}

            </Content>
            )
            :
            (<Content >
              <CardItem style={{ backgroundColor: Colors.bg_color }}>
                <Left>
                  <Text style={[{ fontSize: 14, color: Colors.dark_gray }, font_style.font_bold]}>Search Results</Text>
                </Left>

              </CardItem>
              {(this.state.search_result === undefined || this.state.search_result === null) ?
                (<Loader />)
                : (
                  <View>
                    {this.state.search_result.map((item, index) => {
                      return (

                        <ListItem key={index} onPress={() => { this._MealDetails(item.food_id) }}>
                          <Left>
                            <View style={{ flexDirection: 'column' }}>
                              <Text style={[font_style.font_medium, styles.meta_txt]}>{item.food_name}</Text>
                              <View></View>
                              <Text note style={[font_style.font_medium, styles.sub_meta_txt]}>{this._getMetaData(item.food_description)}</Text>
                            </View>
                          </Left>
                          <Right>
                            <Text style={[font_style.font_medium, styles.meta_txt]}>{this._getValue(item.food_description)}</Text>
                          </Right>
                        </ListItem>


                        // <Card style={{
                        //   flex: 0,
                        //   borderBottomColor: Colors.light_gray,
                        //   borderBottomWidth: 1,
                        //   marginStart: 16, marginEnd: 16,
                        //   marginTop: -5,
                        // }} key={item.id} transparent>
                        //   <CardItem style={{ backgroundColor: Colors.bg_color }}>

                        //   </CardItem>
                        // </Card>
                      )
                    })}
                  </View>)}


            </Content>)
          }


        </Container>

      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,

  },
  icon_view: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingEnd: 20,
    alignItems: 'center'
  },
  meta_txt: {
    color: Colors.dark_gray,
    fontSize: 14,
  },
  sub_meta_txt: {
    color: Colors.meta_color,
    fontSize: 12,
    marginTop: 6
  }

});