import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
  Dimensions,
  AsyncStorage,
  FlatList
} from 'react-native';
import HeaderLeft from '../components/HeaderLeft';
import HeaderRight from '../components/HeaderRight';
import moment from 'moment';

import { textInput, buttonStyle, text_color, textHeader } from '../components/styles';
import Colors from '../constants/Colors';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base'
import Constant from '../constants/Constant';
import { font_style } from '../components/styles';
import HeaderBackground from '../components/HeaderBackground';
import HeaderBackLeft from '../components/HeaderBackLeft';
import Loader from '../components/Loader';
import ApiService from '../webservice/ApiService';
import Utils from '../constants/Utils';

export default class OrdersHistoryScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.view_order_details}`.toUpperCase()}</Text>,
    headerLeft: <TouchableOpacity
      onPress={() => {
        navigation.navigate("MyOrder", {
          refresh: 'yes'
        })
      }}
      style={textHeader.leftIcon}>
      <Image source={require('./../assets/images/icons/arrow-right_3x.png')}
        resizeMode="contain" style={{ width: 20, height: 20, alignSelf: 'flex-start' }} />
    </TouchableOpacity>,
    //headerRight: <HeaderRight navigationProps={navigation} />,
  });

  state = {
    data: null
  }

  componentDidMount = () => {
    const orderId = this.props.navigation.getParam('orderId', null);

    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        var headers = {
          'X-Access-Token': json.access_key,
        }

        ApiService.api.get(ApiService.OrderDetails + orderId, { headers: headers })
          .then((response) => {
            if (response !== null && response.data != null) {
              if (response.data.status === Constant.SUCCESS) {
                this.setState({
                  data: response.data.data !== null ? response.data.data : {},
                })
              } else if (response.data.status === Constant.WARNING) {
                if (response.data.session !== undefined && response.data.session !== null) {
                  if (response.data.session === false) {
                    Utils.toastShow(response.data.message)
                    Utils._signOutAsync(this.props.navigation)
                  }
                }
              }
            }

          })
          .catch(function (error) {
            Utils._SentryReport(error)
            console.log("error " + JSON.stringify(error.response));
          })

      })
      .catch(error => console.log('error!'));
  }

  _CheckoutOrders = () => {

    let newArray = []
    this.state.data.all_products.forEach(element => {
      const product = {
        name: element.product_name,
        image: element.product_image,
        id: element.product_id,
        description: element.product_description,
        sub_title: element.product_subtitle,
        price: element.unit_price

      }
      let userJson = {
        product: product,
        admin_id: element.admin_id,
        product_id: element.product_id,
        quantity: 1,
        unit_price: parseFloat(element.unit_price),
        price: parseFloat(element.unit_price) * 1,
      }
      newArray = [...newArray, userJson]
    });
    AsyncStorage.setItem(Constant.addToCart, JSON.stringify(newArray))
    this.props.navigation.navigate('Cart', {
      shipping: null,
      redirect: 'MyOrder'
    })

  }
  render() {
    const state = this.state
    if (this.state.data === undefined || this.state.data === null) {
      return (<Loader />)
    } else {
      return (
        <ScrollView style={styles.container}>
          <View style={styles.container}>

            <View style={{
              paddingLeft: 20,
              paddingRight: 20,
              marginTop: 10
            }}>
              <View style={{
                flexDirection: 'row',
                paddingBottom: 8,
                marginTop: 8
              }}>
                <Text style={[{ flex: 1 }, styles.txt_style, font_style.font_medium]}>Order Date</Text>
                <Text style={[{ alignItems: 'flex-end' }, styles.txt_style, font_style.font_medium]}>{this.state.data.order_date}</Text>
              </View>


              <View style={{
                flexDirection: 'row',
                paddingBottom: 8,
                marginTop: 8
              }}>
                <Text style={[{ flex: 1 }, styles.txt_style, font_style.font_medium]}>Order Number</Text>
                <Text style={[{ alignItems: 'flex-end' }, styles.txt_style, font_style.font_medium]}>{state.data.order_number}</Text>
              </View>

              <View style={{
                flexDirection: 'row',
                marginTop: 8
              }}>
                <Text style={[{ flex: 1 }, styles.txt_style, font_style.font_medium]}>Order Total</Text>
                <Text style={[{ alignItems: 'flex-end' }, styles.txt_style, font_style.font_medium]}>${state.data.total_amount}</Text>
              </View>

              <View style={{
                flexDirection: 'row',
                paddingBottom: 8,
                marginTop: 8
              }}>
                <Text style={[{ flex: 1 }, styles.txt_style, font_style.font_medium]}>Tax</Text>
                <Text style={[{ alignItems: 'flex-end' }, styles.txt_style, font_style.font_medium]}>${state.data.taxAmount}</Text>
              </View>

              {state.data.shipping !== null && state.data.shipping !== 0 && (
                <View style={{
                  flexDirection: 'row',
                  paddingBottom: 8,
                  marginTop: 8
                }}>
                  <Text style={[{ flex: 1 }, styles.txt_style, font_style.font_medium]}>Shipping</Text>
                  <Text style={[{ alignItems: 'flex-end' }, styles.txt_style, font_style.font_medium]}>${state.data.shipping}</Text>
                </View>
              )}


              <View style={{
                borderBottomColor: Colors.light_gray,
                borderBottomWidth: 1,
                marginTop: 20,
                marginBottom: 20
              }} />

              <Text style={[styles.heading_txt, font_style.font_bold]}>Payment Information</Text>
              <View style={{
                flexDirection: 'row',
              }}>
                <Text style={[{ flex: 1 }, styles.txt_style, font_style.font_medium]}>Payment Method</Text>
                <Text style={[{ alignItems: 'flex-end' }, styles.txt_style, font_style.font_medium]}>RuPay ending 7854</Text>
              </View>

              <View style={{
                borderBottomColor: Colors.light_gray,
                borderBottomWidth: 1,
                marginTop: 20,
                marginBottom: 20
              }} />

              <Text style={[styles.heading_txt, font_style.font_bold]}>Purchase Details</Text>

              <Text style={[styles.product_status, font_style.font_medium]}>Successful</Text>

              {/* <Text style={[styles.description_txt, font_style.font_Book]}>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
        </Text> */}

            </View>


            <FlatList
              style={{ paddingTop: 6, paddingBottom: 6 }}
              data={this.state.data.all_products}
              scrollEnabled={false}
              keyExtractor={(item, index) => {
                return index.toString();
              }}
              renderItem={({ item, index }) => (
                <Card transparent >
                  <CardItem style={{ backgroundColor: Colors.bg_color }} >
                    <Left>
                      <View style={{
                        height: 80,
                        width: 80,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: '#fff',
                        borderRadius: 5
                      }}>
                        <Image style={styles.image} source={{ uri: item.product_image }} style={{
                          height: 65,
                          width: 65,
                          alignItems: 'center'
                        }} />
                      </View>
                      <Body>
                        <Text style={[font_style.font_bold, { color: Colors.dark_gray, fontSize: 14 }]}>{item.product_name}</Text>
                        <Text style={[font_style.font_bold, styles.meta_txt]}>${item.unit_price} * {item.quantity} = ${parseFloat(item.unit_price * item.quantity).toFixed(2)}</Text>
                        {/* <Text style={[font_style.font_Book, styles.meta_txt]} numberOfLines={2}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.</Text> */}
                      </Body>
                    </Left>
                  </CardItem>
                </Card>


              )}
            />

            <TouchableOpacity
              onPress={() => this._CheckoutOrders()}
              style={{
                paddingTop: 10,
                paddingBottom: 10,
                backgroundColor: Colors.primaryColor,
                borderRadius: 50,
                height: 46,
                justifyContent: 'center',
                marginStart: 20,
                marginEnd: 20, marginBottom: 30
              }}
              underlayColor='#fff'>
              <Text style={[buttonStyle.btnText, { fontSize: 14 }]}>{'PURCHASE AGAIN'.toUpperCase()}</Text>
            </TouchableOpacity>

          </View>
        </ScrollView >
      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,

  },

  meta_txt: {
    color: Colors.dark_gray,
    fontSize: 14,
    marginTop: 4
  },
  txt_style: {
    fontSize: 14,
    color: Colors.dark_gray,
  },
  heading_txt: {
    marginBottom: 20,
    fontSize: 14,
    color: '#413F40'
  },
  product_status: {
    fontSize: 16,
    color: '#0AB50A',
    marginBottom: 20
  },
  description_txt: {
    fontSize: 14,
    color: Colors.dark_gray,
    marginBottom: 8,
  },
  product_title: {
    fontSize: 14,
    color: Colors.dark_gray,
    marginBottom: 8,
  }
});
