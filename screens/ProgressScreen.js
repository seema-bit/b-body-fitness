import React, { Component } from 'react'
import { ScrollView, StyleSheet, Image, Text, Dimensions, TouchableOpacity, AsyncStorage, ImageBackground } from 'react-native';
import { View } from 'native-base';
import HeaderLeft from '../components/HeaderLeft';
import Constant from '../constants/Constant';
import Colors from '../constants/Colors';
import { textHeader, font_style } from '../components/styles';
import {
  LineChart
} from 'react-native-chart-kit'
import { Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right, ListItem } from 'native-base'
import HeaderProgressRight from '../components/HeaderProgressRight';
import HeaderBackground from '../components/HeaderBackground';
import Modal from 'react-native-modalbox';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button'
import { TextInput } from 'react-native-gesture-handler';
import Utils from '../constants/Utils';
import ApiService from '../webservice/ApiService';
import moment from 'moment'
import Loader from '../components/Loader';
import DataNotFound from '../components/DataNotFound';

export default class ProgressScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.progress}`.toUpperCase()}</Text>,
    headerLeft: <HeaderLeft navigationProps={navigation} />,
    headerRight: <TouchableOpacity
      onPress={() => { _this._RedirectIntoScreen() }}
      style={textHeader.rightIcon}>
      <Image source={require('./../assets/images/icons/plus_3x.png')}
        style={{ width: 20, height: 20, alignSelf: 'flex-end' }} resizeMode="contain" />
    </TouchableOpacity>


  });

  state = {
    data: null,
    isDisabled1: false,
    isDisabled2: false,
    isOpen: false,
    temp_measurements: {
      index: 0,
      value: 'weight',
      name: 'Weight'
    },
    temp_time: {
      index: 1,
      value: "month",
      name: '1 Month'
    },
    measurements: {
      index: 0,
      value: 'weight',
      // name: 'Weight'
      name: 'InBody'
    },
    time: {
      index: 1,
      value: "month",
      name: '1 Month'
    },
    measurement: '',
    setDate: moment().format('MM-DD-YYYY'),
    image: '',
    random: '',
    userInfo: '',
    datasets: [],
    labels: [],
    userInfo: null,
    inBodyData: null,
    facts: [
      'Weight',
      'SMM(SkeletalMuscleMass)',
      'BFM(BodyFatMass)',
      'PBF(PercentBodyFat)',
    ],

  }

  componentDidMount() {
    _this = this
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        console.log('json : ', json)
        this._FetchData(this.state.measurements, this.state.time)
        this._getFullInBodyData()
      })
      .catch(error => console.log('error!', error));

  }

  _getFullInBodyData = () => {
    const params = {
      usertoken: 2139991871
    }
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    ApiService.api.post(ApiService.getFullInBodyData, params, { headers: headers })
      .then((response) => {
        console.log('getFullInBodyData : ', response.data.data)
        this.setState({ inBodyData: [] })
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            if (response.data.data !== null) {
              let splitArray = JSON.stringify(response.data.data);
              splitArray = splitArray.split("\",")
              splitArray.forEach(element => {
                const pair = element.split(":")
                let title = pair[0].replace(/"/g, '')
                console.log(title)
                let item = pair[1].replace(/"/g, '')
                const check = this.state.facts.includes(title)
                console.log('check : ', check)
                if (check) {
                  let facts = { title: title, item: item }
                  this.setState({ inBodyData: [...this.state.inBodyData, facts] })
                  //tempArray = [...tempArray, facts]

                }
              })


            } else {

            }
            //this.setState({ inBodyData: response.data.data !== null ? response.data.data : null })

          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })


  }

  _UpdateData() {
    this.setState({ data: null })
    this._FetchData(this.state.measurements, this.state.time)
  }

  componentWillReceiveProps = () => {
    _this = this
    this.setState({ data: null })
    this._FetchData(this.state.measurements, this.state.time)
  }

  componentWillMount() {
    _this = this
  }

  componentWillUpdate() {
    _this = this
  }

  _FetchData = (measurement, time) => {
    const params = {
      type: measurement.value,
      time_period: time.value
    }

    this.setState({ data: null, labels: [], datasets: [] })

    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    ApiService.api.post(ApiService.fetchProgress, params, { headers: headers })
      .then((response) => {
        this.setState({ data: [], labels: [], datasets: [] })
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            this.setState({ data: response.data.data != null ? response.data.data : [] })

            if (response.data.graph_data !== undefined && response.data.graph_data !== null && response.data.graph_data.length !== 0) {
              response.data.graph_data.forEach(element => {
                //const label = ''
                // if (element.day !== undefined && element.day !== null && element.day !== '' && element.day !== 'undefined' && element.day !== 'null') {
                //   label = moment(element.day).format("DD MMM")
                // }
                // else {
                //   label = ''
                // }


                let progressCount = element.peoples !== null && element.peoples !== undefined && element.peoples !== '' ? element.peoples : 0
                this.setState({

                  labels: [...this.state.labels, moment(element.day, 'MM-DD-YYYY').format("DD MMM")],
                  datasets: [...this.state.datasets, progressCount]
                })
              });
            }


          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })
  }
  _RedirectIntoScreen = () => {
    switch (this.state.measurements.index) {

      case 0:
        this.props.navigation.navigate('AddProgress', {
          type: this.state.measurements.value,
          //_UpdateData: () => this._UpdateData()
        })

        break

      case 1:
        this.setState({ measurement: '' })
        this.refs.modal3.open()
        break

      case 2:
        this.setState({ measurement: '' })
        this.refs.modal3.open()
        break

      case 3:
        this.setState({ measurement: '' })
        this.refs.modal3.open()
        break


      default:
        break
    }
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  onMeasurementSelect(index, value) {
    let name = this.setName("0", index)
    this.setState({
      temp_measurements: { index: index, value: value, name: name }
    })
  }

  onTimeSelect(index, value) {
    let name = this.setName("1", index)
    this.setState({
      temp_time: { index: index, value: value, name: name }
    })
  }

  saveValue = (type) => {

    if (type === "0") {
      this.setState({ measurements: this.state.temp_measurements })
      this.refs.modal1.close()
    } else {
      this.setState({ time: {} })
      let newObj = {
        index: this.state.temp_time.index,
        value: this.state.temp_time.value,
        name: this.state.temp_time.name,
      }

      this.setState({ time: { ...this.state.temp_time, newObj } })
      this.refs.modal2.close()
    }
    this._FetchData(this.state.temp_measurements, this.state.temp_time)
  }

  setName = (type, index) => {
    if (type === "0") {
      switch (index) {

        case 0:
          return "InBody"

        // case 1:
        //   return "Neck"

        case 1:
          return "Waist"

        case 2:
          return "Hips"


        default:
          return "InBody"
      }
    } else {
      switch (index) {
        case 0:
          return "1 Week"

        case 1:
          return "1 Month"

        case 2:
          return "2 Months"

        case 3:
          return "3 Months"

        case 4:
          return "6 Months"

        case 5:
          return "1 Year"

        case 6:
          return "All"


        default:
          return "1 Month"
      }
    }
  }

  _SetMeasurement = () => {
    this.refs.modal3.close()
    this._AddProgress()
  }

  _AddProgress = () => {

    if (this.state.measurement !== null && this.state.measurement !== '') {

      this.setState({ random: Utils.handleClick() })
      var params = new FormData();
      params.append('type', this.state.measurements.value)
      params.append('measurement', this.state.measurement);
      params.append('current_date', this.state.setDate);

      var headers = {
        'X-Access-Token': this.state.userInfo.access_key,
      }
      ApiService.api.post(ApiService.addProgress, params, { headers: headers })
        .then((response) => {
          if (response !== null && response.data != null) {
            if (response.data.status === Constant.SUCCESS) {
              this._FetchData(this.state.measurements, this.state.time)
              //this.props.navigation.goBack()
            } else if (response.data.status === Constant.WARNING) {
              if (response.data.session !== undefined && response.data.session !== null) {
                if (response.data.session === false) {
                  Utils.toastShow(response.data.message)
                  Utils._signOutAsync(this.props.navigation)
                }
              }
            }
          }

        })
        .catch(function (error) {
          Utils._SentryReport(error)
          console.log("error " + JSON.stringify(error.response));
        })

    } else {
      Utils.toastShow("Please enter a valid weight")
    }


  }

  render() {

    if (this.state.data === undefined || this.state.data === null) {
      return (<Loader />)
    } else {
      return (
        <View style={styles.container}>

          <Modal
            animationIn="fadeIn"
            animationOut="fadeOut"
            hideModalContentWhileAnimating={true}
            animationInTiming={1500}
            animationOutTiming={1500}
            backdropTransitionOutTiming={0}
            style={[styles.modal, { width: 300, height: 150 }]} position={"center"} ref={"modal3"}
            coverScreen={true}>

            <View >
              <Text style={[{ marginStart: 24, marginTop: 24, fontSize: 16, marginBottom: 16 }, font_style.font_medium]}>Today's {this.state.measurements.name}</Text>

              <TextInput style={[{ marginStart: 24, }, styles.txt_style, font_style.font_medium]}
                placeholder="0.0"
                placeholderTextColor={Colors.dark_gray}
                keyboardType={'numeric'}
                onChangeText={(measurement) => this.setState({ measurement })}
                value={this.state.measurement}
              />

              <View style={styles.btn_view}>
                <TouchableOpacity
                  style={styles.cancel_btn}
                  onPress={() => this.refs.modal3.close()}>
                  <Text style={{ color: Colors.primaryColor }}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.ok_btn}
                  onPress={() => this._SetMeasurement()}>
                  <Text style={{ color: Colors.primaryColor }}>Set</Text>
                </TouchableOpacity>
              </View>

            </View>

          </Modal>

          <Modal
            animationIn="fadeIn"
            animationOut="fadeOut"
            hideModalContentWhileAnimating={true}
            animationInTiming={1500}
            animationOutTiming={1500}
            backdropTransitionOutTiming={0}
            coverScreen={true} style={[styles.modal, { height: 320, width: Dimensions.get('screen').width - 40 }]} position={"center"} ref={"modal1"} isDisabled={this.state.isDisabled1}>

            <View style={{ flex: 1, }}>
              <Text style={[{ marginStart: 24, marginTop: 24, fontSize: 16, marginBottom: 16 }, font_style.font_medium]}>InBody</Text>


              {this.state.inBodyData !== null ?
                <ScrollView style={{ flex: 1, }}>
                  {this.state.inBodyData.map((item, index) => {
                    return (

                      <ListItem key={item.id} style={{ marginEnd: 20, }}>
                        <Left>
                          <Text style={[font_style.font_medium, styles.meta_txt, { textTransform: 'capitalize' }]}>{item.title}</Text>
                        </Left>
                        <Right>
                          <Text style={[font_style.font_medium, styles.meta_txt]}>{item.item}</Text>
                        </Right>
                      </ListItem>
                    )
                  })}
                </ScrollView>

                : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}><Loader /></View>
              }


              <View style={[styles.btn_view, { justifyContent: 'flex-end' }]}>
                <TouchableOpacity
                  style={styles.cancel_btn}
                  onPress={() => this.refs.modal1.close()}>
                  <Text style={{ color: Colors.primaryColor }}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.ok_btn}
                  onPress={() => this.refs.modal1.close()}>
                  <Text style={{ color: Colors.primaryColor }}>Ok</Text>
                </TouchableOpacity>
              </View>

            </View>

          </Modal>


          <Modal
            animationIn="fadeIn"
            animationOut="fadeOut"
            hideModalContentWhileAnimating={true}
            animationInTiming={1500}
            animationOutTiming={1500}
            backdropTransitionOutTiming={0}
            coverScreen={true} style={[styles.modal, styles.modal3, { height: 400 }]}
            position={"center"} ref={"modal2"}
          >

            <View >
              <Text style={[{ marginStart: 24, marginTop: 24, fontSize: 16, marginBottom: 16 }, font_style.font_medium]}>Graph Period</Text>
              {/* <View style={{ height: 180, }}> */}
              <RadioGroup
                style={{ marginStart: 8, justifyContent: 'center' }}
                selectedIndex={this.state.time.index}
                onSelect={(index, value) => this.onTimeSelect(index, value)}
              >
                <RadioButton value={'week'} >
                  <Text>1 Week</Text>
                </RadioButton>

                <RadioButton value={'month'}>
                  <Text>1 Month</Text>
                </RadioButton>

                <RadioButton value={'2_months'}>
                  <Text>2 Months</Text>
                </RadioButton>

                <RadioButton value={'3_months'}>
                  <Text>3 Months</Text>
                </RadioButton>

                <RadioButton value={'6_months'}>
                  <Text>6 Months</Text>
                </RadioButton>

                <RadioButton value={'1_year'}>
                  <Text>1 Year</Text>
                </RadioButton>

                {/* <RadioButton value={'all'}>
                  <Text>All</Text>
                </RadioButton> */}
              </RadioGroup>
              {/* </View> */}


              <View style={styles.btn_view}>
                <TouchableOpacity
                  style={styles.cancel_btn}
                  onPress={() => this.refs.modal2.close()}>
                  <Text style={{ color: Colors.primaryColor }}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.ok_btn}
                  onPress={() => this.saveValue("1")}>
                  <Text style={{ color: Colors.primaryColor }}>Ok</Text>
                </TouchableOpacity>
              </View>

            </View>

          </Modal>

          <View style={{
            flexDirection: 'row', height: 46,
            borderBottomColor: Colors.light_gray,
            borderBottomWidth: 1,
            backgroundColor: '#fff',
            justifyContent: 'center'
          }}>

            <TouchableOpacity style={{ width: '49%', alignItems: 'center', justifyContent: 'center', }}
              onPress={() => this.refs.modal1.open()}
              activeOpacity={1}
            >

              <View style={{ alignSelf: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                <Image source={require('../assets/images/icons/weight-scale_3x.png')} resizeMode="contain" style={[this.state.selectedTab === 0 ? { tintColor: Colors.primaryColor } : { tintColor: Colors.dark_gray }, { width: 20, height: 20, marginRight: 12, alignSelf: 'center' }]} />
                <Text style={[this.state.selectedTab === 0 ? { color: Colors.primaryColor } : { color: Colors.dark_gray }, { fontSize: 19, }]}>{this.state.measurements.name}</Text>
              </View>

            </TouchableOpacity>
            <View style={{
              borderLeftColor: Colors.light_gray,
              borderLeftWidth: 1,
              marginTop: 6,
              marginBottom: 6
            }}>
            </View>
            <TouchableOpacity style={{ width: '49%', alignItems: 'center', justifyContent: 'center', }}
              onPress={() => this.refs.modal2.open()}
              activeOpacity={1}>

              <View style={{ alignSelf: 'center', flexDirection: 'row', }}>
                <Image source={require('../assets/images/icons/caledar_3x.png')} resizeMode="contain" style={[this.state.selectedTab === 1 ? { tintColor: Colors.primaryColor } : { tintColor: Colors.dark_gray }, { width: 20, height: 20, marginRight: 12, alignSelf: 'center' }]} />
                <Text style={[this.state.selectedTab === 1 ? { color: Colors.primaryColor } : { color: Colors.dark_gray }, { fontSize: 19, }]}>{this.state.time.name}</Text>
              </View>

            </TouchableOpacity>
          </View>

          {this.state.data.length !== 0 ?
            (<ScrollView style={{ flex: 1 }}>
              <View style={{ backgroundColor: '#fff' }}>
                <LineChart
                  data={{
                    labels: this.state.labels,
                    datasets: [{
                      data: this.state.datasets
                    }]
                  }}
                  width={Dimensions.get('window').width} // from react-native
                  height={220}
                  yAxisLabel={''}
                  withShadow={false}
                  withOuterLines={false}
                  withInnerLines={true}

                  //drawGridBackground={false}
                  chartConfig={{
                    backgroundColor: Colors.bg_color,
                    backgroundGradientFrom: Colors.bg_color,
                    backgroundGradientTo: Colors.bg_color,
                    decimalPlaces: 0, // optional, defaults to 2dp
                    color: (opacity = 1) => `rgba(0,0,255,1)`,
                    style: {
                      borderRadius: 16,
                      paddingStart: -20
                    }
                  }}
                  style={{
                    //marginVertical: 8,
                    backgroundColor: Colors.primaryColor,
                    color: '#000',
                    fontSize: 8,
                    borderColor: '#fff',
                    borderWidth: 20,
                    alignItems: 'center',
                    justifyContent: 'center',


                  }}
                />
              </View>

              <View style={{ backgroundColor: Colors.bg_color, paddingTop: 10, flex: 1 }}>
                {/* <Text style={[{ fontSize: 14, color: Colors.dark_gray, marginStart: 16 }, font_style.font_medium]}>Share your Progress</Text> */}
                {this.state.data.map((item, index) => {
                  return (
                    <Card style={{
                      flex: 0,
                      borderBottomColor: Colors.light_gray,
                      borderBottomWidth: 1,
                      marginStart: 16, marginEnd: 16,
                    }} key={item.id} transparent>

                      {this.state.measurements.value === 'weight' && (
                        <CardItem style={{ borderRadius: 4, backgroundColor: Colors.bg_color }}>
                          <Left>

                            <ImageBackground source={require('../assets/images/thumbnail.png')}
                              style={styles.img_view}
                              imageStyle={styles.imageStyle}>
                              <Image source={{ uri: item.image }} style={styles.img_view} />
                            </ImageBackground>

                            {/* <Thumbnail source={item.image !== null && item.image !== '' ? { uri: item.image } : require('../assets/images/thumbnail.png')} /> */}
                            <TouchableOpacity onPress={() => {
                              this.props.navigation.navigate("PhotoProgress",
                                {
                                  data: this.state.data
                                }
                              )
                            }}>
                              <Body>
                                {(item.current_date !== null && item.current_date !== undefined && item.current_date !== '' && item.current_date !== 'undefined') &&
                                  <Text style={[font_style.font_medium, styles.meta_txt]}>{item.current_date}</Text>
                                }

                                {(item.weight !== null && item.weight !== undefined && item.weight !== '' && item.weight !== 'undefined') ?
                                  (<Text style={[font_style.font_medium, styles.meta_txt]}>{item.weight} lbs</Text>)
                                  :
                                  (<Text style={[font_style.font_medium, styles.meta_txt]}>0 lbs</Text>)
                                }

                              </Body>
                            </TouchableOpacity>

                          </Left>
                        </CardItem>

                      )}

                      {/* {this.state.measurements.value === 'neck' && (
                        <CardItem style={{ borderRadius: 4, backgroundColor: Colors.bg_color }}>
                          <Body>
                            {(item.created_at !== null && item.created_at !== undefined && item.created_at !== '' && item.created_at !== 'undefined') &&
                              <Text style={[font_style.font_medium, styles.meta_txt]}>{moment(item.created_at).format('MM-DD-YYYY')}</Text>
                            }
                            {(item.neck_measurement !== null && item.neck_measurement !== undefined && item.neck_measurement !== '' && item.neck_measurement !== 'undefined') ?
                              (<Text style={[font_style.font_medium, styles.meta_txt]}>{item.neck_measurement}</Text>)
                              :
                              (<Text style={[font_style.font_medium, styles.meta_txt]}>0</Text>)
                            }

                          </Body>
                        </CardItem>
                      )} */}

                      {this.state.measurements.value === 'waist' && (
                        <CardItem style={{ borderRadius: 4, backgroundColor: Colors.bg_color }}>
                          <Body>
                            {(item.current_date !== null && item.current_date !== undefined && item.current_date !== '' && item.current_date !== 'undefined') &&
                              <Text style={[font_style.font_medium, styles.meta_txt]}>{item.current_date}</Text>
                            }
                            {(item.waist_measurement !== null && item.waist_measurement !== undefined && item.waist_measurement !== '' && item.waist_measurement !== 'undefined') ?
                              (<Text style={[font_style.font_medium, styles.meta_txt]}>{item.waist_measurement} inch</Text>)
                              :
                              (<Text style={[font_style.font_medium, styles.meta_txt]}>0 inch</Text>)
                            }

                          </Body>
                        </CardItem>
                      )}


                      {this.state.measurements.value === 'hips' && (
                        <CardItem style={{ borderRadius: 4, backgroundColor: Colors.bg_color }}>
                          <Body>
                            {(item.current_date !== null && item.current_date !== undefined && item.current_date !== '' && item.current_date !== 'undefined') &&
                              <Text style={[font_style.font_medium, styles.meta_txt]}>{item.current_date}</Text>
                            }
                            {(item.hips_measurement !== null && item.hips_measurement !== undefined && item.hips_measurement !== '' && item.hips_measurement !== 'undefined') ?
                              (<Text style={[font_style.font_medium, styles.meta_txt]}>{item.hips_measurement} inch</Text>)
                              :
                              (<Text style={[font_style.font_medium, styles.meta_txt]}>0 inch</Text>)
                            }

                          </Body>
                        </CardItem>
                      )}

                    </Card>
                  )
                })}
              </View>

            </ScrollView>)
            :
            (<DataNotFound />)
          }

        </View >
      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },
  meta_txt: {
    color: Colors.dark_gray,
    fontSize: 14,
    marginTop: 4
  },
  modal: {

  },

  modal2: {
    backgroundColor: "#3B5998"
  },

  modal3: {
    width: 300,
    height: 250,
  },
  btn_view: {
    marginTop: 16,
    flexDirection: 'row',
    height: 46, alignItems: 'flex-end',
    justifyContent: 'flex-end',

  },
  ok_btn: { height: 46, padding: 4, paddingStart: 16, justifyContent: 'center', paddingEnd: 24 },
  cancel_btn: { marginEnd: 24, padding: 4, height: 46, justifyContent: 'center', },
  txt_style: {
    fontSize: 14,
    color: Colors.dark_gray,
  },
  img_view: {
    width: 56,
    height: 56,
    borderRadius: 56 / 2
  },

  imageStyle: { borderRadius: 56 / 2 }
})