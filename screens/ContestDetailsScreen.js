import React from 'react';
import { ScrollView, StyleSheet, Image, Text, TouchableOpacity, AsyncStorage, ImageBackground } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import HeaderLeft from '../components/HeaderLeft';
import HeaderRight from '../components/HeaderRight';
import Colors from '../constants/Colors';
import { View } from 'native-base';
import Constant from '../constants/Constant';
import { textHeader, font_style, buttonStyle } from '../components/styles';
import HeaderBackground from '../components/HeaderBackground';
import HeaderBackLeft from '../components/HeaderBackLeft';
import Loader from '../components/Loader';
import ApiService from '../webservice/ApiService';
import Utils from '../constants/Utils';

export default class ContestDetailsScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.contest_details}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,
    //headerRight: <HeaderRight navigationProps={navigation} />,
  });

  state = {
    contestId: null,
    contestData: null
  }

  componentDidMount = () => {
    const contestId = this.props.navigation.getParam('contestId', null);
    this.setState({ contestId: contestId })
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        var headers = {
          'X-Access-Token': json.access_key,
        }

        ApiService.api.get(ApiService.contestDetails + contestId, { headers: headers })
          .then((response) => {
            if (response !== null && response.data != null) {
              if (response.data.status === Constant.SUCCESS) {
                this.setState({
                  contestData: response.data.data != null ? response.data.data : null,
                })
              } else if (response.data.status === Constant.WARNING) {
                if (response.data.session !== undefined && response.data.session !== null) {
                  if (response.data.session === false) {
                    Utils.toastShow(response.data.message)
                    Utils._signOutAsync(this.props.navigation)
                  }
                }
              }

            }

          })
          .catch(function (error) {
            Utils._SentryReport(error)
            console.log("error " + JSON.stringify(error.response));
          })

      })
      .catch(error => console.log('error!'));
  }

  _RedirectToRegister = (status, item) => {
    if (status !== true) {
      this.props.navigation.navigate('ContestRegister', {
        data: item
      })
    }

  }

  render() {
    state = this.state
    if (this.state.contestData === undefined || this.state.contestData === null) {
      return (<Loader />)
    } else {

      return (
        <ScrollView style={styles.container}>
          <View style={{}}>
            <ImageBackground source={require('../assets/images/thumbnail.png')} style={{ height: 163, width: '100%' }}>
              {state.contestData.image && (<Image source={{ uri: state.contestData.image }} style={{ height: 163, width: '100%' }} resizeMode="cover" />)}
            </ImageBackground>

            <View style={{ flexDirection: 'row', marginStart: 20, marginTop: 16, marginBottom: 14 }}>
              <View style={{ flexDirection: 'row', flex: 1 }}>
                <Image source={require('../assets/images/icons/trophy_3x.png')} style={{ height: 20, width: 20 }} resizeMode="contain" />
                <Text style={[styles.contest_title, font_style.font_medium]}>{state.contestData.name}</Text>
              </View>
              <View style={styles.status}>
                <Text style={[styles.status_txt, font_style.font_medium,]}>Active</Text>
              </View>

            </View>
            <View style={{ marginStart: 20, marginEnd: 20, marginBottom: 10 }}>
              <Text style={[styles.contest_details_txt, font_style.font_medium]}>Contest Detials:</Text>
              <Text style={[styles.contest_dis, font_style.font_Book]}>{state.contestData.description}</Text>
            </View>
            <View style={[styles.view_bg, { flexDirection: 'row' }]}>
              <Image source={require('../assets/images/icons/calendar_3x.png')} style={{ height: 20, width: 20 }} resizeMode="contain" />
              <Text style={[styles.heavy_text, font_style.font_heavy]}>Start Date : {state.contestData.start_date}</Text>
            </View>
            <View style={[styles.view_bg, { flexDirection: 'row' }]}>
              <Image source={require('../assets/images/icons/calendar_3x.png')} style={{ height: 20, width: 20 }} resizeMode="contain" />
              <Text style={[styles.heavy_text, font_style.font_heavy]}>End Date : {state.contestData.end_date}</Text>
            </View>
            {/* <View style={[styles.view_bg, { flexDirection: 'row' }]}>
            <Image source={require('../assets/images/icons/participant_3x.png')} style={{ height: 20, width: 20 }} resizeMode="contain" />
            <Text style={[styles.heavy_text, font_style.font_heavy]}>Participant : 11</Text>
          </View> */}
            {/* <View style={{ margin: 20 }}>
            <Text style={[styles.contest_details_txt, font_style.font_medium]}>Terms & Conditions</Text>
            <Text style={[styles.contest_dis, font_style.font_Book]}>Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</Text>
          </View> */}
            <TouchableOpacity
              onPress={() => {
                this._RedirectToRegister(state.contestData.user_contest, this.state.data)

              }}
              style={[{
                paddingTop: 10,
                paddingBottom: 10,


                borderRadius: 50,
                height: 46,
                justifyContent: 'center',
                marginStart: 20,
                marginEnd: 20, marginBottom: 30,
                marginTop: 10,
              },
              state.contestData.user_contest ? { backgroundColor: '#fff', } : { backgroundColor: Colors.primaryColor, }]}
              underlayColor='#fff'>
              <Text style={[buttonStyle.btnText, { fontSize: 14 }, state.contestData.user_contest ? { color: Colors.dark_gray, } : { color: '#fff' }]}>{state.contestData.user_contest ? 'Registered'.toUpperCase() : 'Register Now'.toUpperCase()}</Text>
            </TouchableOpacity>

          </View>

        </ScrollView>
      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },
  contest_title: {
    color: Colors.dark_gray,
    fontSize: 20,
    marginStart: 10

  },
  status_txt: {
    color: '#fff',
    fontSize: 14,
    paddingEnd: 8,
  },
  status: {
    backgroundColor: Colors.green,
    borderBottomLeftRadius: 26 / 2,
    borderTopLeftRadius: 26 / 2,
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: 54,
    height: 26,
  },
  contest_details_txt: {
    fontSize: 16,
    color: Colors.dark_gray
  },
  contest_dis: {
    color: Colors.dark_gray,
    fontSize: 14,
    marginTop: 10
  },

  price_text: {
    color: Colors.dark_gray,
    fontSize: 14,
    marginStart: 10
  },
  heavy_text: {
    color: Colors.dark_gray,
    fontSize: 16,
    marginStart: 10
  },

  view_bg: {
    backgroundColor: '#fff',
    height: 46,
    marginTop: 10,
    alignItems: 'center',
    paddingStart: 20
  },
  participate_btn: {
    fontSize: 14,
    color: '#fff',
    textAlign: 'center',
    marginTop: 30
  }
});
