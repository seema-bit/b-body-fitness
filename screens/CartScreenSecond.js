import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  ListView,
  TouchableOpacity,
  View,
  Image,
  Text,
  FlatList,
  AsyncStorage,
  ImageBackground
} from 'react-native';
import HeaderLeft from '../components/HeaderLeft';
import HeaderRight from '../components/HeaderRight';
import Constant from '../constants/Constant';
import { textHeader, font_style, buttonStyle } from '../components/styles';
import { FlatGrid } from 'react-native-super-grid';
import Colors from '../constants/Colors';
import { Rating, AirbnbRating } from 'react-native-elements';
import { Icon } from 'react-native-elements';
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import HeaderBackground from '../components/HeaderBackground';
import HeaderBackLeft from '../components/HeaderBackLeft';
import { ScrollView } from 'react-native-gesture-handler';
import Loader from '../components/Loader';
import ApiService from '../webservice/ApiService';
import Spinner from 'react-native-loading-spinner-overlay';
import DataNotFound from '../components/DataNotFound';
import CustomProgressBar from '../components/CustomProgressBar';
import { LinearGradient } from 'expo-linear-gradient';
import Utils from '../constants/Utils';

export default class CartScreenSecond extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.cart}`.toUpperCase()}</Text>,
    headerLeft: <TouchableOpacity
      onPress={() => {
        navigation.replace('MyOrder',
          {
            refresh: "yes"
          })
      }}
      style={textHeader.leftIcon}>
      <Image source={require('./../assets/images/icons/arrow-right_3x.png')}
        resizeMode="contain" style={{ width: 20, height: 20, alignSelf: 'flex-start' }} />
    </TouchableOpacity>,
    //headerRight: <HeaderRight navigationProps={navigation} />,
  });

  state = {
    data: null,
    tableHead: ['Total', '$440'],
    tableData: [
      ['Sub Total', '$400'],
      ['Shipping Cost', '$40'],
      ['Tax 2.5%', '$20'],
    ],
    totalPrice: 0,
    shipping: 0,
    tax: 0,
    priceCalculate: 0,
    shippingJson: null,
    userInfo: '',
    spinner: false
  }

  componentWillMount() {
    _this = this
  }
  componentDidMount() {
    _this = this
    const shippingCharge = this.props.navigation.getParam('shipping', null);

    AsyncStorage.getItem(Constant.addToCart)
      .then(req => JSON.parse(req))
      .then(json => {
        if (json !== null && json !== undefined) {
          this.setState({ data: json, shippingJson: shippingCharge !== null ? shippingCharge : null })
          this._PriceCalculate(json)

        } else {
          this.setState({ data: [], shippingJson: shippingCharge !== null ? shippingCharge : null })
        }

      })
      .catch(error => console.log('error!' + JSON.stringify(error.response)));
  }

  _AddItem = (index) => {
    //alert("add")
    let newArray = [...this.state.data]

    //let total = 0
    newArray.forEach((item, itemIndex) => {

      if (item.product_id === this.state.data[index].product_id) {

        newArray[itemIndex].quantity = parseInt(newArray[itemIndex].quantity) + 1
        newArray[itemIndex].price = parseInt(newArray[itemIndex].unit_price) * parseInt(newArray[itemIndex].quantity)

      }
      //total = total + newArray[itemIndex].price
    })

    this.setState({ data: newArray })

    this._PriceCalculate(this.state.data)

    AsyncStorage.setItem(Constant.addToCart, JSON.stringify(newArray))
  }

  _RemoveItem = (index) => {
    //alert("remove")
    let newArray = [...this.state.data]
    //let total = 0

    newArray.forEach((item, itemIndex) => {

      if (item.product_id === this.state.data[index].product_id) {

        if (newArray[itemIndex].quantity === 1) {
          newArray = [...this.state.data.slice(0, index), ...this.state.data.slice(index + 1)]
        } else {
          newArray[itemIndex].quantity = parseInt(newArray[itemIndex].quantity) - 1
          newArray[itemIndex].price = parseInt(newArray[itemIndex].unit_price) * parseInt(newArray[itemIndex].quantity)
        }

      }
      //total = total + newArray[itemIndex].price
    })

    this.setState({ data: newArray })

    this._PriceCalculate(this.state.data)

    AsyncStorage.setItem(Constant.addToCart, JSON.stringify(newArray))
  }

  _CloseItem = (index) => {
    //alert("close")
    let newArray = [...this.state.data]
    //let total = 0
    newArray.forEach((item, itemIndex) => {

      if (item.product_id === this.state.data[index].product_id) {
        newArray = [...this.state.data.slice(0, index), ...this.state.data.slice(index + 1)]

      }
      //total = total + newArray[itemIndex].price
    })

    this.setState({ data: newArray })
    this._PriceCalculate(this.state.data)


    AsyncStorage.setItem(Constant.addToCart, JSON.stringify(newArray))
  }

  _PriceCalculate = (productData) => {
    let total = 0
    productData.forEach(item => {
      total = total + parseInt(item.price)
    });
    total = total.toFixed(2)

    let taxAmount = (total / 100) * 2.5
    taxAmount = taxAmount.toFixed(2)


    let shippingCharge = 0
    if (this.state.shippingJson !== null) {
      this.state.shippingJson.forEach(item => {
        if (total <= parseInt(item.amount_limit)) {
          shippingCharge = parseInt(item.cost)
        }
      });
    }

    let calculate = parseInt(total) + parseInt(taxAmount) + parseInt(shippingCharge)
    this.setState({
      totalPrice: total, tax: taxAmount, shipping: shippingCharge, priceCalculate: calculate
    })
  }

  async removeItemValue(key) {
    try {
      await AsyncStorage.removeItem(key);
      return true;
    }
    catch (exception) {
      return false;
    }
  }


  _CheckoutOrder = () => {
    let params = []
    this.state.data.forEach(item => {
      let json = {
        product_id: item.product_id,
        quantity: item.quantity,
        admin_id: item.admin_id,
        unit_price: item.unit_price,
        price: item.price,
      }
      params = [...params, json]
    });

    const jsonparams = {
      products: params,
      total_amount: this.state.totalPrice,
      taxAmount: 0,
      shipping: this.state.shipping,

    }

    this.setState({ spinner: true })
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        var headers = {
          'X-Access-Token': json.access_key,
        }
        ApiService.api.post(ApiService.order, jsonparams, { headers: headers })
          .then((response) => {
            this.setState({ spinner: false })
            if (response !== undefined && response !== null && response.data.status !== undefined && response.data.status === Constant.SUCCESS) {
              this.removeItemValue(Constant.addToCart)
              this.props.navigation.navigate('MyOrder',
                {
                  refresh: "yes"
                })
            }


          })
          .catch(function (error) {
            //alert('Error')
            console.log("error " + JSON.stringify(error.response));
            //this.setState({ spinner: false })
            Utils._SentryReport(error)

          })

      })
      .catch(error => console.log('error!' + JSON.stringify(error.response)));

  }

  render() {

    if (this.state.data === undefined || this.state.data === null) {
      return (<Loader />)
    } else {
      if (this.state.data.length === 0) {
        return (<DataNotFound />)

      } else {
        return (
          <ScrollView style={styles.container}>
            <CustomProgressBar spinner={this.state.spinner} />
            <View >

              <FlatList
                style={{ paddingTop: 6, paddingBottom: 6 }}
                data={this.state.data}
                scrollEnabled={false}
                keyExtractor={(item, index) => {
                  return index.toString();
                }}
                renderItem={({ item, index }) => (
                  <View key={index}
                    activeOpacity={1}
                  >
                    <View style={styles.box}>
                      <View style={{
                        alignItems: 'center',
                        height: 72,
                        width: 72,
                        justifyContent: 'center',
                        margin: 8
                      }}>
                        <ImageBackground source={require('../assets/images/thumbnail.png')}
                          style={styles.image}>
                          <Image style={[styles.image, { margin: 0, }]} source={{ uri: item.product.image }} />
                        </ImageBackground>
                      </View>
                      <View style={{ width: '50%', justifyContent: 'center' }}>
                        <Text style={[{ padding: 4, fontSize: 14, color: Colors.dark_gray, textTransform: 'capitalize' }, font_style.font_medium]}>{item.product.name}</Text>
                        {/* <Rating
                          type='star'
                          imageSize={12} startingValue={4.5}
                          ratingImage={require('../assets/images/icons/star_3x.png')}
                          ratingBackgroundColor='gray'
                          style={{ alignItems: 'flex-start', marginTop: 4 }} /> */}
                        <Text style={[{
                          color: Colors.primaryColor,
                          textAlign: 'left',
                          marginTop: 8,
                          marginBottom: 8
                        }, font_style.font_bold]}>${item.price}</Text>
                      </View>
                      <View style={styles.info}>

                        <View style={{ alignItems: 'center', justifyContent: 'center', marginEnd: 16 }}>
                          <TouchableOpacity
                            onPress={() => this._AddItem(index)}
                            activeOpacity={1}
                            style={styles.icon_view}
                          >
                            <Image source={require('../assets/images/icons/increase_3x.png')}
                              style={{ width: 18, height: 18, alignSelf: 'center' }} />
                            {/* <Icon
                              name={Platform.OS === 'ios' ? 'ios-add' : 'md-add'}
                              type='ionicon'
                              color={Colors.cart_color_icon}
                              size={20}

                            /> */}
                          </TouchableOpacity>
                          <Text style={{ padding: 4 }}>{item.quantity}</Text>
                          <TouchableOpacity
                            onPress={() => this._RemoveItem(index)}
                            activeOpacity={1}
                            style={styles.icon_view}
                          >
                            {/* <Icon

                              name={Platform.OS === 'ios' ? 'ios-remove' : 'md-remove'}
                              type='ionicon'
                              color={Colors.cart_color_icon}
                              size={20}

                            /> */}
                            <Image source={require('../assets/images/icons/decrease_3x.png')}
                              style={{ width: 18, height: 18, alignSelf: 'center' }} />
                          </TouchableOpacity>
                        </View>
                        <TouchableOpacity
                          onPress={() => this._CloseItem(index)}
                          activeOpacity={1}
                          style={styles.icon_view}
                        >
                          {/* <Icon
                            name={Platform.OS === 'ios' ? 'ios-close' : 'md-close'}
                            type='ionicon'
                            color='red'
                            size={28}

                          /> */}
                          <Image source={require('../assets/images/icons/close_3x.png')}
                            style={{ width: 16, height: 16, alignSelf: 'flex-start' }} />
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                )}
              />

              <View style={{ width: '60%', alignSelf: 'flex-end', }}>
                <View style={{ flexDirection: 'row', alignItems: 'flex-end', aspectRatio: 2, }}>
                  <View style={{ flex: 1.2 }}>
                    <Text style={styles.text}>Sub Total</Text>
                    <Text style={styles.text}>Shipping Cost</Text>
                    <Text style={styles.text}>Tax 2.5%</Text>
                  </View>
                  <View style={{ flex: 0.7 }}>
                    <Text style={styles.text}>${this.state.totalPrice}</Text>
                    <Text style={styles.text}>${this.state.shipping}</Text>
                    <Text style={styles.text}>${this.state.tax}</Text>
                  </View>
                </View>
                <View style={{
                  flexDirection: 'row',
                  borderTopColor: '#000',
                  borderTopWidth: 1,
                  aspectRatio: 2,
                }}>
                  <Text style={[styles.text1, { flex: 1.2 }]}>Total</Text>
                  <Text style={[styles.text1, { flex: 0.7 }]}>${this.state.priceCalculate}</Text>
                </View>
              </View>

              {/* <Table borderStyle={{ borderColor: 'transparent' }} style={{ width: '60%', alignSelf: 'flex-end', }}>

          {
            this.state.tableData.map((rowData, index) => (
              <TableWrapper key={index} style={styles.row}>
                {
                  rowData.map((cellData, cellIndex) => (

                    <Cell key={cellIndex} data={cellData} textStyle={styles.text} />
                  ))
                }
              </TableWrapper>
            ))
          }
          <Row data={this.state.tableHead} style={styles.head} textStyle={styles.text1} />
        </Table> */}
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => { this._CheckoutOrder() }}
              >
                <LinearGradient
                  colors={['#0000FF', '#3333FF', '#5858FF']}
                  style={{
                    height: 46, alignItems: 'center', borderRadius: 46 / 2, justifyContent: 'center',
                    marginBottom: 16,
                    marginStart: 6, marginEnd: 6
                  }}
                  start={{ x: 0, y: 1 }}
                  end={{ x: 1, y: 0 }}>
                  <Text
                    style={[{
                      backgroundColor: 'transparent',
                      fontSize: 14,
                      color: '#fff',
                    }, font_style.font_bold]}>
                    CHECKOUT
                </Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          </ScrollView>
        )
      }

    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
    paddingStart: 12, paddingEnd: 12

  },
  gridView: {
    marginTop: 10,
    marginBottom: 10,
    flex: 1,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,

  },
  itemName: {
    fontSize: 16,
    color: '#000',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#000',
  },
  icon: {
    width: 30,
    height: 30,
  },
  image: {
    width: 70,
    height: 70,
    margin: 4,

  },
  box: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'row',
    shadowColor: 'black',
    shadowOpacity: .2,
    shadowOffset: {
      height: 1,
      width: -2
    },
    elevation: 2,
    marginStart: 10,
    marginEnd: 10,
    marginTop: 14,
    borderRadius: 4,
  },
  info: {
    width: 100,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  name: {
    fontSize: 20,
    marginTop: 10,
    color: '#333'
  },

  iconContainer: {
    flex: 1,
    alignItems: 'center'
  },
  iconFonts: {
    color: 'gray',
  },
  red: {
    color: '#FF4500',
  },

  //table style
  head: {
    height: 40, borderBottomColor: '#000',
    borderTopWidth: 1,

  },
  text1: {
    margin: 6,

    fontSize: 16,
    fontFamily: 'futura-bold'
  },
  text: {
    margin: 6,

    fontSize: 16,
    fontFamily: 'futura-medium',
    color: Colors.dark_gray
  },
  row: {
    flexDirection: 'row',
  },
  icon_view: {
    width: 32, height: 32,
    justifyContent: 'center',
    alignItems: 'center',

  }
});

