import React, { Component } from 'react'
import { ScrollView, StyleSheet, Image, Text, Dimensions } from 'react-native';
import { View } from 'native-base';
import HeaderLeft from '../components/HeaderLeft';
import Constant from '../constants/Constant';
import { textHeader } from '../components/styles';
import Colors from '../constants/Colors';
import HeaderRight from '../components/HeaderRight';
import HeaderBackground from '../components/HeaderBackground';
import ApiService from '../webservice/ApiService';
import { WebView } from 'react-native-webview';
import { SafeAreaView } from 'react-navigation';
import Utils from '../constants/Utils';

let WebViewRef;

export default class ConditionScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.term_condition}`.toUpperCase()}</Text>,
    headerLeft: <HeaderLeft navigationProps={navigation} />,
    //headerRight: <HeaderRight navigationProps={navigation} />,

  });

  state = {
    content: ''
  }

  componentWillReceiveProps() {
    WebViewRef && WebViewRef.reload();
  }

  render() {
    return (
      <View style={styles.container}>
        <WebView
          //ref={WEBVIEW_REF => (WebViewRef = WEBVIEW_REF)}
          source={{ uri: ApiService.BASE_URL + ApiService.condition_url }}
          style={styles.container}
          renderLoading={Utils.ActivityIndicatorLoadingView}
          startInLoadingState={true}
        />
        <SafeAreaView />
      </View>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white_color,
    paddingStart: 8, paddingEnd: 8
  },
})
