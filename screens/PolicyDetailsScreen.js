import React, { Component } from 'react'
import {
  ScrollView, StyleSheet, Image, Text, Dimensions, WebView,
  TouchableOpacity
} from 'react-native';
import { View } from 'native-base';
import HeaderLeft from '../components/HeaderLeft';
import Constant from '../constants/Constant';
import { textHeader, font_style } from '../components/styles';
import Colors from '../constants/Colors';
import HeaderRight from '../components/HeaderRight';
import HeaderBackground from '../components/HeaderBackground';
import HTML from 'react-native-render-html';
import ApiService from '../webservice/ApiService';
import HeaderBackLeft from '../components/HeaderBackLeft';

export default class PolicyDetailsScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.privacy_policy}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,

  });

  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={{ margin: 20 }}>
          <Text style={[font_style.font_Book, styles.title]}>Who we are ?</Text>
          <Text style={[font_style.font_Book, styles.meta_txt]}>
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.{"\n"}{"\n"}

            It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.{"\n"}{"\n"}

            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.{"\n"}{"\n"}

            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book

          </Text>
        </View>


      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },
  title: {
    fontSize: 22,
    color: Colors.primaryColor
  },
  meta_txt: {
    fontSize: 16,
    color: Colors.dark_gray,
    marginTop: 20
  }
});
