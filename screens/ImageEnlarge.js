import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  Button,
  View,
  ImageBackground,
  AsyncStorage,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import Constant from '../constants/Constant';
import Utils from '../constants/Utils';
import Modal from 'react-native-modalbox';
import ImageZoom from 'react-native-image-pan-zoom';
import { SafeAreaView } from 'react-navigation';
import Colors from '../constants/Colors';

export default class ImageEnlarge extends React.Component {
  static navigationOptions = {
    header: null
  };
  constructor() {
    super();
    this.state = {

      isVisible: true,

    }
  }


  showModal() {
    this.refs.modal1.open()
  }

  hideModal() {
    this.refs.modal1.close()
  }

  render() {
    return (


      <Modal position={"center"} ref={"modal1"} style={{ backgroundColor: Colors.black_color }} backdropColor={Colors.black_color} visible={this.props.visible} >
        <View>

          <ImageZoom cropWidth={Dimensions.get('window').width}
            cropHeight={Dimensions.get('window').height}
            imageWidth={200}
            imageHeight={200}>
            <Image style={{ width: 200, height: 200 }}
              source={{ uri: 'http://lorempixel.com/400/400/sports/' }} />
          </ImageZoom>
          <TouchableOpacity
            onPress={() => this.refs.modal1.close()}
            activeOpacity={1}
            style={styles.icon_view}
          >
            <Image source={require('../assets/images/icons/close_3x.png')}
              style={{ width: 20, height: 20, tintColor: Colors.white_color, }} />
          </TouchableOpacity>
        </View>
      </Modal>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },
  meta_txt: {
    color: Colors.dark_gray,
    fontSize: 14,
    marginTop: 4
  },
  modal: {

  },

  modal2: {
    backgroundColor: "#3B5998"
  },

  modal3: {
    width: 300,
    height: 280,
  },
  icon_view: {
    width: 32, height: 32,

    position: 'absolute',
    top: 40, right: 0

  }
})