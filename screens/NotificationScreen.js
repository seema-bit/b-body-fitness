import React, { Component } from 'react'
import {
  ScrollView, StyleSheet, Image, Text, KeyboardAvoidingView, TextInput,
  TouchableOpacity,
  View,
  AsyncStorage,
  ImageBackground,
  FlatList
} from 'react-native';

import HeaderLeft from '../components/HeaderLeft';
import Constant from '../constants/Constant';
import { textHeader, font_style } from '../components/styles';
import Colors from '../constants/Colors';
import HeaderProfileRight from '../components/HeaderProfileRight';
import HeaderBackground from '../components/HeaderBackground';
import { Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from 'native-base'
import ApiService from '../webservice/ApiService';
import Loader from '../components/Loader';
import moment from 'moment'
import DataNotFound from '../components/DataNotFound';
import Utils from '../constants/Utils';
import { SafeAreaView } from 'react-navigation';
import { connect } from 'react-redux';
import { updateCount, fetchFeedDetails, notificationList } from '../redux/action';


class NotificationScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.notification}`.toUpperCase()}</Text>,
    headerLeft: <TouchableOpacity
      onPress={() => {
        navigation.navigate("FeedScreen", {
          refresh: 'yes'
        })
      }}
      style={textHeader.leftIcon}>
      <Image source={require('./../assets/images/icons/arrow-right_3x.png')}
        resizeMode="contain" style={{ width: 20, height: 20, alignSelf: 'flex-start' }} />
    </TouchableOpacity>,

  });

  constructor(props) {
    super(props);
    this.state = {
      data: null,
      userInfo: null,
      loading: false
    };
  }

  showLoader = () => {
    this.setState({ loading: true });
  }

  hideLoader = () => {
    this.setState({ loading: false });
  }

  componentDidMount = () => {
    console.log('Notifications componentDidMount')
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        this.showLoader();
        this._FetchNotifications()
      })
      .catch(error => console.log('error!'));

  }

  // componentWillReceiveProps = () => {
  //   console.log('Notifications componentWillReceiveProps')
  //   _this = this
  //   //this.setState({ allFeed: null })
  //   AsyncStorage.getItem(Constant.USERINFO)
  //     .then(req => JSON.parse(req))
  //     .then(json => {
  //       this.setState({ userInfo: json })
  //       //this._FetchNotifications()
  //     })
  //     .catch(error => console.log('feed error!', error.response));
  // }

  _FetchNotifications = () => {

    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    ApiService.api.get(ApiService.notification, { headers: headers })
      .then((response) => {
        this.hideLoader();
        this.setState({ disableBtn: false, })

        if (response !== null && response.data != null) {

          if (response.data.status === Constant.SUCCESS) {

            this.props.notificationList({
              data: response.data.data,
              count: 0
            })


          } else if (response.data.status === Constant.WARNING) {

            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        this.hideLoader();
        console.log("error " + error);
      })
  }

  _StatusChange = (index) => {
    let newArray = [...this.props.lists.notification_list]
    let flag = false;
    if (newArray[index].view_status === "0") {
      newArray[index].view_status = "1"
      flag = true
    }

    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    if (flag) {
      this.props.updateCounts(this.props.counts.notification_count !== 0 ? this.props.counts.notification_count - 1 : 0);
      ApiService.api.get(ApiService.update_notification + newArray[index].id, { headers: headers })
        .then((response) => {

        })
        .catch(function (error) {
          Utils._SentryReport(error)
        })

    }

    if (newArray[index].feed_id !== undefined && newArray[index].feed_id !== null && newArray[index].feed_id !== '') {
      if (newArray[index].type !== undefined && newArray[index].type !== null && newArray[index].type === 'comment') {
        this.props.navigation.navigate("Comment",
          {
            redirect: 'notification',
            feed_id: newArray[index].feed_id,
            user_id: this.state.userInfo.id,
            comment_id: newArray[index].comment_id,
            access_key: this.state.userInfo.access_key,
          }
        )
      } else {
        this.props.fetchFeedDetails(null)
        this.props.navigation.navigate("FeedDetails",
          {
            redirect: 'notification',
            feed_id: newArray[index].feed_id,
          }
        )
      }

    } else if (newArray[index].class_id !== undefined && newArray[index].class_id !== null && newArray[index].class_id !== '') {

      this.props.navigation.navigate("ClassDetails",
        {
          redirect: 'notification',
          classId: newArray[index].class_id,
        }
      )
    }

    this.props.notificationList({
      data: newArray,
      count: this.props.counts.notification_count
    })

  }

  render() {
    return (
      <View style={styles.container}>

        {
          this.state.loading ? (<Loader />) :

            this.props.lists.notification_list.length === 0 ? (<DataNotFound />)
              : (<View style={styles.container} >
                <FlatList
                  style={{ backgroundColor: Colors.bg_color }}
                  showsVerticalScrollIndicator={false}
                  data={this.props.lists.notification_list}
                  //onRefresh={this._handleRefresh}
                  refreshing={this.state.refreshing}
                  onEndReachedThreshold={0.5}
                  //onEndReached={this._handleLoadMore}
                  keyExtractor={(item, index) => {
                    return index.toString();
                  }}
                  contentContainerStyle={{ backgroundColor: Colors.bg_color }}
                  renderItem={({ item, index }) => (
                    <TouchableOpacity style={[{ borderRadius: 4, backgroundColor: Colors.bg_color, marginStart: 16, marginEnd: 16, },]} key={index}
                      onPress={() => { this._StatusChange(index) }}
                      activeOpacity={1}>
                      <Card style={{ flex: 0, }} transparent >
                        <CardItem style={{ borderRadius: 5 }} style={[item.view_status === "0" ? { backgroundColor: Colors.light_gray } : { backgroundColor: '#fff' }]}>
                          {item.image !== undefined && item.image !== null && item.image ?
                            (<ImageBackground source={require('../assets/images/admin.jpg')}
                              style={{ height: 56, width: 56, borderRadius: 56 / 2 }}
                              imageStyle={{ borderRadius: 56 / 2 }}>

                              <Image source={{ uri: item.image }} style={{ height: 56, width: 56, borderRadius: 56 / 2 }} />

                            </ImageBackground>)
                            : (<Image source={require('../assets/images/admin.jpg')} style={{ height: 56, width: 56, borderRadius: 56 / 2 }} />)}


                          <Body style={{ marginStart: 16 }}>
                            <Text style={[font_style.font_medium, { color: Colors.dark_gray, fontSize: 14 }]}>{item.message}</Text>
                            <Text style={[font_style.font_light, styles.meta_txt, { color: Colors.dark_gray, marginTop: 10 }]}>{moment(item.updated_at).fromNow(true)}</Text>
                          </Body>


                        </CardItem>
                      </Card>
                    </TouchableOpacity>

                  )}
                />
              </View>
              )

        }
      </View>
    )


  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,

  },
});


//state map
const mapStateToProps = state => {
  return {
    counts: state.counts,
    lists: state.lists
  }
}

const mapDispatchToProps = dispatch => {
  return {
    updateCounts: (item) => {
      dispatch(updateCount(item))
    },

    fetchFeedDetails: (item) => {
      dispatch(fetchFeedDetails(item))
    },

    notificationList: (item) => {
      dispatch(notificationList(item))
    },
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationScreen)