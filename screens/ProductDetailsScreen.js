import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
  Dimensions,
  AsyncStorage,
  FlatList
} from 'react-native';
import HeaderLeft from '../components/HeaderLeft';
import HeaderRight from '../components/HeaderRight';
import moment from 'moment';

import { textInput, buttonStyle, text_color, textHeader } from '../components/styles';
import Colors from '../constants/Colors';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base'
import Constant from '../constants/Constant';
import { font_style } from '../components/styles';
import HeaderBackground from '../components/HeaderBackground';
import HeaderBackLeft from '../components/HeaderBackLeft';
import Loader from '../components/Loader';
import ApiService from '../webservice/ApiService';
import Utils from '../constants/Utils';
import ImageEnlarge from './ImageEnlarge';
import Modal from 'react-native-modalbox';
import ImageZoom from 'react-native-image-pan-zoom';

export default class ProductDetailsScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.product_details}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,
    //headerRight: <HeaderRight navigationProps={navigation} />,
  });

  state = {
    data: null,
    shipping: null,
    quantity: 1,
    totalPrice: 0,
    userInfo: {}
  }

  componentDidMount = () => {
    const productId = this.props.navigation.getParam('productId', null);
    const shippingCharge = this.props.navigation.getParam('shipping', null);

    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        var headers = {
          'X-Access-Token': json.access_key,
        }

        ApiService.api.get(ApiService.productDetails + productId, { headers: headers })
          .then((response) => {
            if (response !== null && response.data != null) {
              if (response.data.status === Constant.SUCCESS) {
                this.setState({
                  data: response.data.data != null ? response.data.data : [],
                  totalPrice: response.data.data.price,
                  shipping: shippingCharge
                })

                //shipping = response.data.shipping
              } else if (response.data.status === Constant.WARNING) {
                if (response.data.session !== undefined && response.data.session !== null) {
                  if (response.data.session === false) {
                    Utils.toastShow(response.data.message)
                    Utils._signOutAsync(this.props.navigation)
                  }
                }
              }
            }

          })
          .catch(function (error) {
            Utils._SentryReport(error)
            console.log("error " + JSON.stringify(error.response));
          })

      })
      .catch(error => console.log('error!'));

  }

  _AddItem = () => {
    // const quantity = this.state.quantity + 1
    // let newArray = this.state.data
    // newArray.price =
    this.setState({ quantity: this.state.quantity + 1 })
  }

  _RemoveItem = () => {
    if (this.state.quantity === 1) {
      this.props.navigation.goBack()
    } else {
      this.setState({ quantity: this.state.quantity - 1 })
    }

  }

  _AddToCart = (index) => {

    AsyncStorage.getItem(Constant.addToCart)
      .then(req => JSON.parse(req))
      .then(json => {
        let flag = true
        let newArray = []
        if (json !== null && json !== undefined) {
          newArray = [...json]

          json.forEach((item, itemIndex) => {
            if (item.product_id === this.state.data.id) {

              newArray[itemIndex].quantity = parseInt(newArray[itemIndex].quantity) + this.state.quantity
              const price = parseFloat(newArray[itemIndex].unit_price) * parseInt(newArray[itemIndex].quantity)
              newArray[itemIndex].price = parseFloat(price).toFixed(2)
              flag = false
            }
          })

          if (flag) {
            const count = this.state.quantity
            const price = parseFloat(this.state.data.price) * count
            let userJson = {
              product: this.state.data,
              product_id: this.state.data.id,
              admin_id: this.state.data.admin_id,
              quantity: count,
              unit_price: parseFloat(this.state.data.price).toFixed(2),
              price: parseFloat(price).toFixed(2),
            }
            newArray = [...newArray, userJson]
          }

        } else {
          //newArray = []
          const count = this.state.quantity
          const price = parseFloat(this.state.data.price) * count

          let userJson = {
            product: this.state.data,
            admin_id: this.state.data.admin_id,
            product_id: this.state.data.id,
            quantity: count,
            unit_price: parseFloat(this.state.data.price).toFixed(2),
            price: parseFloat(price).toFixed(2),
          }
          newArray = [...newArray, userJson]
        }

        AsyncStorage.setItem(Constant.addToCart, JSON.stringify(newArray))
        this.props.navigation.navigate('Cart', {
          shipping: this.state.shipping
        })
      })
      .catch(error => console.log('error!' + JSON.stringify(error.response)));

  }

  render() {
    const state = this.state
    if (this.state.data === undefined || this.state.data === null) {
      return (<Loader />)
    } else {
      return (
        <View style={styles.container}>

          <Modal
            backdropTransitionOutTiming={0}
            coverScreen={true}
            backdropPressToClose={() => this.refs.modal1.close()}
            position={"center"} ref={"modal1"} style={{ margin: 0, }}  >
            <View>

              <ImageZoom cropWidth={Dimensions.get('window').width}
                cropHeight={Dimensions.get('window').height}
                imageWidth={Dimensions.get('window').width}
                imageHeight={Dimensions.get('window').width}>
                <Image style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').width }}
                  source={{ uri: state.data.image }} />
              </ImageZoom>
              <TouchableOpacity
                onPress={() => this.refs.modal1.close()}
                activeOpacity={1}
                style={textHeader.icon_view}
              >
                <Image source={require('../assets/images/icons/close_3x.png')}
                  style={{ width: 20, height: 20, tintColor: Colors.black_color, }} />
              </TouchableOpacity>
            </View>
          </Modal>


          <ScrollView style={styles.container}>

            <View style={{ alignItems: 'center' }} >

              {state.data.image !== undefined && state.data.image !== null ?
                <TouchableOpacity
                  onPress={() => this.refs.modal1.open()}>
                  <Image source={{ uri: state.data.image }} style={{ width: 154, height: 154, marginTop: 16 }} />
                </TouchableOpacity>
                :
                (<Image source={require('../assets/images/product.png')} style={{ width: 154, height: 154, marginTop: 16 }} />)
              }




              <Text style={[styles.product_title, font_style.font_medium]}>{state.data.name}, {state.data.size} {state.data.sub_title !== null ? ", " + state.data.sub_title : ''}</Text>
              <Text style={[styles.product_price, font_style.font_bold]}>${parseFloat(state.data.price * state.quantity).toFixed(2)}</Text>
              <View style={{ flexDirection: 'row', marginBottom: 30, justifyContent: 'center', alignItems: 'center' }}>

                <TouchableOpacity
                  onPress={() => this._RemoveItem()}
                  style={styles.btn}>
                  <Icon
                    name={Platform.OS === 'ios' ? 'ios-remove' : 'md-remove'}
                    type='ionicon'
                    color={Colors.cart_color_icon}
                    size={20}
                  />

                </TouchableOpacity>

                <Text style={{ fontSize: 18, color: Colors.gray_shade, marginLeft: 34, marginRight: 34, textAlign: 'center' }}>{state.quantity}</Text>
                <TouchableOpacity
                  onPress={() => this._AddItem()}
                  style={styles.btn}>
                  <Icon
                    name={Platform.OS === 'ios' ? 'ios-add' : 'md-add'}
                    type='ionicon'
                    color={Colors.cart_color_icon}
                    size={20}

                  />
                </TouchableOpacity>
              </View>

              <TouchableOpacity
                onPress={() => this._AddToCart()}
                style={styles.add_to_cart}
                underlayColor='#fff'>
                <Text style={[buttonStyle.btnText, { fontSize: 14 }]}>{'ADD TO CART'.toUpperCase()}</Text>
              </TouchableOpacity>
            </View>

            <View style={styles.horizontal_line} />
            {(state.data.description !== undefined && state.data.description !== null && state.data.description !== 'null') &&
              (<View style={{ marginRight: 20, marginLeft: 20 }}>
                <Text style={[styles.product_details, font_style.font_heavy]}>Product Details</Text>
                <Text style={[styles.description_txt, font_style.font_light]}>{state.data.description}</Text>
              </View>)
            }

          </ScrollView>

        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',

  },

  meta_txt: {
    color: Colors.dark_gray,
    fontSize: 14,
    marginTop: 4
  },
  txt_style: {
    fontSize: 14,
    color: Colors.dark_gray,
  },
  heading_txt: {
    marginBottom: 20,
    fontSize: 14,
    color: '#413F40'
  },
  product_status: {
    fontSize: 16,
    color: '#0AB50A',
    marginBottom: 20
  },
  description_txt: {
    color: Colors.dark_gray,
    fontSize: 14,
  },
  product_title: {
    fontSize: 18,
    color: Colors.gray_shade,
    textAlign: 'center',
    marginTop: 4
  },
  product_price: {
    color: Colors.primaryColor,
    textAlign: 'center',
    marginTop: 20,
    marginBottom: 20
  },
  product_details: {
    color: Colors.primaryColor,
    fontSize: 16,
    alignItems: 'flex-start',
    marginTop: 30,
    marginBottom: 14
  },
  add_to_cart: {
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: Colors.primaryColor,
    borderRadius: 50,
    height: 46,
    width: 300,
    justifyContent: 'center',
    marginBottom: 30
  },
  horizontal_line: {
    borderStyle: 'solid',
    borderWidth: 0.5,
    width: '100%', borderColor: Colors.light_gray,
    marginTop: 6,
  },
  btn: {
    width: 34,
    height: 34,
    borderRadius: 34 / 2,
    backgroundColor: Colors.bg_color,
    alignItems: 'center'
  },
  icon_view: {
    width: 32, height: 32,
    position: 'absolute',
    top: 20, right: 10
  }

});
