import React from 'react';
import { ScrollView, StyleSheet, Image, Text, TouchableOpacity, AsyncStorage, KeyboardAvoidingView, Keyboard, ImageBackground, FlatList, Dimensions, Platform, ActivityIndicator, StatusBar, TextInput } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import HeaderLeft from '../components/HeaderLeft';
import HeaderRight from '../components/HeaderRight';
import Colors from '../constants/Colors';
import { View } from 'native-base';
import Constant from '../constants/Constant';
import { textHeader, font_style } from '../components/styles';
import HeaderBackground from '../components/HeaderBackground';
import { Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from 'native-base'
import HeaderBackLeft from '../components/HeaderBackLeft';
import ApiService from '../webservice/ApiService';
import Loader from '../components/Loader';
import moment from 'moment'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Utils from '../constants/Utils';
import CustomProgressBar from '../components/CustomProgressBar';
import AutogrowInput from 'react-native-autogrow-input';
import DataNotFound from '../components/DataNotFound';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import { connect } from 'react-redux';
import {
  fetchFeedList,
} from '../redux/action';
import { SafeAreaView } from 'react-navigation';
import Modal from 'react-native-modalbox';
import ImageZoom from 'react-native-image-pan-zoom';

let focusText = false;

const user_img_size = 56;

class CommentScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.comments}`.toUpperCase()}</Text>,
    headerLeft:
      <TouchableOpacity
        onPress={() => {
          (navigation.getParam('redirect', null) !== undefined && navigation.getParam('redirect', null) !== null && navigation.getParam('redirect', null) === 'notification') ?
            navigation.navigate("Notification")
            :
            navigation.navigate("Feed", {
              refresh: 'yes'
            })
        }} style={textHeader.leftIcon}>
        <Image source={require('./../assets/images/icons/arrow-right_3x.png')} style={{ width: 11, height: 20, alignSelf: 'flex-start' }} />
      </TouchableOpacity>

    //headerRight: <HeaderRight navigationProps={navigation} />,
  });

  constructor(props) {
    super(props);
    this.a = React.createRef();

  }

  state = {
    data: null,
    message: '',
    user_id: '',
    feed_id: '',
    access_key: '',
    focusInput: false,
    comment_id: '',
    userInfo: '',
    disableBtn: false,
    spinner: false,
    scrollFlag: false,
    page: 0,
    loading: false,
    loadingMore: false,
    filtering: false,
    refreshing: false,
    error: null,
    hasNextPage: false,
    hasPreviousPage: false,
    loadPage: 1,
    comment_count: 0,
    loadMoreLoading: false,
    loadPreviousLoading: false,
    defaultHeight: 30,
    userLikes: null,
    image_enlarge: null
  }


  _Redirect = () => {
    const redirect = this.props.navigation.getParam('redirect', null);
    // if (redirect !== undefined && redirect !== null && redirect === 'notification') {
    //   this.props.navigation.goBack()
    // } else {
    //   this.props.navigation.navigate("Feed", {
    //     refresh: 'yes',
    //     feed_id: this.state.feed_id,
    //     comment_count: this.state.data !== null ? this.state.data.length : 0
    //   })
    // }

    // this.props.navigation.dispatch(NavigationActions.setParams({
    //   params: { refresh: 'yes' },
    //   key: 'Feed',
    // }))


    if (redirect !== undefined && redirect !== null && redirect === 'notification') {
      this.props.navigation.navigate("Notification")
    } else {
      this.props.navigation.navigate("Feed", {
        refresh: 'yes'
      })
    }
  }

  componentDidMount = () => {

    //_this = this
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        const feed_id = this.props.navigation.getParam('feed_id', '');
        const comment_id = this.props.navigation.getParam('comment_id', null)

        console.log('comment_id', comment_id + " " + feed_id)

        this.setState({
          user_id: json.id,
          feed_id: feed_id,
          access_key: json.access_key,
          userInfo: json,
          comment_id: comment_id,
        })
        this.getComment()

      })
      .catch(error => console.log('error!', error.response));

  }

  getComment = () => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }


    let ApiCall = ''

    if (this.state.comment_id !== null && this.state.comment_id !== undefined && this.state.comment_id !== '') {
      if (this.state.page === 0) {
        ApiCall = ApiService.api.get(ApiService.fetchSpecificComment + "n/" + this.state.feed_id + "/" + this.state.comment_id + "/" + this.state.page, { headers: headers })
        console.log('if', ApiService.fetchSpecificComment + "n/" + this.state.feed_id + "/" + this.state.comment_id + "/" + this.state.page)
      } else {
        this.showLoader()
        let page = this.state.page === 0 ? this.state.page : this.state.page - 1

        ApiCall = ApiService.api.get(ApiService.fetchSpecificComment + "p/" + this.state.feed_id + "/" + this.state.comment_id + "/" + page, { headers: headers })
        console.log('if previous', ApiService.fetchSpecificComment + "p/" + this.state.feed_id + "/" + this.state.comment_id + "/" + page)
      }
    } else {
      this.showLoader()
      ApiCall = ApiService.api.get(ApiService.fetchAllComment + this.state.feed_id + "/" + this.state.page, { headers: headers })
      console.log('else', ApiService.fetchAllComment + this.state.feed_id + "/" + this.state.page)
    }
    let pageCount = this.state.page
    //console.log('ApiCall', ApiCall)
    ApiCall.then((response) => {
      //this.setState({ data: [] })

      if (response !== null && response.data != null) {

        this.setState({ comment_count: response.data.count })
        this.updateCommentCount(response.data.count)
        if (this.state.comment_id !== null && this.state.comment_id !== undefined && this.state.comment_id !== '') {
          if (this.state.page === 0) {
            this.setState({ hasNextPage: response.data.hasNextPage, hasPreviousPage: response.data.hasPreviousPage })
          } else {
            this.setState({ hasPreviousPage: response.data.hasPreviousPage })

          }
        } else {
          this.setState({ hasNextPage: response.data.hasNextPage, hasPreviousPage: response.data.hasPreviousPage })
        }
        pageCount = pageCount + 1

        if (response.data.status !== null && response.data.status !== 'undefined' && response.data.status === true) {
          // if (this.state.page < 1) {
          //   this.setState({ data: response.data.data != null ? response.data.data : [] })
          //   //this.scrollView.scrollTo(0);
          // } else {
          if (this.state.data !== null) {
            if (response.data.data !== null && response.data.data.length !== 0) {
              this.setState({ data: [...response.data.data, ...this.state.data] })
            }

          } else {
            this.setState({ data: response.data.data !== null && response.data.data.length !== 0 ? response.data.data : [] })
          }


          //}

          if (response.data.hasPreviousPage !== undefined && response.data.hasPreviousPage !== null && response.data.hasPreviousPage) {
            this.setState({
              page: pageCount,
              loadingMore: true
            });
            this.hideLoader()
          } else {
            this.setState({
              loadingMore: false
            });
          }


        } else if (response.data.status === Constant.WARNING) {
          if (this.state.page === 0) {
            this.setState({
              data: []
            })
          }
          if (response.data.session !== undefined && response.data.session !== null) {
            if (response.data.session === false) {
              Utils._signOutAsync(this.props.navigation)
            }
          }
        }
      }

    })
      .catch(function (error) {
        console.log("error " + JSON.stringify(error.response) + " && page " + pageCount);
        this.hideLoader()
      })

  }

  showLoader = () => {
    this.setState({ loadPreviousLoading: true })
  }

  hideLoader = () => {
    this.setState({ loadPreviousLoading: false })
  }

  showMoreLoader = () => {
    this.setState({ loadMoreLoading: true })
  }

  hideMoreLoader = () => {
    this.setState({ loadMoreLoading: false })
  }

  updateCommentCount(count) {
    this.props.lists.feed_list.forEach((element, index) => {

      if (element.feed.id === this.state.feed_id) {
        if (count !== undefined && count !== null && element.comments_count !== count) {
          element.comments_count = count

          const newallFeed = [...this.props.lists.feed_list.slice(0, index), element, ...this.props.lists.feed_list.slice(index + 1)]
          this.props.fetchFeedList(newallFeed)
        }

        //break
      }
    });
  }

  _AddComment = () => {
    Keyboard.dismiss()

    if (this.state.message !== null && this.state.message !== '') {
      this.setState({ defaultHeight: 30 })
      this.setState({ disableBtn: true, spinner: true })
      var headers = {
        'X-Access-Token': this.state.access_key,
      }
      let params;
      let ApiCall;
      if (this.state.comment_id) {

        params = {
          comment_id: this.state.comment_id,
          feed_id: this.state.feed_id,
          reply: this.state.message
        }

        ApiCall = ApiService.api.post(ApiService.commentReply, params, { headers: headers })

      } else {
        params = {
          user_id: this.state.user_id,
          feed_id: this.state.feed_id,
          comment: this.state.message
        }

        ApiCall = ApiService.api.post(ApiService.addComment, params, { headers: headers })

      }

      this.setState({ message: '' })
      ApiCall.then((response) => {
        if (response !== null && response.data != null) {
          this.setState({ disableBtn: false, spinner: false, })
          console.log(response.data.data)
          if (response.data.status === Constant.SUCCESS) {

            //this.setState({ page: 0, data: null })
            let comment_json = {}
            let comment_update_flag = false
            if (this.state.comment_id) {
              let newArray = [...this.state.data]
              this.state.data.forEach((element, index) => {

                if (element.id === this.state.comment_id) {
                  comment_json = element;
                  comment_update_flag = true;
                  if (newArray[index].allreply !== undefined && newArray[index].allreply !== null && newArray[index].allreply.length !== 0) {
                    const newDataAdded = response.data.data;
                    newDataAdded.count_like = 0
                    newDataAdded.user_like = false
                    newArray[index].allreply = [...element.allreply, newDataAdded]
                  } else {
                    const newDataAdded = response.data.data;
                    newDataAdded.count_like = 0
                    newDataAdded.user_like = false
                    newArray[index].allreply = []
                    newArray[index].allreply = [...newArray[index].allreply, newDataAdded]
                  }
                  this.setState({ message: '', comment_id: '', data: newArray })
                  console.log('_addComment', this.state.data[index])
                }
              });


            } else {
              //this.scrollView.scrollToEnd({ animated: true });

              comment_json = response.data.data
              comment_json.count_like = 0
              comment_json.user_like = false
              this.setState({ message: '', comment_id: '', data: [...this.state.data, comment_json], scrollFlag: true, comment_count: this.state.comment_count + 1 })
              this.updateCommentCount(this.state.comment_count);
            }

          } else if (response.data.status === Constant.WARNING) {
            this.setState({ disableBtn: false })
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          } else {
            this.setState({ disableBtn: false })
          }
        } else {
          this.setState({ disableBtn: false })
        }

      })
        .catch(function (error) {
          //this.setState({ disableBtn: false })
          console.log("error " + JSON.stringify(error.response));
        })
    }

  }

  _CommentReply = (item, index) => {

    this.autogrowInput.focus()
    this.setState({ comment_id: item.id })


  }

  _CommentLike = (item, index) => {

    const newArray = this.state.data;
    if (newArray[index].user_like !== null && newArray[index].user_like !== '' && newArray[index].user_like === Constant.user_like) {
      newArray[index].count_like = newArray[index].count_like !== undefined ? newArray[index].count_like - 1 : 0;
      newArray[index].user_like = Constant.user_unlike
    } else {
      newArray[index].count_like = newArray[index].count_like !== undefined ? newArray[index].count_like + 1 : 1;
      newArray[index].user_like = Constant.user_like
    }

    this.setState({ data: newArray, focusInput: true })

    var params = {
      comment_id: item.id,
      feed_id: this.state.feed_id
    }

    this._NetworkCallForLikeDislike(params)

  }

  _CommentReplyLike = (item, replyIndex, index) => {

    const newArray = [...this.state.data];
    if (newArray[index].allreply[replyIndex].user_like !== null && newArray[index].allreply[replyIndex].user_like !== '' && newArray[index].allreply[replyIndex].user_like === Constant.user_like) {
      newArray[index].allreply[replyIndex].count_like = newArray[index].allreply[replyIndex].count_like !== undefined ? newArray[index].allreply[replyIndex].count_like - 1 : 0;
      newArray[index].allreply[replyIndex].user_like = Constant.user_unlike
    } else {
      newArray[index].allreply[replyIndex].count_like = newArray[index].allreply[replyIndex].count_like !== undefined ? newArray[index].allreply[replyIndex].count_like + 1 : 1;
      newArray[index].allreply[replyIndex].user_like = Constant.user_like
    }

    this.setState({ data: newArray, focusInput: true })

    var params = {
      comment_id: this.state.data[index].id,
      reply_comment_id: item.id,
      feed_id: this.state.feed_id
    }

    this._NetworkCallForLikeDislike(params)

  }

  _NetworkCallForLikeDislike = (params) => {
    console.log('params', params)
    var headers = {
      'X-Access-Token': this.state.access_key,
    }
    ApiService.api.post(ApiService.commentLikeDislike, params, { headers: headers })
      .then((response) => {
        console.log('response', response.data)
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            this.setState({ message: '', focusInput: false })
          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })
  }

  displayMoreReplies = (index) => {
    console.log('before', this.state.data[index])
    const newArray = [...this.state.data];

    newArray[index].showReply = true

    this.setState({ data: newArray })

    console.log('after', this.state.data[index])
  }

  _SetReply = (index) => {
    const newArray = [...this.state.data];

    newArray[index].showReply = false

    this.setState({ data: newArray })
    const item = this.state.data[index]
  }

  _onInputSizeChange(height) {
    if (height > 30) {
      this.setState({ defaultHeight: height > 90 ? 90 : height })
    }

    setTimeout(function () {
      this.scrollView.scrollToEnd({ animated: false });
    }.bind(this))
  }

  _handlePreviousMore = () => {
    if (this.state.loadingMore) {
      this.getComment()

    }
  };

  _handleLoadMore = () => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }


    let ApiCall = ''

    if (this.state.comment_id !== null && this.state.comment_id !== undefined && this.state.comment_id !== '') {
      ApiCall = ApiService.api.get(ApiService.fetchSpecificComment + "n/" + this.state.feed_id + "/" + this.state.comment_id + "/" + this.state.loadPage, { headers: headers })
      console.log('load more comment', ApiService.fetchSpecificComment + "n/" + this.state.feed_id + "/" + this.state.comment_id + "/" + this.state.loadPage)
      this.showMoreLoader()
    }

    ApiCall.then((response) => {
      //this.setState({ data: [] })
      this.hideMoreLoader()
      if (response !== null && response.data != null) {
        this.setState
        this.setState({ hasNextPage: response.data.hasNextPage })
        if (response.data.status !== null && response.data.status !== 'undefined' && response.data.status === true) {

          if (response.data.data !== null && response.data.data.length !== 0) {
            this.setState({ data: [...this.state.data, ...response.data.data] })
          }

          if (response.data.hasNextPage !== undefined && response.data.hasNextPage !== null && response.data.hasNextPage) {
            this.setState({
              loadPage: this.state.loadPage + 1,
            });
          }


        } else if (response.data.status === Constant.WARNING) {
          if (response.data.session !== undefined && response.data.session !== null) {
            if (response.data.session === false) {
              Utils._signOutAsync(this.props.navigation)
            }
          }
        }
      }

    })
      .catch(function (error) {
        Utils._SentryReport(error)
        this.hideMoreLoader()
        console.log("error " + JSON.stringify(error.response));
      })
  };


  _renderFooter = () => {
    if (!this.state.loadingMore) return null;

    return (
      <View
        style={{
          height: 30,
          position: 'relative',
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: Colors.bg_color,
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <Loader />
      </View>
    );
  };

  enlargeImage = (image) => {
    this.setState({ image_enlarge: image })
    this.refs.modal1.open()
  }

  renderModal() {
    return (
      <Modal
        backButtonClose={true}
        swipeToClose={true}
        coverScreen={true}
        backdropPressToClose={true}
        animationIn="fadeIn"
        animationOut="fadeOut"
        hideModalContentWhileAnimating={true}
        animationInTiming={1500}
        animationOutTiming={1500}
        backdropTransitionOutTiming={0}
        position={"center"} ref={"modal1"} style={{ margin: 0, }}  >
        <StatusBar barStyle="dark-content"
        />
        <View>

          <ImageZoom cropWidth={Dimensions.get('window').width}
            cropHeight={Dimensions.get('window').height}
            imageWidth={Dimensions.get('window').width}
            imageHeight={Dimensions.get('window').width}>
            <Image style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').width }}
              source={{ uri: this.state.image_enlarge }} />
          </ImageZoom>
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => this.refs.modal1.close()}
            activeOpacity={1}
            style={styles.icon_view}
          >

            <Image source={require('../assets/images/icons/close_3x.png')}
              style={{ width: 20, height: 20, tintColor: Colors.black_color, }} />
          </TouchableOpacity>
        </View>
      </Modal>
    )


  }
  renderLikeModal() {
    return (
      <Modal
        backButtonClose={true}
        swipeToClose={false}
        coverScreen={true}
        backdropPressToClose={true}
        animationIn="fadeIn"
        animationOut="fadeOut"
        hideModalContentWhileAnimating={true}
        animationInTiming={1500}
        animationOutTiming={1500}
        backdropTransitionOutTiming={0}
        position={"center"} ref={"likeModal"} style={{ margin: 0, backgroundColor: '#00000040', borderTopLeftRadius: 20, borderTopRightRadius: 20 }}  >
        <SafeAreaView />
        <StatusBar barStyle="dark-content" />
        <View style={{ flex: 1, marginTop: 80, backgroundColor: '#fff', borderTopLeftRadius: 20, borderTopRightRadius: 20 }}>

          <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 20 }}>
            <Text style={[font_style.font_medium, { position: 'absolute', width: '100%', fontSize: 16, textAlign: 'center', flex: 1 }]}>LIKES</Text>
            <TouchableOpacity
              activeOpacity={1}
              onPress={() => this.refs.likeModal.close()}
              activeOpacity={1}
              style={styles.like_icon_view}
            >
              <Image source={require('../assets/images/icons/arrow-right_3x.png')}
                resizeMode='contain'
                style={{ width: 22, height: 22, tintColor: Colors.black_color, justifyContent: 'center' }} />
            </TouchableOpacity>
          </View>
          {this.state.userLikes === undefined || this.state.userLikes === null ?
            (<Loader />)
            :
            <View style={{ flex: 1 }}>
              {this.state.userLikes.length === 0 ?
                (<DataNotFound />) :
                <FlatList
                  showsVerticalScrollIndicator={false}
                  data={this.state.userLikes}
                  //onRefresh={this._handleRefresh}
                  refreshing={this.state.refreshing}
                  onEndReachedThreshold={0.5}
                  //onEndReached={this._handleLoadMore}
                  keyExtractor={(item, index) => {
                    return index.toString();
                  }}
                  contentContainerStyle={{ backgroundColor: Colors.white_color }}
                  renderItem={({ item, index }) => (
                    <Card style={{
                      flex: 0,
                      borderBottomColor: Colors.light_gray,
                      borderBottomWidth: 1,
                      marginStart: 16, marginEnd: 16,
                    }} key={index} transparent>

                      <CardItem>
                        <Left>
                          <ImageBackground source={require('../assets/images/admin.jpg')}
                            style={styles.img_view}
                            imageStyle={styles.imageStyle}>
                            <TouchableOpacity
                              activeOpacity={1}
                              onPress={() => this.enlargeImage(item.image)}>
                              <Image source={{ uri: item.image }} style={styles.img_view} />
                            </TouchableOpacity>

                          </ImageBackground>

                          <Body>
                            <Text style={[font_style.font_medium, { fontSize: 14, textTransform: 'capitalize' }]}>{item.name}</Text>
                          </Body>
                        </Left>


                      </CardItem>
                    </Card>
                  )}
                />

              }
            </View>
          }




        </View>
        <SafeAreaView />
      </Modal>
    )
  }


  getCommentUserLikes = (comment_id, index) => {

    this.refs.likeModal.open()
    const params = {
      comment_id: comment_id,
    }
    console.log('getCommentReplyUserLikes', params)
    this._FetchUserLikes(params)
    //}
  }

  getCommentReplyUserLikes = (comment_id, reply_comment_id, index) => {

    this.refs.likeModal.open()
    const params = {
      comment_id: comment_id,
      reply_comment_id: reply_comment_id
    }

    console.log('getCommentReplyUserLikes', params)
    this._FetchUserLikes(params)
    //}
  }

  _FetchUserLikes = (params) => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    this.setState({ userLikes: null })
    ApiService.api.post(ApiService.commentLikedUsers, params, { headers: headers })
      .then((response) => {
        this.setState({ userLikes: [] })
        console.log('response.data', response.data)
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            this.setState({ userLikes: response.data.data != null ? response.data.data : [] })
          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        this.setState({ userLikes: [] })
        console.log("error " + JSON.stringify(error.response));


      })
  }

  _textChange = (message) => {
    this.setState({ message })
  }


  render() {
    if (this.state.data === undefined || this.state.data === null) {
      return (<Loader />)
    } else {

      return (
        <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', backgroundColor: Colors.white_color }} behavior="height" enabled keyboardVerticalOffset={Platform.OS === 'ios' ? 96 : 110}  >
          {this.renderLikeModal()}
          {this.renderModal()}
          <CustomProgressBar spinner={this.state.spinner} />
          <View style={styles.container}>

            {this.state.data.length === 0 ? (
              <DataNotFound />
            ) :
              (
                <ScrollView ref={ref => this.scrollView = ref}
                  onContentSizeChange={(contentWidth, contentHeight) => {
                    if (this.state.scrollFlag) {
                      this.scrollView.scrollToEnd({ animated: true });
                      this.setState({ scrollFlag: false })
                    }
                  }}

                  onLayout={() => this.scrollView.scrollToEnd({ animated: true })}>
                  {this.state.hasPreviousPage && <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', flex: 1, alignSelf: 'center' }} onPress={() => { this._handlePreviousMore() }}>
                    {this.state.loadPreviousLoading && <ActivityIndicator size="small" color={Colors.primaryColor} />}
                    <Text style={[{ padding: 8, }, font_style.font_medium]}>Load Previous Comments</Text>
                  </TouchableOpacity>}

                  <FlatList

                    showsVerticalScrollIndicator={false}
                    data={this.state.data}
                    //onRefresh={this._handleRefresh}
                    //refreshing={this.state.refreshing}
                    //initialNumToRender={10}
                    //onEndReachedThreshold={0.4}
                    //onEndReached={this._handleLoadMore}
                    keyExtractor={(item, index) => {
                      return index.toString();
                    }}
                    contentContainerStyle={{ backgroundColor: Colors.bg_color }}
                    renderItem={({ item, index }) => (
                      <Card key={index} transparent>
                        <CardItem style={{ borderRadius: 4, backgroundColor: Colors.bg_color, }}>
                          <Left style={{ justifyContent: 'flex-start', alignItems: 'flex-start', }}>
                            {(item.user.images !== 'undefined' && item.user.images) ?
                              (<ImageBackground source={require('../assets/images/admin.jpg')}
                                style={{ marginTop: 5, width: user_img_size, height: user_img_size, borderRadius: user_img_size / 2, alignItems: 'flex-start' }}
                                imageStyle={{ borderRadius: user_img_size / 2 }}>
                                <Image source={{ uri: item.user.images }} style={{ width: user_img_size, height: user_img_size, borderRadius: user_img_size / 2, alignItems: 'flex-start' }} />
                              </ImageBackground>)
                              : (<Image source={require('../assets/images/admin.jpg')} style={{ marginTop: 5, width: user_img_size, height: user_img_size, borderRadius: user_img_size / 2, alignItems: 'flex-start' }} />)
                            }
                            <View style={{ flex: 1 }}>
                              <View style={{
                                flex: 1,
                                padding: 20,
                                borderRadius: 20,
                                marginStart: 16, borderBottomColor: Colors.light_gray,
                                // borderBottomWidth: 1,
                                backgroundColor: Colors.white_color
                              }}>
                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                  <Text style={[font_style.font_bold, { flex: 1, color: Colors.dark_gray, fontSize: 14 }]}>{item.user.name}</Text>
                                  <Text style={[font_style.font_medium, { color: '#A4A4A4', fontSize: 12, }]}>{(moment.utc(item.created_at)).local().fromNow(true)}</Text>
                                </View>

                                <Text style={[font_style.font_medium, styles.meta_txt]}>{item.comment}</Text>
                              </View>

                              <View style={{ flex: 1, paddingLeft: 20, flexDirection: 'row', marginTop: 12, paddingBottom: 20, alignItems: 'center' }}>
                                <TouchableOpacity
                                  onPress={() => { this._CommentLike(item, index) }} transparent style={{ flexDirection: 'row', marginRight: 15 }}>
                                  {item.user_like !== null && item.user_like !== '' && item.user_like === Constant.user_like
                                    ?
                                    (<Image source={require('../assets/images/icons/liked_3x.png')} resizeMode="contain" style={{ width: 18, height: 18, }} />)
                                    :
                                    (<Image source={require('../assets/images/icons/like_3x.png')} resizeMode="contain" style={{ width: 18, height: 18 }} />)
                                  }
                                  {/* <Text style={[font_style.font_medium, { color: '#A4A4A4', fontSize: 12, marginStart: 8 }]}>{item.count_like !== null && item.count_like !== undefined ? item.count_like : 0} {item.count_like < 2 ? 'Like' : 'Likes'}</Text> */}
                                </TouchableOpacity>
                                <TouchableOpacity
                                  onPress={() => { this._CommentReply(item, index) }}>
                                  {/* <Text style={[font_style.font_medium, { color: '#A4A4A4', fontSize: 12, }]}>Reply</Text> */}
                                  <Image source={require('../assets/images/icons/comment_new.png')} style={{ width: 18, height: 18 }} resizeMode="contain" />
                                </TouchableOpacity>

                                <View style={{ flex: 1 }} />

                                <TouchableOpacity
                                  onPress={() => { this.getCommentUserLikes(item.id, index) }}
                                  style={{ flexDirection: 'row', alignItems: 'center' }}>
                                  <Image source={require('../assets/images/icons/like_filled.png')} style={{ width: 22, height: 22 }} resizeMode="contain" />
                                  <Text style={[font_style.font_medium, { marginLeft: 5, color: '#A4A4A4', fontSize: 12, }]}>{item.count_like !== null && item.count_like !== undefined ? item.count_like : 0}</Text>
                                </TouchableOpacity>

                              </View>

                              <View>
                                {
                                  //this._SetReply(index)
                                  (item.allreply !== undefined && item.allreply !== null && item.allreply.length !== 0 && item.allreply.length >= 1 && (item.showReply === undefined || item.showReply === false)) ?
                                    (<View>
                                      {item.allreply.length >= 2 &&
                                        (<TouchableOpacity

                                          onPress={() => { this.displayMoreReplies(index) }}
                                          activeOpacity={1}
                                        >
                                          <Text style={[{ color: Colors.dark_gray, fontSize: 12, marginTop: 15, marginBottom: 15 }, font_style.font_bold]}>{item.allreply.length - 1} Reply</Text>
                                        </TouchableOpacity>)
                                      }


                                      <Card transparent>
                                        <CardItem style={{
                                          backgroundColor: Colors.bg_color,
                                          paddingLeft: -10,
                                          paddingEnd: -10,
                                          paddingTop: -10,
                                          paddingBottom: -10

                                        }}>
                                          <Left style={{ justifyContent: 'flex-start', alignItems: 'flex-start', }}>
                                            {item.allreply[0].user.images ?
                                              (<ImageBackground source={require('../assets/images/admin.jpg')}
                                                style={{ width: user_img_size, height: user_img_size, borderRadius: user_img_size / 2, alignItems: 'flex-start' }}
                                                imageStyle={{ borderRadius: user_img_size / 2 }}>
                                                <Image source={{ uri: item.allreply[0].user.images }} style={{ width: user_img_size, height: user_img_size, borderRadius: user_img_size / 2, alignItems: 'flex-start' }} />
                                              </ImageBackground>)
                                              : (<Image source={require('../assets/images/admin.jpg')} style={{ width: user_img_size, height: user_img_size, borderRadius: user_img_size / 2, alignItems: 'flex-start' }} />)
                                            }

                                            <View style={{ flex: 1 }}>
                                              <View style={{
                                                borderRadius: 20,
                                                padding: 20,
                                                flex: 1,
                                                marginStart: 16,
                                                backgroundColor: '#fff'
                                              }}>
                                                <View style={{ flexDirection: 'row' }}>
                                                  <Text style={[font_style.font_bold, { flex: 1, color: Colors.dark_gray, fontSize: 14 }]}>{item.allreply[0].user.name}</Text>
                                                  <Text style={[font_style.font_medium, { color: '#A4A4A4', fontSize: 12, }]}>{(moment.utc(item.allreply[0].created_at)).local().fromNow(true)}</Text>
                                                </View>
                                                <Text style={[font_style.font_medium, styles.meta_txt]}>{item.allreply[0].reply}</Text>
                                              </View>
                                              <View style={{ flex: 1, paddingLeft: 20, flexDirection: 'row', marginTop: 12, paddingBottom: 20, alignItems: 'center' }}>
                                                <TouchableOpacity
                                                  onPress={() => { this._CommentReplyLike(item.allreply[0], 0, index) }}
                                                  style={{ marginRight: 15 }}>
                                                  {item.allreply[0].user_like !== null && item.allreply[0].user_like !== '' && item.allreply[0].user_like === Constant.user_like
                                                    ?
                                                    (<Image source={require('../assets/images/icons/liked_3x.png')} resizeMode="contain" style={{ width: 18, height: 18, }} />)
                                                    :
                                                    (<Image source={require('../assets/images/icons/like_3x.png')} resizeMode="contain" style={{ width: 18, height: 18 }} />)
                                                  }
                                                </TouchableOpacity>
                                                {/* <Text style={[font_style.font_medium, { color: '#A4A4A4', fontSize: 12, paddingStart: 4 }]}>{item.allreply[0].count_like !== null && item.allreply[0].count_like !== undefined ? item.allreply[0].count_like : 0} {item.allreply[0].count_like < 2 ? 'Like' : 'Likes'}</Text> */}
                                                <TouchableOpacity
                                                  onPress={() => { this._CommentReply(item, index) }}>
                                                  {/* <Text style={[font_style.font_medium, { color: '#A4A4A4', fontSize: 12, }]}>Reply</Text> */}
                                                  <Image source={require('../assets/images/icons/comment_new.png')} style={{ width: 18, height: 18 }} resizeMode="contain" />
                                                </TouchableOpacity>

                                                <View style={{ flex: 1 }} />

                                                <TouchableOpacity
                                                  onPress={() => { this.getCommentReplyUserLikes(item.id, item.allreply[0].id, index) }}
                                                  style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                  {/* <Text style={[font_style.font_medium, { color: '#A4A4A4', fontSize: 12, }]}>Reply</Text> */}
                                                  <Image source={require('../assets/images/icons/like_filled.png')} style={{ width: 22, height: 22 }} resizeMode="contain" />
                                                  <Text style={[font_style.font_medium, { marginLeft: 5, color: '#A4A4A4', fontSize: 12, }]}>{item.allreply[0].count_like !== null && item.allreply[0].count_like !== undefined ? item.allreply[0].count_like : 0}</Text>
                                                </TouchableOpacity>

                                              </View>
                                            </View>

                                          </Left>
                                        </CardItem>
                                      </Card>
                                    </View>)
                                    : (<View>
                                      {(item.allreply !== undefined && item.allreply !== null && item.allreply.length !== 0) && item.allreply.map((replyItem, replyIndex) => {
                                        return (
                                          <Card transparent key={replyIndex}>
                                            <CardItem style={{
                                              backgroundColor: Colors.bg_color,
                                              paddingLeft: -10,
                                              paddingEnd: -10,
                                              paddingTop: -10,
                                              paddingBottom: -10

                                            }}>
                                              <Left style={{ justifyContent: 'flex-start', alignItems: 'flex-start', }}>
                                                {replyItem.user.images ?
                                                  (<ImageBackground source={require('../assets/images/admin.jpg')}
                                                    style={{ width: user_img_size, height: user_img_size, borderRadius: user_img_size / 2, alignItems: 'flex-start' }}
                                                    imageStyle={{ borderRadius: user_img_size / 2 }}>
                                                    <Image source={{ uri: replyItem.user.images }} style={{ width: user_img_size, height: user_img_size, borderRadius: user_img_size / 2, alignItems: 'flex-start' }} />
                                                  </ImageBackground>)
                                                  : (<Image source={require('../assets/images/admin.jpg')} style={{ width: user_img_size, height: user_img_size, borderRadius: user_img_size / 2, alignItems: 'flex-start' }} />)
                                                }

                                                <View style={{ flex: 1 }}>
                                                  <View style={{
                                                    borderRadius: 20,
                                                    padding: 20,
                                                    flex: 1,
                                                    marginStart: 16,
                                                    backgroundColor: '#fff'
                                                  }}>
                                                    <View style={{ flexDirection: 'row' }}>
                                                      <Text style={[font_style.font_bold, { flex: 1, color: Colors.dark_gray, fontSize: 14 }]}>{replyItem.user.name}</Text>
                                                      <Text style={[font_style.font_medium, { color: '#A4A4A4', fontSize: 12, }]}>{moment(replyItem.created_at).utc().local().fromNow(true) === 'a few seconds' ? 'Just' : moment(replyItem.created_at).utc().local().fromNow(true)}</Text>
                                                    </View>
                                                    <Text style={[font_style.font_medium, styles.meta_txt]}>{replyItem.reply}</Text>
                                                  </View>
                                                  <View style={{ flex: 1, paddingLeft: 20, flexDirection: 'row', marginTop: 12, paddingBottom: 20, alignItems: 'center' }}>
                                                    <TouchableOpacity
                                                      style={{ marginRight: 15 }}
                                                      onPress={() => { this._CommentReplyLike(replyItem, replyIndex, index) }}>
                                                      {replyItem.user_like !== null && replyItem.user_like !== '' && replyItem.user_like === Constant.user_like
                                                        ?
                                                        (<Image source={require('../assets/images/icons/liked_3x.png')} resizeMode="contain" style={{ width: 18, height: 18, }} />)
                                                        :
                                                        (<Image source={require('../assets/images/icons/like_3x.png')} resizeMode="contain" style={{ width: 18, height: 18 }} />)
                                                      }
                                                    </TouchableOpacity>
                                                    {/* <Text style={[font_style.font_medium, { color: '#A4A4A4', fontSize: 12, paddingStart: 4 }]}>{item.allreply[0].count_like !== null && item.allreply[0].count_like !== undefined ? item.allreply[0].count_like : 0} {item.allreply[0].count_like < 2 ? 'Like' : 'Likes'}</Text> */}
                                                    <TouchableOpacity
                                                      onPress={() => { this._CommentReply(item, index) }}>
                                                      {/* <Text style={[font_style.font_medium, { color: '#A4A4A4', fontSize: 12, }]}>Reply</Text> */}
                                                      <Image source={require('../assets/images/icons/comment_new.png')} style={{ width: 18, height: 18 }} resizeMode="contain" />
                                                    </TouchableOpacity>

                                                    <View style={{ flex: 1 }} />

                                                    <TouchableOpacity
                                                      onPress={() => { this.getCommentReplyUserLikes(item.id, replyItem.id, index) }}
                                                      style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                      {/* <Text style={[font_style.font_medium, { color: '#A4A4A4', fontSize: 12, }]}>Reply</Text> */}
                                                      <Image source={require('../assets/images/icons/like_filled.png')} style={{ width: 22, height: 22 }} resizeMode="contain" />
                                                      <Text style={[font_style.font_medium, { marginLeft: 5, color: '#A4A4A4', fontSize: 12, }]}>{replyItem.count_like !== null && replyItem.count_like !== undefined ? replyItem.count_like : 0}</Text>
                                                    </TouchableOpacity>

                                                  </View>
                                                </View>

                                                {/* <Body style={{
                                                }}>
                                                  <Text style={[font_style.font_bold, { color: Colors.dark_gray, fontSize: 14 }]}>{replyItem.user.name}</Text>
                                                  <Text style={[font_style.font_medium, styles.meta_txt]}>{replyItem.reply}</Text>
                                                  <View style={{ flexDirection: 'row', marginTop: 12, paddingBottom: 20, }}>
                                                    <Text style={[font_style.font_medium, { color: '#A4A4A4', fontSize: 12, flex: 0.3 }]}>{moment(replyItem.created_at).utc().fromNow(true) === 'a few seconds' ? 'Just' : moment(replyItem.created_at).utc().fromNow(true)}</Text>
                                                    <View style={{ flex: 0.4, }}>
                                                      <View transparent style={{ flexDirection: 'row', }}>
                                                        <TouchableOpacity

                                                          onPress={() => { this._CommentReplyLike(replyItem, replyIndex, index) }}>
                                                          {replyItem.user_like !== null && replyItem.user_like !== '' && replyItem.user_like === Constant.user_like
                                                            ?
                                                            (<Image source={require('../assets/images/icons/liked.png')} style={{ width: 14, height: 14 }} resizeMode="contain" />)
                                                            :
                                                            (<Image source={require('../assets/images/icons/like.png')} style={{ width: 14, height: 14 }} resizeMode="contain" />)
                                                          }

                                                        </TouchableOpacity>
                                                        <Text style={[font_style.font_medium, { color: '#A4A4A4', fontSize: 12, paddingStart: 4 }]}>{replyItem.count_like !== null && replyItem.count_like !== undefined ? replyItem.count_like : 0} {replyItem.count_like < 2 ? 'Like' : 'Likes'}</Text>
                                                      </View>

                                                    </View>

                                                    <View style={{ flex: 0.3, }}>
                                                      <TouchableOpacity
                                                        onPress={() => { this._CommentReply(item, index) }}>
                                                        <Text style={[font_style.font_medium, { color: '#A4A4A4', fontSize: 12, }]}>Reply</Text>
                                                      </TouchableOpacity>
                                                    </View>

                                                  </View>
                                                </Body> */}
                                              </Left>
                                            </CardItem>
                                          </Card>
                                        )
                                      })

                                      }
                                    </View>)
                                }


                              </View>
                            </View>

                          </Left>
                        </CardItem>
                      </Card>
                    )}

                  //ListFooterComponent={this._renderFooter}

                  />
                  {this.state.hasNextPage && <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', flex: 1, alignSelf: 'center' }} onPress={() => { this._handleLoadMore() }}>
                    {this.state.loadMoreLoading && <ActivityIndicator size="small" color={Colors.primaryColor} />}
                    <Text style={[{ padding: 8, textAlign: 'center' }, font_style.font_medium]}>Load More Comments</Text>
                  </TouchableOpacity>}
                </ScrollView>

              )}

            <View style={{ justifyContent: 'flex-end', }}>
              <View style={styles.inputBar}>

                <TextInput style={[styles.textBox, { height: this.state.defaultHeight !== 0 && this.state.defaultHeight, }]}
                  ref={(ref) => { this.autogrowInput = ref }}
                  multiline={true}
                  //defaultHeight={this.state.defaultHeight}
                  onChangeText={(message) => { this._textChange(message) }}
                  onContentSizeChange={(e) => this._onInputSizeChange(e.nativeEvent.contentSize.height)}
                  value={this.state.message}
                  placeholder="Type your comment here"
                  placeholderTextColor='rgba(90,91,93,0.4)'


                />
                <TouchableOpacity
                  style={{ width: 32, height: 32, alignItems: 'flex-end', justifyContent: 'center' }}
                  onPress={() => { this._AddComment() }}
                  disabled={this.state.disableBtn}>
                  <Image source={require('../assets/images/icons/send_3x.png')} style={{ width: 24, height: 24, }} resizeMode="contain" />

                </TouchableOpacity>
              </View>

            </View>
            {/* <SafeAreaView /> */}
          </View>
          {/* <KeyboardSpacer  /> */}
          <SafeAreaView />
        </KeyboardAvoidingView >

      )


    }
  }
}


//state map
const mapStateToProps = state => {
  return {
    lists: state.lists
  }
}

const mapDispatchToProps = dispatch => {
  return {

    fetchFeedList: (item) => {
      dispatch(fetchFeedList(item))
    },
  }

}
export default connect(mapStateToProps, mapDispatchToProps)(CommentScreen)

class InputBar extends React.Component {

  //AutogrowInput doesn't change its size when the text is changed from the outside.
  //Thus, when text is reset to zero, we'll call it's reset function which will take it back to the original size.
  //Another possible solution here would be if InputBar kept the text as state and only reported it when the Send button
  //was pressed. Then, resetInputText() could be called when the Send button is pressed. However, this limits the ability
  //of the InputBar's text to be set from the outside.
  componentWillReceiveProps(nextProps) {
    if (nextProps.text === '') {
      this.autogrowInput.resetInputText();
    }
  }

  render() {
    return (
      <View></View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
    margin: 0,
    height: '100%'
  },

  meta_txt: {
    color: 'rgba(65,63,64,0.7)',
    fontSize: 14,
    marginTop: 4
  },
  txt_style: {
    fontSize: 14,
    color: Colors.dark_gray,
  },
  inputStyle: {
    flex: 1,
    fontFamily: 'futura-medium',
    marginStart: 10,
    marginEnd: 10,
    height: 50,

  },
  textBox: {
    flex: 1,
    fontFamily: 'futura-medium',
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  sendButton: {
    marginLeft: 5,
    paddingRight: 15,
    width: 32,
    height: 32,
    justifyContent: 'center'
  },
  inputBar: {
    backgroundColor: Colors.white_color,
    flexDirection: 'row',

    paddingStart: 10, paddingEnd: 10,
    backgroundColor: Colors.white_color,

    justifyContent: 'center',
    alignItems: 'center'
  },
  img_view: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2
  },

  imageStyle: { borderRadius: 50 / 2 },
  icon_view: {
    //backgroundColor: Colors.primaryColor,
    width: 48, height: 48,
    position: 'absolute',
    top: Platform.OS === 'ios' ? 30 : 10,
    right: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },

  like_icon_view: {
    //backgroundColor: Colors.primaryColor,
    width: 48, height: 48,
    right: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
});
