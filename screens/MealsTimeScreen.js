import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Text, Dimensions, Platform, TouchableOpacity, Image, AsyncStorage } from 'react-native';
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import Constant from '../constants/Constant'
import { textHeader, font_style } from '../components/styles'
import Colors from '../constants/Colors';
import HeaderBackground from '../components/HeaderBackground';
import ApiService from '../webservice/ApiService';
import Utils from '../constants/Utils';
import DatePicker from 'react-native-datepicker'
import moment from 'moment';
import HeaderBackLeft from '../components/HeaderBackLeft';
import Loader from '../components/Loader';
import { Appearance } from 'react-native-appearance';

const screenWidth = Math.round(Dimensions.get('window').width);

export default class MealsTimeScreen extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.meals_time}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,
    headerRight: <TouchableOpacity
      onPress={() => { _this._UpdateTime() }}
      style={textHeader.rightIcon}>
      <Image source={require('./../assets/images/icons/tick_3x.png')}
        resizeMode="contain" style={{ width: 20, height: 20, alignSelf: 'flex-end' }} />
    </TouchableOpacity>

  });
  state = {
    userInfo: {},
    data: null,
    time1: '',
    time2: '',
    time3: '',
    time4: '',
    time5: '',
    timerArray: []
  }

  componentDidMount = () => {
    _this = this
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        var headers = {
          'X-Access-Token': json.access_key,
        }

        ApiService.api.get(ApiService.get_meals_time, { headers: headers })
          .then((response) => {
            if (response !== null && response.data != null) {
              if (response.data.status === Constant.SUCCESS) {

                if (response.data.data !== null) {
                  let res = response.data.data;


                  this.setState({
                    data: res,
                    time1: res.meal1 !== undefined && res.meal1 !== null && res.meal1 !== '' ? res.meal1 : '',
                    time2: res.meal2 !== undefined && res.meal2 !== null && res.meal2 !== '' ? res.meal2 : '',
                    time3: res.meal3 !== undefined && res.meal3 !== null && res.meal3 !== '' ? res.meal3 : '',
                    time4: res.meal4 !== undefined && res.meal4 !== null && res.meal4 !== '' ? res.meal4 : '',
                    time5: res.meal5 !== undefined && res.meal5 !== null && res.meal5 !== '' ? res.meal5 : '',
                  })


                  this.createArray(res.meal1, res.meal2, res.meal3, res.meal4, res.meal5)
                }

                this.setState({ data: {} })



              } else if (response.data.status === Constant.WARNING) {
                if (response.data.session !== undefined && response.data.session !== null) {
                  if (response.data.session === false) {
                    Utils.toastShow(response.data.message)
                    Utils._signOutAsync(this.props.navigation)
                  }
                }
              }
            }

          })
          .catch(function (error) {
            Utils._SentryReport(error)
            console.log("error " + JSON.stringify(error.response));
          })

      })
      .catch(error => console.log('error!'));
  }

  createArray = (meal1, meal2, meal3, meal4, meal5) => {

    const time1 = meal1 !== undefined && meal1 !== null && meal1 !== '' && meal1 !== 0 ? moment(meal1, 'hh:mm a').format('HH.mm') : 0
    const time2 = meal2 !== undefined && meal2 !== null && meal2 !== '' && meal2 !== 0 ? moment(meal2, 'hh:mm a').format('HH.mm') : 0
    const time3 = meal3 !== undefined && meal3 !== null && meal3 !== '' && meal3 !== 0 ? moment(meal3, 'hh:mm a').format('HH.mm') : 0
    const time4 = meal4 !== undefined && meal4 !== null && meal4 !== '' && meal4 !== 0 ? moment(meal4, 'hh:mm a').format('HH.mm') : 0
    const time5 = meal5 !== undefined && meal5 !== null && meal5 !== '' && meal5 !== 0 ? moment(meal5, 'hh:mm a').format('HH.mm') : 0
    this.setState({ timerArray: [time1, time2, time3, time4, time5] })
  }

  _UpdateTime = async () => {
    await this.createArray(this.state.time1, this.state.time2, this.state.time3, this.state.time4, this.state.time5)

    var flag = false;
    for (var i = 0; i < this.state.timerArray.length; i++) {

      if (i == 0) {
        if (this.state.timerArray[i] !== 0 && this.state.timerArray[i + 1] !== 0) {
          if (this.state.timerArray[i] > this.state.timerArray[i + 1]) {
            flag = true;
            let count = i + 1
            let count1 = i + 2
            Utils.toastShow('Meal ' + count + ' time should be less than Meal ' + count1 + ' time')
            break
          }
        }

      } else if (i == 4) {
        if (this.state.timerArray[i] !== 0 && this.state.timerArray[i + 1] !== 0) {
          if (this.state.timerArray[i] > this.state.timerArray[i + 1]) {
            flag = true;
            let count = i + 1
            let count1 = i + 2
            Utils.toastShow('Meal ' + count + ' time should be less than Meal ' + count1 + ' time')
            break
          }
        }
      } else {
        if (this.state.timerArray[i] !== 0 && (this.state.timerArray[i + 1] !== 0 || this.state.timerArray[i - 1])) {
          if (this.state.timerArray[i] !== 0 && this.state.timerArray[i - 1] !== 0 && this.state.timerArray[i] < this.state.timerArray[i - 1]) {
            flag = true;
            let count = i + 1
            let count1 = i + 2
            Utils.toastShow('Meal ' + count1 + ' time should be greater than Meal ' + count + ' time')
            break
          }

          if (this.state.timerArray[i] !== 0 && this.state.timerArray[i + 1] !== 0 && this.state.timerArray[i] > this.state.timerArray[i + 1]) {
            flag = true;
            let count = i + 1
            let count1 = i + 2
            Utils.toastShow('Meal ' + count + ' time should be less than Meal ' + count1 + ' time')
            break
          }

        }

      }

    }
    if (!flag) {
      var headers = {
        'X-Access-Token': this.state.userInfo.access_key,
      }

      const params = {
        meal1: this.state.time1,
        meal2: this.state.time2,
        meal3: this.state.time3,
        meal4: this.state.time4,
        meal5: this.state.time5,

      }

      ApiService.api.post(ApiService.meals_time, params, { headers: headers })
        .then((response) => {
          if (response !== null && response.data != null) {
            this.props.navigation.navigate('Meal')
            if (response.data.status === Constant.SUCCESS) {

              Utils.toastShow(response.data.message)

            } else if (response.data.status === Constant.WARNING) {
              if (response.data.session !== undefined && response.data.session !== null) {
                if (response.data.session === false) {
                  Utils.toastShow(response.data.message)
                  Utils._signOutAsync(this.props.navigation)
                }
              }
            }
          }

        })
        .catch(function (error) {
          Utils._SentryReport(error)
          console.log("error " + JSON.stringify(error.response));
        })
    }


  }

  render() {

    const colorScheme = Appearance.getColorScheme();
    let bgColor;
    if (colorScheme === 'dark') {
      bgColor = '#000';
    } else {
      bgColor = '#fff';
    }

    if (this.state.data === undefined || this.state.data === null) {
      return (<Loader />)
    } else {
      return (
        <View style={styles.container}>
          <View style={{ margin: 20 }}>
            <View style={styles.menu_layout}>
              <Text style={[styles.txt, font_style.font_bold]}>Meal 1</Text>

              <TouchableOpacity style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', }}>
                <View style={{
                  alignItems: 'flex-end',
                  justifyContent: 'center',
                  borderBottomColor: Colors.light_gray,
                  borderBottomWidth: 1,
                }}>
                  <DatePicker
                    style={{}}
                    date={this.state.time1}
                    //maxDate={this.state.endDate}
                    mode="time"
                    showIcon={false}
                    placeholder="Select Time"
                    format="hh:mm a"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        right: 0,
                        marginLeft: 0,
                        borderColor: Colors.bg_color
                      },
                      dateInput: {
                        borderColor: Colors.bg_color,
                        color: '#797979'
                      },
                      datePickerCon: {
                        backgroundColor: bgColor
                      }
                      // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => { this.setState({ time1: date }) }}
                  />
                </View>
              </TouchableOpacity>


            </View>
            <View style={styles.menu_layout}>
              <Text style={[styles.txt, font_style.font_bold]}>Meal 2</Text>

              <TouchableOpacity style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', }}>
                <View style={{
                  alignItems: 'flex-end',
                  justifyContent: 'center',
                  borderBottomColor: Colors.light_gray,
                  borderBottomWidth: 1,
                }}>
                  <DatePicker
                    style={{}}
                    date={this.state.time2}
                    //maxDate={this.state.endDate}
                    mode="time"
                    showIcon={false}
                    placeholder="Select Time"
                    format="hh:mm a"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        right: 0,
                        marginLeft: 0,
                        borderColor: Colors.bg_color
                      },
                      dateInput: {
                        borderColor: Colors.bg_color,
                        color: '#797979'
                      },
                      datePickerCon: {
                        backgroundColor: bgColor
                      }
                      // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => { this.setState({ time2: date }) }}
                  />
                </View>
              </TouchableOpacity>


            </View>

            <View style={styles.menu_layout}>
              <Text style={[styles.txt, font_style.font_bold]}>Meal 3</Text>

              <TouchableOpacity style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', }}>
                <View style={{
                  alignItems: 'flex-end',
                  justifyContent: 'center',
                  borderBottomColor: Colors.light_gray,
                  borderBottomWidth: 1,
                }}>
                  <DatePicker
                    style={{}}
                    date={this.state.time3}
                    //maxDate={this.state.endDate}
                    mode="time"
                    showIcon={false}
                    placeholder="Select Time"
                    format="hh:mm a"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        right: 0,
                        marginLeft: 0,
                        borderColor: Colors.bg_color
                      },
                      dateInput: {
                        borderColor: Colors.bg_color,
                        color: '#797979',
                        fontSize: 12
                      },
                      datePickerCon: {
                        backgroundColor: bgColor
                      }
                      // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => { this.setState({ time3: date }) }}
                  />
                </View>
              </TouchableOpacity>


            </View>

            <View style={styles.menu_layout}>
              <Text style={[styles.txt, font_style.font_bold]}>Meal 4</Text>

              <TouchableOpacity style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', }}>
                <View style={{
                  alignItems: 'flex-end',
                  justifyContent: 'center',
                  borderBottomColor: Colors.light_gray,
                  borderBottomWidth: 1,
                }}>
                  <DatePicker
                    style={{}}
                    date={this.state.time4}
                    //maxDate={this.state.endDate}
                    mode="time"
                    showIcon={false}
                    placeholder="Select Time"
                    format="hh:mm a"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        right: 0,
                        marginLeft: 0,
                        borderColor: Colors.bg_color
                      },
                      dateInput: {
                        borderColor: Colors.bg_color,
                        color: '#797979'
                      },
                      datePickerCon: {
                        backgroundColor: bgColor
                      }
                      // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => { this.setState({ time4: date }) }}
                  />
                </View>
              </TouchableOpacity>


            </View>
            <View style={styles.menu_layout}>
              <Text style={[styles.txt, font_style.font_bold]}>Meal 5</Text>

              <TouchableOpacity style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', }}>
                <View style={{
                  alignItems: 'flex-end',
                  justifyContent: 'center',
                  borderBottomColor: Colors.light_gray,
                  borderBottomWidth: 1,
                }}>
                  <DatePicker
                    style={{}}
                    date={this.state.time5}
                    //maxDate={this.state.endDate}
                    mode="time"
                    showIcon={false}
                    placeholder="Select Time"
                    format="hh:mm a"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        right: 0,
                        marginLeft: 0,
                        borderColor: Colors.bg_color
                      },
                      dateInput: {
                        borderColor: Colors.bg_color,
                        color: '#797979'
                      },
                      datePickerCon: {
                        backgroundColor: bgColor
                      }
                      // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => { this.setState({ time5: date }) }}
                  />
                </View>
              </TouchableOpacity>


            </View>

          </View>
        </View >
      )
    }

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color
  },
  menu_layout: {
    width: '100%',
    flexDirection: 'row',
    marginBottom: 8,
    justifyContent: 'center',
    height: 46,
    alignItems: 'center'
  },
  txt: { flex: 1, fontSize: 14, color: Colors.primaryColor, },
  plus_btn_view: { width: 32, height: 32, justifyContent: 'center' }

});