import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  AsyncStorage,
  Dimensions,
  Keyboard,
  KeyboardAvoidingView
} from 'react-native';
import { TextInput, ScrollView } from 'react-native-gesture-handler';
import Constant from '../constants/Constant';
import ApiService from '../webservice/ApiService';
import { Icon } from 'react-native-elements';
import { textInput, buttonStyle, font_style } from '../components/styles';
import { Notifications } from 'expo';
import Constants from 'expo-constants';
import * as Location from 'expo-location';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';

import Utils from '../constants/Utils';
import * as firebase from 'firebase';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Spinner from 'react-native-loading-spinner-overlay';
import CustomProgressBar from '../components/CustomProgressBar';
import Colors from '../constants/Colors';
import Modal from 'react-native-modalbox';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { LinearGradient } from 'expo-linear-gradient';
import Loader from '../components/Loader';

const fbId = '';

// const recaptchaVerifier = React.useRef(null);
// const [phoneNumber, setPhoneNumber] = React.useState();
// const [verificationId, setVerificationId] = React.useState();
// const [verificationCode, setVerificationCode] = React.useState();
// const firebaseConfig = firebase.apps.length ? firebase.app().options : undefined;
// const [message, showMessage] = React.useState((!firebaseConfig || Platform.OS === 'web')
//   ? { text: "To get started, provide a valid firebase config in App.js and open this snack on an iOS or Android device." }
//   : undefined);

export default class SignUpScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {
    name: '',
    username: '',
    email: '',
    mobile: '',
    password: '',
    cpassword: '',
    visible_password: true,
    visible_cpassword: true,
    userId: '',
    userInfo: {},
    token: '',
    mapRegion: null,
    hasLocationPermissions: false,
    locationResult: null,
    disableBtn: false,
    expoToken: '',
    notification: null,
    title: 'Hello World',
    body: 'Say something!',
    spinner: false,
    scrollViewPosition: 0,
    otpModal: false,
    confirmResult: null,
    time: {},
    seconds: 59,
    otpLoader: false,
  }
  constructor() {
    super()
    this.timer = 0;
    this.startTimer = this.startTimer.bind(this);
    this.countDown = this.countDown.bind(this);
  }

  componentDidMount() {
    this._getLocationAsync();
    this.registerForPushNotifications()
  }

  startTimer() {
    console.log('this.timer' + this.timer)
    if (this.timer == 0 && this.state.seconds > 0) {
      this.timer = setInterval(this.countDown, 1000);
    }
  }

  secondsToTime(secs) {
    let hours = Math.floor(secs / (60 * 60));

    let divisor_for_minutes = secs % (60 * 60);
    let minutes = Math.floor(divisor_for_minutes / 60);

    let divisor_for_seconds = divisor_for_minutes % 60;
    let seconds = Math.ceil(divisor_for_seconds);

    let obj = {
      "h": hours,
      "m": minutes,
      "s": seconds
    };
    return obj;
  }

  countDown() {
    // Remove one second, set state so a re-render happens.
    let seconds = this.state.seconds - 1;
    this.setState({
      time: this.secondsToTime(seconds),
      seconds: seconds,
    });

    console.log('time', this.state.time)
    console.log('seconds', this.state.seconds)

    // Check if we're at zero.
    if (seconds == 0) {
      console.log('clear interval')
      clearInterval(this.timer);
    }
  }

  async registerForPushNotifications() {
    const { status } = await Permissions.getAsync(Permissions.NOTIFICATIONS);


    if (status !== 'granted') {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      if (status !== 'granted') {
        return;
      }
    }

    const expoToken = await Notifications.getExpoPushTokenAsync();

    this.subscription = Notifications.addListener(this.handleNotification);

    this.setState({
      expoToken,
    });
    this.writeUserData(expoToken)
  }

  handleNotification = notification => {
    this.setState({
      notification,
    });
  };

  writeUserData(token) {
    firebase.database().ref('/users/').push({
      token: token,
    }).then((data) => {
      //success callback
      console.log('data ', data)
    }).catch((error) => {
      //error callback
      console.log('error ', error)
    })
  }

  showOtpLoader = () => {
    this.setState({ otpLoader: true });
  }

  hideOtpLoader = () => {
    this.setState({ otpLoader: false });
  }

  _handleMapRegionChange = mapRegion => {
    console.log(mapRegion);
    this.setState({ mapRegion });
  };


  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        locationResult: 'Permission to access location was denied',
      });
    } else {
      this.setState({ hasLocationPermissions: true });
    }

    if (this.state.hasLocationPermissions) {
      let location = await Location.getCurrentPositionAsync({});
      if (location !== null && location !== undefined) {
        // Center the map on the location we just fetched.
        this.setState({ locationResult: JSON.stringify(location), mapRegion: { latitude: location.coords.latitude, longitude: location.coords.longitude, latitudeDelta: 0.0922, longitudeDelta: 0.0421 } });
      }

    }
  };

  _mobileVerification = async (type, mobile) => {
    // The FirebaseRecaptchaVerifierModal ref implements the
    // FirebaseAuthApplicationVerifier interface and can be
    // passed directly to `verifyPhoneNumber`.
    try {
      const phoneProvider = new firebase.auth.PhoneAuthProvider();
      const verificationId = await phoneProvider.verifyPhoneNumber(
        "+1" + mobile,
        recaptchaVerifier.current
      );
      setVerificationId(verificationId);
      Utils.toastShow("Verification code has been sent to your phone.");
    } catch (err) {
      Utils.toastShow(`Error: ${err.message}`);
    }
  }


  storeData = async (user_id) => {
    try {
      await AsyncStorage.setItem('id', user_id)
    } catch (e) {
      console.log('AsyncStorage Error: ' + error.message);
    }
  }

  _SignUp = () => {
    let msg = '';
    if (this.state.name !== null && this.state.name !== '') {
      if (this.state.username !== null && this.state.username !== '') {
        if (this.state.email !== null && this.state.email !== '') {
          if (this._emailValidate(this.state.email)) {
            if (this.state.password !== null && this.state.password !== '') {
              if (this.state.password.length >= 6) {
                if (this.state.cpassword !== null && this.state.cpassword !== '') {
                  if (this.state.password === this.state.cpassword) {

                    if (this.state.mobile.trim().length !== 0) {

                      if (this.state.mobile.length < 10) {
                        Utils.toastShow('Enter valid mobile number')

                      } else {
                        let mobile = this.state.mobile;
                        mobile = mobile.replace(/\-/g, '')
                        console.log('_saveChanges', mobile)

                        //this._checkMobileNumber(mobile)
                        //this._mobileVerification(1, mobile)

                      }

                      this.props.navigation.navigate('CompleteSignUp', {
                        device_token: Constants.deviceId,
                        name: this.state.name,
                        username: this.state.username,
                        email: this.state.email,
                        password: this.state.password,
                        latitude: this.state.mapRegion === null ? '' : this.state.mapRegion.latitude,
                        longitude: this.state.mapRegion === null ? '' : this.state.mapRegion.longitude,
                        gym_id: 8,
                        fcm_token: this.state.expoToken
                      })

                    } else {
                      Utils.toastShow('Mobile number is required')
                    }

                  } else {
                    msg = 'Password & confirm password should be same'
                    Utils.toastShow(msg)
                  }
                } else {
                  msg = 'Confirm password required'
                  Utils.toastShow(msg)
                }
              } else {
                msg = 'Password should be at least 6 characters'
                Utils.toastShow(msg)
              }

            } else {
              msg = 'Password required'
              Utils.toastShow(msg)
            }

          } else {
            msg = 'Email is not correct'
            Utils.toastShow(msg)
          }

        } else {
          msg = 'Email required'
          Utils.toastShow(msg)
        }
      } else {
        msg = 'Username required'
        Utils.toastShow(msg)
      }
    } else {
      msg = 'Name required'
      Utils.toastShow(msg)
    }

  }

  _emailValidate = (text) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      console.log("Email is Not Correct");

      this.setState({ email: text })
      return false;
    } else {
      this.setState({ email: text, error_msg: '' })
      console.log("Email is Correct");
      return true;
    }
  }

  onTextChange(text) {
    var cleaned = ('' + text).replace(/\D/g, '')
    console.log('cleaned', cleaned);
    var match = cleaned.split('')
    let number = ''
    console.log('match', match)
    if (match.length === 10) {
      //var intlCode = (match[1] ? '+1 ' : ''),
      match.forEach((element, index) => {
        let underscore = index === 2 || index === 5 ? '-' : ''
        number = number + element + underscore
      });
      this.setState({
        mobile: number
      });

      return;
    } else {
      if (match.length === 6 || match.length === 3) {
        match.forEach((element, index) => {
          let underscore = ''
          if (match.length !== 3) {
            underscore = index === 2 ? '-' : ''
          }

          number = number + element + underscore
        });
        this.setState({
          mobile: number
        });
        return;
      }

    }

    this.setState({
      mobile: text
    });
  }

  handleVerifyCode = async () => {
    try {
      const credential = firebase.auth.PhoneAuthProvider.credential(
        verificationId,
        verificationCode
      );
      await firebase.auth().signInWithCredential(credential);
      Utils.toastShow("Phone authentication successful 👍");
    } catch (err) {
      Utils.toastShow(`Error: ${err.message}`);
    }
  }


  _renderOtpVerifyModal = () => {
    return (
      <Modal
        visible={this.state.otpModal}
        transparent
        animationType='slide'
        onRequestClose={() => { this.setState({ otpModal: false }) }}>
        <View style={{ flex: 1, backgroundColor: '#00000040', justifyContent: 'center' }}>

          <View style={[styles.main_container, { backgroundColor: Colors.white, flex: 0, margin: 24, borderRadius: 5, padding: 16 }]}>

            {/* <ActivityIndicator size='small' color={Colors.primaryColor} /> */}
            <Text style={[styles.title, font_style.font_extra_bold]}>Verification</Text>
            <Text style={[styles.otp_msg, font_style.font_normal]}>Enter the 6-digit code sent to you at</Text>
            <Text style={[styles.mobile_text, font_style.font_normal]}>+1 {this.state.mobile}  <Text style={{ color: Colors.primaryColor }} onPress={() => { this.setState({ otpModal: false }) }} >Edit</Text></Text>

            <OTPInputView
              style={{ width: '100%', height: 80 }}
              pinCount={6}
              code={this.state.verificationCode} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
              onCodeChanged={verificationCode => { this.setState({ verificationCode }) }}
              autoFocusOnLoad={false}
              codeInputFieldStyle={styles.underlineStyleBase}
              codeInputHighlightStyle={styles.underlineStyleHighLighted}
            // onCodeFilled={(code => {
            //   console.log(`Code is ${verificationCode}, you are good to go!`)
            // })}
            />

            <TouchableOpacity style={{ marginStart: 24, marginEnd: 24, marginTop: 38 }} onPress={() => { this.handleVerifyCode() }}>
              <LinearGradient colors={[Colors.lighter_blue, Colors.light_blue, Colors.primaryColor,]}
                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 1 }}
                style={[buttonStyle.sign_btn]}>
                <Text style={[styles.btn_text, font_style.font_bold]} >Submit</Text>
              </LinearGradient>
            </TouchableOpacity>

            {this.state.seconds == 0 ?
              (<TouchableOpacity onPress={() => { this._mobileVerification(2) }}>
                <Text style={[styles.resend_code, font_style.font_normal,]}>Resend code</Text>
              </TouchableOpacity>)
              : (<Text style={[styles.resend_txt, font_style.font_normal]}>Resend code in {this.state.time.m}:{this.state.time.s}</Text>)
            }
          </View>

          <Loader loading={this.state.otpLoader} />
        </View>
      </Modal>
    )
  }

  render() {
    let viewHeight = 0
    return (
      <ImageBackground source={require('../assets/images/bg.png')} style={{ width: '100%', height: '100%' }}
      >
        {this._renderOtpVerifyModal()}
        <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', paddingBottom: 20, backgroundColor: 'transparent' }} behavior="padding" enabled keyboardVerticalOffset={0} >
          {/* <KeyboardAvoidingView behavior="padding" enabled> */}
          <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{
              flexGrow: 1, justifyContent: 'space-between',
              flexDirection: 'column',

            }}
            ref={ref => this.scrollView = ref}
            keyboardShouldPersistTaps='handled'
          >
            <CustomProgressBar spinner={this.state.spinner} />
            <View style={styles.main_container}>
              <Text style={[styles.sign_in_txt, font_style.font_Book]}>{'Sign Up'.toUpperCase()}</Text>
              <TextInput
                style={[textInput.auth_textInput, styles.top_margin]}
                onChangeText={(name) => this.setState({ name })}
                value={this.state.name}
                placeholder="Name"
                placeholderTextColor="#fff"
                autoCompleteType="off"
              />

              <TextInput
                style={[textInput.auth_textInput, styles.top_margin]}
                onChangeText={(username) => this.setState({ username })}
                value={this.state.username}
                placeholder="Username"
                placeholderTextColor="#fff"
                autoCapitalize='none'
                autoCompleteType="off"
              />
              <TextInput
                style={[textInput.auth_textInput, styles.top_margin]}
                onChangeText={(email) => this.setState({ email })}
                value={this.state.email}
                placeholder="Email"
                placeholderTextColor="#fff"
                autoCapitalize='none'
                autoCompleteType="off" />
              <View style={[textInput.auth_textInput, { justifyContent: 'center', alignItems: 'center', }]}>
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                  <View style={{ marginEnd: 8 }}><Text style={{ fontFamily: 'futura-medium', fontSize: 14, color: '#fff' }}>+1</Text></View>
                  <TextInput
                    style={[{ fontFamily: 'futura-medium', fontSize: 14, color: '#fff' }, styles.top_margin]}
                    onChangeText={(mobile) => this.onTextChange(mobile)}
                    value={this.state.mobile}
                    placeholder="Mobile Number"
                    placeholderTextColor="#fff"
                    maxLength={12}
                    keyboardType={'numeric'} />

                </View>

              </View>
              <View style={{ width: '100%', justifyContent: 'center' }}>
                <TextInput
                  style={[textInput.auth_textInput, styles.top_margin]}
                  onChangeText={(password) => this.setState({ password })}
                  value={this.state.password}
                  placeholder="Password"
                  placeholderTextColor="#fff"
                  secureTextEntry={this.state.visible_password}
                  password={this.state.visible_password}
                  autoCompleteType="off" />
                <TouchableOpacity style={styles.view_password} onPress={() => this.setState({ visible_password: this.state.visible_password === false ? true : false })}  >
                  <Image source={this.state.visible_password === false ? require('../assets/images/icons/view_password_3x.png') : require('../assets/images/icons/eye.png')} style={[styles.image]} />
                </TouchableOpacity>
              </View>

              <View style={{ width: '100%', justifyContent: 'center' }}>
                <TextInput
                  style={[textInput.auth_textInput, styles.top_margin]}
                  onChangeText={(cpassword) => this.setState({ cpassword })}
                  value={this.state.cpassword}
                  placeholder="Confirm Password"
                  placeholderTextColor="#fff"
                  secureTextEntry={this.state.visible_cpassword}
                  password={this.state.visible_cpassword}
                  autoCompleteType="off"
                />

                <TouchableOpacity style={styles.view_password} onPress={() => this.setState({ visible_cpassword: this.state.visible_cpassword === false ? true : false })}  >
                  <Image source={this.state.visible_cpassword === false ? require('../assets/images/icons/view_password_3x.png') : require('../assets/images/icons/eye.png')} style={[styles.image]} />
                </TouchableOpacity>

              </View>

              <TouchableOpacity
                activeOpacity={1}
                onPress={() => { Keyboard.dismiss(), this._SignUp() }}
                style={[buttonStyle.auth_btn, styles.top_margin]}
                underlayColor='#fff'
              >
                <Text style={buttonStyle.loginText}>{'Sign Up'.toUpperCase()}</Text>
              </TouchableOpacity>
              <Text style={{ color: 'white', marginTop: 16, marginBottom: 8, fontFamily: 'futura-medium', fontSize: 14 }}>Already have an account?</Text>
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => { Keyboard.dismiss(), this.props.navigation.navigate('Login') }}
                style={buttonStyle.login_btn}
                underlayColor='#fff'>
                <Text style={[buttonStyle.btnText]}>{'Sign In'.toUpperCase()}</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
          {/* </KeyboardAvoidingView> */}
        </KeyboardAvoidingView>
      </ImageBackground >


    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    flex: 1,
    flexDirection: 'column',
    //alignItems: 'center',
  },
  top_margin: {
    marginBottom: 10
  },
  iconstyle: {
    flex: 1,
    marginTop: 10,
    justifyContent: 'center',
  },
  main_container: {
    marginTop: 20,
    flex: 1,
    padding: 24,
    width: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },

  sign_in_txt: {
    color: 'white',
    marginTop: 10,
    marginBottom: 10,
    textAlign: 'center',
    fontSize: 25,
  },
  view_password: { position: 'absolute', right: 16 },
  image: {
    width: 18,
    height: 14,
    tintColor: Colors.white_color
  },
});