import React, { Component } from 'react'
import {
  ScrollView, StyleSheet, Image, Text, Dimensions, WebView,
  TouchableOpacity
} from 'react-native';
import { View } from 'native-base';
import HeaderLeft from '../components/HeaderLeft';
import Constant from '../constants/Constant';
import { textHeader, font_style } from '../components/styles';
import Colors from '../constants/Colors';
import HeaderRight from '../components/HeaderRight';
import HeaderBackground from '../components/HeaderBackground';
import HTML from 'react-native-render-html';
import ApiService from '../webservice/ApiService';
import HeaderBackLeft from '../components/HeaderBackLeft';

export default class FAQAnswersScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.faq}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,

  });

  state = {
    data: {}
  }

  componentDidMount = () => {
    const data = this.props.navigation.getParam('data', null)
    this.setState({ data: data })

  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={{ margin: 20 }}>
          <Text style={[font_style.font_Book, styles.title]}>{this.state.data.question}</Text>
          <Text style={[font_style.font_Book, styles.meta_txt]}>{this.state.data.answer}</Text>
        </View>


      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },
  title: {
    fontSize: 22,
    color: Colors.primaryColor
  },
  meta_txt: {
    fontSize: 16,
    color: Colors.dark_gray,
    marginTop: 20
  }
});
