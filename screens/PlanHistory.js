import React, { Component } from 'react'
import { ScrollView, StyleSheet, Image, Text, Dimensions, TouchableOpacity, ImageBackground, AsyncStorage, Platform } from 'react-native';
import { View } from 'native-base';
import HeaderLeft from '../components/HeaderLeft';
import Constant from '../constants/Constant';
import Colors from '../constants/Colors';
import Carousel from 'react-native-snap-carousel';
import { buttonStyle, textHeader, font_style } from '../components/styles';
import { FlatGrid } from 'react-native-super-grid';
import HeaderBackground from '../components/HeaderBackground';
import HeaderRight from '../components/HeaderRight';
import HeaderBackLeft from '../components/HeaderBackLeft';
import Loader from '../components/Loader';
import { SafeAreaView } from 'react-navigation';
import { Ionicons } from '@expo/vector-icons';
import Utils from '../constants/Utils';
import ApiService from '../webservice/ApiService';
import DataNotFound from '../components/DataNotFound';
import moment from 'moment'
import { FlatList } from 'react-native-gesture-handler';
import { WebView } from 'react-native-webview';

const horizontalMargin = 10;
const sliderWidth = Dimensions.get('window').width;
const slideWidth = sliderWidth - 80;
const itemWidth = slideWidth + horizontalMargin * 2;
const itemHeight = 200;

export default class PlanHistory extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.plan_history}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,

  });

  state = {
    entries: null,
  }

  componentDidMount() {
    _this = this
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        var headers = {
          'X-Access-Token': json.access_key,
        }
        ApiService.api.get(ApiService.planHistory, { headers: headers })
          .then((response) => {
            this.setState({ entries: [] })
            if (response !== null && response.data != null) {
              if (response.data.status === Constant.SUCCESS) {
                this.setState({
                  entries: response.data.data != null ? response.data.data : [],

                })


              } else if (response.data.status === Constant.WARNING) {
                if (response.data.session !== undefined && response.data.session !== null) {
                  if (response.data.session === false) {
                    Utils.toastShow(response.data.message)
                    Utils._signOutAsync(this.props.navigation)
                  }
                }
              }
            }

          })
          .catch(function (error) {
            Utils._SentryReport(error)
            console.log("error " + JSON.stringify(error.response));
          })

      })
      .catch(error => console.log('error!'));
  }

  ShowAlert = (item, index) => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }
    let newArray = [...this.state.entries]
    newArray[index].auto_renewal = newArray[index].auto_renewal === "active" ? 'inactive' : 'active'
    this.setState({ entries: newArray })
    ApiService.api.get(ApiService.planSetting, { headers: headers })
      .then((response) => {

        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {

          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })
  }
  render() {
    if (this.state.entries === undefined || this.state.entries === null) {
      return (<Loader />)
    } else {
      if (this.state.entries.length === 0) {
        return (<DataNotFound />)
      } else {
        return (
          <View style={styles.container}>
            <FlatList
              style={{ margin: 20 }}
              showsVerticalScrollIndicator={false}
              data={this.state.entries}
              renderItem={({ item, index }) =>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('PlanHistoryDeatils', {
                      planId: item.id
                    })
                  }}
                  style={{
                    backgroundColor: Colors.white_color,
                    borderRadius: 5,
                    padding: 16,
                    marginBottom: 16, flex: 1
                  }}>

                  <View style={[styles.status, item.status === 'active' ? { backgroundColor: Colors.green, } : { backgroundColor: Colors.light_gray }]}>
                    <Text style={[styles.status_txt, font_style.font_medium, item.status === 'active' ? { color: Colors.white_color, } : { color: Colors.black_color }]}>{item.status === 'active' ? 'Active' : 'Expired'}</Text>
                  </View>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={[{ fontSize: 20, }, font_style.font_medium]}>{item.sub_title !== undefined && item.sub_title !== null ? '$' + item.price : item.title}</Text>
                    {item.sub_title !== undefined && item.sub_title !== null && (
                      <View style={{ alignSelf: 'flex-end' }}>
                        <Text style={[{ fontSize: 14, }, font_style.font_medium]}>/{item.sub_title}</Text>
                      </View>)}


                  </View>
                  {/* <Text style={[{ color: Colors.gray_shade, fontSize: 14, marginTop: 24 }, font_style.font_medium,]}>${item.price} per sessions</Text> */}

                  <Text style={[{ color: Colors.gray_shade, fontSize: 14, marginTop: 8 }, font_style.font_medium,]}>Price : ${item.price}</Text>
                  {item.quantity !== undefined && item.quantity !== null &&
                    <Text style={[{ color: Colors.gray_shade, fontSize: 14, marginTop: 8 }, font_style.font_medium,]}>Session Quantity : {item.quantity === '-1' ? 'Unlimited' : item.quantity}</Text>
                  }

                  {item.remanning_quantity !== undefined && item.remanning_quantity !== null && item.remanning_quantity >= 0 &&
                    <Text style={[{ color: Colors.gray_shade, fontSize: 14, marginTop: 8 }, font_style.font_medium,]}>Remaining Sessions : {item.remanning_quantity === '-1' ? 'Unlimited' : item.remanning_quantity}</Text>
                  }

                  {item.start_date !== undefined && item.start_date !== null &&
                    <Text style={[{ color: Colors.gray_shade, fontSize: 14, marginTop: 8 }, font_style.font_medium,]}>Start Date : {item.start_date}</Text>
                  }
                  {item.end_date !== undefined && item.end_date !== null &&
                    <Text style={[{ color: Colors.gray_shade, fontSize: 14, marginTop: 8 }, font_style.font_medium,]}>End Date : {item.end_date}</Text>
                  }

                  {item.status === 'active'
                    ? (
                      <View />
                      //   <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                      //   <View style={{ flex: 1, justifyContent: 'center' }}>
                      //     <Text style={[styles.title, font_style.font_medium]}>Plan Auto Renewal</Text>
                      //   </View>
                      //   <TouchableOpacity onPress={() => { this.ShowAlert(item, index) }}>
                      //     <Image source={item.auto_renewal === "active" ? require('../assets/images/yes_3x.png') : require('../assets/images/no_3x.png')} style={{ width: 78, height: 38 }} resizeMode="contain"></Image>
                      //   </TouchableOpacity>
                      // </View>
                    )

                    : (
                      <View></View>
                      //   <TouchableOpacity
                      //   activeOpacity={1}
                      //   onPress={() => {
                      //     this.props.navigation.navigate('PlanDetails', {
                      //       planId: item.plan_id
                      //     })
                      //   }}
                      //   style={[buttonStyle.auth_btn, styles.btn_register]}
                      //   underlayColor='#fff'
                      //   disabled={this.state.disableBtn}>
                      //   <Text style={[buttonStyle.loginText, { color: Colors.dark_gray }]}>Buy Again</Text>


                      // </TouchableOpacity>
                    )}

                </TouchableOpacity>
              }
              keyExtractor={item => item.id}
            />


            <SafeAreaView />
          </View>
        )
      }

    }

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },

  TouchableOpacityStyle: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 20,
    bottom: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  status: {

    borderBottomLeftRadius: 26 / 2,
    borderTopLeftRadius: 26 / 2,
    alignItems: 'flex-end',
    justifyContent: 'center',
    height: 26,
    position: 'absolute',
    right: 0
  },
  status_txt: {
    color: '#fff',
    fontSize: 14,
    paddingEnd: 8,
    paddingStart: 8
  },
  btn_register: {
    height: 46,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: 1, width: 0 },
        shadowOpacity: 0.2,
        shadowRadius: 5,
      },
      android: {
        elevation: 4,
      },
    }),
  }
})