import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage,
  Dimensions,
  Platform,
  ProgressBarAndroid,
  ProgressViewIOS,
} from 'react-native';
import Colors from '../constants/Colors';
import Constant from '../constants/Constant';
import HeaderRight from '../components/HeaderRight';
import HeaderBackground from '../components/HeaderBackground';
import HeaderLeft from '../components/HeaderLeft';
import { textInput, buttonStyle, text_color, textHeader, font_style } from '../components/styles';
import {
  PieChart,
} from 'react-native-chart-kit'
import * as Progress from 'react-native-progress';
import { Container, Content, Card, CardItem, Thumbnail, Left, Body, Right, ListItem } from 'native-base'
import { ScrollView } from 'react-native-gesture-handler';
import PureChart from 'react-native-pure-chart';
import CalendarStripGuideline from '../components/calender/CalendarStripGuideline';
import moment from 'moment';
import Loader from '../components/Loader';
import ApiService from '../webservice/ApiService';
import { AntDesign } from '@expo/vector-icons'
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';

// import {
//   Select,
//   Option,
//   OptionList,
//   updatePosition
// } from 'react-native-dropdown'

import { Dropdown } from 'react-native-material-dropdown';
import CalendarStripMonth from '../components/calender/CalendarStripMonth';
import DataNotFound from '../components/DataNotFound';
import Utils from '../constants/Utils';

export default class NutritionScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.nutrition}`.toUpperCase()}</Text>,
    headerLeft: <HeaderLeft navigationProps={navigation} />,
    //headerRight: <HeaderRight navigationProps={navigation} screen="MealNutrition" />,
  });

  state = {
    data: [
      { name: 'Carbs', name_value: 'carbohydrate', value: 0, color: '#C10376', legendFontColor: '#7F7F7F', legendFontSize: 15, goal: 0, total: 0 },
      { name: 'Protein', name_value: 'protein', value: 0, color: '#1370EE', legendFontColor: '#7F7F7F', legendFontSize: 15, goal: 0, total: 0 },
      //{ name: 'Calcium', value: 25, color: '#FF9036', legendFontColor: '#7F7F7F', legendFontSize: 15 },
      { name: 'Fat', name_value: 'fat', value: 0, color: '#2F2FAA', legendFontColor: '#7F7F7F', legendFontSize: 15, goal: 0, total: 0 },
    ],
    nutritionData: null,
    nutritionFactVisible: false,
    progressBarProgress: 100.0,
    selected: 0,
    selectedDate: null,
    showDropdown: false,
    marked: null,
    markDate: [],
  };

  hideDropdown = () => {
    this.setState({ showDropdown: false })
  }
  componentWillMount = () => {
    _this = this
  }

  componentDidMount = () => {
    _this = this
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        console.log('json', json)
        const date = moment().format('YYYY-DD-MM');
        this.setState({
          userInfo: json,
          markDate: [date]
        })

        this.anotherFunc([date])

        this._NutritionData(moment().format('MM-DD-YYYY'), this.state.selected)

      })
      .catch(error => console.log('error!', error));
  }


  anotherFunc = (markDate) => {
    var obj = markDate.reduce((c, v) => Object.assign(c, { [v]: { selected: true, selectedColor: Colors.primaryColor, alignSelf: 'center' } }), {});
    this.setState({ marked: obj });
  }
  _NutritionData = (dateFormate, selected) => {

    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    let params = {}
    // if (selected === 0) {
    params = {
      type: 'day',
      date: dateFormate
    }
    // } else {
    //   const startOfMonth = moment(dateFormate, 'MM-DD-YYYY').startOf('month').format('MM-DD-YYYY');
    //   const endOfMonth = moment(dateFormate, 'MM-DD-YYYY').endOf('month').format('MM-DD-YYYY');
    //   params = {
    //     type: 'month',
    //     first_date: startOfMonth,
    //     last_date: endOfMonth
    //   }
    // }

    //this.setState({ allClasses: [] })
    ApiService.api.post(ApiService.getNutrition, params, { headers: headers })
      .then((response) => {
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            console.log('data', response.data.data)
            //this.setState({ allWorkout: response.data.data != null ? response.data.data : [] })
            if (response.data.data !== undefined && response.data.data !== null && response.data.data.length !== 0) {
              this.setState({ nutritionData: response.data.data })
              if (response.data.data.total_calories !== 0) {
                this._graphData()
              }


            } else {
              this.setState({ nutritionData: [] })
            }
          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })
  }

  _graphData = () => {
    let carbohydrate = parseInt(this.state.nutritionData.total_carbohydrate) * 4
    let protein = parseInt(this.state.nutritionData.total_protein) * 4
    let fat = parseInt(this.state.nutritionData.total_fat) * 9


    const total = parseInt(carbohydrate) + parseInt(protein) + parseInt(fat)
    let carbohydrate_percent = (parseInt(carbohydrate) * 100) / total
    carbohydrate_percent = Number((carbohydrate_percent).toFixed(0))

    let protein_percent = (parseInt(protein) * 100) / total
    protein_percent = Number((protein_percent).toFixed(0))

    let fat_percent = (parseInt(fat) * 100) / total
    fat_percent = Number((fat_percent).toFixed(0))

    let updateData = [...this.state.data]
    this.state.data.forEach((element, index) => {
      switch (element.name_value) {
        case 'carbohydrate':
          updateData[index].value = Number(this.state.nutritionData.carbohydrate_percent)
          updateData[index].total = this.state.nutritionData.total_carbohydrate
          updateData[index].goal = this.state.nutritionData.carbohydrate_percent_goal
          updateData[index].grams = parseInt(this.state.nutritionData.carbohydrates_gram)
          break;
        case 'protein':
          updateData[index].value = Number(this.state.nutritionData.protein_percent)
          updateData[index].total = this.state.nutritionData.total_protein
          updateData[index].goal = this.state.nutritionData.protein_percent_goal
          updateData[index].grams = parseInt(this.state.nutritionData.protein_gram)
          break;
        case 'fat':
          updateData[index].value = Number(this.state.nutritionData.fat_percent)
          updateData[index].total = this.state.nutritionData.total_fat
          updateData[index].goal = this.state.nutritionData.fat_percent_goal
          updateData[index].grams = parseInt(this.state.nutritionData.fat_gram)

          break;

        default:
          break;
      }
    })

    this.setState({ data: updateData })

    console.log('updateData', updateData)
  }
  onChangeTextPress(value) {
    this.setState({ showDropdown: false })
    const startOfMonth = moment().startOf('month').format('MM-DD-YYYY');
    const endOfMonth = moment().endOf('month').format('MM-DD-YYYY');

    this.setState({
      selected: value === 'Days View' ? 0 : 1, nutritionData: null
    })


    let dateFormate = value === 1 ? moment(date.dateString).format("MM-DD-YYYY") : moment().format("MM-DD-YYYY")
    this._NutritionData(dateFormate, value === 'Days View' ? 0 : 1)
  }

  _DateSelected = (date) => {

    let dateFormate = this.state.selected === 1 ? moment(date.dateString).format("MM-DD-YYYY") : moment(date).format("MM-DD-YYYY")

    if (this.state.selected === 1) {
      const newData = date.dateString
      this.setState({
        markDate: [newData]
      })

      this.anotherFunc([newData])
    }
    this.setState({ selectedDate: dateFormate, nutritionData: null })
    this._NutritionData(dateFormate, this.state.selected)
  }

  _RenderData = () => {
    return (
      <View style={styles.container}>
        {(this.state.nutritionData === undefined || this.state.nutritionData === null) ?
          (<Loader />)
          : (
            <View style={styles.second_container}>
              {
                this.state.nutritionData.total_calories !== 0 ?
                  (<ScrollView style={styles.container} >
                    <TouchableOpacity style={styles.container} activeOpacity={1}

                      onPress={() => { this.setState({ showDropdown: false }) }}>
                      <View style={styles.second_container}>
                        <View style={{ padding: 20 }}>

                          <View style={{ alignItems: 'center', }}>
                            {/* <PureChart data={this.state.data} type='pie' width={100} height={100} /> */}
                            <PieChart
                              style={{ width: 220, }}
                              data={this.state.data}
                              width={Dimensions.get('window').width}
                              height={220}
                              chartConfig={{
                                decimalPlaces: 2, // optional, defaults to 2dp
                                color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                                alignItems: 'center'
                              }}
                              accessor="value"
                              backgroundColor="transparent"
                              paddingLeft="15"
                              hasLegend={false}
                            />
                          </View>


                          <View style={{ flexDirection: 'row', marginTop: 20 }} >
                            <View style={{ width: '50%', flexDirection: 'row', }}>

                            </View>
                            <View style={{ width: '20%', alignItems: 'center' }}>
                              <Text style={[font_style.font_medium, styles.meta_txt]}>Total</Text>
                            </View>
                            <View style={{ width: '30%', alignItems: 'center' }}>
                              <Text style={[font_style.font_medium, styles.meta_txt]}>Goal</Text>
                            </View>
                          </View>
                          {this.state.data.map((item, index) => {
                            return (

                              <View style={{ flexDirection: 'row', marginTop: 20 }} key={index}>
                                <View style={{ width: '50%', flexDirection: 'row', }}>
                                  <View style={{ width: 16, height: 16, borderRadius: 16 / 2, backgroundColor: item.color, marginEnd: 4 }} />
                                  <Text style={[font_style.font_medium, styles.meta_txt]}>{item.name} ({item.total}g)</Text>

                                </View>
                                <View style={{ width: '20%', alignItems: 'center' }}>
                                  <Text style={[font_style.font_medium, styles.meta_txt]}>{item.value}%</Text>
                                </View>
                                <TouchableOpacity style={{ width: '30%', alignItems: 'center' }}
                                  activeOpacity={1}
                                  onPress={() => this.props.navigation.navigate("NutritionGuideline")}>
                                  <Text style={[font_style.font_medium, styles.meta_txt, { color: Colors.primaryColor }]}>{item.goal}%<Text style={{ color: Colors.dark_gray, }}>({item.grams}g)</Text></Text>
                                </TouchableOpacity>
                              </View>
                            )
                          })
                          }

                        </View>


                      </View>

                      <View style={{ backgroundColor: '#fff' }}>
                        <View  >
                          <Text style={[font_style.font_bold, {
                            color: '#413F40',
                            fontSize: 14,
                            paddingStart: 20,
                            paddingEnd: 20,
                            paddingBottom: 8,
                            paddingTop: 20
                          }]}>Foods highest in Carbohydrates</Text>
                          {this.state.nutritionData.carbohydrate.map((item, index) => {
                            return (
                              <Card transparent style={{ marginTop: 0, marginRight: 4 }} key={index}>
                                <CardItem >
                                  <Left>
                                    <Text style={[font_style.font_medium, styles.meta_txt]}>{item.food_name}</Text>
                                  </Left>
                                  <Right>
                                    <Text style={[font_style.font_medium, styles.meta_txt]}>{item.carbohydrate} g</Text>
                                  </Right>
                                </CardItem>
                              </Card>
                            )
                          })}


                          {/* <TouchableOpacity
                            onPress={() => this.props.navigation.goBack()}
                            style={{
                              paddingTop: 10,
                              paddingBottom: 10,
                              backgroundColor: Colors.primaryColor,
                              borderRadius: 50,
                              height: 46,
                              justifyContent: 'center',
                              marginStart: 20,
                              marginEnd: 20, marginBottom: 30,
                              marginTop: 10,
                            }}
                            underlayColor='#fff'>
                            <Text style={[buttonStyle.btnText, { fontSize: 14 }]}>{'ANALYZE MY FOODS'.toUpperCase()}</Text>
                          </TouchableOpacity> */}
                        </View>

                        <View  >
                          <Text style={[font_style.font_bold, {
                            color: '#413F40',
                            fontSize: 14,
                            paddingStart: 20,
                            paddingEnd: 20,
                            paddingBottom: 8,
                            paddingTop: 20
                          }]}>Foods highest in Protein</Text>
                          {this.state.nutritionData.protein.map((item, index) => {
                            return (
                              <Card transparent style={{ marginTop: 0, backgroundColor: '#fff', }} key={index}>
                                <CardItem style={{ backgroundColor: '#fff' }}>
                                  <Left>
                                    <Text style={[font_style.font_medium, styles.meta_txt]}>{item.food_name}</Text>
                                  </Left>
                                  <Right>
                                    <Text style={[font_style.font_medium, styles.meta_txt]}>{item.protein} g</Text>
                                  </Right>
                                </CardItem>
                              </Card>
                            )
                          })}


                          {/* <TouchableOpacity
                            onPress={() => this.props.navigation.goBack()}
                            style={{
                              paddingTop: 10,
                              paddingBottom: 10,
                              backgroundColor: Colors.primaryColor,
                              borderRadius: 50,
                              height: 46,
                              justifyContent: 'center',
                              marginStart: 20,
                              marginEnd: 20, marginBottom: 30,
                              marginTop: 10,
                            }}
                            underlayColor='#fff'>
                            <Text style={[buttonStyle.btnText, { fontSize: 14 }]}>{'ANALYZE MY FOODS'.toUpperCase()}</Text>
                          </TouchableOpacity> */}
                        </View>

                        <View  >
                          <Text style={[font_style.font_bold, {
                            color: '#413F40',
                            fontSize: 14,
                            paddingStart: 20,
                            paddingEnd: 20,
                            paddingBottom: 8,
                            paddingTop: 20
                          }]}>Foods highest in Fat</Text>
                          {this.state.nutritionData.fat.map((item, index) => {
                            return (
                              <Card transparent style={{ marginTop: 0 }} key={index}>
                                <CardItem >
                                  <Left>
                                    <Text style={[font_style.font_medium, styles.meta_txt]}>{item.food_name}</Text>
                                  </Left>
                                  <Right>
                                    <Text style={[font_style.font_medium, styles.meta_txt]}>{item.fat} g</Text>
                                  </Right>
                                </CardItem>
                              </Card>
                            )
                          })}


                          {/* <TouchableOpacity
                            onPress={() => this.props.navigation.goBack()}
                            style={{
                              paddingTop: 10,
                              paddingBottom: 10,
                              backgroundColor: Colors.primaryColor,
                              borderRadius: 50,
                              height: 46,
                              justifyContent: 'center',
                              marginStart: 20,
                              marginEnd: 20, marginBottom: 30,
                              marginTop: 10,
                            }}
                            underlayColor='#fff'>
                            <Text style={[buttonStyle.btnText, { fontSize: 14 }]}>{'ANALYZE MY FOODS'.toUpperCase()}</Text>
                          </TouchableOpacity> */}
                        </View>

                      </View>
                    </TouchableOpacity>
                  </ScrollView>)

                  :
                  (this.state.selected === 1 ?
                    <TouchableOpacity style={styles.container} activeOpacity={1}

                      onPress={() => { this.setState({ showDropdown: false }) }}>
                      <View style={{ alignItems: 'center', marginTop: 40 }}>
                        <DataNotFound />
                      </View>
                    </TouchableOpacity>
                    : <TouchableOpacity style={styles.container} activeOpacity={1}

                      onPress={() => { this.setState({ showDropdown: false }) }}><DataNotFound /></TouchableOpacity>)
              }
            </View>
          )}
      </View>
    )
  }
  render() {
    let data = [{
      value: 'Days View', name: 'day'
    }, {
      value: 'Months View', name: 'month'
    }];


    return (


      <View style={styles.container}>

        <View style={{ backgroundColor: Colors.bg_color }}>
          <View style={{ alignItems: 'center' }}>
            <TouchableOpacity
              onPress={() => { this.setState({ showDropdown: true }) }}
              style={{ height: 48, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: Dimensions.get('window').width }}>
              <Text style={[{ color: '#413F40', marginEnd: 4, fontSize: 14 }, font_style.font_medium,]}>{data[this.state.selected].value}</Text>

              <View>
                <AntDesign name="caretdown" size={10} color={Colors.dark_gray} />
              </View>

            </TouchableOpacity>


            {/* <Dropdown
              value={data[0].value}
              data={data}
              containerStyle={{ width: 120 }}
              overlayStyle={{ width: '100%', alignItems: 'center' }}
              pickerStyle={{ width: Dimensions.get('window').width, alignItems: 'center' }}
              inputContainerStyle={{ textAlign: 'center' }}
              shadeOpacity={0}
              inputContainerStyle={{ fontSize: 14 }}
              fontSize={14}
              itemTextStyle={{ color: '#413F40', textAlign: 'center', alignItems: 'center' }}
              dropdownOffset={{ top: 0, bottom: -10 }}
              dropdownMargins={{ min: 0, max: 0 }}
              rippleCentered={true}


              rippleInsets={{ top: 0, bottom: -10 }}

              valueExtractor={({ value }) => value}
              onChangeText={(value) => { this.onChangeTextPress(value) }}
            /> */}
          </View>

        </View>

        {this.state.selected === 0
          ?
          (
            <View style={styles.container}>

              <CalendarStripGuideline
                calendarAnimation={{ type: 'sequence', duration: 30 }}
                daySelectionAnimation={{ type: 'border', duration: 200, borderWidth: 1, borderHighlightColor: 'black', backgroundColor: Colors.primaryColor }}
                style={{ height: 40, paddingTop: 0, paddingBottom: 0 }}
                calendarHeaderStyle={{ color: 'black' }}
                calendarColor={'#fff'}
                showMonth={false}

                dateNumberStyle={{ color: 'black' }}
                dateNameStyle={{ color: 'black' }}
                dateMonthStyle={{ color: 'black' }}
                highlightDateNumberStyle={{ color: 'black' }}
                highlightDateNameStyle={{ color: 'black' }}
                disabledDateNameStyle={{ color: 'black' }}
                //datesWhitelist={datesWhitelist}
                getPreviousWeek={date => { this._DateSelected(date) }}
                getNextWeek={date => { this._DateSelected(date) }}
                disabledDateNumberStyle={{ color: 'black' }}
                iconContainer={{ flex: 0.1 }}
                onDateSelected={date => { this._DateSelected(date) }}
              />

              {this._RenderData()}
            </View>
          )
          :
          (
            <ScrollView style={styles.container}
              onScroll={(event) => {
                this.setState({ showDropdown: false })
              }}
            >
              <View style={styles.container}>
                <Calendar
                  // Collection of dates that have to be marked. Default = {}
                  markedDates={this.state.marked}
                  //showWeekNumbers={true}

                  onDayPress={date => { this._DateSelected(date) }}
                  theme={{
                    backgroundColor: '#ffffff',
                    calendarBackground: '#ffffff',
                    textSectionTitleColor: '#b6c1cd',
                    todayTextColor: '#00adf5',
                    dayTextColor: '#000',
                    textDisabledColor: '#d9e1e8',
                    arrowColor: Colors.dark_gray,
                    arrowSize: 24,
                    monthTextColor: Colors.primaryColor,
                    indicatorColor: 'blue',
                    textDayFontSize: 14,
                    textMonthFontSize: 18,
                    textDayHeaderFontSize: 14,
                    textDayFontFamily: 'futura-medium',
                    textMonthFontFamily: 'futura-medium',
                    textDayHeaderFontFamily: 'futura-medium',
                    'stylesheet.day.basic': {
                      text: {
                        ...Platform.select({
                          ios: {
                            marginTop: 7,
                          },
                          android: {
                            marginTop: 6,
                          },
                        }),
                      }
                    }

                  }}
                />
                {this._RenderData()}
              </View>
            </ScrollView>
          )
        }





        {/* <TouchableOpacity style={{ width: '50%', justifyContent: 'center', }}>

              <View style={{ flexDirection: 'row', }}>

                <Text style={[{ color: Colors.dark_gray, fontSize: 16, marginStart: 20 }, font_style.font_medium]}>Last 6 months</Text>
              </View>

            </TouchableOpacity>
            <TouchableOpacity style={{ width: '50%', alignItems: 'flex-end', justifyContent: 'center', }}>

              <View style={{ flexDirection: 'row', }}>
                <Image source={require('../assets/images/icons/filter_3x.png')} style={[{ width: 22, height: 22, marginRight: 20 }]} />

              </View>

            </TouchableOpacity> */}




        {this.state.showDropdown && <View style={{ position: 'absolute', width: Dimensions.get('window').width, backgroundColor: Colors.white_color, top: 46 }}>
          <TouchableOpacity
            onPress={() => { this.onChangeTextPress(data[0].value) }}
            style={{ height: 48, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={[{ fontSize: 14, }, font_style.font_medium, this.state.selected === 0 ? { color: Colors.black_color } : { color: '#413F40' }]}>Days View</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => { this.onChangeTextPress(data[1].value) }}
            style={{ height: 48, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={[{ fontSize: 14, }, font_style.font_medium, this.state.selected === 1 ? { color: Colors.black_color } : { color: '#413F40' }]}>Months View</Text>
          </TouchableOpacity>
        </View>}

      </View>
    )

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,

  },
  txt_style: {
    fontSize: 14,
    color: Colors.dark_gray,
  },
  second_container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
    height: '100%',
  },

  progressbar_heading: {
    fontSize: 16,
    color: Colors.cart_color_icon,
  },
  progressbar_view: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 10
  },

  meta_txt: {
    color: Colors.dark_gray,
    fontSize: 14,
  },
  pickerStyle: {
    width: Dimensions.get('window').width - 48,
    paddingStart: 20,
    marginStart: 24, marginEnd: 24, marginTop: 60
  }

});