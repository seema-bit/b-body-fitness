import React, { Component } from 'react'
import { ScrollView, StyleSheet, Image, Text, Dimensions, SafeAreaView } from 'react-native';
import { View } from 'native-base';
import HeaderLeft from '../components/HeaderLeft';
import Constant from '../constants/Constant';
import { textHeader } from '../components/styles';
import Colors from '../constants/Colors';
import HeaderRight from '../components/HeaderRight';
import HeaderBackground from '../components/HeaderBackground';
import ApiService from '../webservice/ApiService';
import HeaderBackLeft from '../components/HeaderBackLeft';
import { WebView } from 'react-native-webview';
import Utils from '../constants/Utils';

const htmlContent = `
    <h1 style="margin-top: 0px; margin-bottom: 0px; font-size: 24px; font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgb(51, 51, 51); background-color: rgb(249, 249, 249);">Privacy Policy</h1>
`;

export default class ConditionSecondScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.term_condition}`.toUpperCase()}</Text>,
    headerLeft: <HeaderBackLeft navigationProps={navigation} />,
    //headerRight: <HeaderRight navigationProps={navigation} />,

  });

  state = {
    content: ''
  }

  render() {
    return (
      <View style={styles.container}>
        <WebView
          source={{ uri: ApiService.BASE_URL + ApiService.condition_url }}
          style={styles.container}
          renderLoading={Utils.ActivityIndicatorLoadingView}
          startInLoadingState={true}
        />
        <SafeAreaView />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white_color,
    paddingStart: 8, paddingEnd: 8
  },
})
