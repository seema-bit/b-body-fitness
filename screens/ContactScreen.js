import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage,
  KeyboardAvoidingView,
  Platform,
  Keyboard,
  Linking, Alert
} from 'react-native';
import { Icon } from 'react-native-elements'
import { TextInput, ScrollView } from 'react-native-gesture-handler';
import { textInput, buttonStyle, text_color, textHeader } from '../components/styles';
import Colors from '../constants/Colors';
import Constant from '../constants/Constant';
import HeaderLeft from '../components/HeaderLeft';
import HeaderRight from '../components/HeaderRight';
import HeaderProfileRight from '../components/HeaderProfileRight';

import PopupDialog, { DialogContent, DialogButton, DialogTitle } from 'react-native-popup-dialog';
import ModalSelector from 'react-native-modal-selector'
import ApiService from '../webservice/ApiService';
import Utils from '../constants/Utils';
import HeaderBackground from '../components/HeaderBackground';
import Spinner from 'react-native-loading-spinner-overlay';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CustomProgressBar from '../components/CustomProgressBar';
import * as MailComposer from 'expo-mail-composer';


export default class ContactScreen extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.contact_us}`.toUpperCase()}</Text>,
    headerLeft: <HeaderLeft navigationProps={navigation} />,

  });

  state = {
    userDetails: '',
    name: '',
    email: '',
    mobile: '',
    message: '',
    access_key: '',
    disableBtn: false,
    spinner: false,
    scrollViewPosition: 0
  }

  _ProfileUpdate = () => {

    // var params = new FormData();
    // params.append('name', this.state.name);
    // params.append('email', this.state.email);
    // params.append('mobile', this.state.mobile);
    // params.append('message', this.state.message);

    let msg = '';
    if (this.state.name !== null && this.state.name !== '') {
      if (this.state.email !== null && this.state.email !== '') {
        if (Utils._emailValidate(this.state.email)) {
          if (this.state.mobile !== null && this.state.mobile !== '') {
            if (this.state.mobile.length >= 10) {
              if (this.state.message !== null && this.state.message !== '') {
                this.setState({ disableBtn: true })
                this.networkCall()
              } else {
                msg = 'Message required'
                Utils.toastShow(msg)
              }
            } else {
              msg = 'Enter valid mobile number'
              Utils.toastShow(msg)
            }

          } else {
            msg = 'Mobile number required'
            Utils.toastShow(msg)
          }

        } else {
          msg = 'Email is not correct'
          Utils.toastShow(msg)
        }

      } else {
        msg = 'Email required'
        Utils.toastShow(msg)
      }
    } else {
      msg = 'Name required'
      Utils.toastShow(msg)
    }
  }

  networkCall = () => {
    this.setState({ spinner: true })
    const params = {
      name: this.state.name,
      email: this.state.email,
      mobile: this.state.mobile,
      message: this.state.message,
    }



    ApiService.api.post(ApiService.contact_us, params)
      .then((response) => {
        this.setState({ disableBtn: false, spinner: false })
        if (response !== null && response.data != null) {
          Utils.toastShow(response.data.message)
          if (response.data.status === Constant.SUCCESS) {

            this.setState({
              name: '',
              email: '',
              mobile: '',
              message: '',
            })
          } else if (response.data.status === Constant.WARNING) {
            this.setState({ disableBtn: false })
            Utils.toastShow(response.data.message)
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils._signOutAsync(this.props.navigation)
              }
            }

          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        Utils.toastShow(Constant.message)
        //this.setState({ disableBtn: false, spinner: false })
        console.log("error " + error);
      })
  }

  _phoneCall = () => {

    let phoneNumber = Constant.mobile_number;
    console.log('phoneNumber', phoneNumber)
    if (Platform.OS !== 'android') {
      phoneNumber = `telprompt:${Constant.mobile_number}`;
    }
    else {
      phoneNumber = `tel:${Constant.mobile_number}`;
    }

    Linking.openURL(phoneNumber);

  }

  _sentEmail = () => {
    Linking.openURL(`mailto:${Constant.email}`)
  }


  render() {

    return (

      <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', backgroundColor: Colors.bg_color, paddingBottom: 10 }} behavior="padding" enabled keyboardVerticalOffset={Platform.OS === 'ios' ? 100 : 110} >

        <ScrollView style={styles.container} ref={ref => this.scrollView = ref}
          keyboardShouldPersistTaps="always" >
          <CustomProgressBar spinner={this.state.spinner} />
          <View style={styles.container}>
            <Image source={require('../assets/images/icons/mail_box_3x.png')} style={{
              width: 56, height: 56, alignSelf: 'center', marginTop: 20, marginBottom: 20
            }} resizeMode="contain" />
            <View style={styles.bodyContent}>
              <Text style={styles.text_style}>Name</Text>
              <View style={styles.textInputMain}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(name) => this.setState({ name })}
                  value={this.state.name}
                  placeholder="Name"
                  placeholderTextColor={Colors.dark_gray}
                />
              </View>

              <Text style={styles.text_style}>Email</Text>
              <View style={styles.textInputMain}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(email) => this.setState({ email })}
                  value={this.state.email}
                  placeholder="Enter Email"
                  placeholderTextColor={Colors.dark_gray}
                  autoCapitalize='none'
                />
              </View>

              <Text style={styles.text_style}>Phone Number</Text>

              <View style={styles.textInputMain}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(mobile) => this.setState({ mobile })}
                  value={this.state.mobile}
                  placeholder="Enter Phone Number"
                  placeholderTextColor={Colors.dark_gray}
                  keyboardType="numeric"
                  maxLength={10}
                />
              </View>

              <Text style={styles.text_style}>Message</Text>


              <View style={[{

                borderWidth: 1,
                borderColor: Colors.light_gray,
                width: '100%',
                marginTop: 6,
                borderRadius: 5, height: 100, justifyContent: 'flex-start',
                justifyContent: 'flex-start',

              }]}
                onLayout={({ nativeEvent: { layout: { x, y, width, height } } }) => {
                  this.setState({
                    scrollViewPosition: y
                  });

                }}>
                <TextInput
                  style={[styles.inputStyleNew, {
                    paddingStart: 16,
                    paddingTop: 16,
                    textAlignVertical: 'top',
                  }, Platform.OS === 'ios' ? { flex: 1, } : { height: 105, paddingBottom: 10 }]}
                  onChangeText={(message) => this.setState({ message })}
                  value={this.state.message}
                  placeholder="Type Message"
                  placeholderTextColor={Colors.dark_gray}
                  multiline={true}
                  autoCorrect={false}
                  onFocus={() => {
                    let height = this.state.scrollViewPosition - 100

                    this.scrollView.scrollTo({ x: 0, y: Platform.OS === 'ios' ? height : height, animated: true });
                  }}

                />
              </View>


              <TouchableOpacity
                disabled={this.state.disableBtn}
                onPress={() => { Keyboard.dismiss(), this._ProfileUpdate() }}
                style={[buttonStyle.login_btn, { marginBottom: 16 }]}
                underlayColor='gray'>
                <Text style={buttonStyle.btnText}>{'Submit'.toUpperCase()}</Text>
              </TouchableOpacity>
              <View style={{
                height: 70,
                width: '100%',
                marginBottom: 20
              }}>
                <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap', width: '100%' }}>
                  <TouchableOpacity
                    style={[styles.btn_call_email, { marginRight: 16, }]}
                    underlayColor='#fff'
                    onPress={() => { Keyboard.dismiss(), this._phoneCall() }}
                    activeOpacity={1}
                  >
                    <Text style={[buttonStyle.btnText]}>{'Call'.toUpperCase()}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    activeOpacity={1}
                    style={[styles.btn_call_email, { marginLeft: 16, }]}
                    onPress={() => { Keyboard.dismiss(), this._sentEmail() }}
                    underlayColor='#fff'>
                    <Text style={[buttonStyle.btnText]}>{'Email'.toUpperCase()}</Text>
                  </TouchableOpacity>

                </View>
              </View>

            </View>

          </View>
        </ScrollView>
      </KeyboardAvoidingView>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,

  },
  header: {
    backgroundColor: Colors.bg_color,
    padding: 10,
    alignItems: 'center',
  },

  avatar: {
    width: 110,
    height: 110,
    borderRadius: 57,
    borderWidth: 2,
    borderColor: "white",
    marginBottom: 10,
  },
  name: {
    fontSize: 22,
    fontWeight: '600',
  },
  bodyContent: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 10,
    marginBottom: 20,
  },

  bodyContent: {
    paddingStart: 24,
    paddingEnd: 24,
    flexDirection: 'row',
    flexWrap: 'wrap'

  },
  cross_icon_view: {
    width: 32,
    height: 32,
    position: 'absolute',
    backgroundColor: 'white',
    borderRadius: 50,
    backgroundColor: 'white',
    shadowOffset: { width: 1, height: 1, },
    shadowColor: 'black',
    shadowOpacity: .4,
    marginTop: 90,
    marginLeft: 60,
    left: 150,
    top: 0,
    zIndex: 10,
    alignSelf: 'center',
    justifyContent: 'center'
  },
  text_style: {
    marginTop: 16,
    color: Colors.txt_gray,
    paddingStart: 16,
    fontFamily: 'futura-medium',
    fontWeight: 'bold',

  },
  textInputMain: {
    flexDirection: 'row',
    borderRadius: 50,
    borderWidth: 1,
    borderColor: Colors.light_gray,
    width: '100%',
    height: 46,
    marginTop: 6,

  },
  inputStyle: {
    flex: 1,
    paddingStart: 16,
    fontFamily: 'futura-medium',
    alignSelf: 'center',
    height: 56,

  },
  inputStyleNew: {
    fontSize: 14,
    fontFamily: 'futura-medium',
  },
  edit_icon_view: {
    width: 32,
    height: 32,
    borderRadius: 50,
    backgroundColor: 'white',
    shadowOffset: { width: 1, height: 1, },
    shadowColor: 'black',
    shadowOpacity: .4,
    alignSelf: 'center',
    marginRight: 10,
    justifyContent: 'center'
  },

  btn_call_email: {
    marginTop: 10,
    padding: 10,
    backgroundColor: Colors.shades_blue,
    borderRadius: 50,
    height: 54,
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row'
  },
});