import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage,
  Platform,
  KeyboardAvoidingView,
  Keyboard,
} from 'react-native';
import { Icon } from 'react-native-elements'
import { TextInput, ScrollView } from 'react-native-gesture-handler';
import { textInput, buttonStyle, text_color, textHeader, modalSelector } from '../components/styles';
import Colors from '../constants/Colors';
import Constant from '../constants/Constant';
import HeaderLeft from '../components/HeaderLeft';
import HeaderRight from '../components/HeaderRight';
import HeaderProfileRight from '../components/HeaderProfileRight';

import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import PopupDialog, { DialogContent, DialogButton, DialogTitle } from 'react-native-popup-dialog';
import ModalSelector from 'react-native-modal-selector'
import ApiService from '../webservice/ApiService';
import Utils from '../constants/Utils';
import HeaderBackground from '../components/HeaderBackground';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Spinner from 'react-native-loading-spinner-overlay';
import CustomProgressBar from '../components/CustomProgressBar';
import { SafeAreaView } from 'react-navigation';

export default class EditProfileScreen extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.profile}`.toUpperCase()}</Text>,
    headerLeft: <HeaderLeft navigationProps={navigation} />,
    //headerRight: <HeaderProfileRight navigationProps={navigation} />,
  });

  state = {
    userDetails: '',
    name: '',
    email: '',
    mobile: '',
    country: '',
    city: '',
    state: '',
    address: '',
    zipcode: '',
    access_key: '',
    image: null,
    random: '',
    visible: false,
    visible_name: false,
    visible_email: false,
    visible_mobile: false,
    visible_country: false,
    visible_city: false,
    visible_address: false,
    visible_state: false,
    visible_zipcode: false,
    disableBtn: false,
    spinner: false
  }

  componentDidMount = async () => {
    // const info = await AsyncStorage.getItem(Constant.USERINFO);
    // const data = JSON.parse(info);
    // this.setState({ userDetails: data })
    return AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({
          userDetails: json,
          name: json.name !== 'null' && json.name !== null ? json.name : '',
          email: json.email !== 'null' && json.email !== null ? json.email : '',
          mobile: json.mobile !== 'null' && json.mobile !== null ? json.mobile : '',
          country: json.country !== 'null' && json.country !== null ? json.country : '',
          city: json.city !== 'null' && json.city !== null ? json.city : '',
          address: json.address !== 'null' && json.address !== null ? json.address : '',
          state: json.state !== 'null' && json.state !== null ? json.state : '',
          zipcode: json.zipcode !== 'null' && json.zipcode !== null ? json.zipcode : '',
          access_key: json.access_key,
          image: json.image !== '' && json.image !== null ? json.image : null,
          visible_name: json.name !== '' && json.name !== null ? false : true,
          visible_email: json.email !== '' && json.email !== null ? false : true,
          visible_mobile: json.mobile !== '' && json.mobile !== null ? false : true,
          visible_country: json.country !== '' && json.country !== null ? false : true,
          visible_city: json.city !== '' && json.city !== null ? false : true,
          visible_address: json.address !== '' && json.address !== null ? false : true,
          visible_state: json.state !== undefined && json.state !== '' && json.state !== null ? false : true,
          visible_zipcode: json.zipcode !== undefined && json.zipcode !== '' && json.zipcode !== null ? false : true,
        })


      })
      .catch(error => console.log('error!'));

  }
  _pickImageFromGallery = async () => {

    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

    if (status === 'granted') {
      let result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        //aspect: [4, 3],
      });


      if (!result.cancelled) {
        this.setState({ image: result.uri, visible: false });
      }
    }

  };

  _pickImageFromCamera = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL);

    if (status === 'granted') {
      try {
        let result = await ImagePicker.launchCameraAsync(
          {
            allowsEditing: true,
            //aspect: [4, 3]
          })

        if (!result.cancelled) {
          this.setState({ image: result.uri, visible: false });
        }
        this.handleClick()
      } catch (error) {
        console.log(error.response)
      }

    }
  }

  handleClick() {
    const min = 1;
    const max = 100;
    const rand = min + Math.random() * (max - min);
    this.setState({ random: this.state.random + rand });
  }

  showPopup = () => {
    this.setState({ visible: true });
  }

  showImageSelectDialog = (key) => {
    if (key !== null && key !== '') {
      if (key === 0) {
        this._pickImageFromCamera()
      } else if (key === 1) {
        this._pickImageFromGallery()
      }
    }
  }

  editTextField = (key) => {
    switch (key) {
      case 'name':
        this.setState({ visible_name: true })
        break;

      case 'email':
        this.setState({ visible_email: true })
        break;

      case 'mobile':
        this.setState({ visible_mobile: true })
        break;

      case 'country':
        this.setState({ visible_country: true })
        break;

      case 'city':
        this.setState({ visible_city: true })
        break;

      case 'city':
        this.setState({ visible_city: true })
        break;

      case 'state':
        this.setState({ visible_state: true })
        break;

      case 'zipcode':
        this.setState({ visible_zipcode: true })
        break;

      case 'address':
        this.setState({ visible_address: true })
        break;

      default:
        break;
    }
  }

  _ProfileUpdate = () => {

    const imageName = Utils.handleClick()
    this.setState({
      random: imageName,
    })
    let msg = '';
    if (this.state.name !== null && this.state.name !== '') {
      if (this.state.mobile !== null && this.state.mobile !== '') {
        if (this.state.mobile.length >= 10) {
          if (this.state.address !== null && this.state.address !== '') {
            if (this.state.city !== null && this.state.city !== '') {
              if (this.state.state !== null && this.state.state !== '') {
                if (this.state.zipcode !== null && this.state.zipcode !== '') {
                  if (this.state.country !== null && this.state.country !== '') {
                    this.setState({ disableBtn: true, spinner: true })
                    this.networkCall()
                  } else {
                    msg = 'Country required'
                    Utils.toastShow(msg)
                  }
                } else {
                  msg = 'Zip Code required'
                  Utils.toastShow(msg)
                }
              } else {
                msg = 'State required'
                Utils.toastShow(msg)
              }
            } else {
              msg = 'City required'
              Utils.toastShow(msg)
            }
          } else {
            msg = 'Address required'
            Utils.toastShow(msg)
          }

        } else {
          msg = 'Enter valid mobile number'
          Utils.toastShow(msg)
        }

      } else {
        msg = 'Mobile Number is required'
        Utils.toastShow(msg)
      }
    } else {
      msg = 'Name required'
      Utils.toastShow(msg)
    }
  }

  networkCall = () => {
    var params = new FormData();
    params.append('name', this.state.name);
    params.append('email', this.state.email);
    params.append('mobile', this.state.mobile);
    params.append('country', this.state.country);
    params.append('city', this.state.city);
    params.append('state', this.state.state);
    params.append('zipcode', this.state.zipcode);
    params.append('address', this.state.address);

    if (this.state.image !== null) {
      params.append('image',
        { uri: this.state.image, name: `${this.state.random}` + '.jpg', type: 'image/jpg' });
    }
    var headers = {
      'X-Access-Token': this.state.access_key,
    }


    ApiService.api.post(ApiService.profile, params, { headers: headers })
      .then((response) => {
        this.setState({ disableBtn: false, spinner: false })
        if (response !== null && response.data != null) {
          Utils.toastShow(response.data.message)
          if (response.data.status === Constant.SUCCESS) {
            this.setState({
              visible_name: false,
              visible_email: false,
              visible_mobile: false,
              visible_country: false,
              visible_city: false,
              visible_address: false,
              visible_state: false,
              visible_zipcode: false,
            })
            Utils.toastShow(response.data.message)
            AsyncStorage.setItem(Constant.USERINFO, JSON.stringify(response.data.data));
            this.props.navigation.closeDrawer({
              refresh: 'yes'
            })
          } else if (response.data.status === Constant.WARNING) {
            Utils.toastShow(response.data.message)
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils.toastShow(Constant.message)
        Utils._SentryReport(error)
        //this.setState({ disableBtn: false, spinner: false })
      })
  }

  render() {
    let index = 0;
    const data = [
      { key: index++, label: 'Take a Photo...' },
      { key: index++, label: 'Choose from Library...' },
    ];

    return (
      <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', backgroundColor: Colors.bg_color, paddingBottom: 10 }} behavior="padding" enabled keyboardVerticalOffset={Platform.OS === 'ios' ? 100 : 110} >

        <ScrollView style={styles.container}
          keyboardShouldPersistTaps="always"
        >
          <PopupDialog
            style={{
              flex: 1,
              alignSelf: 'center'
            }}
            visible={this.state.visible}
            onTouchOutside={() => {
              this.setState({ visible: false });
            }}
            dialogTitle={<DialogTitle title="Select a Photo" />}
            ac

          >
            <DialogContent
              style={{ height: 200 }}>
              <DialogButton
                text="Take Photo..."
                onPress={() => this._pickImageFromCamera()}
              />
              <DialogButton
                text="Choose from Library..."
                onPress={() => this._pickImageFromGallery()}
              />
              <DialogButton
                text="CANCEL"
                onPress={() => { this.setState({ visible: true }); }}
              />
            </DialogContent>
          </PopupDialog>
          <CustomProgressBar spinner={this.state.spinner} />
          <View style={styles.container}>
            <View style={styles.outerCircle}>
              <ModalSelector
                cancelText="Cancel"
                touchableActiveOpacity={1}
                data={data}
                onChange={option => { this.showImageSelectDialog(option.key) }}
                overlayStyle={modalSelector.overlayStyle}
                optionContainerStyle={modalSelector.optionContainerStyle}

                optionStyle={modalSelector.optionStyle}
                optionTextStyle={modalSelector.optionTextStyle}

                cancelStyle={modalSelector.cancelStyle}
                cancelTextStyle={modalSelector.cancelTextStyle}
              >
                <TouchableOpacity>
                  <View style={styles.header}>

                    {this.state.image !== null ? (
                      <Image style={styles.avatar} source={{ uri: this.state.image }} />
                    ) :
                      <Image style={styles.avatar}
                        source={require('../assets/images/admin.jpg')}
                      />}
                    <View
                      style={styles.cross_icon_view}>
                      <Icon
                        name='camera-alt'
                        type='material'
                        color='gray'
                        size={16}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              </ModalSelector>
            </View>

            <View style={styles.body}>
              <View style={styles.bodyContent}>
                <Text style={styles.text_style}>Name</Text>

                {this.state.name === '' || this.state.name === null || this.state.visible_name ?
                  (
                    <View style={styles.textInputMain}>
                      <TextInput
                        style={[styles.inputStyle, { height: 56 }]}
                        onChangeText={(name) => this.setState({ name })}
                        value={this.state.name}
                        placeholder="Name"
                        placeholderTextColor={Colors.dark_gray}
                      />
                    </View>)
                  :
                  (<View style={[styles.textInputMain]}>
                    <Text style={[styles.inputStyle]}>
                      {this.state.name}
                    </Text>
                    <View
                      style={styles.edit_icon_view}>
                      <Icon
                        name='edit'
                        type='material'
                        color='gray'
                        size={16}
                        onPress={() => this.editTextField('name')}
                      />
                    </View>
                  </View>
                  )
                }

                <Text style={styles.text_style}>Email</Text>

                <View style={styles.textInputMain}>

                  <Text style={[styles.inputStyle]}>
                    {this.state.email}
                  </Text>

                </View>

                <Text style={styles.text_style}>Phone Number</Text>

                {this.state.mobile === '' || this.state.mobile === null || this.state.visible_mobile ?
                  (
                    <View style={styles.textInputMain}>
                      <TextInput
                        style={[styles.inputStyle, { height: 56 }]}
                        onChangeText={(mobile) => this.setState({ mobile })}
                        value={this.state.mobile}
                        placeholder="Enter Phone Number"
                        placeholderTextColor={Colors.dark_gray}
                        keyboardType="numeric"
                        maxLength={10}
                      />
                    </View>)
                  :
                  (<View style={styles.textInputMain}>
                    <Text style={[styles.inputStyle]}>
                      {this.state.mobile}
                    </Text>
                    <View
                      style={styles.edit_icon_view}>
                      <Icon
                        name='edit'
                        type='material'
                        color='gray'
                        size={16}
                        onPress={() => this.editTextField('mobile')}
                      />
                    </View>
                  </View>
                  )
                }

                <Text style={styles.text_style}>Address</Text>
                {this.state.address === '' || this.state.address === null || this.state.visible_address ?
                  (
                    <View style={styles.textInputMain}>
                      <TextInput
                        style={[styles.inputStyle, { height: 56 }]}
                        onChangeText={(address) => this.setState({ address })}
                        value={this.state.address}
                        placeholder="Enter Address"
                        placeholderTextColor={Colors.dark_gray}
                      />
                    </View>)
                  :
                  (<View style={styles.textInputMain}>

                    <Text style={[styles.inputStyle]}>
                      {this.state.address}
                    </Text>
                    <View
                      style={styles.edit_icon_view}>
                      <Icon
                        name='edit'
                        type='material'
                        color='gray'
                        size={16}
                        onPress={() => this.editTextField('address')}
                      />
                    </View>
                  </View>
                  )
                }

                <Text style={styles.text_style}>City</Text>
                {this.state.city === '' || this.state.city === null || this.state.visible_city ?
                  (
                    <View style={styles.textInputMain}>
                      <TextInput
                        style={[styles.inputStyle, { height: 56 }]}
                        onChangeText={(city) => this.setState({ city })}
                        value={this.state.city}
                        placeholder="Enter City"
                        placeholderTextColor={Colors.dark_gray}
                      />
                    </View>)
                  :
                  (<View style={styles.textInputMain}>
                    <Text style={[styles.inputStyle]}>
                      {this.state.city}
                    </Text>
                    <View
                      style={styles.edit_icon_view}>
                      <Icon
                        name='edit'
                        type='material'
                        color='gray'
                        size={16}
                        onPress={() => this.editTextField('city')}
                      />
                    </View>
                  </View>
                  )
                }

                <Text style={styles.text_style}>State</Text>
                {this.state.state === '' || this.state.state === null || this.state.visible_state ?
                  (
                    <View style={styles.textInputMain}>
                      <TextInput
                        style={[styles.inputStyle, { height: 56 }]}
                        onChangeText={(state) => this.setState({ state })}
                        value={this.state.state}
                        placeholder="Enter State"
                        placeholderTextColor={Colors.dark_gray}
                      />
                    </View>)
                  :
                  (<View style={styles.textInputMain}>
                    <Text style={[styles.inputStyle]}>
                      {this.state.state}
                    </Text>
                    <View
                      style={styles.edit_icon_view}>
                      <Icon
                        name='edit'
                        type='material'
                        color='gray'
                        size={16}
                        onPress={() => this.editTextField('state')}
                      />
                    </View>
                  </View>
                  )
                }

                <Text style={styles.text_style}>Zip Code</Text>
                {this.state.zipcode === '' || this.state.zipcode === null || this.state.visible_zipcode ?
                  (
                    <View style={styles.textInputMain}>
                      <TextInput
                        style={[styles.inputStyle, { height: 56 }]}
                        onChangeText={(zipcode) => this.setState({ zipcode })}
                        value={this.state.zipcode}
                        placeholder="Enter Zip Code"
                        placeholderTextColor={Colors.dark_gray}
                      />
                    </View>)
                  :
                  (<View style={styles.textInputMain}>
                    <Text style={[styles.inputStyle]}>
                      {this.state.zipcode}
                    </Text>
                    <View
                      style={styles.edit_icon_view}>
                      <Icon
                        name='edit'
                        type='material'
                        color='gray'
                        size={16}
                        onPress={() => this.editTextField('zipcode')}
                      />
                    </View>
                  </View>
                  )
                }

                <Text style={styles.text_style}>Country</Text>
                {this.state.country === '' || this.state.country === null || this.state.visible_country ?
                  (
                    <View style={styles.textInputMain}>
                      <TextInput
                        style={[styles.inputStyle, { height: 56 }]}
                        onChangeText={(country) => this.setState({ country })}
                        value={this.state.country}
                        placeholder="Enter Country"
                        placeholderTextColor={Colors.dark_gray}

                      />
                    </View>)
                  :
                  (<View style={styles.textInputMain}>
                    <Text style={[styles.inputStyle]}>
                      {this.state.country}
                    </Text>
                    <View
                      style={styles.edit_icon_view}>
                      <Icon
                        name='edit'
                        type='material'
                        color='gray'
                        size={16}
                        onPress={() => this.editTextField('country')}
                      />
                    </View>
                  </View>
                  )
                }

                <TouchableOpacity
                  onPress={() => { Keyboard.dismiss(), this._ProfileUpdate() }}
                  style={[buttonStyle.login_btn, { marginBottom: 80 }]}
                  activeOpacity={1}
                  disabled={this.state.disableBtn}>
                  <Text style={buttonStyle.btnText}>{'Submit'.toUpperCase()}</Text>
                </TouchableOpacity>

              </View>
            </View>
          </View>
        </ScrollView>
        <SafeAreaView />

      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },
  header: {
    backgroundColor: Colors.bg_color,
    padding: 10,
    alignItems: 'center',
  },

  avatar: {
    width: 110,
    height: 110,
    borderRadius: 57,
    borderWidth: 2,
    borderColor: "white",
    marginBottom: 10,
  },
  name: {
    fontSize: 22,
    fontWeight: '600',
  },
  body: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap'
  },

  bodyContent: {
    paddingStart: 24,
    paddingEnd: 24,
    flexDirection: 'row',
    flexWrap: 'wrap'

  },
  cross_icon_view: {
    width: 32,
    height: 32,
    position: 'absolute',
    backgroundColor: 'white',
    borderRadius: 50,
    backgroundColor: 'white',
    shadowOffset: { width: 1, height: 1, },
    shadowColor: 'black',
    shadowOpacity: .4,

    elevation: 10,


    marginTop: 90,
    marginLeft: 60,
    left: 150,
    top: 0,
    zIndex: 10,
    alignSelf: 'center',
    justifyContent: 'center'
  },
  text_style: {
    marginTop: 16,
    color: Colors.txt_gray,
    paddingStart: 16,
    fontFamily: 'futura-medium',
    fontWeight: 'bold',

  },
  textInputMain: {
    flexDirection: 'row',
    borderRadius: 50,
    borderWidth: 1,
    borderColor: Colors.light_gray,
    width: '100%',
    height: 46,
    marginTop: 6,
    justifyContent: 'center',
  },
  inputStyle: {
    flex: 1,
    paddingStart: 16,
    fontFamily: 'futura-medium',
    fontWeight: 'bold',
    alignSelf: 'center',


  },
  edit_icon_view: {
    width: 32,
    height: 32,
    borderRadius: 50,
    backgroundColor: 'white',
    shadowOffset: { width: 1, height: 1, },
    shadowColor: 'black',
    shadowOpacity: .4,
    alignSelf: 'center',
    marginRight: 10,
    justifyContent: 'center'
  },
});