import React, { Component } from 'react'
import { ScrollView, StyleSheet, Image, Text, Dimensions, TouchableOpacity, AsyncStorage, Platform } from 'react-native';
import { View } from 'native-base';
import HeaderLeft from '../components/HeaderLeft';
import Constant from '../constants/Constant';
import Colors from '../constants/Colors';
import { textHeader, font_style } from '../components/styles';
import { Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from 'native-base'
import HeaderProgressRight from '../components/HeaderProgressRight';
import HeaderBackground from '../components/HeaderBackground';
import DatePicker from 'react-native-datepicker'
import moment from 'moment';
import Loader from '../components/Loader';
import ApiService from '../webservice/ApiService';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import Utils from '../constants/Utils';
import DataNotFound from '../components/DataNotFound';
import { Appearance } from 'react-native-appearance';

export default class MyClassessScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.my_classess}`.toUpperCase()}</Text>,
    headerLeft: <HeaderLeft navigationProps={navigation} />,
    //headerRight: <HeaderProgressRight navigationProps={navigation} />,

  });

  state = {
    data: [],
    //startDate: "2016-05-15",
    endDate: moment().format("MM-DD-YYYY"),
    dateFrom: moment().format("MM-DD-YYYY"),
    dateTo: moment().format("MM-DD-YYYY"),

    date: "2016-05-15",
    allClasses: null,
    selectedTab: 0,
    marked: null,
    markDate: [],
    allClassesByDate: null,
    selectedDate: ''
  }

  componentDidMount = () => {
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        const date = moment().format('YYYY-DD-MM');
        this.anotherFunc([date])
        this.setState({ userInfo: json, selectedDate: moment().format('MM-DD-YYYY'), markDate: [date] })
        this._FetchClasses(this.state.dateFrom, this.state.dateTo);

        this._CalendarClassess();


      })
      .catch(error => console.log('error!'));

  }

  componentWillReceiveProps() {
    this._FetchClasses(this.state.dateFrom, this.state.dateTo);

    this._CalendarClassess();
  }

  _FetchClasses = (dateFrom, dateTo) => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
      'Content-Type': 'application/json',
    }

    const params = {
      date_from: dateFrom,
      date_to: dateTo
    }

    ApiService.api.post(ApiService.myClasses, params, { headers: headers })
      .then((response) => {
        this.setState({ allClasses: [] })
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            this.setState({ allClasses: response.data.data != null ? response.data.data : [] })

          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        Utils._SentryReport(error)
        console.log("error " + JSON.stringify(error.response));
      })


  }

  _CalendarClassess = () => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
      'Content-Type': 'application/json',
    }
    ApiService.api.get(ApiService.all_joined_classes, { headers: headers })
      .then((response) => {
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {

            //this.anotherFunc()
            this._FetchClassesByDate(this.state.selectedDate);
          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        //alert('Error')
        console.log("error " + JSON.stringify(error.response));
      })
  }


  _FetchClassesByDate = (dateFrom) => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
      'Content-Type': 'application/json',
    }

    const params = {
      date: dateFrom
    }

    ApiService.api.post(ApiService.getclass, params, { headers: headers })
      .then((response) => {
        this.setState({ allClassesByDate: [] })
        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {
            this.setState({ allClassesByDate: response.data.data != null ? response.data.data : [] })

          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils.toastShow(response.data.message)
                Utils._signOutAsync(this.props.navigation)
              }
            }
          }
        }

      })
      .catch(function (error) {
        //alert('Error')
        console.log("error " + JSON.stringify(error.response));
      })
  }

  anotherFunc = (markDate) => {
    var obj = markDate.reduce((c, v) => Object.assign(c, { [v]: { selected: true, selectedColor: Colors.primaryColor, alignSelf: 'center' } }), {});
    this.setState({ marked: obj });
  }

  _StartDate = (date) => {
    if (this.compare(date, this.state.dateTo) === -1) {
      this.setState({ dateFrom: date })
      this._FetchClasses(date, this.state.dateTo)
    } else {
      Utils.toastShow('Start date should be less than end date')
    }

  }

  _EndDate = (date) => {
    console.log('date', date)
    console.log('this.state.dateFrom')
    //let date1 = moment(date.dateString).format('MM-DD-YYYY')
    if (this.compare(this.state.dateFrom, date) === -1) {
      this.setState({ dateTo: date })
      this._FetchClasses(this.state.dateFrom, date)
    } else {
      Utils.toastShow('End date should be greater than start date')
    }

  }

  compare(dateTimeA, dateTimeB) {
    var momentA = moment(dateTimeA, "MM-DD-YYYY");
    var momentB = moment(dateTimeB, "MM-DD-YYYY");
    if (momentA > momentB) return 1;
    else if (momentA < momentB) return -1;
    else return 0;
  }

  _DateSelected = (date) => {
    let dateFormate = moment(date.dateString).format('MM-DD-YYYY')

    if (this.state.selectedTab === 1) {
      const newData = date.dateString
      this.setState({
        markDate: [newData]
      })

      this.anotherFunc([newData])
    }
    this.setState({ selectedDate: dateFormate, })
    this._CalendarClassess();
  }

  _ListViewClasses = (bgColor) => {
    if (this.state.allClasses === undefined || this.state.allClasses === null) {
      return (<Loader />)
    } else {
      return (
        <View style={{ flex: 1 }}>
          <View style={{
            flexDirection: 'row', height: 58,
            borderBottomColor: Colors.light_gray,
            borderBottomWidth: 1,
            backgroundColor: '#fff'
          }}>

            <View style={{ flexDirection: 'row', width: '30%', alignItems: 'center', paddingStart: 20 }} >
              <Image source={require('../assets/images/icons/caledar_3x.png')} style={{ width: 18, height: 18 }} resizeMode="contain" />
              <Text style={[{ fontSize: 14, color: '#797979', paddingStart: 8 }, font_style.font_Book]}>Date:</Text>
            </View>

            <TouchableOpacity style={{ width: '35%', alignItems: 'center', justifyContent: 'center', }}>
              <View style={{
                flexDirection: 'row', alignItems: 'center',
                justifyContent: 'center',
                borderBottomColor: Colors.light_gray,
                borderBottomWidth: 1,
              }}>
                <DatePicker
                  style={{ width: 100 }}
                  date={this.state.dateFrom}
                  //maxDate={this.state.endDate}
                  mode="date"
                  showIcon={false}
                  placeholder="select date"
                  format="MM-DD-YYYY"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      left: 0,
                      marginLeft: 0,
                      borderColor: '#fff'
                    },
                    dateInput: {
                      borderColor: '#fff',
                      color: '#797979'
                    },
                    datePickerCon: {
                      backgroundColor: bgColor
                    }
                    // ... You can check the source to find the other keys.
                  }}
                  onDateChange={(date) => { this._StartDate(date) }}
                />
                <Image source={require('../assets/images/icons/arrow_down_3x.png')} style={{ width: 8, height: 8, tintColor: '#797979' }} resizeMode="contain" />
              </View>
            </TouchableOpacity>

            <TouchableOpacity style={{ width: '35%', alignItems: 'center', justifyContent: 'center', }}>

              {/* <View style={{ alignSelf: 'center', flexDirection: 'row', }}>
              <Image source={require('../assets/images/icons/meal.png')} style={[this.state.selectedTab === 1 ? { tintColor: Colors.primaryColor } : { tintColor: Colors.dark_gray }, { width: 22, height: 22, marginRight: 12 }]} />
              <Text style={[this.state.selectedTab === 1 ? { color: Colors.primaryColor } : { color: Colors.dark_gray }, { fontSize: 19, }]}>1 Month</Text>
            </View> */}

              <View style={{
                flexDirection: 'row', alignItems: 'center',
                justifyContent: 'center',
                borderBottomColor: Colors.light_gray,
                borderBottomWidth: 1,
              }}>
                <DatePicker
                  style={{ width: 100 }}
                  date={this.state.dateTo}
                  //maxDate={this.state.endDate}
                  mode="date"
                  showIcon={false}
                  placeholder="select date"
                  format="MM-DD-YYYY"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      left: 0,
                      marginLeft: 0,
                      borderColor: '#fff'
                    },
                    dateInput: {
                      borderColor: '#fff',
                      color: '#797979'
                    },
                    datePickerCon: {
                      backgroundColor: bgColor
                    }
                    // ... You can check the source to find the other keys.
                  }}
                  onDateChange={(date) => { this._EndDate(date) }}
                />
                <Image source={require('../assets/images/icons/arrow_down_3x.png')} style={{ width: 8, height: 8, tintColor: '#797979' }} resizeMode="contain" />
              </View>

            </TouchableOpacity>
          </View>
          {this.state.allClasses.length === 0
            ?
            (
              <DataNotFound />
            )
            :
            (<Container style={{ backgroundColor: Colors.bg_color, paddingTop: 10 }} >
              <Content >
                {this.state.allClasses.map((item, index) => {
                  return (
                    <Card style={{
                      flex: 0,
                      borderBottomColor: Colors.light_gray,
                      borderBottomWidth: 1,
                      marginStart: 16, marginEnd: 16,
                    }} key={index} transparent>
                      <CardItem style={{ borderRadius: 4, backgroundColor: Colors.bg_color }}>
                        <Left>
                          {item.image && (<Thumbnail source={{ uri: item.image }} style={{ width: 60, height: 60 }} />)}

                          <Body style={{ marginStart: 16 }}>
                            <Text style={[font_style.font_medium, { color: Colors.dark_gray, fontSize: 16 }]}>{item.exercise_type}</Text>
                            <Text style={[font_style.font_medium, styles.meta_txt]}>{item.join_date}</Text>
                            <Text style={[font_style.font_medium, styles.meta_txt]}>From {item.time_from} To {item.time_to}</Text>
                          </Body>
                        </Left>
                      </CardItem>
                    </Card>
                  )
                })}
              </Content>
            </Container>
            )}
        </View>

      )
    }

  }

  _CalendarViewClasses = () => {
    return (
      <ScrollView style={{ flex: 1, backgroundColor: Colors.bg_color }}>

        <Calendar
          // Collection of dates that have to be marked. Default = {}
          markedDates={this.state.marked}
          //showWeekNumbers={true}

          onDayPress={date => { this._DateSelected(date) }}
          theme={{
            backgroundColor: '#ffffff',
            calendarBackground: '#ffffff',
            textSectionTitleColor: '#b6c1cd',
            todayTextColor: '#00adf5',
            dayTextColor: '#000',
            textDisabledColor: '#d9e1e8',
            arrowColor: Colors.dark_gray,
            arrowSize: 24,
            monthTextColor: Colors.primaryColor,
            indicatorColor: 'blue',
            textDayFontSize: 14,
            textMonthFontSize: 18,
            textDayHeaderFontSize: 14,
            textDayFontFamily: 'futura-medium',
            textMonthFontFamily: 'futura-medium',
            textDayHeaderFontFamily: 'futura-medium',
            'stylesheet.day.basic': {
              text: {
                ...Platform.select({
                  ios: {
                    marginTop: 7,
                  },
                  android: {
                    marginTop: 6,
                  },
                }),
              }
            }
          }}
        />
        {this.state.allClassesByDate !== undefined && this.state.allClassesByDate === null
          ? (<Loader />)
          : this.state.allClassesByDate.length === 0
            ?
            ((
              <View style={{ alignItems: 'center', marginTop: 40 }}>
                <DataNotFound />
              </View>
            ))
            :
            (<View style={{ backgroundColor: Colors.bg_color, paddingTop: 10 }} >

              {this.state.allClassesByDate.map((item, index) => {
                return (
                  <Card style={{
                    flex: 0,
                    borderBottomColor: Colors.light_gray,
                    borderBottomWidth: 1,
                    marginStart: 16, marginEnd: 16,
                  }} key={index} transparent>
                    <CardItem style={{ borderRadius: 4, backgroundColor: Colors.bg_color }}>
                      <Left>
                        {item.image && (<Thumbnail source={{ uri: item.image }} style={{ width: 60, height: 60 }} />)}

                        <Body style={{ marginStart: 16 }}>
                          <Text style={[font_style.font_medium, { color: Colors.dark_gray, fontSize: 16 }]}>{item.exercise_type}</Text>
                          <Text style={[font_style.font_medium, styles.meta_txt]}>{item.join_date}</Text>
                          <Text style={[font_style.font_medium, styles.meta_txt]}>From {item.time_from} To {item.time_to}</Text>
                        </Body>
                      </Left>
                    </CardItem>
                  </Card>
                )
              })}

            </View>
            )}

      </ScrollView>
    )

  }


  render() {

    const colorScheme = Appearance.getColorScheme();
    //const colorScheme = '';
    let bgColor;
    if (colorScheme === 'dark') {
      bgColor = '#000';
    } else {
      bgColor = '#fff';
    }


    return (
      <View style={styles.container}>
        <View style={{
          flexDirection: 'row', height: 46,
          borderBottomColor: Colors.light_gray,
          borderBottomWidth: 1,
        }}>

          <TouchableOpacity onPress={() => { this.setState({ selectedTab: 0 }) }}
            style={{ width: '49%', alignItems: 'center', justifyContent: 'center', }}>

            <View style={{ alignSelf: 'center', flexDirection: 'row', }}>
              <Image source={require('../assets/images/icons/list.png')} style={[this.state.selectedTab === 0 ? { tintColor: Colors.primaryColor } : { tintColor: Colors.dark_gray }, { width: 20, height: 20, marginRight: 12 }]} />
              <Text style={[this.state.selectedTab === 0 ? { color: Colors.primaryColor } : { color: Colors.dark_gray }, { fontSize: 16, }]}>{'List View'.toUpperCase()}</Text>
            </View>

          </TouchableOpacity>
          <View style={{
            borderLeftColor: Colors.light_gray,
            borderLeftWidth: 1,
            marginTop: 6,
            marginBottom: 6
          }}>
          </View>
          <TouchableOpacity onPress={() => { this.setState({ selectedTab: 1 }) }}
            style={{ width: '49%', alignItems: 'center', justifyContent: 'center', }}>

            <View style={{ alignSelf: 'center', flexDirection: 'row', }}>
              <Image source={require('../assets/images/icons/caledar_3x.png')} style={[this.state.selectedTab === 1 ? { tintColor: Colors.primaryColor } : { tintColor: Colors.dark_gray }, { width: 20, height: 20, marginRight: 12 }]} />
              <Text style={[this.state.selectedTab === 1 ? { color: Colors.primaryColor } : { color: Colors.dark_gray }, { fontSize: 16, }]}>{'Calendar View'.toUpperCase()}</Text>
            </View>

          </TouchableOpacity>
        </View>
        {this.state.selectedTab === 0 ?
          (this._ListViewClasses(bgColor))
          :
          (this._CalendarViewClasses())}
      </View>
    )

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color,
  },
  meta_txt: {
    color: '#A4A4A4',
    fontSize: 14,
    marginTop: 4
  },
  txt_style: {
    fontSize: 14,
    color: Colors.dark_gray,
  },
})