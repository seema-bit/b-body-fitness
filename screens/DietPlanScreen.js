import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Text, Dimensions, Platform, TouchableOpacity, Image } from 'react-native';
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import HeaderLeft from '../components/HeaderLeft'
import HeaderRight from '../components/HeaderRight'
import Constant from '../constants/Constant'
import { textHeader, font_style } from '../components/styles'
import Colors from '../constants/Colors';
import { Icon } from 'react-native-elements';
import HeaderBackground from '../components/HeaderBackground';
import ApiService from '../webservice/ApiService';
import moment from 'moment'
import Loader from '../components/Loader';
import Spinner from 'react-native-loading-spinner-overlay';
import Utils from '../constants/Utils';
import { LinearGradient } from 'expo-linear-gradient';

export default class DietPlanScreen extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>{`${Constant.meal}`.toUpperCase()}</Text>,
    headerLeft: <HeaderLeft navigationProps={navigation} />,
    headerRight: <HeaderRight navigationProps={navigation} />,
  });
  constructor(props) {
    super(props);
    this.state = {
      tableHead: ['Name', 'Quantity'],
      tableHeadWater: ['Name', 'Quantity'],
    }
  }

  componentDidMount = () => {
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
        var headers = {
          'X-Access-Token': json.access_key,
        }

        ApiService.api.get(ApiService.userBMI, { headers: headers })
          .then((response) => {
            if (response !== null && response.data != null) {
              if (response.data.status === Constant.SUCCESS) {
                this.setState({
                  entries: response.data.data != null ? response.data.data : [],
                })
              } else if (response.data.status === Constant.WARNING) {
                if (response.data.session !== undefined && response.data.session !== null) {
                  if (response.data.session === false) {
                    Utils.toastShow(response.data.message)
                    Utils._signOutAsync(this.props.navigation)
                  }
                }
              }
            }

          })
          .catch(function (error) {
            Utils._SentryReport(error)
            console.log("error " + JSON.stringify(error.response));
          })

      })
      .catch(error => console.log('error!'));
  }


  render() {
    const state = this.state;
    const tableDataWater = [
      ['Water', '250 ml']
    ];


    const tableData = [
      ['Banana', '6'],
      ['Pulses', '30g'],
      ['Eggs', '4'],
      ['Milk', '250ml'],
    ];


    const element = (cellData, cellIndex) => (

      <View key={cellIndex} style={{
        width: '100%',
        height: '100%',
        borderLeftColor: Colors.light_gray,
        borderLeftWidth: 0.5,
        marginTop: -1,
        justifyContent: 'center'
      }}>
        <Text style={[styles.text, font_style.font_medium]}>{cellData}</Text>
      </View>

    );
    return (
      <View style={styles.container}>
        <ScrollView style={styles.dataWrapper} showsVerticalScrollIndicator={false}>
          <View style={{ margin: 24, }}>
            <View style={styles.menu_layout}>
              <Text style={[{ flex: 1, fontSize: 14, color: Colors.red_color }, font_style.font_bold]}>BreakFast</Text>

            </View>
            <View style={styles.box_layout}>
              <Table borderStyle={{ borderColor: 'transparent' }} style={{ borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
                <Row data={state.tableHead} style={styles.header} textStyle={[styles.headText, font_style.font_bold]} />
              </Table>

              <Table borderStyle={{ borderColor: 'transparent' }} style={{ borderRadius: 100 }}>
                {
                  tableData.map((rowData, index) => (
                    <TableWrapper key={index} style={styles.row} style={[styles.row, index % 2 ? { backgroundColor: '#f5f5f6' } : { backgroundColor: '#ffff', }, index === tableData.length - 1 && { borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }]} >
                      {
                        rowData.map((cellData, cellIndex) => (
                          <Cell key={cellIndex} data={cellIndex === 1 ? element(cellData, index) : cellData} textStyle={[styles.text, font_style.font_medium]} />
                        ))
                      }
                    </TableWrapper>
                  ))
                }
              </Table>


            </View>

            <View style={[styles.menu_layout, { marginTop: 16 }]}>
              <Text style={[{ flex: 1, fontSize: 14, color: Colors.primaryColor }, font_style.font_bold]}>Lunch</Text>


            </View>
            <View style={styles.box_layout}>
              <Table borderStyle={{ borderColor: 'transparent' }} style={{ borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
                <Row data={state.tableHead} style={styles.header} textStyle={[styles.headText, font_style.font_bold]} />
              </Table>

              <Table borderStyle={{ borderColor: 'transparent' }} style={{ borderRadius: 100 }}>
                {
                  tableData.map((rowData, index) => (
                    <TableWrapper key={index} style={styles.row} style={[styles.row, index % 2 ? { backgroundColor: '#f5f5f6' } : { backgroundColor: '#ffff', }, index === tableData.length - 1 && { borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }]} >
                      {
                        rowData.map((cellData, cellIndex) => (
                          <Cell key={cellIndex} data={cellIndex === 1 ? element(cellData, index) : cellData} textStyle={[styles.text, font_style.font_medium]} />
                        ))
                      }
                    </TableWrapper>
                  ))
                }
              </Table>


            </View>

            <View style={[styles.menu_layout, { marginTop: 16 }]}>
              <Text style={[{ flex: 1, fontSize: 14, color: Colors.red_color }, font_style.font_bold]}>Dinner</Text>


            </View>
            <View style={styles.box_layout}>
              <Table borderStyle={{ borderColor: 'transparent' }} style={{ borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
                <Row data={state.tableHead} style={styles.header} textStyle={[styles.headText, font_style.font_bold]} />
              </Table>

              <Table borderStyle={{ borderColor: 'transparent' }} style={{ borderRadius: 100 }}>
                {
                  tableData.map((rowData, index) => (
                    <TableWrapper key={index} style={styles.row} style={[styles.row, index % 2 ? { backgroundColor: '#f5f5f6' } : { backgroundColor: '#ffff', }, index === tableData.length - 1 && { borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }]} >
                      {
                        rowData.map((cellData, cellIndex) => (
                          <Cell key={cellIndex} data={cellIndex === 1 ? element(cellData, index) : cellData} textStyle={[styles.text, font_style.font_medium]} />
                        ))
                      }
                    </TableWrapper>
                  ))
                }
              </Table>


            </View>

            <View style={[styles.menu_layout, { marginTop: 16 }]}>
              <Text style={[{ flex: 1, fontSize: 14, color: Colors.red_color }, font_style.font_bold]}>Snacks</Text>


            </View>
            <View style={styles.box_layout}>
              <Table borderStyle={{ borderColor: 'transparent' }} style={{ borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
                <Row data={state.tableHead} style={styles.header} textStyle={[styles.headText, font_style.font_bold]} />
              </Table>

              <Table borderStyle={{ borderColor: 'transparent' }} style={{ borderRadius: 100 }}>
                {
                  tableData.map((rowData, index) => (
                    <TableWrapper key={index} style={styles.row} style={[styles.row, index % 2 ? { backgroundColor: '#f5f5f6' } : { backgroundColor: '#ffff', }, index === tableData.length - 1 && { borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }]} >
                      {
                        rowData.map((cellData, cellIndex) => (
                          <Cell key={cellIndex} data={cellIndex === 1 ? element(cellData, index) : cellData} textStyle={[styles.text, font_style.font_medium]} />
                        ))
                      }
                    </TableWrapper>
                  ))
                }
              </Table>


            </View>


            <View style={[styles.menu_layout, { marginTop: 16 }]}>
              <Text style={[{ flex: 1, fontSize: 14, color: Colors.red_color }, font_style.font_bold]}>Water</Text>


            </View>
            <View style={styles.box_layout}>
              <Table borderStyle={{ borderColor: 'transparent' }} style={{ borderTopLeftRadius: 10, borderTopRightRadius: 10 }}>
                <Row data={state.tableHeadWater} style={styles.header} textStyle={[styles.headText, font_style.font_bold]} />
              </Table>

              <Table borderStyle={{ borderColor: 'transparent' }} style={{ borderRadius: 100 }}>
                {
                  tableDataWater.map((rowData, index) => (
                    <TableWrapper key={index} style={styles.row} style={[styles.row, index % 2 ? { backgroundColor: '#f5f5f6' } : { backgroundColor: '#ffff', }, index === tableDataWater.length - 1 && { borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }]} >
                      {
                        rowData.map((cellData, cellIndex) => (
                          <Cell key={cellIndex} data={cellIndex === 1 ? element(cellData, index) : cellData} textStyle={[styles.text, font_style.font_medium]} />
                        ))
                      }
                    </TableWrapper>
                  ))
                }
              </Table>


            </View>



          </View>

        </ScrollView>

        <TouchableOpacity
          activeOpacity={0.7}
          onPress={this.clickHandler}
          style={styles.TouchableOpacityStyle}>
          <LinearGradient
            colors={['#0000FF', '#3333FF', '#5858FF']}
            style={{
              width: 50,
              height: 50,
              borderRadius: 45 / 2,
              alignItems: 'center',
              borderRadius: 50,
              justifyContent: 'center',
              marginBottom: 16
            }}
            start={{ x: 0, y: 1 }}
            end={{ x: 1, y: 0 }}>
            <Icon

              name='plus'
              type='font-awesome'
              color='#fff'
              size={32}

            />
          </LinearGradient>
        </TouchableOpacity>
      </View>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bg_color
  },
  header: {
    height: 50,
    backgroundColor: Colors.dark_gray,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,

  },
  headText: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 14
  },
  text: {
    textAlign: 'center',
    color: Colors.dark_gray,
    fontSize: 14

  },
  dataWrapper: {
    marginTop: -1
  },
  row: {
    flexDirection: 'row',
    height: 40,
    backgroundColor: '#fff',
  },

  TouchableOpacityStyle: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 20,
  },

  FloatingButtonStyle: {
    resizeMode: 'contain',
    width: 50,
    height: 50,
    //
  },
  box_layout: {
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: 1, width: 1 },
        shadowOpacity: 0.2,
        shadowRadius: 10,
      },
      android: {
        elevation: 20,
      },
    }),
  },

  menu_layout: {
    width: '100%',
    flexDirection: 'row',
    marginBottom: 8
  },

});