import React from 'react';
import { View, ImageBackground, StatusBar, TextInput, TouchableOpacity, Image, AsyncStorage, Text, Keyboard, StyleSheet } from 'react-native';
import { font_style, textInput, buttonStyle, textHeader } from '../components/styles';
import HeaderBackground from '../components/HeaderBackground';
import HeaderLeft from '../components/HeaderLeft';
import { Loader } from '../components/Loader';
import DatePicker from 'react-native-datepicker'
import moment from 'moment';
import { LinearGradient } from 'expo-linear-gradient';
import Colors from '../constants/Colors';
import Utils from '../constants/Utils';

export default class AddCard extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerBackground: <HeaderBackground />,
    headerStyle: textHeader.header_style,
    headerTitle: <Text style={textHeader.header}>Add/Update card</Text>,
    headerLeft:
      <View>
        {(typeof navigation.state.params.from != "undefined" !== undefined && navigation.state.params.from !== null && navigation.state.params.from === 'setting') && (<TouchableOpacity
          onPress={() => { navigation.navigate('NotificationSetting') }}
          style={textHeader.leftIcon}>
          <Image source={require('./../assets/images/icons/arrow-right_3x.png')} style={{ width: 11, height: 20, alignSelf: 'flex-start' }} />
        </TouchableOpacity>)}
      </View>

  });

  constructor() {
    super();
    this.state = {
      card_type: '',
      card_name: '',
      card_number: '',
      month: '',
      year: '',
      cvv: '',
      credit_card: false,
      debit_card: true,
      token: '',
      cardType: null,
      cardDetails: null,
      loading: false
    }
  }

  _addCard = () => {
    var numbers = /^[0-9]+$/;

    if (this.state.card_number.trim().length <= 0) {
      Utils.toastShow('Card number is required')
      return;
    }

    if (this.state.card_number.trim().length < 12 || this.state.card_number.trim().length > 16) {
      Utils.toastShow('Card number should be min 12 and max 16 digits and only numeric value accepted')
      return;
    }

    if (numbers.test(this.state.card_number.trim()) === false) {
      Utils.toastShow('Card number should be min 12 and max 16 digits and only numeric value accepted')
      return
    }

    if (Utils._checkAllNonZero(this.state.card_number.trim())) {
      Utils.toastShow('Enter valid card number')
      return
    }

    if (this.state.month.trim().length <= 0) {
      Utils.toastShow('Expiry month is required')
      return;
    }

    if (numbers.test(this.state.month.trim()) === false) {
      Utils.toastShow('Expiry month should be accepted only numeric value')
      return
    }

    if (this.state.year.trim().length <= 0) {
      Utils.toastShow('Expiry year is required')
      return;
    }

    if (numbers.test(this.state.year.trim()) === false) {
      Utils.toastShow('Expiry year should be accepted only numeric value')
      return
    }

    if (this.state.cvv.trim().length <= 0) {
      Utils.toastShow('CVV is required')
      return;
    }

    if (numbers.test(this.state.cvv.trim()) === false) {
      Utils.toastShow('CVV should be accepted only numeric value')
      return
    }

    this.props.navigation.navigate('BMR', {

      from: 'AddCard'
    })

  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.main_card_view}>
          <View style={styles.add_card_view}>
            <Text style={[styles.card_text, font_style.font_medium]}>Enter Your Card Number</Text>
            <View style={{ borderBottomColor: Colors.gray, borderBottomWidth: 1, paddingBottom: 8 }}>
              <TextInput style={[styles.card_number, font_style.font_normal]}
                onChangeText={(card_number) => this.setState({ card_number })}
                value={this.state.card_number}
                allowFontScaling={false}
                placeholder="Card Number"
                placeholderTextColor={Colors.gray_shade}
                keyboardType="numeric"
                maxLength={16}
              />
              <Image source={require('../assets/images/credit_card.png')} style={{ height: 22, width: 30, position: 'absolute', right: 6, tintColor: Colors.gray }} />
            </View>
            <View style={{ flexDirection: 'row', marginTop: 40 }}>
              <View style={{ flex: 1, flexDirection: 'row', }}>
                <DatePicker
                  style={{ flex: 1, paddingTop: 12 }}
                  date={this.state.month}
                  mode="date"
                  placeholder="MM"
                  format="MM"
                  minDate={moment().format('MM')}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  showIcon={false}
                  customStyles={{
                    dateInput: styles.dateInput,
                    dateText: [styles.dateText, font_style.font_normal],
                    placeholderText: [styles.dateText, { color: Colors.gray_shade }, font_style.font_normal],
                  }}
                  onDateChange={(month) => { this.setState({ month: month }) }}
                />

                <DatePicker
                  style={{ flex: 1, paddingTop: 12 }}
                  date={this.state.year}
                  mode="date"
                  placeholder="YYYY"
                  format="YYYY"
                  minDate={moment().format('YYYY')}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  showIcon={false}
                  customStyles={{
                    dateInput: styles.dateInput,
                    dateText: [styles.dateText, font_style.font_normal],
                    placeholderText: [styles.dateText, { color: Colors.gray_shade }, font_style.font_normal],
                  }}
                  onDateChange={(year) => { this.setState({ year: year }) }}
                />

                <TextInput style={[styles.card_details, font_style.font_normal]}
                  onChangeText={(cvv) => this.setState({ cvv })}
                  value={this.state.cvv}
                  allowFontScaling={false}
                  placeholder="CVV"
                  placeholderTextColor={Colors.gray_shade}
                  keyboardType="numeric"
                  maxLength={3}
                />
              </View>
              <TouchableOpacity onPress={() => Utils.toastShow('The CVV Number on your credit card or debit card is a 3 digit number on VISA®, MasterCard® and Discover® branded credit and debit cards.')}>
                <Image source={require('../assets/images/info.png')} style={styles.info_icon} />
              </TouchableOpacity>

            </View>

          </View>
          <Text style={[styles.note_text, font_style.font_normal]} >This card will be securely saved for a faster payment experience. CVV number will not be saved</Text>
          <TouchableOpacity activeOpacity={1} onPress={() => this._addCard()} >
            <LinearGradient
              colors={['#0000FF', '#3333FF', '#5858FF']}
              style={styles.btn}
              start={{ x: 0, y: 1 }}
              end={{ x: 1, y: 0 }}>
              <Text style={[styles.btn_text, font_style.font_bold]}>Submit</Text>
            </LinearGradient>
          </TouchableOpacity>

        </View >

      </View >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.bg_color

  },

  //add card
  left_icon: {
    width: 22, height: 22,
    borderRadius: 22 / 2,
    borderWidth: 2,
    borderColor: Colors.gray
  },
  left_icon_active: {
    width: 22, height: 22,
    borderRadius: 22 / 2,
    backgroundColor: Colors.primaryColor,
    alignItems: 'center'
  },
  card_type: {
    fontSize: 16,
    color: Colors.text,
    marginStart: 10
  },
  main_card_view: {
    marginStart: 12,
    marginEnd: 12, marginTop: 30,

  },
  select_card_view: {
    flexDirection: 'row',
    marginBottom: 20
  },
  add_card_view: {
    backgroundColor: Colors.white,
    padding: 12,
    paddingTop: 20,
    paddingBottom: 20,
    borderRadius: 6,
    shadowColor: "#AFAFAF",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.2,
    shadowRadius: 8,

    elevation: 5,
  },
  card_text: {
    fontSize: 14,
    color: Colors.light_gray,
    lineHeight: 18
  },
  card_number: {
    fontSize: 16,
    color: Colors.black,
    paddingTop: 6
  },
  card_details: {
    fontSize: 16,
    color: Colors.black,
    paddingTop: 6,
    flex: 1,
    borderBottomColor: Colors.gray,
    borderBottomWidth: 1,
    marginEnd: 16,
    //paddingBottom: 12,
  },
  info_icon: {
    width: 14,
    height: 14,
    tintColor: Colors.primaryColor,
    marginTop: 16
  },
  note_text: {
    fontSize: 12,
    color: Colors.dark_gray,
    textAlign: 'center',
    marginTop: 10,
    marginStart: 34, marginEnd: 34,
    marginBottom: 30
  },
  pay_btn: {
    color: Colors.white,
    fontSize: 16,
    textAlign: 'center'
  },
  dateInput: {
    borderWidth: 0,
    paddingTop: 6,
    borderBottomColor: Colors.gray,
    borderBottomWidth: 1,
    marginEnd: 16,

    alignItems: 'flex-start',
  },
  dateText: {
    fontSize: 16,
    color: Colors.black,
    textAlign: 'left',
    paddingBottom: 12,
  },
  btn: {
    height: 46, alignItems: 'center', borderRadius: 46 / 2, justifyContent: 'center',
    marginBottom: 16,
    marginStart: 6, marginEnd: 6
  },
  btn_text: {
    backgroundColor: 'transparent',
    fontSize: 14,
    color: '#fff',
  }
});
