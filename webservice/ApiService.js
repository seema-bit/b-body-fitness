import axios from "axios";

const BASE_URL = 'http://bbody.hireconsultingwhiz.com/api/v1/';

const api = axios.create({
  baseURL: BASE_URL,
  //timeout: 5000,
});

const BaseUrlIndex = 'http://bbody.hireconsultingwhiz.com/index.php/'

const pdfUrl = 'http://bbody.hireconsultingwhiz.com/public/file/franchise_application.pdf'

const apiForUrl = axios.create({
  baseURL: BaseUrlIndex,
  //timeout: 5000,
});


//food api
TOKEN_URL = 'https://oauth.fatsecret.com/connect/token'

export default {
  BaseUrlIndex,
  BASE_URL,
  api,
  apiForUrl,
  TOKEN_URL,
  pdfUrl,
  login: 'user/login',
  signup: 'user/signup',
  forgot_password: 'user/forgetpassword',
  reset_password: 'user/resetpassword',
  profile: 'user/profile',
  fetchAllFeeds: 'user/allfeed',
  likeDislike: 'user/like-dislike',
  addFeed: 'user/feed',
  editFeed: 'user/edit-feed/',
  deleteFeed: 'user/delete-feed/',
  fetchAllComment: 'user/allcomment/',
  fetchSpecificComment: 'user/comment-id/',
  addComment: 'user/feedcomment',
  commentReply: 'user/comment-reply',
  commentLikeDislike: 'user/likedislike-comment',
  commentLikedUsers: 'comment/users',
  commentReplyLikedUsers: 'comment/reply/users/',

  getUserLikes: 'user/feed/userlikes/',
  feedDetails: 'user/feed-details/',
  nofification_count: 'user/notification-count',

  getReportCategory: 'getCategoryList',
  postReport: 'reportSubmit',


  fetchProducts: 'user/product',
  order: 'user/order',
  orderHistory: 'user/orderlist',
  OrderDetails: 'user/product-orderdetail/',
  productDetails: 'user/productdetail/',

  addProgress: 'user/add-progress',
  fetchProgress: 'user/get-progress',
  getFullInBodyData: 'user/getFullInBodyData',

  allClasses: 'user/classdetails',
  joinClass: 'user/joinclass',
  myClasses: 'user/myclass',
  all_joined_classes: 'user/getallclass',
  getclass: 'user/getclass',


  getWorkout: 'user/getuserworkout',
  workout: 'user/workoutdetails',
  addWorkout: 'user/addworkout',
  getWorkoutName: 'user/workout',
  teamworkout: 'teamworkout',
  singleWorkout: 'teamworkoutdetail/',

  search_meal: 'user/searchfood',
  add_meal: 'user/userfood',
  myMeal: 'user/getfood',
  add_water: 'user/userwater',
  getNutrition: 'user/nutrition',
  delete_meal: 'user/delete-userfood/',
  meals_time: 'user/meal-time',
  get_meals_time: 'user/user-meal-time',
  cal_intake: 'user/user-intake-cal',
  class_details: 'user/getclass-details/',
  create_workout: 'user/create-workout',

  fetchGyms: 'user/gym',
  single_gym: 'user/single-gym',

  policy_url: 'user/privacy-policy',
  faq: 'user/faq',
  condition_url: 'user/termcondition',
  about_us: 'user/aboutus',
  gym_info: 'user/getgym',
  contact_us: 'user/contactus',
  notification: 'user/getnotification',
  update_notification: 'user/notification/',
  notificationSetting: 'user/notification_setting',
  search_history: 'user/userfoodsearch',

  getAllPlans: 'user/get-all-plan',
  joinPlan: 'user/join-plan',
  allDiscount: 'user/alldiscount',
  sessionPrice: 'user/pricesesion',
  planHistory: 'user/plan-history',
  planHistoryDetails: 'user/history-plan-detail/',
  planDetails: 'user/plan-details/',
  planSetting: 'user/auto-renewal',
  planStatus: 'user/mempership',

  contest: 'user/contest',
  contestDetails: 'user/contest/',
  joinContest: 'user/joincontest',
  contest_winners: 'user/contest-winner',

  instructor_details: 'user/instructor',

  frenchise: 'user/frenchise',
  updateInfo: 'user/updateinfo',
  userBMI: 'user/bmi',
  setGoal: 'user/setGoal',

  locationUpdate: 'user/update-location',

  change_password: 'user/changePassword',

  logout: 'user/userlogout',

};
