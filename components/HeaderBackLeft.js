import React, { Component } from 'react'
import {
  Platform,
  TouchableOpacity,
  View,
  Image
} from 'react-native';
import { Icon } from 'react-native-elements';
import { textHeader } from './styles';
export default class HeaderBackLeft extends Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    return (

      <TouchableOpacity
        onPress={() => { this.props.navigationProps.goBack() }} style={textHeader.leftIcon}>
        {/* <Icon
            style={{ alignSelf: 'start' }}
            name={Platform.OS === 'ios' ? 'ios-search' : 'md-search'}
            type='ionicon'
            color='white'
            size={24}

          /> */}
        <Image source={require('./../assets/images/icons/arrow-right_3x.png')} style={{ width: 11, height: 20, alignSelf: 'flex-start' }} />
      </TouchableOpacity>

    )
  }
}
