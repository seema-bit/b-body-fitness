import React, { Component } from 'react'
import {
  Platform,
  TouchableOpacity,
  View,
  Image,
  StyleSheet
} from 'react-native';
import { Icon } from 'react-native-elements';
import Colors from '../constants/Colors';

export default class HeaderRight extends Component {

  _clickAction = (action) => {
    switch (action) {
      case "feed":
        this.props.navigationProps.push('AddFeed')
        break

      default:

        break
    }
  }

  renderIcon = (action) => {
    switch (action) {
      case "feed":
        return (
          <TouchableOpacity
            style={styles.top_layout}
            activeOpacity={1}
            onPress={() => { this.props.navigationProps.navigate('AddFeed') }}>
            <Image source={require('./../assets/images/icons/plus_3x.png')}
              style={{ width: 20, height: 20, }} />
          </TouchableOpacity>
        )

      case 'AddFeed':
        return (
          <TouchableOpacity
            style={styles.top_layout}
            activeOpacity={1}
            onPress={() => { this.props.navigationProps.goBack() }} >
            <Image source={require('./../assets/images/icons/tick_3x.png')}
              resizeMode="contain" style={{ width: 20, height: 20, }} />
          </TouchableOpacity>
        )


      case 'AddMeal':
        return (
          <TouchableOpacity
            style={styles.top_layout}
            activeOpacity={1}
            onPress={() => { this.props.navigationProps.goBack() }} >
            <Image source={require('./../assets/images/icons/tick_3x.png')}
              resizeMode="contain" style={{ width: 20, height: 20, }} />
          </TouchableOpacity>
        )


      case 'MealNutrition':
        return (
          <TouchableOpacity
            style={styles.top_layout}
            activeOpacity={1}
            onPress={() => { this.props.navigationProps.navigate('Meal') }} >
            <Image source={require('./../assets/images/icons/tick_3x.png')}
              resizeMode="contain" style={{ width: 20, height: 20, }} />
          </TouchableOpacity>
        )

      case 'PhotoProgress':
        return (
          <TouchableOpacity
            style={styles.top_layout}
            activeOpacity={1}
            onPress={() => { this.props.navigationProps.navigate('Meal') }} >
            <Image source={require('./../assets/images/icons/share_3x.png')}
              resizeMode="contain" style={{ width: 20, height: 20, tintColor: '#fff' }} />
          </TouchableOpacity>
        )

      case 'addToCart':
        console.log('this.props.shipping : ' + this.props.shipping)
        return (
          <TouchableOpacity
            style={styles.top_layout}
            activeOpacity={1}
            onPress={() => {
              this.props.navigationProps.navigate('Cart', {
                shipping: this.props.shipping
              })
            }} >
            <Image source={require('./../assets/images/icons/product.png')}
              resizeMode="contain" style={{ width: 20, height: 20, tintColor: '#fff' }} />
          </TouchableOpacity>
        )

      case 'MealTime':
        return (
          <TouchableOpacity
            style={styles.top_layout}
            activeOpacity={1}
            onPress={() => { this.props.navigationProps.goBack() }} >
            <Image source={require('./../assets/images/icons/clock.png')}
              resizeMode="contain" style={{ width: 20, height: 20, tintColor: Colors.white_color }} />
          </TouchableOpacity>
        )


      default:
        return (
          <TouchableOpacity
            style={styles.top_layout}
            activeOpacity={1}
          //onPress={() => { this.props.navigationProps.navigate('AddFeed') }}
          >
            <Image source={require('./../assets/images/icons/magnifying_glass_3x.png')}
              style={{ width: 20, height: 20, }} />
          </TouchableOpacity>
        )
    }
  }

  render() {
    return (
      <View style={{
        justifyContent: 'center',

      }}>
        {this.renderIcon(this.props.screen)}

      </View>
    )
  }
}

const styles = StyleSheet.create({
  top_layout: {
    width: 32,
    height: 32,
    paddingRight: 20,
    paddingLeft: 20
  }
})
