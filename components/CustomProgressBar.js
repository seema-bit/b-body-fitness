
import React from 'react';
import { View, StyleSheet, Image, ActivityIndicator } from 'react-native';
import Colors from '../constants/Colors';
import ProgressLoader from 'rn-progress-loader'

export default class CustomProgressBar extends React.Component {
  render() {
    return (

      <ProgressLoader
        visible={this.props.spinner}
        isModal={true} isHUD={true}
        hudColor={Colors.bg_color}
        color={Colors.black_color} />

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  }
});