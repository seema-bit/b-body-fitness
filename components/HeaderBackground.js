import React, { Component } from 'react'
import {
  Platform,
} from 'react-native';

import Colors from '../constants/Colors';
import { LinearGradient } from 'expo-linear-gradient';

export default class HeaderBackground extends Component {

  render() {
    return (
      <LinearGradient
        colors={[Colors.startColor, Colors.startColor]}
        style={{ flex: 1, }}
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
      />
    )
  }
}
