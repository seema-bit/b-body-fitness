import React, { Component } from 'react'
import {
  Platform,
  TouchableOpacity,
  View,
  Image,
  AsyncStorage,
  Text
} from 'react-native';
import { Icon } from 'react-native-elements';
import Utils from '../constants/Utils';
import {
  updateCount,
} from '../redux/action';
import { connect } from 'react-redux';
import { textHeader } from '../components/styles';
import ApiService from '../webservice/ApiService';
import Colors from '../constants/Colors';
import Constant from '../constants/Constant';

class HeaderFeedRight extends Component {
  static navigationOptions = {
    header: null,
  };

  componentDidMount = () => {

    console.log('HeaderFeedRight')
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json, allFeed: null })
        this._NotificationCount()
      })
      .catch(error => console.log('feed error!', error.response));
  }

  _NotificationCount = () => {
    var headers = {
      'X-Access-Token': this.state.userInfo.access_key,
    }

    ApiService.api.get(ApiService.nofification_count, { headers: headers })
      .then((response) => {

        if (response !== null && response.data != null) {
          if (response.data.status === Constant.SUCCESS) {

            this.props.updateCounts(response.data.data);



          } else if (response.data.status === Constant.WARNING) {
            if (response.data.session !== undefined && response.data.session !== null) {
              if (response.data.session === false) {
                Utils._signOutAsync(this.props.navigationProps)
              }
            }
          }
        }

      })
      .catch(function (error) {

        console.log("error _NotificationCount" + JSON.stringify(error.response));
      })

  }

  render() {
    return (
      <View style={{
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center'
      }}>
        <TouchableOpacity
          onPress={() => {
            this.props.navigationProps.navigate('NotificationScreen', {
              refresh: 'yes',
            })
          }} style={[textHeader.rightIcon, { paddingEnd: 10 }]}>
          <Image source={require('../assets/images/icons/notification_icon.png')}
            style={{ width: 22, height: 22, alignSelf: 'flex-end' }} resizeMode="contain" />
          {this.props.counts !== undefined && this.props.counts.notification_count !== undefined && this.props.counts.notification_count !== null && this.props.counts.notification_count !== 0 && (
            <View style={{
              position: 'absolute',
              right: 6,
              top: 6,
              width: 16,
              height: 16,
              backgroundColor: Colors.red_color,
              borderRadius: 16 / 2,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
              <Text style={{ color: Colors.white_color, fontSize: 10 }}>{this.props.counts.notification_count}</Text>
            </View>
          )}
        </TouchableOpacity>


        <TouchableOpacity
          style={textHeader.rightIcon}
          activeOpacity={1}
          onPress={() => {
            this.props.navigationProps.navigate('AddFeed', {
              title: Constant.add_feed
            })
          }}>
          <Image source={require('../assets/images/icons/plus_3x.png')}
            style={{ width: 20, height: 20, alignSelf: 'flex-end' }} />
        </TouchableOpacity>
      </View>
    )
  }
}

//state map
const mapStateToProps = state => {
  return {
    counts: state.counts,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    updateCounts: (item) => {
      console.log('updateCounts', item)
      dispatch(updateCount(item))
    },
  }

}
export default connect(mapStateToProps, mapDispatchToProps)(HeaderFeedRight)
