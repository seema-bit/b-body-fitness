import { StyleSheet } from 'react-native';
import Colors from '../constants/Colors';
import { Platform } from '@unimodules/core';

const textInput = StyleSheet.create({
  auth_textInput: {
    width: '100%',
    height: 54,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#fff',
    color: '#fff',
    marginTop: 10,
    textAlign: 'center',
    fontFamily: 'futura-medium',
    fontSize: 14,

  },
  gray_textInput: {
    width: '100%',
    height: 54,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: Colors.light_gray,
    //color: Colors.light_gray,
    marginTop: 10,
    textAlign: 'center',
    fontFamily: 'futura-book',
    fontWeight: 'bold',
    fontSize: 14,
  }
});

const buttonStyle = StyleSheet.create({
  auth_btn: {
    marginTop: 16,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#fff',
    borderRadius: 50,
    width: '100%',
    height: 54,
    justifyContent: 'center',

  },
  loginText: {
    color: Colors.primaryColor,
    textAlign: 'center',
    alignSelf: 'center',
    textAlign: 'center',
    fontFamily: 'futura-bold',
    fontSize: 14
  },

  login_btn: {
    marginTop: 16,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: Colors.shades_blue,
    borderRadius: 50,
    width: '100%',
    height: 54,
    justifyContent: 'center',
  },

  btnText: {
    color: '#fff',
    textAlign: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    alignSelf: 'center',
    textAlign: 'center',
    fontFamily: 'futura-bold',
    fontSize: 14
  },

  primaryBtn: {
    marginTop: 16,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: Colors.primaryColor,
    borderRadius: 50,
    width: '100%',
    height: 54,
    justifyContent: 'center',
  },

  lightGrayBtn: {
    marginTop: 16,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: Colors.light_gray,
    borderRadius: 50,
    width: '100%',
    height: 54,
    justifyContent: 'center',
    fontFamily: 'futura-bold',
    color: Colors.dark_gray
  },
  spinnerTextStyle: {
    color: '#fff',
    fontFamily: 'futura-medium',
    fontSize: 16,
  },
});

const textHeader = StyleSheet.create({
  header: {
    color: '#fff',
    fontFamily: 'futura-medium',
    fontSize: 20,
    textAlign: 'center',
    //width: '100%'
  },
  header_style: {
    height: 80
  },
  leftIcon: {
    justifyContent: 'center',
    paddingLeft: 20,
    width: 48,
    height: 48,
  },
  rightIcon: {
    justifyContent: 'center',
    paddingRight: 20,
    width: 48,
    height: 48,
  },
  icon_view: {
    width: 32, height: 32,
    position: 'absolute',
    top: Platform.OS === 'ios' ? 40 : 10,
    right: 10
  }
});


const text_color = StyleSheet.create({
  white_color: {
    color: '#fff'
  },
  gray_color: {
    color: Colors.light_gray
  },
  black_color: {
    color: '#000'
  }
});

const font_style = StyleSheet.create({
  font_heavy: {
    fontFamily: 'futura-heavy'
  },
  font_Book: {
    fontFamily: 'futura-book'
  },
  font_bold: {
    fontFamily: 'futura-bold'
  },
  font_medium: {
    fontFamily: 'futura-medium'
  },
  font_light: {
    fontFamily: 'futura-light'
  }
});

const modalSelector = StyleSheet.create({
  overlayStyle: {
    justifyContent: 'flex-end',
    width: '100%',
    marginBottom: -30,

  },
  optionContainerStyle: {
    backgroundColor: Colors.white_color
    // backgroundColor: 'transparent',
    // padding: 0,
    // width: '100%',
  },

  optionStyle: {
    // width: '100%',
    // padding: 12,
    // backgroundColor: Colors.white_color,
    // borderRadius: 5,
    // borderBottomColor: 'transparent',
    // marginTop: 10
  },
  optionTextStyle: {
    fontSize: 20,
  },

  cancelStyle: {
    padding: 12,
    backgroundColor: Colors.white_color,
    borderRadius: 5,
    //marginRight: 10, marginLeft: 10,
    marginTop: 5
  },
  cancelTextStyle: {
    fontSize: 20,
  }
})

export { textInput, buttonStyle, text_color, font_style, textHeader, modalSelector }


