import React, { Component } from 'react'
import {
  Platform,
  TouchableOpacity,
  View,
  Image
} from 'react-native';
import { Icon } from 'react-native-elements';
export default class HeaderProfileRight extends Component {
  static navigationOptions = {
    header: null,
  };

  renderIcon = (action) => {
    switch (action) {
      case "setting":
        return (
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => { this.props.navigationProps.navigate('NotificationSetting') }}>
            <Image source={require('./../assets/images/icons/settings_3x.png')} style={{ width: 20, height: 20, }} />
          </TouchableOpacity>
        )



      default:
        return (
          <TouchableOpacity
            activeOpacity={1}
          //onPress={() => { this.props.navigationProps.navigate('AddFeed') }}
          >
            <Image source={require('./../assets/images/icons/settings_3x.png')}
              style={{ width: 20, height: 20, }} />
          </TouchableOpacity>
        )
    }
  }

  render() {
    return (
      <View style={{
        justifyContent: 'center',
        marginRight: 24,
      }}>
        {this.renderIcon(this.props.screen)}

      </View>
    )
  }
}
