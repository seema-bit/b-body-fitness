import React, { Component } from 'react'
import {
  Platform,
  TouchableOpacity,
  View,
  Image
} from 'react-native';
import { Icon } from 'react-native-elements';
import { textHeader } from './styles';
export default class HeaderLeft extends Component {
  toggleDrawer = () => {

    this.props.navigationProps.toggleDrawer();

  }

  render() {
    return (

      <TouchableOpacity onPress={this.toggleDrawer.bind(this)}
        activeOpacity={1}
        style={textHeader.leftIcon}>
        {/* <Icon
            style={{ alignSelf: 'start' }}
            name={Platform.OS === 'ios' ? 'ios-menu' : 'md-menu'}
            type='ionicon'
            color='white'
            size={24}

          /> */}
        <Image source={require('./../assets/images/icons/menu_3x.png')} style={{ width: 26, height: 20, alignSelf: 'flex-start' }} />
      </TouchableOpacity>

    )
  }
}
