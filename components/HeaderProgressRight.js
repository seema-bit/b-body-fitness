import React, { Component } from 'react'
import {
  Platform,
  TouchableOpacity,
  View,
  Image
} from 'react-native';
import { Icon } from 'react-native-elements';

export default class HeaderProgressRight extends Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <View style={{
        justifyContent: 'center',
        marginRight: 24,
        flexDirection: 'row'
      }}>
        <TouchableOpacity
          onPress={() => { this.props.navigationProps.navigate('PhotoProgress') }}>
          <Image source={require('./../assets/images/icons/export_file_3x.png')}
            style={{ width: 20, height: 20, }} resizeMode="contain" />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => { this.props.navigationProps.navigate('PhotoProgress') }}
          style={{ marginLeft: 16, justifyContent: 'center' }}>
          <Image source={require('./../assets/images/icons/plus_3x.png')}
            style={{ width: 20, height: 20, }} resizeMode="contain" />
        </TouchableOpacity>
      </View>
    )
  }
}
