import React from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { Camera, Permissions } from 'expo';
import Toolbar from './components/Toolbar';
import Gallery from './components/Gallery';

const { width: winWidth, height: winHeight } = Dimensions.get('window');

export default class App extends React.Component {
  camera = null;

  state = {
    captures: [],
    // setting flash to be turned off by default
    flashMode: Camera.Constants.FlashMode.off,
    capturing: null,
    // start the back camera by default
    cameraType: Camera.Constants.Type.back,
    hasCameraPermission: null,
  };

  setFlashMode = (flashMode) => this.setState({ flashMode });
  setCameraType = (cameraType) => this.setState({ cameraType });
  handleCaptureIn = () => this.setState({ capturing: true });

  handleCaptureOut = () => {
    if (this.state.capturing)
      this.camera.stopRecording();
  };

  handleShortCapture = async () => {
    console.log('handleShortCapture')
    const photoData = await this.camera.takePictureAsync();
    console.log('this.state.captures', this.state.captures)
    this.setState({ capturing: false, captures: [photoData, ...this.state.captures] })
  };

  // handleLongCapture = async () => {
  //   const videoData = await this.camera.recordAsync();
  //   this.setState({ capturing: false, captures: [videoData, ...this.state.captures] });
  // };


  async componentDidMount() {
    const camera = await Permissions.askAsync(Permissions.CAMERA);
    const hasCameraPermission = (camera.status === 'granted');

    this.setState({ hasCameraPermission });
  };

  render() {
    const { hasCameraPermission, flashMode, cameraType, capturing, captures } = this.state;

    if (hasCameraPermission === null) {
      return <View />;
    } else if (hasCameraPermission === false) {
      return <Text>Access to camera has been denied.</Text>;
    }

    return (
      <View style={{ flex: 1 }}>
        <View>
          <Camera
            type={cameraType}
            flashMode={flashMode}
            style={styles.preview}
            ref={camera => this.camera = camera}
          />
        </View>

        {captures.length > 0 && <Gallery captures={captures} />}
        <Toolbar
          capturing={capturing}
          flashMode={flashMode}
          cameraType={cameraType}
          setFlashMode={this.setFlashMode}
          setCameraType={this.setCameraType}
          onCaptureIn={this.handleCaptureIn}
          onCaptureOut={this.handleCaptureOut}
          //onLongCapture={this.handleLongCapture}
          onShortCapture={this.handleShortCapture}
        />
      </View>
    );
  };
};

const styles = StyleSheet.create({
  preview: {
    height: winHeight,
    width: winWidth,
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
  },
})