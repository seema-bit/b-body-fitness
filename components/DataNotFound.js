
import React from 'react';
import { View, StyleSheet, Image, ActivityIndicator, Text } from 'react-native';
import Colors from '../constants/Colors';
import { font_style } from './styles';

export default class DataNotFound extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        {/* <Image source={require('../assets/images/no_data.png')} resizeMode='contain' style={{width:'60%', height:200,marginBottom:10}}/> */}
        <Text style={[styles.text_style, font_style.font_medium]}>{this.props.msg ? this.props.msg : "Data not found"}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.bg_color,
  },

  text_style: {
    //fontSize: 16,
    //color: Colors.dark_gray
  }
});