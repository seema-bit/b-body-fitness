import * as Action from './action';

const initialState = {

    feed_list: [],
    feed_details: null,
    product_list: [],
    cart_list: [],
    cart_count: 0,
    notification_list: [],
    notification_count: 0,
    fetchIntake: null,

}

const listReducer = (state = initialState, action) => {
    switch (action.type) {
        case Action.FetchFeedList:
            return {
                ...state,
                feed_list: action.payload
            };
        case Action.FetchFeedDetails:
            return {
                ...state,
                feed_details: action.payload
            };
        case Action.FetchProductList:
            return {
                ...state,
                product_list: action.payload.data,
                shipping: action.payload.shipping
            };

        case Action.CartList:
            return {
                ...state,
                cart_list: action.payload.data,
                cart_count: action.payload.count
            };
        case Action.NotificationList:
            return {
                ...state,
                notification_list: action.payload.data,
                notification_count: action.payload.count
            };

        case Action.FetchIntake:

            return {
                ...state,
                fetchIntake: action.payload
            };
        default:
            return state;
    }
}

export default listReducer;