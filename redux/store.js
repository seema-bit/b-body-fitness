import { createStore, combineReducers } from 'redux';
import countReducer from './reducer';
import listReducer from './list_reducer';

const appReducer = combineReducers({
    counts: countReducer,
    lists: listReducer,
});

const configureStore = () => {
    return createStore(appReducer);
}

export default configureStore;