export const UPDATE_COUNT = "notification_count";

export const FetchFeedList = "fetch_feed_list";
export const UpdateFeedList = "update_feed_list";
export const FetchFeedDetails = "fetch_feed_details";
export const FetchProductList = "fetch_product_list";
export const CartList = "cart_list";
export const NotificationList = "notification_list";
export const FetchIntake = "fetchIntake"

export const updateCount = (count) => ({
    type: UPDATE_COUNT,
    payload: count
});

export const updateFeedList = (data) => ({
    type: UpdateFeedList,
    payload: data
});

export const fetchFeedList = (data) => ({
    type: FetchFeedList,
    payload: data
});

export const fetchFeedDetails = (data) => ({
    type: FetchFeedDetails,
    payload: data
});

export const fetchProductList = (data) => ({
    type: FetchProductList,
    payload: data
});

export const cartList = (data) => ({
    type: CartList,
    payload: data
});

export const notificationList = (data) => ({
    type: NotificationList,
    payload: data
});

export const fetchIntake = (data) => ({
    type: FetchIntake,
    payload: data
});


