import * as Action from './action';

const initialState = {
    notification_count: 0,

}

const countReducer = (state = initialState, action) => {
    switch (action.type) {
        case Action.UPDATE_COUNT:
            return {
                ...state,
                notification_count: action.payload,
            }

        default:
            return state;
    }
}

export default countReducer;