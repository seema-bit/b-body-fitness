import React from 'react';
import { Platform, Image } from 'react-native';

import { createStackNavigator } from "react-navigation-stack";
import { createMaterialTopTabNavigator, createBottomTabNavigator } from "react-navigation-tabs";

import ClassScreen from '../screens/ClassScreen';
import ContestScreen from '../screens/ContestScreen';
import MealScreen from '../screens/MealScreen';
import WorkoutScreen from '../screens/WorkoutScreen';
import Constant from '../constants/Constant';
import Colors from '../constants/Colors';
import EditProfileScreen from '../screens/EditProfileScreen';
import ProductScreen from '../screens/ProductScreen';
import { font_style } from '../components/styles';
import CartScreen from '../screens/CartScreen';
import AddFeedScreen from '../screens/AddFeedScreen';
import AddMealScreen from '../screens/AddMealScreen';
import MyMealScreen from '../screens/MyMealScreen';
import DietPlanScreen from '../screens/DietPlanScreen';
import MealNutritionDetailsScreen from '../screens/MealNutritionDetailsScreen';
import CommentScreen from '../screens/CommentScreen';
import FeedScreen from '../screens/FeedScreen';
import ProductDetailsScreen from '../screens/ProductDetailsScreen';
import AddWaterScreen from '../screens/AddWaterScreen';
import WorkoutListScreen from '../screens/WorkoutListScreen';
import AddWorkoutScreen from '../screens/AddWorkoutScreen';
import MyClassessScreen from '../screens/MyClassessScreen';
import MyOrdersScreen from '../screens/MyOrdersScreen';
import NutritionScreen from '../screens/NutritionScreen';
import NutritionalGuidelineScreen from '../screens/NutritionalGuidelineScreen';
import ProgressScreen from '../screens/ProgressScreen';
import PhotoProgressScreen from '../screens/PhotoProgressScreen';
import AddProgressScreen from '../screens/AddProgressScreen';
import InstructorDetailsScreen from '../screens/InstructorDetailsScreen';
import MealsTimeScreen from '../screens/MealsTimeScreen';
import ClassDetails from '../screens/ClassDetails';
import CreateWorkout from '../screens/CreateWorkout';
import FeedDetails from '../screens/FeedDetails';

import SwipeNavigator from 'react-native-swipe-navigation'
import NotificationScreen from '../screens/NotificationScreen';
import TeamWorkoutSingle from '../screens/TeamWorkoutSingle';
import Utils from '../constants/Utils';



// const Navigator = SwipeNavigator({
//   Feed: {
//     screen: FeedScreen,
//     navigationOptions: {
//       headerBackTitle: null,
//       header: null
//     },
//     right: 'NotificationScreen',
//   },

//   NotificationScreen: {
//     screen: NotificationScreen,

//   },
// }, { headerLayoutPreset: 'center', })



const FeedStack1 = createStackNavigator({
  screen: FeedScreen,
}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

FeedStack1.navigationOptions = {
  //tabBarLabel: [Constant.feed]
};

const NotificationStack = createStackNavigator({
  screen: NotificationScreen,
}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

NotificationStack.navigationOptions = {
  //tabBarLabel: [Constant.feed]
  drawerLockMode: 'locked-open',
  disableGestures: true
};

const SwipeableNavigator = createMaterialTopTabNavigator({
  FeedScreen: FeedStack1,
  NotificationScreen: NotificationStack
},
  {
    swipeEnabled: true,
    tabBarOptions: {
      style: { display: 'none' }
    },
    initialRouteName: 'FeedScreen'
  },
  {
    defaultNavigationOptions: {
      headerMode: "none",
      header: null,

    }
  }, {
  navigationOptions: { header: null, headerMode: "none", } // hides header in flowOne
});

// const AnotherStack = createStackNavigator({

// }, {
//   headerLayoutPreset: 'center',
// })

const FeedStack = createStackNavigator({
  Feed: {
    screen: SwipeableNavigator,
    navigationOptions: {
      header: null // Will hide header for HomePage
    },
  },
  MyClass: MyClassessScreen,
  MyOrder: MyOrdersScreen,
  Nutrition: NutritionScreen,
  NutritionGuideline: NutritionalGuidelineScreen,
  Progress: ProgressScreen,
  PhotoProgress: PhotoProgressScreen,
  AddProgress: AddProgressScreen
}, {
  headerLayoutPreset: 'center',
  initialRouteName: 'Feed',
  transitionConfig: Utils.transitionConfig
  //headerMode: 'none'
},
);

FeedStack.navigationOptions = {
  header: null,
  tabBarLabel: Constant.feed,
  tabBarIcon: ({ focused }) => (

    <Image source={focused ? require('../assets/images/icons/feed_select.png') : require('../assets/images/icons/feed.png')} style={{ width: 24, height: 24 }} resizeMode="contain"></Image>
  ),
};

const MealStack = createStackNavigator({
  Meal: MealScreen,
  AddMeal: AddMealScreen,
  MyMeal: MyMealScreen,
  DietMeal: DietPlanScreen,
  MealNutrition: MealNutritionDetailsScreen,
  Water: AddWaterScreen,
  MealsTime: MealsTimeScreen,
}, {
  headerLayoutPreset: 'center',
  initialRouteName: 'Meal',
  transitionConfig: Utils.transitionConfig

});

MealStack.navigationOptions = {
  tabBarLabel: Constant.meal,
  tabBarIcon: ({ focused }) => (
    <Image source={focused ? require('../assets/images/icons/nutrition_3x_new.png') : require('../assets/images/icons/nutrition_3x_new.png')} style={[{ width: 26, height: 26 }, focused ? { tintColor: Colors.icon_color } : { tintColor: Colors.bottom_bar }]} resizeMode="contain"></Image>
  ),
};

const WorkoutStack = createStackNavigator({
  Workout: WorkoutScreen,
  WorkoutList: WorkoutListScreen,
  AddWorkout: AddWorkoutScreen,
  Instructor: InstructorDetailsScreen,
  CreateWorkout: CreateWorkout,
  TeamWorkoutSingle: TeamWorkoutSingle,
  transitionConfig: Utils.transitionConfig

}, {
  headerLayoutPreset: 'center',
  initialRouteName: 'Workout',
  transitionConfig: Utils.transitionConfig
});

WorkoutStack.navigationOptions = {
  tabBarLabel: Constant.workout,
  tabBarIcon: ({ focused }) => (
    <Image source={focused ? require('../assets/images/icons/workout_select.png') : require('../assets/images/icons/workout.png')} style={{ width: 24, height: 24 }} resizeMode="contain"></Image>
  ),
};

const ClassStack = createStackNavigator({
  Class: ClassScreen,
  ClassDetails: ClassDetails
}, {
  headerLayoutPreset: 'center',
  initialRouteName: 'Class',
  transitionConfig: Utils.transitionConfig
});

ClassStack.navigationOptions = {
  tabBarLabel: Constant.class,
  tabBarIcon: ({ focused }) => (
    <Image source={focused ? require('../assets/images/icons/scheudle_select.png') : require('../assets/images/icons/scheudle.png')} style={{ width: 24, height: 24 }} resizeMode="contain"></Image>
  ),
};

const ProductStack = createStackNavigator({
  Product: ProductScreen,
  Cart: CartScreen,
  ProductDetails: ProductDetailsScreen
}, {
  headerLayoutPreset: 'center',
  initialRouteName: 'Product',
  transitionConfig: Utils.transitionConfig
});
ProductStack.navigationOptions = {
  tabBarLabel: Constant.products,
  tabBarIcon: ({ focused }) => (
    <Image source={focused ? require('../assets/images/icons/product_select.png') : require('../assets/images/icons/product.png')} style={{ width: 24, height: 24 }} resizeMode="contain"></Image>
  ),
};

// const EmptyStack = createStackNavigator({
//   screen: ProductScreen,
// });

// EmptyStack.navigationOptions = {
//   tabBarLabel: 'a',

// };


export default createBottomTabNavigator({
  FeedStack,
  MealStack,
  WorkoutStack,
  //ClassStack,
  ProductStack,
  //EmptyStack

},
  {
    /* The header config from HomeScreen is now here */
    defaultNavigationOptions: ({ navigation }) => ({

      headerStyle: {
        backgroundColor: '#f4511e',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      headerMode: "none",
      //tabBarOnPress: tabBarOnPress(navigation),

    }),
    tabBarOptions: {
      activeTintColor: Colors.icon_color,
      inactiveTintColor: Colors.bottom_bar,
      labelStyle: {
        marginBottom: 14,
        fontSize: 12,
        fontFamily: 'futura-book',
      },
      style: {
        //----------add this line------------------------//
        height: 70,
        justifyContent: 'center'
      },
    },
  });

//not sure about the syntax here
const tabBarOnPress = (navigation) => config => {
  //do something with value
  const { routeName } = navigation.state;
  navigation.navigate("Meal")

  alert(routeName);

}
