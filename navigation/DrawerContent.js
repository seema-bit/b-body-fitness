import React, { Component } from 'react';
import { NavigationActions, } from 'react-navigation';
import { ScrollView, TouchableOpacity, View, Image, AsyncStorage, Share, ImageBackground, Dimensions, Alert, Keyboard } from 'react-native';
import Colors from '../constants/Colors';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base'
import Constant from '../constants/Constant';
import ApiService from '../webservice/ApiService';
import Utils from '../constants/Utils';
import Modal from 'react-native-modalbox';
import { font_style } from '../components/styles';

class DrawerContent extends Component {
  static navigationOptions = {
    header: null
  };
  state = {
    channels: [

      { screen: Constant.feed_sidemenu, title: Constant.feed_sidemenu, select_img: require('../assets/images/icons/feed_select_menu_3x.png'), image_name: require('../assets/images/icons/feed_menu_3x.png') },
      { screen: Constant.about_us, title: Constant.about_us, select_img: require('../assets/images/icons/about_us_select_3x.png'), image_name: require('../assets/images/icons/about_us_3x.png') },
      { screen: Constant.plans, title: Constant.membership_plan, select_img: require('../assets/images/icons/plans_select_3x.png'), image_name: require('../assets/images/icons/plans_3x.png') },
      //{ screen: Constant.my_classess, title: Constant.my_classess, select_img: require('../assets/images/icons/my_class_select_3x.png'), image_name: require('../assets/images/icons/my_class_3x.png') },
      { screen: Constant.nutrition, title: Constant.blueprint, select_img: require('../assets/images/icons/nutrition_select_3x.png'), image_name: require('../assets/images/icons/nutrition_3x.png') },
      { screen: Constant.progress, title: Constant.progress, select_img: require('../assets/images/icons/progress_select_3x.png'), image_name: require('../assets/images/icons/progress_3x.png') },
      { screen: Constant.my_order, title: Constant.my_order, select_img: require('../assets/images/icons/my_order_select_3x.png'), image_name: require('../assets/images/icons/my_order_3x.png') },
      { screen: Constant.contest, title: Constant.contest, select_img: require('../assets/images/icons/contest_select_3x.png'), image_name: require('../assets/images/icons/contest_3x.png') },
      { screen: Constant.gym_near_me, title: Constant.gym_near_me, select_img: require('../assets/images/icons/gym_near_select_3x.png'), image_name: require('../assets/images/icons/gym_near_3x.png') },
      { screen: Constant.franchise, title: Constant.franchise, select_img: require('../assets/images/icons/franchise_select.png'), image_name: require('../assets/images/icons/franchise.png') },
      { screen: Constant.contact_us, title: Constant.contact_us, select_img: require('../assets/images/icons/contact_select_3x.png'), image_name: require('../assets/images/icons/contact_us_3x.png') },
      { screen: Constant.faq, title: Constant.faq, select_img: require('../assets/images/icons/faq_select_3x.png'), image_name: require('../assets/images/icons/faq_3x.png') },
      { screen: Constant.share_app, title: Constant.share_app, select_img: require('../assets/images/icons/share_app_select_3x.png'), image_name: require('../assets/images/icons/share_app_3x.png') },
      { screen: Constant.edit_profile, title: Constant.edit_profile, select_img: require('../assets/images/icons/edit_profile_select_3x.png'), image_name: require('../assets/images/icons/edit_profile_3x.png') },
      { screen: Constant.settings, title: Constant.settings, select_img: require('../assets/images/icons/setting_select_3x.png'), image_name: require('../assets/images/icons/setting_3x.png') },
      { screen: Constant.privacy_policy, title: Constant.privacy_policy, select_img: require('../assets/images/icons/privacy_select_3x.png'), image_name: require('../assets/images/icons/privacy_3x.png') },
      { screen: Constant.term_condition, title: Constant.term_condition, select_img: require('../assets/images/icons/terms_select_3x.png'), image_name: require('../assets/images/icons/terms_3x.png') },
      { screen: Constant.change_password, title: Constant.change_password, select_img: require('../assets/images/icons/password_select_3x.png'), image_name: require('../assets/images/icons/password_3x.png') },
      { screen: Constant.logout, title: Constant.logout, select_img: require('../assets/images/icons/logout_select_3x.png'), image_name: require('../assets/images/icons/logout_3x.png') },



      // { screen: Constant.meal, title: Constant.meal, select_img: require('../assets/images/icons/meal.png'), image_name: require('../assets/images/icons/meal.png') },
      // { screen: Constant.workout, title: Constant.workout, select_img: require('../assets/images/icons/workout.png'), image_name: require('../assets/images/icons/workout.png') },
      // { screen: Constant.schedule, title: Constant.schedule, select_img: require('../assets/images/icons/scheudle.png'), image_name: require('../assets/images/icons/scheudle.png') },
      // { screen: Constant.notification, title: Constant.notification, select_img: require('../assets/images/icons/notification_select_3x.png'), image_name: require('../assets/images/icons/notification_3x.png') },
      //{ screen: Constant.delete_account, title: Constant.delete_account, select_img: require('../assets/images/icons/delete_acc_select_3x.png'), image_name: require('../assets/images/icons/delete_account_3x.png') },
      //{ screen: Constant.bmr_calculation, title: Constant.bmr_calculation, select_img: require('../assets/images/icons/about_us_select_3x.png'), image_name: require('../assets/images/icons/about_us_3x.png') },


    ],
    selectedRoute: Constant.feed_sidemenu,
    userInfo: {}
  };

  componentDidMount = () => {
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({
          userInfo: json,
        })
        //this.setState({ selectedRoute: Constant.feed_sidemenu })
      })
      .catch(error => console.log('error : ' + error));
  }



  componentWillReceiveProps = () => {
    Keyboard.dismiss()
    this._UpdateProfile()
  }

  _Logout = () => {
    Utils._signOutAsync(this.props.navigation)
  }

  navigateToScreen = route => () => {
    console.log('route', route)
    this.setState({ selectedRoute: route === Constant.share_app ? '' : route })
    this.props.navigation.closeDrawer()

    switch (route) {
      case Constant.logout:
        Alert.alert(
          'Logout',
          'Are you sure you want to logout?',
          [
            {
              text: 'No',
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel',
            },
            { text: 'Yes', onPress: () => this._Logout() },
          ],
          { cancelable: false }
        );

        //this.refs.logout_modal.open();
        break;

      case Constant.feed_sidemenu:
        this.props.navigation.navigate('Feed', {
          refresh: 'yes'
        })
        break;

      case Constant.my_classess:
        this.props.navigation.navigate('MyClass', {
          refresh: 'yes'
        })
        break;

      case Constant.about_us:
        this.props.navigation.navigate('AboutUs', {
          refresh: 'yes'
        })
        break;

      case Constant.privacy_policy:
        this.props.navigation.navigate('Policy', {
          refresh: 'yes'
        })
        break;

      case Constant.term_condition:
        this.props.navigation.navigate('Terms', {
          refresh: 'yes'
        })
        break;

      case Constant.my_order:
        this.props.navigation.navigate('MyOrder', {
          refresh: 'yes'
        })
        break;


      case Constant.nutrition:
        this.props.navigation.navigate('Meal', { tab: 1 })
        break;

      case Constant.progress:
        this.props.navigation.navigate('Progress')
        break;


      case Constant.share_app:
        this.setState({ selectedRoute: Constant.feed_sidemenu })
        this.props.navigation.navigate('Feed')
        this._ShareApp();
        break

      // case Constant.change_password:
      //   this.setState({ selectedRoute: Constant.feed_sidemenu })
      //   this.props.navigation.navigate('ChangePass')
      //   break

      // case Constant.plans:
      //   this.setState({ selectedRoute: Constant.feed_sidemenu })
      //   this.props.navigation.navigate('GymPlans')
      //   break

      default:
        const navigate = NavigationActions.navigate({
          routeName: route,
        });

        this.props.navigation.dispatch(navigate);
        break;
    }

  };


  _ShareApp = async () => {
    try {
      const result = await Share.share({
        message:
          'B-Body App',
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType

        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed

      }
    } catch (error) {
      Utils.toastShow(error.message);
    }

  }
  _UpdateProfile = () => {
    //this.setState({ userInfo: {} })
    AsyncStorage.getItem(Constant.USERINFO)
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({ userInfo: json })
      })
      .catch(error => console.log('error : ' + error));
  }

  renderChannelButtons() {
    return this.state.channels.map(({ screen, title, select_img, image_name }) => (

      <TouchableOpacity
        key={screen + title}
        onPress={this.navigateToScreen(screen)}
        activeOpacity={1}
      //onFocus={this._UpdateProfile()}
      >

        <Card transparent style={{ height: 46 }}>

          {
            screen === this.state.selectedRoute ?
              (
                <View style={{ borderLeftWidth: 4, borderLeftColor: '#fff' }}>
                  <CardItem style={{ backgroundColor: 'transparent' }} transparent>
                    <Image source={select_img} style={{ width: 42, height: 42 }} />
                    <Text style={[{ color: '#fff', marginStart: 8, }, styles.font_applied]}>{title}</Text>
                  </CardItem>
                </View>)
              :
              (<View style={{ backgroundColor: 'transparent' }} >
                <CardItem style={{ backgroundColor: 'transparent', marginStart: 4 }} transparent>
                  <Image source={image_name} style={{ width: 42, height: 42 }} />
                  <Text style={[{ color: '#fff', marginStart: 8 }, styles.font_applied]}>{title}</Text>
                </CardItem>
              </View>
              )
          }

        </Card>

      </TouchableOpacity>
    ));
  }


  renderLogoutModal = () => {
    return (
      <Modal style={[{ width: 300, height: 150 }]} position={"center"} ref={"logout_modal"} backdropTransitionOutTiming={0} >

        <View >
          <Text style={[{ marginStart: 24, marginTop: 24, fontSize: 16, marginBottom: 16, color: Colors.black_color }, font_style.font_medium]}>Logout</Text>

          <Text style={[{ marginStart: 24, marginBottom: 16, fontSize: 14, color: Colors.dark_gray }, font_style.font_medium]}>Are you sure you want to logout?</Text>

          <View style={styles.btn_view}>
            <TouchableOpacity
              style={{ marginEnd: 24 }}
              onPress={() => this.refs.logout_modal.close()}>
              <Text style={{ color: Colors.primaryColor }}>No</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this._Logout()}>
              <Text style={{ color: Colors.primaryColor }}>Yes</Text>
            </TouchableOpacity>
          </View>

        </View>

      </Modal>
    )
  }

  render() {
    return (
      // <ImageBackground source={require('../assets/images/group_bg_3x.png')} style={{
      //   width: 350, height: '100%',
      //   shadowOffset: { height: 0, width: 0 },
      //   shadowOpacity: 0,
      //   elevation: 0,
      //   marginStart: -6,
      //   marginBottom: -10,
      // }}>

      <View style={[styles.containerStyle]}>
        {this.renderLogoutModal()}
        {/* <Image style={{ flex: 1, position: 'absolute', top: 0, height: Dimensions.get('window').height, width: Dimensions.get('window').width }} source={require('./menuOverlay.png')} /> */}
        <View style={{ flex: 1, backgroundColor: Colors.primaryColor, position: 'absolute', top: 0, bottom: 0, height: '100%', width: 300, opacity: 0.8 }} />

        <View style={{ paddingTop: 32, marginStart: 4 }}>
          <Card style={{ flex: 0, backgroundColor: 'transparent' }} transparent>
            {/* <CardItem style={{ backgroundColor: 'transparent' }} transparent>
              <Text style={[{ color: '#fff', fontSize: 20 }, styles.font_applied]}>Setting</Text>
            </CardItem> */}
            <CardItem style={{ backgroundColor: 'transparent' }} transparent>
              <Left>
                {this.state.userInfo.image !== null && this.state.userInfo.image
                  ?
                  (<ImageBackground source={require('../assets/images/admin.jpg')}
                    style={styles.img_view}
                    imageStyle={styles.imageStyle}>
                    <Image source={{ uri: this.state.userInfo.image }} style={styles.img_view} />
                  </ImageBackground>
                  )
                  :
                  (<Image source={require('../assets/images/admin.jpg')} style={styles.img_view} />)
                }

                <Body>
                  {(this.state.userInfo.name !== null && this.state.userInfo.name !== '') && <Text style={[{ color: '#fff' }, styles.font_applied]}> {this.state.userInfo.name}</Text>}
                  {(this.state.userInfo.username !== null && this.state.userInfo.username !== '') && <Text note style={[{ color: '#fff', padding: 4 }, styles.font_applied]}>{this.state.userInfo.username}</Text>}

                </Body>
              </Left>
            </CardItem>
          </Card>
        </View>

        <ScrollView showsVerticalScrollIndicator={false} >
          <View style={{ paddingBottom: 20 }}>
            {this.renderChannelButtons()}
          </View>

        </ScrollView>
      </View>

    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    //backgroundColor: Colors.primaryColor,
  },
  font_applied: {
    fontFamily: 'futura-medium',
    fontWeight: 'bold',

  },
  img_view: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2
  },

  imageStyle: { borderRadius: 50 / 2 },
  btn_view: {
    flexDirection: 'row',
    height: 46, alignItems: 'flex-end',
    justifyContent: 'flex-end',
    marginEnd: 24
  },
};

export default DrawerContent;
