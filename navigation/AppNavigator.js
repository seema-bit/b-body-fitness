import React from 'react';
import { Dimensions } from 'react-native'
import { createAppContainer, createSwitchNavigator, } from 'react-navigation';

import { createStackNavigator } from "react-navigation-stack";
import { createMaterialTopTabNavigator, createBottomTabNavigator } from "react-navigation-tabs";

import { createDrawerNavigator } from "react-navigation-drawer";

import MainTabNavigator from './MainTabNavigator';
import SplashScreen from '../screens/SplashScreen';
import LoginScreen from '../screens/LoginScreen';
import SignUpScreen from '../screens/SignUpScreen';
import ForgotPasswordScreen from '../screens/ForgotPasswordScreen';
import ResetPasswordScreen from '../screens/ResetPasswordScreen';
import ClassScreen from '../screens/ClassScreen';
import ContestScreen from '../screens/ContestScreen';
import EditProfileScreen from '../screens/EditProfileScreen';
import HeaderLeft from '../components/HeaderLeft';
import Colors from '../constants/Colors';
import HeaderRight from '../components/HeaderRight';
import Constant from '../constants/Constant';
import NotificationScreen from '../screens/NotificationScreen'
import { Icon } from 'react-native-elements'
import DrawerContent from './DrawerContent'
import AboutUsScreen from '../screens/AboutUsScreen';
import ProgressScreen from '../screens/ProgressScreen';
import PhotoProgressScreen from '../screens/PhotoProgressScreen';
import MembershipPlanScreen from '../screens/MembershipPlanScreen';
import ProductScreen from '../screens/ProductScreen';
import NearByGymScreen from '../screens/NearByGymScreen';
import ReminderScreen from '../screens/ReminderScreen';
import MyOrdersScreen from '../screens/MyOrdersScreen'
import MyClassessScreen from '../screens/MyClassessScreen';
import OrdersHistoryScreen from '../screens/OrdersHistoryScreen';
import NutritionScreen from '../screens/NutritionScreen';
import PolicyScreen from '../screens/PolicyScreen';
import ConditionScreen from '../screens/ConditionScreen';
import AddProgressScreen from '../screens/AddProgressScreen';
import FAQScreen from "../screens/FAQScreen";
import DetailsScreen from '../screens/PolicyDetailsScreen';
import PolicyDetailsScreen from '../screens/PolicyDetailsScreen';
import FAQAnswersScreen from '../screens/FAQAnswersScreen';
import ContactScreen from '../screens/ContactScreen'
import NotificationSettingScreen from '../screens/NotificationSettingScreen'
import BMRCalculationScreen from '../screens/BMRCalculationScreen';
import ChangePasswordScreen from '../screens/ChangePasswordScreen';
import ContestDetailsScreen from '../screens/ContestDetailsScreen';
import ContestRegisterScreen from '../screens/ContestRegisterScreen';
import NutritionalGuidelineScreen from '../screens/NutritionalGuidelineScreen';
import FrenchiseScreen from '../screens/FrenchiseScreen';
import ConditionSecondScreen from '../screens/ConditionSecondScreen';
import CommentScreen from '../screens/CommentScreen';
import ContestWinnersScreen from '../screens/ContestWinnersScreen';
import Discounts from '../screens/Discounts';
import ContactScreenSecond from '../screens/ContactScreenSecond';
import PlanHistory from '../screens/PlanHistory';
import PlanHistoryDetails from '../screens/PlanHistoryDetails';
import PlanDetail from '../screens/PlanDetail';

import SwipeNavigator from 'react-native-swipe-navigation'
import EnqueryScreen from '../screens/EnqueryScreen';
import AgreeTerms from '../screens/AgreeTerms';
import AddFeedScreen from '../screens/AddFeedScreen';
import FeedDetails from '../screens/FeedDetails';
import FeedScreen from '../screens/FeedScreen';
import Utils from '../constants/Utils';
import AddCard from '../screens/AddCard';

//splash
const SplashStack = createStackNavigator({
  Splash: SplashScreen,
}, {
  transitionConfig: Utils.transitionConfig
});

SplashStack.navigationOptions = {
  tabBarLabel: 'Splash'
};

//login
const AuthStack = createStackNavigator({
  Login: {
    screen: LoginScreen,
    navigationOptions: {
      headerBackTitle: null,
    }
  },

  SignUp: {
    screen: SignUpScreen,
    navigationOptions: {
      headerBackTitle: null,
    }
  },

  ForgotPassword: {
    screen: ForgotPasswordScreen,
    navigationOptions: {
      headerBackTitle: null,
    }
  },
  ResetPassword: {
    screen: ResetPasswordScreen,
    navigationOptions: {
      headerBackTitle: null,
    }
  },

  CompleteSignUp: {
    screen: AgreeTerms,
    navigationOptions: {
      headerBackTitle: null,
    }
  },

},
  {
    initialRouteName: 'Login',
    transitionConfig: Utils.transitionConfig
  });

const BMRCalculationStack = createStackNavigator({
  BMRCalculation: BMRCalculationScreen,

}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

BMRCalculationStack.navigationOptions = {
  tabBarLabel: [Constant.bmr_calculation]
};

// const AddCardStack = createStackNavigator({
//   AddCard: AddCard,

// }, {
//   headerLayoutPreset: 'center',
//   transitionConfig: Utils.transitionConfig
// });

// AddCardStack.navigationOptions = {
//   tabBarLabel: [Constant.bmr_calculation]
// };


const CommentStack = createStackNavigator({
  screen: CommentScreen,
}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

CommentStack.navigationOptions = {
  tabBarLabel: [Constant.comments]
};

const EditProfileStack = createStackNavigator({
  screen: EditProfileScreen,
}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

EditProfileStack.navigationOptions = {
  tabBarLabel: [Constant.edit_profile]
};


const NotificationStack = createStackNavigator({
  Notification: NotificationScreen,
  NotificationSetting: NotificationSettingScreen,
  BMR: BMRCalculationScreen,
  AddCard: AddCard
}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

NotificationStack.navigationOptions = {
  tabBarLabel: [Constant.notification]
};

const AboutUsStack = createStackNavigator({
  AboutUs: AboutUsScreen,
}, {
  headerLayoutPreset: 'center',
  initialRouteName: 'AboutUs',
  transitionConfig: Utils.transitionConfig
});

AboutUsStack.navigationOptions = {
  tabBarLabel: [Constant.about_us]
};


const MembershipStack = createStackNavigator({
  Membership: MembershipPlanScreen,
  Discounts: Discounts,
  ContactUs: ContactScreenSecond,
  PlanHistory: PlanHistory,
  PlanHistoryDeatils: PlanHistoryDetails,
  PlanDetails: PlanDetail,
  //Enquery: EnqueryScreen
}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

MembershipStack.navigationOptions = {
  tabBarLabel: [Constant.membership_plan]
};

const GymNearMeStack = createStackNavigator({
  screen: NearByGymScreen,
}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

GymNearMeStack.navigationOptions = {
  tabBarLabel: [Constant.gym_near_me]
};

const ProgressStack = createStackNavigator({
  Progress: ProgressScreen,
  PhotoProgress: PhotoProgressScreen,
  AddProgress: AddProgressScreen
}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

ProgressStack.navigationOptions = {
  tabBarLabel: [Constant.progress]
};

const ReminderStack = createStackNavigator({
  screen: NotificationSettingScreen,
}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

ReminderStack.navigationOptions = {
  tabBarLabel: [Constant.settings]
};


const OrderStack = createStackNavigator({
  MyOrder: MyOrdersScreen,
  OrderDetails: OrdersHistoryScreen,
}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

OrderStack.navigationOptions = {
  tabBarLabel: [Constant.my_order]
};

const MyClassessStack = createStackNavigator({
  screen: MyClassessScreen,
}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

MyClassessStack.navigationOptions = {
  tabBarLabel: [Constant.my_classess]
};

const NutritionStack = createStackNavigator({
  Nutrition: NutritionScreen,
  NutritionGuideline: NutritionalGuidelineScreen
}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

NutritionStack.navigationOptions = {
  tabBarLabel: [Constant.nutrition]
};

const PolicyStack = createStackNavigator({
  Policy: PolicyScreen,
  PolicyDetails: PolicyDetailsScreen,
}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

PolicyStack.navigationOptions = {
  tabBarLabel: [Constant.privacy_policy]
};

const ConditionStack = createStackNavigator({
  Terms: ConditionScreen,
}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

ConditionStack.navigationOptions = {
  tabBarLabel: [Constant.term_condition]
};

const ContestStack = createStackNavigator({
  Contest: ContestScreen,
  ContestDetails: ContestDetailsScreen,
  ContestRegister: ContestRegisterScreen,
  TermsConditions: ConditionSecondScreen,
  ContestWinners: ContestWinnersScreen
}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

ContestStack.navigationOptions = {
  tabBarLabel: [Constant.contest]
};

const FAQStack = createStackNavigator({
  FAQ: FAQScreen,
  FAQAnswer: FAQAnswersScreen,

}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

FAQStack.navigationOptions = {
  tabBarLabel: [Constant.faq]
};

const ContactStack = createStackNavigator({
  Contact: ContactScreen,

}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

ContactStack.navigationOptions = {
  tabBarLabel: [Constant.contact_us]
};

const ChangePasswordStack = createStackNavigator({
  ChangePassword: ChangePasswordScreen,

}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

ChangePasswordStack.navigationOptions = {
  tabBarLabel: [Constant.change_password]
};
const FrenchiseStack = createStackNavigator({
  Frenchise: FrenchiseScreen,

}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

FrenchiseStack.navigationOptions = {
  tabBarLabel: [Constant.franchise]
};



const AddFeedStack = createStackNavigator({
  screen: AddFeedScreen,

}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

AddFeedStack.navigationOptions = {
  tabBarLabel: [Constant.add_feed]
};

const FeedDetailsStack = createStackNavigator({
  screen: FeedDetails,

}, {
  headerLayoutPreset: 'center',
  transitionConfig: Utils.transitionConfig
});

FeedDetailsStack.navigationOptions = {
  tabBarLabel: [Constant.feed_details]
};

const MyDrawerNavigator = createDrawerNavigator(
  {

    Setting: MainTabNavigator,
    [Constant.edit_profile]: EditProfileStack,
    [Constant.plans]: MembershipStack,
    [Constant.feed_sidemenu]: MainTabNavigator,
    [Constant.my_classess]: MainTabNavigator,
    [Constant.my_order]: OrderStack,
    [Constant.progress]: ProgressStack,
    [Constant.settings]: ReminderStack,
    [Constant.nutrition]: NutritionStack,
    [Constant.gym_near_me]: GymNearMeStack,

    [Constant.franchise]: FrenchiseStack,

    [Constant.contest]: ContestStack,
    [Constant.faq]: FAQStack,

    [Constant.about_us1]: AboutUsStack,
    [Constant.notification]: NotificationStack,
    [Constant.share_app]: NotificationStack,
    [Constant.privacy_policy]: PolicyStack,
    [Constant.term_condition]: ConditionStack,
    [Constant.delete_account]: NotificationStack,
    [Constant.contact_us]: ContactStack,
    [Constant.change_password]: ChangePasswordStack,
    //[Constant.bmr_calculation]: BMRStack,

    [Constant.logout]: NotificationStack,

  },
  {
    drawerWidth: Dimensions.get('window').width,
    contentComponent: DrawerContent,
    drawerPosition: 'left',
    drawerWidth: 300,
    drawerPosition: 'left',
    drawerBackgroundColor: 'transparent',
    //drawerBackgroundColor: Colors.primaryColor,
    overlayColor: 0.8,
    keyboardDismissMode: 'none'
  }
);

const MainStack = createStackNavigator({
  Drawer: MyDrawerNavigator,
  BMRCalculation: BMRCalculationStack,
  ChangePass: ChangePasswordStack,
  GymPlans: MembershipStack,
  AddFeed: AddFeedStack,
  FeedDetails: FeedDetailsStack,
  Comment: CommentStack,

}, {
  headerLayoutPreset: 'center', initialRouteName: 'Drawer', headerMode: 'none',
  transitionConfig: Utils.transitionConfig
});


export default createAppContainer(createSwitchNavigator({
  // You could add another route here for authentication.
  // Read more at https://reactnavigation.org/docs/en/auth-flow.html
  Splash: SplashStack,
  Auth: AuthStack,
  Main: MainStack,

  //Comment: CommentStack,
  //Swipe: Navigator
}),
  {
    initialRouteName: 'Splash',
    transitionConfig: Utils.transitionConfig
  }
);

