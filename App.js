import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import React, { useState } from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import * as firebase from 'firebase';

import AppNavigator from './navigation/AppNavigator';
import { Provider } from 'react-redux';
import configureStore from './redux/store';
import * as Sentry from 'sentry-expo';
import Constants from 'expo-constants';
import { YellowBox } from 'react-native';

YellowBox.ignoreWarnings(['VirtualizedLists should never be nested',
  // TODO: Remove when fixed
]);

Sentry.init({
  dsn: 'https://c5cdf0cc0e1a4dcf9c894dc0b2e99f37@sentry.io/2230830',
  enableInExpoDevelopment: true,
  debug: true,
  enableNative: false
});

//Sentry.setRelease(Constants.manifest.revisionId);

// Initialize Firebase
const firebaseConfig = {
  apiKey: "AIzaSyD7ZWyyLw8RojHsVamymxAJs2ssm8tK2Js",
  authDomain: "b-body-2a46a.firebaseapp.com",
  databaseURL: "https://b-body-2a46a.firebaseio.com",
  projectId: "b-body-2a46a",
  storageBucket: "",
  messagingSenderId: "433718255648",
  appId: "1:433718255648:web:103111778618a579"

};

firebase.initializeApp(firebaseConfig);
const store = configureStore();

store.getState();


export default function App(props) {
  const [isLoadingComplete, setLoadingComplete] = useState(false);

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  } else {
    return (
      <Provider store={store} >
        <View style={styles.container}>
          <StatusBar barStyle='light-content' />
          <AppNavigator />
        </View>
      </Provider>

    );
  }
}

async function loadResourcesAsync() {
  await Promise.all([
    Asset.loadAsync([
      require('./assets/images/robot-dev.png'),
      require('./assets/images/icons/memberhip_img_3x.png'),
      require('./assets/images/thumbnail.png'),
      require('./assets/images/admin.jpg'),
      require('./assets/images/membership_bg.png'),
      require('./assets/images/icons/pin3.png')
    ]),
    Font.loadAsync({
      // This is the font that we are using for our tab bar
      ...Ionicons.font,
      // We include SpaceMono because we use it in HomeScreen.js. Feel free to
      // remove this if you are not using it in your app
      'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
      'futura-book': require('./assets/fonts/tt0140m_1.ttf'),
      'futura-bold': require('./assets/fonts/tt0144m_1.ttf'),
      'futura-medium': require('./assets/fonts/tt0142m_1.ttf'),
      'futura-light': require('./assets/fonts/FutuLt.ttf'),
      'futura-heavy': require('./assets/fonts/FUTURAH.ttf'),
      'futura-extra-heavy': require('./assets/fonts/FUTURAXK.ttf'),
    }),
  ]);
}

function handleLoadingError(error) {
  // In this case, you might want to report the error to your error reporting
  // service, for example Sentry
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
